import { SELECTED_USER_PACKAGES, SELECTED_LANGUAGE,CHOSE_AT_CLINIC,QUESTION_ANSWER_SETUP,PROFILE_SETUP, IS_PIN_CODE_SET,FIRST_TIME_DISCOUNT,PIN_CODE,INBOX_COUNT,APPOINTMENT_TOTAL_PRICE, SELECTED_USER_PACKAGES_DETAIL, NOTIFICATION_COUNT, SET_USER,BOARD_DONE ,SKIP_SURVY, ALTER_USER,SET_QUESTION_ANSWER, USER_LOGIN, USER_SIGNUP, REQUEST_DATA,START_LOADING,STOP_LOADING } from './types';
import {Alert} from 'react-native';
const HOST_URL = "https://vita-health.herokuapp.com/api";
import { NavigationActions } from "react-navigation";
import Route from "../network/route.js";

//const route = new Route('https://trifigodevapi.azurewebsites.net/');
const route = new Route("https://vita-health.herokuapp.com/api/");
//const route = new Route("https://reqres.in/");


function requestData() {
    return (dispatch) => { dispatch({ type: REQUEST_DATA}) };
}
function startLoading(){
    return (dispatch) => { dispatch({ type: START_LOADING }) };

}
function stopLoading() {
    return (dispatch) => { dispatch({ type: STOP_LOADING }) };

}

function boardScreenDone() {
    return (dispatch) => { dispatch({ type: BOARD_DONE }) };

}
function skipSurvyScreen() {
    return (dispatch) => { dispatch({ type: SKIP_SURVY }) };

}






function userLogin(credentials){
    return (dispatch)=>
    { dispatch(requestData());

        route.postdata("login", credentials)
      .then(function(response) {

        if(response.error){
            dispatch(requestData());
            Alert.alert('Error', response.error)
        }
        else {
            dispatch(setUser(response.token))
            dispatch(alterUser(response.logedUser))
            Alert.alert('Success', 'Successfuly stored')

            //dispatch(NavigationActions.navigate({ routeName: 'App' }))
        }

      })
     

    ;}
}









// function userLogin(credentials) {
//     return (dispatch) =>{
//         dispatch(requestData());
//         return fetch(`https://reqres.in/api/login`, {
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json'
//             },
//             method: "POST",
//             body: JSON.stringify(credentials)
//         })
        
//           .then(response => {
//               if (response.ok && response.status !== 400 && response.status !== 404) {
//                 return response.json();
//               }
//               else {
//                   console.log("some network error");
//                   return false;
//               }
//           })
//           .then(json => {
//               if(!json){
//                 dispatch(requestData());
//               }
//               else dispatch(setUser(json));
//             })
//             .catch(e => {
//                 console.log(e)
//                 return { name: "some network error", description: "" };
//             });
//     };
// }

function setUser(user) {
    return (dispatch) => {dispatch({type: SET_USER, payload:user})};
}

function changeLanguage(language) {
    return (dispatch) => { dispatch({ type: SELECTED_LANGUAGE,payload:language }) };

}

function setInboxCount(count) {
    return (dispatch) => { dispatch({ type: INBOX_COUNT,payload:count }) };

}

function setNotificationCount(count) {
    return (dispatch) => { dispatch({ type: NOTIFICATION_COUNT,payload:count }) };

}

function alterUser(user) {
    return (dispatch) => {dispatch({type: ALTER_USER, payload:user})};
}


function setQuestionAnswer(questionAnswers){
    return (dispatch) => {dispatch({type: SET_QUESTION_ANSWER, payload:questionAnswers})};
}

function setPinCode(pinCode){
    return (dispatch) => {dispatch({type: PIN_CODE, payload:pinCode})};
}

function setUserSeletedPackages(selectedPackages) {
    return (dispatch) => { dispatch({ type: SELECTED_USER_PACKAGES,payload:selectedPackages }) };

}

function setUserSeletedPackagesDetail(selectedPackagesDetail) {
    return (dispatch) => { dispatch({ type: SELECTED_USER_PACKAGES_DETAIL,payload:selectedPackagesDetail }) };

}

function setAppointmentTotalPrice(totalPrice) {
    return (dispatch) => { dispatch({ type: APPOINTMENT_TOTAL_PRICE,payload:totalPrice }) };

}

function setFirstTimeDiscount(showDiscount) {
    return (dispatch) => { dispatch({ type: FIRST_TIME_DISCOUNT,payload:showDiscount }) };

}


function isSetPinCode(pinCodeSet) {
    return (dispatch) => { dispatch({ type: IS_PIN_CODE_SET, payload: pinCodeSet }) };

}


function isChoseAtClinic(choseAtClinic) {
    return (dispatch) => { dispatch({ type: CHOSE_AT_CLINIC, payload: choseAtClinic }) };

}

function isProfileSetup(profileSetup) {
    return (dispatch) => { dispatch({ type: PROFILE_SETUP, payload: profileSetup }) };

}

function isQuestionAnswerSetup(questionAnswer) {
    return (dispatch) => { dispatch({ type: QUESTION_ANSWER_SETUP, payload: questionAnswer }) };

}

function test(){
    console.log("test action call")
}


export { isQuestionAnswerSetup,isProfileSetup,setUserSeletedPackages,isSetPinCode, isChoseAtClinic, setPinCode,changeLanguage,setFirstTimeDiscount,setAppointmentTotalPrice, setUserSeletedPackagesDetail, setNotificationCount, setInboxCount, setUser,boardScreenDone, skipSurvyScreen,alterUser, userLogin, requestData,startLoading,stopLoading ,setQuestionAnswer,test};
