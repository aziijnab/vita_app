import store from '../redux/store';
import * as actions from '../actions';

// router
import Route from '../modules/route.js'

//const route = new Route('https://trifigodevapi.azurewebsites.net/');
const route = new Route("https://vita-health.herokuapp.com/api/");


// add login . logout here >>

//#region summary
export const getBankingTotalsSummary = (id) => {
    route.post('bankingtotalssummary/' + id).then(function (res, err) {
        if (res) {
            store.dispatch(actions.setBankingTotalsSummary(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getSecurityQuestions = () => {
    route.post('securityquestion/list').then(function (res, err) {
        if (res) {
            store.dispatch(actions.getSecurityQuestion(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    })
}

export const getAdditionalCreditSummary = (id) => {
    let _this = this;
    route.post('additionalcreditsummary/' + id).then(function (res, err) {
        if (res) {
            store.dispatch(actions.setAdditionalCreditSummary(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getPaymentSummary = (id) => {
    let _this = this;
    route.post('paymentssummary/' + id).then(function (res, err) {
        if (res) {
            store.dispatch(actions.setPaymentSummary(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getAccountAgeSummary = (id) => {
    let _this = this;
    route.post('accountagesummary/' + id).then(function (res, err) {
        if (res) {
            store.dispatch(actions.setAccountAgeSummary(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getOtherFactorsSummary = (id) => {
    let _this = this;
    route.post('otherfactorssummary/' + id).then(function (res, err) {
        if (res !== undefined) {
            store.dispatch(actions.setOtherFactorsSummary(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getCreditBossRatingSummary = (id) => {
    let _this = this;
    route.post('creditbossratingsummary/' + id).then(function (res, err) {
        if (res !== undefined) {
            store.dispatch(actions.setCreditBossRatingSummary(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getAccountMixSummary = (id) => {
    let _this = this;
    route.post('accountmixsummary/' + id).then(function (res, err) {
        if (res !== undefined) {
            store.dispatch(actions.setAccountMixSummary(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getCreditInquirySummary = (id) => {
    let _this = this;
    route.post('creditinquirysummary/' + id).then(function (res, err) {
        if (res !== undefined) {
            store.dispatch(actions.setCreditInquirySummary(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getCreditUtilizationSummary = (id) => {
    let _this = this;
    route.post('creditutilizationsummary/' + id).then(function (res, err) {
        if (res !== undefined) {
            store.dispatch(actions.setCreditUtilizationSummary(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getYodleeAccountSummary = (id) => {
    route.post('yodlee/accountsummary/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setYodleeAccountSummary(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getQuovoAccountSummary = (id) => {
    route.post('quovo/accountsummary/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setQuovoAccountSummary(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getRecos = (id, status = null) => {
    let _this = this;
    let url = 'recovalues/' + id;

    if (status !== null)
        url += '/' + status;

    route.post(url).then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRecos(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};

export const getAllRecos = (id) => {
    let _this = this;
    let url = 'recovalues/' + id;

    route.post(url).then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setAllRecos(res));
        }
        else if (err !== undefined) {
            _this.setState({
                error: 'Error',
                loading: null
            });
        }
    });
};
//#endregion

//#region ref data
export const getRefUniversities = () => {
    route.post('/university/slimlist/').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRefUniversities(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};




export const getRefConsumerTypes = () => {
    route.post('consumertype/list').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRefConsumerTypes(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getRefPaymentSentimentTypes = () => {
    route.post('paymentsentimenttype/list').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRefPaymentSentimentTypes(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getRefCreditUtilizationSentimentTypes = () => {
    route.post('creditutilizationsentimenttype/list').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRefCreditUtilizationSentimentTypes(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getRefAccountAgeSentimentTypes = () => {
    route.post('accountagesentimenttype/list').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRefAccountAgeSentimentTypes(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getRefAccountMixSentimentTypes = () => {
    route.post('accountmixsentimenttype/list').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRefAccountMixSentimentTypes(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getRefCreditInquirySentimentTypes = () => {
    route.post('creditinquirysentimenttype/list').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRefCreditInquirySentimentTypes(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getRefOtherFactorsSentimentTypes = () => {
    route.post('otherfactorssentimenttype/list').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRefOtherFactorsSentimentTypes(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getRecoTemplates = () => {
    route.post('recotemplate/list').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setRecoTemplates(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

// get Active (status = A) deal items
export const getDealItems = () => {
    route.post('dealitem/list/A').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setDealItems(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};
//#endregion


// random credit tip >>
export const getRandomCreditTip = () => {
    return route.post('credittip/random');
};

// random banking tip >>
export const getRandomBankingTip = () => {
    return route.post('bankingtip/random');
};

// random 10 credit quiz questions >>
export const getCreditQuiz = () => {
    route.post('creditquiz/random/10').then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setCreditQuiz(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

// save credit boss rating summary >>
export const saveCreditBossRatingSummary = (userId, score) => {
    let now = new Date();

    let creditbossratingsummary = {
        "creditBossRating": score,
        "recordDate": now.toISOString(),
        "userId": userId,
    };

    route.postdata('creditbossratingsummary/create', creditbossratingsummary).then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.setCreditBossRatingSummary(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

// upload Credit Report
export const uploadReport = (userId, data) => {
    return route.postfile('uploadreport/' + userId, data).then(function (res, err) {
        if (res) {
            return res;
        }
        else if (err) {
            throw err;
        }
    });
};

export const uploadReportX = (userId, data) => {
    return route.postfile_x('uploadreport/' + userId, data);
};

// upload profile image
export const uploadProfilePhoto = (userId, data) => {
    route.postfile('uploadphoto/' + userId, data).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

// upload cover image
export const uploadCoverPhoto = (userId, data) => {
    route.postfile('uploadcover/' + userId, data).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

// update reco status >>
export const updateRecoStatus = (id, status) => {
    return route.post('recovalues/update/' + id + '/' + status);
};

// updateuser >>
export const updateUser = (id, user) => {
    return route.postdata('user/update/' + id, user);
};



export const createGoal = (data) => {
    return route.postdata('goal/create', data).then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.storeGoals(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });

}

export const getUserGoal = (id, data) => {
    return route.postdata('goal/byuser/' + id, data).then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.getUserGoals(res));
        }
        else if (err !== undefined) {
            throw err;
        }
    });

}


export const updateGoal = (id, data) => {
    return route.postdata('goal/update/' + id, data).then(function (res, err) {
        if (res !== null && res !== undefined) {
            store.dispatch(actions.getUserGoals(res));

        }
        else if (err !== undefined) {
            throw err;
        }
    });

}

// delete by user id
export const deleteAccountAgeSummary = (id) => {
    return route.post('accountagesummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deleteAccountMixSummary = (id) => {
    return route.post('accountmixsummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deleteAdditionalCreditSummary = (id) => {
    return route.post('additionalcreditsummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deleteBankingTotalsSummary = (id) => {
    return route.post('bankingtotalssummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deletePastCashFlowSummary = (id) => {
    return route.post('pastcashflowsummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deleteCurrentCashFlowSummary = (id) => {
    return route.post('currentcashflowsummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deleteConsumerTypeSummary = (id) => {
    return route.post('consumertypesummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deleteCreditInquirySummary = (id) => {
    return route.post('creditinquirysummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deleteCreditUtilizationSummary = (id) => {
    return route.post('creditutilizationsummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deleteOtherFactorsSummary = (id) => {
    return route.post('otherfactorssummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const deletePaymentsSummary = (id) => {
    return route.post('paymentssummary/delete/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

// refresh yodlee 
export const refreshYodleeAccounts = (id) => {
    return route.post('yodlee/refreshaccounts/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const refreshYodleeTransactions = (id) => {
    return route.post('yodlee/refreshtransactions/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

// refresh yodlee 
export const refreshQuovoAccounts = (id) => {
    return route.post('quovo/refreshaccounts/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const refreshQuovoTransactions = (id) => {
    return route.post('quovo/refreshtransactions/' + id).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

// run trifigo engine
export const runEngineForUser = (id) => {
    let data = {
        userId: id
    };

    return engine_user_run_route.postdata('', data).then(function (res, err) {
        if (res !== null && res !== undefined) {
            return res;
        }
        else if (err !== undefined) {
            throw err;
        }
    });
};

export const getUserByEmail = (email) => {
    return route.post('user/email/' + email);
};

export const setUserToken = (user) => {
    return route.postdata('user/settoken/' + user.id, user);
};

export const reset_password = (user) => {
    return route.postdata('user/resetpassword/' + user.id, user);
};

export const get_user_by_token = (token) => {
    return route.postdata('user/checktoken/' + token);
};

export const send_email = (email) => {
    return route.postdata('mail', email);
};