
const HOST_URL = "http://vitahealthapp.com/api/";
//const HOST_URL = "http://vita-stage-site.ap-southeast-1.elasticbeanstalk.com/api/";
import NavigationService from '../navigation/NavigationService';

export default class Route {

    constructor(rootUrl) {
        this.rootUrl = HOST_URL;
    }

    get = (url) => {
     
        return fetch(this.rootUrl + url)
            
            .then(response => response.json())
            .catch(e => e)
    }
    getAuthenticated = (url,token) => {
        
        return fetch(this.rootUrl + url,{
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/json",
                "Content-Type": "application/json"
            },
        })

            .then(response => response.json())
            .catch(e => e)
    }

    post = (url) => {
        return fetch(this.rootUrl + url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
        }).then(this.checkStatus)
            .then(response => response.json())
            .catch(e => e)
    }

    updateData = (url, data,token) => {
        //console.log('url >>>', this.rootUrl + url);
        //console.log('data >>>', JSON.stringify(data, null, 2));
        return fetch(this.rootUrl + url, {
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(data)
        })


            .then(response => response.json())
            .catch(e => e);
    }
    updatePutRequest = (url, data,token) => {
        //console.log('url >>>', this.rootUrl + url);
        //console.log('data >>>', JSON.stringify(data, null, 2));
        return fetch(this.rootUrl + url, {
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            method: "PUT",
            body: JSON.stringify(data)
        })


            .then(response => response.json())
            .catch(e => e);
    }

    

    postdata = (url, data) => {
        //console.log('url >>>', this.rootUrl + url);
        //console.log('data >>>', JSON.stringify(data, null, 2));
        return fetch(this.rootUrl + url, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          method: "POST",
          body: JSON.stringify(data)
        })
          
          
          .then(response => response.json())
          .catch(e => e);
    }

    postfile = (url, data, token) => {
        return fetch(this.rootUrl + url, {
            headers: {
                Authorization: `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            method: "POST",
            body: data
        })
            .then(this.checkStatus)
            .then(response => response.json())
            .catch(e => e)
    }

    postfile_x = (url, data) => {
        return fetch(this.rootUrl + url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            method: "POST",
            body: data
        });
    }

    checkStatus = (response) => {
        //console.log('checkStatus response >>>', response);
        if (response.ok) {
            return response;
        }
        
        else {
            let error = new Error(response.statusText);
            error.response = response;
            throw error;
        }
    }

    checkTokenExpire(response){
        if(response.status == "Token is Expired"){
            this.props.navigation.navigate('getStarted')
        }
    }

}
