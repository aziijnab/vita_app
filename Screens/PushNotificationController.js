import React, { Component } from "react";

import { Platform } from 'react-native';

import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from "react-native-fcm";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions'; //Import your actions
import {NavigationService } from "./../navigation/NavigationService"

class PushNotificationController extends Component {
    constructor(props) {
        super(props);
    }
    

    componentWillUnmount() {
        this.refreshTokenListener.remove();
    }


    render() {
        return null;
    }
}

function mapStateToProps(state, props) {
    // console.log('map start proos',state,props);
    return {
        userDetail: state.user.userDetail,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PushNotificationController);