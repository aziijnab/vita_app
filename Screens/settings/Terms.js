import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform,
  WebView
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from '../../i18n/i18n';
import Route from "../../network/route.js";
import Spinner from 'react-native-loading-spinner-overlay';

const route = new Route("http://192.168.100.18:8000/api/");


class Terms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      terms:null,

      loading:false,


      data:[
        {name:'Mediveron',price:200},
        {name:'Health Clinic',price:400},
        {name:'Bjourn Health',price:600}

      ]

    };
  }

  componentDidMount() {
    // this.fetchTerms()
  }

//   fetchTerms(){
//     this.setState({loading:true})
//     console.log("static content")
//     route.getAuthenticated('static-content',this.props.myToken).then(async(response)=>{
//     if(response.error){
//       console.log(response.error)
//       this.setState({loading:false})
//     }
//     else {
//       this.setState({loading:false})
//         console.log(response)
        
//         response.content.map((value)=>{
//           if(value.content_key == "Term and Condition"){
//               this.setState({terms:value})
//           }
//         })
        
//         // this.setState({inboxCount:response.data.unreadMsg})    

//         }
//     })
//  }
removeItem(props){
    var arr = this.state.data
      arr.splice(props,1)
      this.setState({data:arr})

}
  render() {
    return (
      <View style={{ flex: 1,backgroundColor:'white' }}>
        <Header
          label={I18n.t('terms')}
         onbackPress={()=>this.props.navigation.pop()}
        />
         <WebView
          source={{uri: 'http://vitahealthapp.com/term-and-condition'}}
          style={{marginTop: 20}}
        /> 
        
        
        
        
      
              
        <View style={{alignSelf:'center',paddingVertical:10}}>

        </View>
              
       
      </View>
      
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles  = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: "flex-start"
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-around"
    },
    titlePrice: {
      fontSize: 15,
      fontWeight: "400",
      
      color: color.primary,
      letterSpacing: 0.5,
      //paddingHorizontal: 10
    },
    title2: {
        fontSize: 13,
        fontWeight: "400",
        letterSpacing: 0.5,
        paddingVertical:2,
        color:'#969696'
    },
    title3: {
      fontSize: 16,
      fontWeight: "600",
      letterSpacing: 0.5,
      paddingVertical:2,
      color:'#969696',
      paddingLeft:10
  },
    iconView: {
        
    
    },
    title1: {
        fontSize: 16,
        fontWeight: "400",
        
        color: "#373535",
        letterSpacing: 0.5,
        //paddingHorizontal: 10
    },
    fieldView: {
        
        flexDirection: "row",
        width:"91%",
        alignItems:"center",
        alignSelf:'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: 'transparent',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: 'white',
    elevation: 2, // Android
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 1 },
        // shadowOpacity: 0.20,
        // shadowRadius: 1.41,
        // elevation: 2,
        paddingHorizontal:11,
        paddingVertical: 10,
        marginTop: 16,
        


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex:1,
        flexDirection:"row",
        paddingHorizontal:20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Terms);
