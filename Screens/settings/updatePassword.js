import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from "react-native";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast'
import Route from "../../network/route.js";
import Header from "../../components/Header";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

class UpdatePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      newPassword:"",
      confirmPassword:"",
    };
  }

  updatePasswordSubmit(){
    this.setState({loading:true})
    let data = {oldPassword:this.state.oldPassword,newPassword:this.state.newPassword}
    route.updateData('update-password',data,this.props.myToken).then(async response => {
        if (response.error) {
          console.log(response.error)
          this.setState({loading:false})
          Alert.alert("Error", JSON.stringify(response.error));
        }else {
            console.log("response",response)
          this.setState({loading:false})
          console.log("user password update create");
          this.refs.toast.show(response.success,DURATION.SHORT);
        }
      });
  }


  componentDidMount() { }


  validateData(){
    var errorMessages = []
    if(this.state.oldPassword == ""){
      errorMessages.push("Please enter your old password")
    }

    if(this.state.newPassword == ""){
      errorMessages.push("Please enter your new password")
    }

    if(this.state.newPassword != this.state.confirmPassword){
      errorMessages.push("Confrim password dos't match")
    }

    if(errorMessages.length == 0){
      this.updatePasswordSubmit()
    }else{
      let errorArray = errorMessages.join("\n")
      this.refs.toast.show(errorArray,DURATION.SHORT);
    }
  }
  render() {
    return (
      <View style={styles.container}>
      <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t('please_wait')}...`}
          textStyle={{color:color.primary}}
          cancelable={true}
          color="#F16638"
        />
      <Toast
        ref="toast"
        style={{backgroundColor:'red'}}
        position='top'
        positionValue={80}
        fadeInDuration={750}
        fadeOutDuration={1000}
        textStyle={{color:'white'}}
      />
        <Header
          label={I18n.t('updatePassword')}
          
          onbackPress ={() => this.props.navigation.pop()}
        />
        <Text style={styles.title}>{I18n.t("updatePassword")}</Text>
        <View style={styles.form}>
          <InputField
            label={I18n.t('old_password')}
            onChangeText={(oldPassword) =>this.setState({ oldPassword })}
            icon="ios-mail"
            returnKeyType="default"
            //iconSize={27}
            //iconColor="#0984e3"
            secureTextEntry
          />
          <InputField
            label={I18n.t('new_password')}
            onChangeText={(newPassword) =>this.setState({ newPassword })}
            icon="ios-mail"
            returnKeyType="default"
            //iconSize={27}
            //iconColor="#0984e3"
            secureTextEntry
          />
          <InputField
            label={I18n.t('confirm_password')}
            onChangeText={(confirmPassword) =>this.setState({ confirmPassword })}
            icon="ios-mail"
            returnKeyType="default"
            //iconSize={27}
            //iconColor="#0984e3"
            secureTextEntry
          />
        </View>

        <Button
          label={I18n.t('updatePassword')}
          buttonStyle={styles.button}
          onPress={() => this.validateData()}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    alignSelf: "center",
    paddingTop: "20%"
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center"
    //paddingHorizontal: "20%"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginVertical: 30,

    backgroundColor: color.primary
  },
  input: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 6,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 20,
    width: "90%",
    /* borderColor: "pink",
    borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    flexWrap: "wrap"
    /* borderColor: "red",
    borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",

    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    myToken: state.user.userToken,

  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdatePassword);
