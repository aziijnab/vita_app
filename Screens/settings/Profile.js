import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/MaterialIcons";
import I18n from "../../i18n/i18n";
import ImagePicker, { cleanSingle } from "react-native-image-crop-picker";
import Route from "../../network/route.js";
import Button from "../../components/Button";
import Modal from "react-native-modal";
import Spinner from "react-native-loading-spinner-overlay";
import { Dropdown } from "react-native-material-dropdown";
import Toast, { DURATION } from "react-native-easy-toast";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { NavigationEvents } from 'react-navigation';

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: "",
      userName: "",
      editing: false,
      userWeight: "",
      userHeight: "",
      userDateOfBirth:"",
      userMobile:"",
      userAge: "",
      userGender: "",
      isModalVisible: false,
      loading: false,
      userProfileId: "",
      heightQuestionId: null,
      weightQuestionId: null,
      measurementQuestions: [],
      modalHeight: "",
      modalGender: "",
      modalAge: "",
      modalWeight: "",
      corporateUserBudget:"",
    };
  }

  toggleModal = () => {
    this.setState({
      modalAge: this.state.userAge,
      modalGender: this.state.userGender,
      modalHeight: this.state.userHeight,
      modalWeight: this.state.userWeight,
      isModalVisible: !this.state.isModalVisible
    });
    console.log("model toggle", this.state.isModalVisible);
  };

  componentDidMount() {
    this.fetchProfileData();
  }
  
  fetchProfileData(){
	console.log("==================User Data==================");
    console.log(this.props.user);
    console.log("====================================");
    console.log("user_id", this.props.user.id);
    this.setState({ loading: true });
    route
      .getAuthenticated(`profile/${this.props.user.id}`, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
          console.log("userProfile", response.user.user_profile);
          this.setState({ userName: response.user.user_profile.full_name || ""});
          this.setState({ userGender: response.user.user_profile.gender || ""});
          this.setState({ userAge: response.user.user_profile.age || "" });
          this.setState({ userProfileId: response.user.user_profile.id || "" });
          this.setState({ userDateOfBirth: response.user.user_profile.date_of_birth || "" });
          this.setState({ userMobile: response.user.user_profile.mobile || "" });
          if (
            response.user.user_profile.image != null &&
            response.user.user_profile.image != ""
          ) {
            this.setState({ image: response.user.user_profile.image });
          }
          console.log(
            "user profile",
            response
          );
          // this.props.setUser(response.user);

          response.user.user_question_answer.map((value, index) => {
            if (value.answer != "") {
              console.log("user answer", value.question.id);
              if (value.question.question == "Weight (kg)") {
                this.setState({
                  userWeight: value.answer,
                  weightQuestionId: value.question.id
                });
              }
              if (value.question.question == "Height (cm)") {
                this.setState({
                  userHeight: value.answer,
                  heightQuestionId: value.question.id
                });

               
              }
            }
          });
          this.setState({ loading: false });
        }
      });
    this.fetchMeasurementQuestions();
    this.getCorporateUserBudget();
  }
  
  getCorporateUserBudget(){
    route
      .getAuthenticated("get-user-budget", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
          console.log(response);
          if(response.available_budget){
            this.setState({corporateUserBudget:response.available_budget})
          }
        }
      });
  }

  pickImage() {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: false
    }).then(image => {
      this.setState({ image: image.path });
      var data = new FormData();
      data.append("image", {
        uri: this.state.image, // your file path string
        name: "image.jpg",
        type: "image/jpg"
      });
      // userImage = {image:this.state.image}
      console.log("user selected image",data)
      route
        .postfile(
          `profile/update-image/${this.props.user.user_profile.id}`,
          data,
          this.props.myToken
        )
        .then(async response => {
          if (response.error) {
            console.log(response.error);
            this.props.stopLoading();
            Alert.alert("Error", JSON.stringify(response.error));
          } else {
            console.log("profile image upload", response);
            this.setState({ loading: false });
          }
        });
    });
  }

  fetchMeasurementQuestions() {
    this.setState({ loading: true });
    route
      .getAuthenticated("questions", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
          console.log(response);
          route.checkTokenExpire(response);
          var allData = response.allQuestions;
          var questions = [];
          console.log("all questions", allData);
          allData.map((item, index) => {
            if (item.question_type == "vitals") {
              questions.push(item);
              if (item.question == "Weight (kg)") {
                this.setState({ weightQuestionId: item.id });
              }
              if (item.question == "Height (cm)") {
                this.setState({ heightQuestionId: item.id });
              }
            }
          });
        }

        this.setState({ measurementQuestions: questions });
        console.log("measurementQuestions", this.state.measurementQuestions);
      });
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  userNameSubmit() {
    var userProfile = this.props.user.user_profile;
    userProfile.full_name = this.state.userName;
    console.log("user profile befor edit", userProfile);
    this.submitProfileRequest(userProfile);
    console.log("user name submit");
    this.setState({ editing: false });
  }

  submitProfileRequest(userProfile) {
    this.setState({ loading: true });
    route
      .updateData(`profile`, userProfile, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log("response rror", response.error);
          this.setState({ loading: false });
        } else {
          console.log("name submit response", response);
          response.token = this.props.myToken;
          this.props.setUser(response);
          this.setState({ loading: false });
        }
      });
  }

  submitProfile() {
    var userProfile = this.props.user.user_profile;

    if (
      this.state.userAge == "" ||
      this.state.userGender == "" ||
      this.state.userHeight == "" ||
      this.state.userWeight == ""
    ) {
      this.refs.toast.show("Please fill all fields", DURATION.SHORT);
    } else {
      userProfile.age = this.state.userAge;
      userProfile.gender = this.state.userGender;
      this.submitProfileRequest(userProfile);
      this.setState({ loading: true });
      userAnswer = [
        {
          question_id: this.state.heightQuestionId,
          answer: this.state.userHeight
        },
        {
          question_id: this.state.weightQuestionId,
          answer: this.state.userWeight
        }
      ];

      userAnswerPost = { questions: userAnswer };

      route
        .updateData(`user-answer`, userAnswerPost, this.props.myToken)
        .then(async response => {
          if (response.error) {
            console.log("response rror", response.error);
            this.setState({ loading: false });
          } else {
            console.log("user answer submit response", response);
            this.setState({ loading: false });
            this.setState({ userHeight: this.state.userHeight });
            this.setState({ userWeight: this.state.userWeight });
            this.toggleModal();
          }
        });
    }
  }

  render() {
    let genderData = [
      { label: "Gender", value: "" },
      { label: "Male", value: "Male" },
      { label: "Female", value: "Female" }
    ];
    return (
      <View
        style={{
          flex: 1,
          height: "100%",
          width: "100%",
          paddingBottom: 6,
          backgroundColor: "white",
          marginBottom:20
        }}
      >
	  <NavigationEvents
                onDidFocus={() => this.fetchProfileData()}
                />
        <Header
          label={I18n.t('user_profile')}
          onbackPress={() => this.props.navigation.pop()}
        />
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: "red" }}
          position="top"
          positionValue={80}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{ color: "white" }}
        />
        <ScrollView
          style={{
            flex: 1,
            paddingVertical: 13,
            height: "100%",
            backgroundColor: "white"
          }}
        >
          <View style={{ flex: 1 }}>
            <TouchableOpacity
              onPress={() => this.pickImage()}
              style={styles.customImage}
            >
              {this.state.image != "" ? (
                <Image
                  source={{ uri: this.state.image }}
                  style={{ width: 150, height: 150, borderRadius: 75 }}
                  resizeMode="cover"
                />
              ) : (
                <Image
                  source={require("../../asset/images/defaultProfile.png")}
                  style={{ width: 150, height: 150, borderRadius: 75 }}
                  resizeMode="cover"
                />
              )}

              <View style={styles.camera}>
                <Icon name="camera-alt" color="white" size={33} />
              </View>
            </TouchableOpacity>

            <View style={styles.editTitle}>
              {this.state.editing ? (
                <TextInput
                  autoCapitalize="words"
                  autoFocus={true}
                  value={this.state.userName}
                  style={{
                    fontSize: 20,
                    fontWeight: "400",
                    paddingVertical: 3,
                    paddingHorizontal: 5
                  }}
                  returnKeyType="done"
                  onChangeText={text => this.setState({ userName: text })}
                  onSubmitEditing={() => this.userNameSubmit()}
                />
              ) : (
                <Text style={styles.title2}>{this.state.userName}</Text>
              )}

              <Icon
                onPress={() => this.setState({ editing: true })}
                name="edit"
                style={{ paddingHorizontal: 3 }}
                color="#373535"
                size={23}
              />
            </View>

            <View style={styles.editPersonal}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.toggleModal()}
                style={{
                  position: "absolute",
                  top: 0,
                  right: 0,
                  marginTop: 5,
                  marginRight: 5
                }}
              >
                <Icon
                  name="edit"
                  style={{ paddingHorizontal: 3 }}
                  color="#373535"
                  size={20}
                />
              </TouchableOpacity>
              <Modal
                isVisible={this.state.isModalVisible}
                onBackButtonPress={() => this.toggleModal()}
                animationType="slide"
              >
                <TouchableOpacity onPress={() => this.toggleModal()}>
                  <Icon
                    name="arrow-back"
                    style={{ paddingHorizontal: 3 }}
                    color="#fff"
                    size={20}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    alignContent: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "white",
                      paddingHorizontal: 20,
                      paddingVertical: 10
                    }}
                  >
                   <KeyboardAwareScrollView>
                    <View>
                      <Text
                        style={{
                          color: "#969696",
                          fontSize: 15,
                          letterSpacing: 0.5,
                          textAlign: "left",
                          fontWeight: "700",
                          paddingLeft: 2,
                          paddingRight: 20
                        }}
                      >
                        {I18n.t("age")}
                      </Text>
                      <TextInput
                        style={styles.input1}
                        selectionColor={"#dfe6e9"}
                        onChangeText={userAge => this.setState({ userAge })}
                        placeholder={this.state.modalAge}
                        autoCapitalize="words"
                        autoCorrect={true}
                        underlineColorAndroid="transparent"
                      />
                    </View>

                    <Dropdown
                      inputContainerStyle={{ borderBottomColor: "transparent" }}
                      onChangeText={(itemValue, itemIndex) =>
                        this.setState({ userGender: itemValue })
                      }
                      labelFontSize={15}
                      rippleOpacity={0.1}
                      dropdownMargins={{ min: 0, max: 0 }}
                      textColor={"#969696"}
                      pickerStyle={{ paddingHorizontal: 10 }}
                      dropdownOffset={{ top: 4, left: 0 }}
                      containerStyle={styles.pickerSt}
                      data={genderData}
                      overlayStyle={{ borderColor: "yellow", borderWidth: 1 }}
                      value={this.state.modalGender}
                      baseColor="#576574"
                    />
                    <View>
                      <Text
                        style={{
                          color: "#969696",
                          fontSize: 15,
                          letterSpacing: 0.5,
                          textAlign: "left",
                          fontWeight: "700",
                          paddingLeft: 2,
                          paddingRight: 20
                        }}
                      >
                        {I18n.t("height")}
                      </Text>
                      <TextInput
                        style={styles.input1}
                        selectionColor={"#dfe6e9"}
                        onChangeText={userHeight =>
                          this.setState({ userHeight })
                        }
                        placeholder={this.state.modalHeight}
                        autoCapitalize="words"
                        autoCorrect={true}
                        underlineColorAndroid="transparent"
                      />
                    </View>
                    <View>
                      <Text
                        style={{
                          color: "#969696",
                          fontSize: 15,
                          letterSpacing: 0.5,
                          textAlign: "left",
                          fontWeight: "700",
                          paddingLeft: 2,
                          paddingRight: 20
                        }}
                      >
                        {I18n.t("weight")}
                      </Text>
                      <TextInput
                        style={styles.input1}
                        selectionColor={"#dfe6e9"}
                        onChangeText={userWeight =>
                          this.setState({ userWeight })
                        }
                        placeholder={this.state.modalWeight}
                        autoCapitalize="words"
                        autoCorrect={true}
                        underlineColorAndroid="transparent"
                      />
                    </View>

                    <Button
                      label={I18n.t("submit")}
                      buttonStyle={styles.button}
                      onPress={() => this.submitProfile()}
                    />
                    <Button
                      label={I18n.t("close")}
                      buttonStyle={styles.button}
                      onPress={() => this.toggleModal()}
                    />
                    </KeyboardAwareScrollView>
                  </View>
                  
                </View>
              </Modal>
              <View style={styles.rowView}>
                <View style={styles.rowInner}>
                  <Text>{I18n.t("age")}</Text>
                  <Text style={{ fontWeight: "600" }}>
                    {this.state.userAge}
                  </Text>
                </View>
                <View style={styles.breakSeparator} />

                <View style={styles.rowInner}>
                  <Text>{I18n.t("gender")}</Text>
                  <Text style={{ fontWeight: "600" }}>
                    {this.state.userGender}
                  </Text>
                </View>
              </View>
              <View style={styles.break} />
              <View style={styles.rowView}>
                <View style={styles.rowInner}>
                  <Text>{I18n.t("height")}</Text>
                  <Text style={{ fontWeight: "600" }}>
                    {this.state.userHeight}{" "}
                    {this.state.userHeight != "" ? "cm" : ""}
                  </Text>
                </View>
                <View style={styles.breakSeparator} />
                <View style={styles.rowInner}>
                  <Text>{I18n.t("weight")}</Text>
                  <Text style={{ fontWeight: "600" }}>
                    {this.state.userWeight}{" "}
                    {this.state.userHeight != "" ? "kg" : ""}
                  </Text>
                </View>
              </View>
             {this.state.corporateUserBudget != "" &&
             <View>
             <View style={styles.break} />
              <View style={styles.rowView}>
                <View style={styles.rowInner}>
                  <Text>{I18n.t("available_budget")}</Text>
                  <Text style={{ fontWeight: "600" }}>
                    {this.state.corporateUserBudget}
                  </Text>
                </View>
              </View>
             </View>
             }
            </View>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("startScreen", {
                  fromProfile: true,
                  basicData:{
                    name:this.state.userName,
                    age:this.state.userAge,
                    gender:this.state.userGender,
                    contact:this.state.userMobile,
                    dob:this.state.userDateOfBirth

                  }
                })
              }
              style={styles.fieldView}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  width: "100%"
                }}
              >
                <Text style={styles.title1}>{I18n.t("personal_history")}</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  width: "100%",
                  alignItems: "center"
                }}
              >
                <View>
                  <Icon name="chevron-right" color="#969696" size={28} />
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("medicalHistory", {
                  fromProfile: true
                })
              }
              style={styles.fieldView}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  width: "100%"
                }}
              >
                <Text style={styles.title1}>{I18n.t("medical_history")}</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  width: "100%",
                  alignItems: "center"
                }}
              >
                <View>
                  <Icon name="chevron-right" color="#969696" size={28} />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("lifestyleHistory", {
                  fromProfile: true
                })
              }
              style={styles.fieldView}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  width: "100%"
                }}
              >
                <Text style={styles.title1}>{I18n.t("lifestyle_history")}</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  width: "100%",
                  alignItems: "center"
                }}
              >
                <View>
                  <Icon name="chevron-right" color="#969696" size={28} />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("symptomScreen", {
                  fromProfile: true
                })
              }
              style={styles.fieldView}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  width: "100%"
                }}
              >
                <Text style={styles.title1}>{I18n.t("symptoms_history")}</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  width: "100%",
                  alignItems: "center"
                }}
              >
                <View>
                  <Icon name="chevron-right" color="#969696" size={28} />
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("measurementScreen", {
                  fromProfile: true
                })
              }
              style={styles.fieldView}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  width: "100%"
                }}
              >
                <Text style={styles.title1}>
                  {I18n.t("measurement_history")}
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  width: "100%",
                  alignItems: "center"
                }}
              >
                <View>
                  <Icon name="chevron-right" color="#969696" size={28} />
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("myuploads")}
              style={styles.fieldView}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  width: "100%"
                }}
              >
                <Text style={styles.title1}>{I18n.t("my_uploads")}</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  width: "100%",
                  alignItems: "center"
                }}
              >
                <View>
                  <Icon name="chevron-right" color="#969696" size={28} />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ alignSelf: "center", paddingVertical: 10 }} />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
	user: state.user.userInfo,
    myToken: state.user.userToken,
    questionAnswer: state.userQuestionAnswer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles = StyleSheet.create({
  camera: {
    position: "absolute",
    bottom: 0,
    marginBottom: 9,
    alignSelf: "center"
  },
  rowView: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center"
  },
  rowInner: {
    flex: 1,
    height: 60,
    alignItems: "center",
    justifyContent: "space-between"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginVertical: 30,

    backgroundColor: color.primary
  },
  break: {
    width: "100%",
    borderColor: "#dadfe1",
    borderBottomWidth: 0.5,
    marginVertical: 10
  },
  editTitle: {
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: 12
  },
  customImage: {
    flexDirection: "column",
    width: 150,
    height: 150,
    borderRadius: 75,
    marginVertical: 20,
    alignSelf: "center"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "4%",
    marginVertical: 20,
    paddingLeft: 20,
    color: "#969696"
  },
  title2: {
    fontSize: 20,
    fontWeight: "400",
    letterSpacing: 0.5
  },
  iconView: {},
  title1: {
    fontSize: 15,
    fontWeight: "400",

    color: "#373535",
    letterSpacing: 0.5
    //paddingHorizontal: 10
  },
  pickerSt: {
    marginVertical: 17,
    width: "100%",
    alignSelf: "center",
    //flexDirection: "row",
    paddingTop: 6,
    paddingHorizontal: 15,
    justifyContent: "center",
    //alignItems: "center",
    //paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  breakSeparator: {
    borderLeftWidth: 0.6,
    borderColor: "#dadfe1",
    height: "100%",
    backgroundColor: "transparent",
    bottom: 0
  },
  editPersonal: {
    width: "91%",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    paddingHorizontal: 11,
    marginVertical: 13,
    paddingVertical: 15
  },
  fieldView: {
    flexDirection: "row",
    width: "91%",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",

    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: "white",
    elevation: 2, // Android

    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 11,
    paddingVertical: 10,
    marginTop: 16
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
