import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/MaterialIcons";
import ImagePicker from 'react-native-image-crop-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import Route from "../../network/route";
const route = new Route("http://192.168.100.18:8000/api/");
import { RNCamera } from 'react-native-camera';


import I18n from '../../i18n/i18n';

const screenWidth = Dimensions.get('window').width
class CameraView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userUploads: [],
      loading: false,
      photoUrl: "",
      flash: "off",
      zoom: 0,
      autoFocus: "on",
      uploads: [
        { image: "https://placeimg.com/640/480/tech" },
        { image: "https://placeimg.com/640/480/animals" },
        { image: "https://placeimg.com/640/480/nature" }
      ]
    };
  }

  componentDidMount() {}

  takePicture = async function() {
    this.setState({ loading: true });
    if (this.camera) {
      const options = {
        quality: 0.25,
        base64: true,
        fixOrientation: true,
        forceUpOrientation: true
      };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);

      this.setState({ photoUrl: data.uri }, () => {
        console.log("image path", this.state.photoUrl);

        
        this.setState({ loading: false });

        
      });
    }
  };
  sendImage(){
    this.setState({ loading: true });

    var data = new FormData();
    data.append("image", {
      uri: this.state.photoUrl, // your file path string
      name: "image.jpg",
      type: "image/jpg"
    });

    route.postfile('add-user-upload',data,this.props.myToken).then(async response => {
        if (response.error) {
            console.log(response.error)
            this.props.stopLoading();
            this.setState({loading:false})
        } else {
                console.log("user image upload",response);
                this.setState({loading:false})
                this.props.navigation.navigate("myuploads")
            }
        });
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          height: "100%",
          width: "100%",
          backgroundColor: "white"
        }}
      >
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />

        {this.state.photoUrl == "" ? (
          <View
            style={{ flex: 1, flexDirection: "row", backgroundColor: "white" }}
          >
            <RNCamera
              ref={ref => {
                this.camera = ref;
              }}
              style={{ height: "100%", width: "100%" }}
              fixOrientation={true}
              orientation={"portait"}
              // captureQuality={'medium'}
              flashMode={this.state.flash}
              autoFocus={this.state.autoFocus}
              zoom={this.state.zoom}
              fixOrientation={true}
              forceUpOrientation={true}
            />
            <View
              style={{
                position: "absolute",
                bottom: 40,
                left: screenWidth / 2 - 30
              }}
            >
              <TouchableOpacity
                onPress={this.takePicture.bind(this)}
                style={{
                  height: 60,
                  width: 60,
                  borderRadius: 30,
                  backgroundColor: color.primary
                }}
              />
            </View>
          </View>
        ) : (
          <View>
            <Image
              source={{ uri: this.state.photoUrl }}
              style={{ height: "92%", width: "100%" }}
            />
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "80%",
                alignSelf: "center",
                marginTop: 10
              }}
            >
              <TouchableOpacity
               
                onPress={() => this.setState({ photoUrl: "" })}
              >
                  <Text style={{ color: color.primary, fontSize: 14 }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
               
                onPress={() => this.sendImage()}
              >
                  <Text style={{ color: color.primary, fontSize: 14 }}>Use This Image</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles  = StyleSheet.create({
    media:{
        width: "90%",
    //height: "35%",
    alignSelf: "center",

    paddingVertical: 8,
    flexWrap: "wrap",
    flexDirection: "row"
    }
    ,
    rowView:{
        flexDirection:'row',
        width:'100%',
        alignItems:'center'
    },
    header:{
        alignSelf:'center',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
,        width:'90%',
        paddingVertical:20

    },
    rowInner:{
       marginHorizontal:3,
       marginBottom:10,
        height:150,
        width:100,
        // justifyContent:'flex-end',
        backgroundColor:'grey',
    },
    iconTop:{
        width:50,
        height:50,
        borderRadius:25,
        backgroundColor:color.primary,
        borderColor:'transparent',
       
        alignItems:'center',
        justifyContent:'center'
    },
    break: {
        width:'100%',
        borderColor: '#dadfe1',
        borderBottomWidth: 0.5,
        marginVertical:10,
    },
    editTitle:{
        alignSelf:'center',
        flexDirection:'row',
        alignItems:'center',
        paddingBottom:12,
    },
    customImage: {
        flexDirection: "column",
        width: 150,
        height:150,
        borderRadius:75,
        marginVertical:20,
        alignSelf:'center'
       
        
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-around"
    },
    title: {

        fontSize: 25,
        fontWeight: "700",
        letterSpacing: 0.5,
        
    },
    title2: {
        fontSize: 20,
        fontWeight: "400",
        letterSpacing: 0.5,
       
    },
    iconView: {
        
    
    },
    title1: {
        fontSize: 15,
        fontWeight: "400",
        
        color: "#373535",
        letterSpacing: 0.5,
        //paddingHorizontal: 10
    },
    breakSeparator:{
        
        borderLeftWidth: 0.6,
        borderColor: '#dadfe1',
        height: '100%',
        backgroundColor:'transparent',
        bottom:0
     
   
  },
    editPersonal:{
        width:"91%",
        alignSelf:'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: 'transparent',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal:11,
        marginVertical:13,
        paddingVertical: 15,

    },
    fieldView: {
        
        flexDirection: "row",
        width:"91%",
        alignItems:"center",
        alignSelf:'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: 'transparent',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal:11,
        paddingVertical: 10,
        marginTop: 16,
        


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex:1,
        flexDirection:"row",
        paddingHorizontal:20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraView);
