import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from '../../i18n/i18n';
import Spinner from 'react-native-loading-spinner-overlay';
import Route from "../../network/route.js";
const route = new Route("http://192.168.100.18:8000/api/");

class Auditlogs extends Component {
  constructor(props) {
    super(props);
    this.state = {

      loading:false,
      auditLog:[],
    };
  }

  componentDidMount() {
    this.fetchAuditLog()

  }

  fetchAuditLog(){
    this.setState({loading:true})
    route.getAuthenticated('get-audit-log',this.props.myToken).then(async(response)=>{
      if(response.error){
        console.log(response.error)
        this.setState({loading:false})
      }
      else {

          
          this.setState({auditLog:response.data},()=>{
            console.log("audit logs",this.state.auditLog)
          })
          this.setState({loading:false})
          // this.setState({inboxCount:response.data.unreadMsg})    

          }
      })
  }

  removeAuditLogAlert(clinic_id) {
        
    Alert.alert(
        `${I18n.t('remove_audit_log')}`,
        `${I18n.t('are_you_sure_to_this_logs')}`,
        [
            { text: `${I18n.t('cancel')}`, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'OK', onPress: () => this.removeAuditLog(clinic_id) },
        ],
        { cancelable: false }
    )
}
    removeAuditLog(clinic_id){
      let data = {clinic_id:clinic_id}
      this.setState({loading:true})
      route.updateData(`delete-audit-log`,data,this.props.myToken).then(async response => {
        if(response.error){
            console.log("response rror",response.error)
            this.setState({loading:false})

        }else{
            console.log("audit delete",response)
            this.fetchAuditLog()
            this.setState({loading:false})
        }
      })
    }
  render() {
    return (
      <View style={{ flex: 1,backgroundColor:'white' }}>
        <Header
          label={I18n.t('audit_logs')}
         onbackPress={()=>this.props.navigation.pop()}
        />
        <Spinner
            visible={this.state.loading}
            textContent={`${I18n.t('please_wait')}...`}
            textStyle={{color:color.primary}}
            cancelable={true}
            color="#F16638"
        />
        <ScrollView style={{ flex: 1,marginVertical:13,backgroundColor:'white' }}>
        <View style={{width:'85%',flexWrap:'wrap',paddingLeft:3}}>
        <Text style={styles.title3}>{I18n.t('all_clinics_have_access_personal_information')}</Text>

        </View>
        {this.state.auditLog.length > 0 ?
        this.state.auditLog.map((item,index)=>{

      
          return <View key={index} style={styles.fieldView}>
               <View style={{
                   flex: 1,
                   flexDirection: "row",
                   justifyContent: "flex-start",
                   width: "100%"}}>
                   <View>
                   
                   <Text style={styles.title1}>{item.clinic.name}</Text>
   
                   </View>
               </View>
               <View
                 style={{
                   flex: 1,
                   flexDirection: "row",
                   justifyContent: "flex-end",
                   width: "100%",
                   alignItems: "center"
                 }}>
                 
                 
                   <TouchableOpacity onPress={()=>this.removeAuditLogAlert(item.clinic.id)}>
                   <Text style={styles.titlePrice}>{I18n.t('remove')}</Text>
                   </TouchableOpacity>
                 </View>
             
           </View>
           })
        :
        null

        }
      
              
        <View style={{alignSelf:'center',paddingVertical:10}}>

        </View>
              
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles  = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: "flex-start"
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-around"
    },
    titlePrice: {
      fontSize: 15,
      fontWeight: "400",
      
      color: color.primary,
      letterSpacing: 0.5,
      //paddingHorizontal: 10
    },
    title2: {
        fontSize: 13,
        fontWeight: "400",
        letterSpacing: 0.5,
        paddingVertical:2,
        color:'#969696'
    },
    title3: {
      fontSize: 16,
      fontWeight: "600",
      letterSpacing: 0.5,
      paddingVertical:2,
      color:'#969696',
      paddingLeft:10
  },
    iconView: {
        
    
    },
    title1: {
        fontSize: 16,
        fontWeight: "400",
        
        color: "#373535",
        letterSpacing: 0.5,
        //paddingHorizontal: 10
    },
    fieldView: {
        
        flexDirection: "row",
        width:"91%",
        alignItems:"center",
        alignSelf:'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: 'transparent',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: 'white',
    elevation: 2, // Android
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 1 },
        // shadowOpacity: 0.20,
        // shadowRadius: 1.41,
        // elevation: 2,
        paddingHorizontal:11,
        paddingVertical: 10,
        marginTop: 16,
        


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex:1,
        flexDirection:"row",
        paddingHorizontal:20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auditlogs);
