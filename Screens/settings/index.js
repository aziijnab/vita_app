import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import NotificationHeader from "../../components/NotificationHeader";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from '../../i18n/i18n';
import Route from "../../network/route.js";
import { NavigationEvents } from "react-navigation";
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType
} from "react-native-fcm";

const route = new Route("http://192.168.100.18:8000/api/");

class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pushAllowed: false,
      user:null,
      pinCode:'',
      isPinCodeSet: this.props.isPinCodeSet
    };
	
	
  }
  static navigationOptions = ({ navigation }) => {
    return {
         tabBarOnPress: ({previousScene, scene, jumpToIndex}) => {
             const { route, index, focused} = scene;
              console.log("tab press",scene)
             if(focused){
                 navigation.state.params.scrollToTop()
             }
             jumpToIndex(0)
         }
     }
};
  allowPushNotifications() {
    this.changeNotificationFlag()

    
  }

  componentDidMount() {

    this.setState({user:this.props.userInfo})
    this.setState({ pinCode: this.props.pinCode})
    this.setState({ isPinCodeSet: this.props.isPinCodeSet})
    if(this.props.userInfo.allow_notification == 0){
      this.setState({ pushAllowed: false })
    }
    if(this.props.userInfo.allow_notification == 1){
      this.setState({ pushAllowed: true })
    }

    console.log("pinCode", this.state.pinCode)
  }

   sendDeviceTokentToApi() {
    FCM.getFCMToken().then(token => {
      console.log("TOKEN (getFCMToken)", token);
      let deviceTokect = { device_id: token }
      route.updateData('remove-device', deviceTokect, this.props.myToken).then(async tokenResponse => {
        if (tokenResponse.error) {
          console.log("user Error", tokenResponse.error);
        } else {
          console.log("Send User Token");
          console.log(tokenResponse);
        }
      });
    });
  }

   logOutPress(){
    this.props.setUser({token:"",userInfo:null})
    this.props.setPinCode('')
    this.props.isSetPinCode(false);
    this.props.isProfileSetup(false);
    this.props.isQuestionAnswerSetup(false);
    this.props.navigation.navigate('getStarted')

     this.sendDeviceTokentToApi()
  }

  changeNotificationFlag(){
    this.setState({loading:true})
    var notificationFlagAllow = 0
    if(this.state.pushAllowed == true){
      notificationFlagAllow = 0
    }else{
      notificationFlagAllow = 1
    }
    console.log("notification flag",notificationFlagAllow)
    let data = {'allow_notification': notificationFlagAllow}
    route.updateData('update-notification-flag',data,this.props.myToken).then(async response => {
    if (response.error) {
        this.setState({loading:false})
        Alert.alert("Error", JSON.stringify(response.error));
    } else {
        this.setState({loading:false})
        console.log("notification response",response);
        var user = this.props.userInfo
        console.log("userInformaiont",this.props.userInfo)
        user.allow_notification = notificationFlagAllow

        
        this.props.setUser({token:this.props.myToken,loggedUser:user})

        this.setState({ pushAllowed: !this.state.pushAllowed })
      }
    })
  }
  changePinIsSet(){
    this.setState({ isPinCodeSet: !this.state.isPinCodeSet},()=>{
      this.props.isSetPinCode(this.state.isPinCodeSet)
    })
  }
  render() {
    return <View style={{ flex: 1, backgroundColor: "white",paddingBottom:15 }}>
      <NavigationEvents
        onWillFocus={() => {
          this.setState({ isPinCodeSet: this.props.isPinCodeSet });
          this.setState({ pinCode: this.props.pinCode })
        }}
      />
        <NotificationHeader label={I18n.t("setting")} inboxCount={this.props.inboxCount} notificationCount={this.props.notificationCount} onMailPress={() => this.props.navigation.navigate("inbox")} onNotificationPress={() => this.props.navigation.navigate("notifications")} />
        <ScrollView style={{ flex: 1, marginVertical: 13, backgroundColor: "white" }}>
          <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate("profile")} style={styles.fieldView}>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
              <Text style={styles.title1}>{I18n.t("profile")}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
              <View>
                <Icon name="ios-arrow-forward" color="#969696" size={28} />
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate("accountHistory")} style={styles.fieldView}>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
              <Text style={styles.title1}>{I18n.t("account_history")}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
              <View>
                <Icon name="ios-arrow-forward" color="#969696" size={28} />
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate("updatePassword")} style={styles.fieldView}>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
              <Text style={styles.title1}>{I18n.t("updatePassword")}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
              <View>
                <Icon name="ios-arrow-forward" color="#969696" size={28} />
              </View>
            </View>
          </TouchableOpacity>

          {this.state.pinCode == "" ? 
          <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate("setpin")} style={styles.fieldView}>
              <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
                <Text style={styles.title1}>
                  {I18n.t("set_your_passcode")}
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
                <TouchableOpacity onPress={() => this.changePinIsSet()}>
                  {/* <Icon name="ios-checkmark-circle-outline" color={this.state.isPinCodeSet == true ? color.primary : "#dfe6e9"} size={28} /> */}
                  <Icon name="ios-arrow-forward" color="#969696" size={28} />
                </TouchableOpacity>
              </View>
            </TouchableOpacity> 
            : 
            <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate("updatePinCode")} style={styles.fieldView}>
              <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
                <Text style={styles.title1}>
                  {I18n.t("update_pin_code")}
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
                <TouchableOpacity onPress={()=> this.changePinIsSet()}>
                  <Icon name="ios-checkmark-circle-outline" color={this.state.isPinCodeSet == true ? color.primary : "#dfe6e9"} size={28} />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>}

          {/* <TouchableOpacity activeOpacity={0.9} onPress={() => this.allowPushNotifications()} style={styles.fieldView}>
            <View style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "flex-start",
              width: "100%"
            }}>
              <Text style={styles.title1}>{I18n.t('allow_push')}</Text>
            </View>
            <View
              style={{
                flex: 0.3,
                flexDirection: "row",
                justifyContent: "flex-end",
                width: "100%",
                alignItems: "center"
              }}>


              <View>


                <Icon name="ios-checkmark-circle-outline" color={this.state.pushAllowed ? color.primary : '#dfe6e9'} size={28} />



              </View>
            </View>

          </TouchableOpacity> */}
		  
		  <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate("languageScreen")} style={styles.fieldView}>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
              <Text style={styles.title1}>{I18n.t("language")}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
              <View>
                <Icon name="ios-arrow-forward" color="#969696" size={28} />
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate("auditLogs")} style={styles.fieldView}>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
              <Text style={styles.title1}>{I18n.t("audit_logs")}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
              <View>
                <Icon name="ios-arrow-forward" color="#969696" size={28} />
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate("terms")} style={styles.fieldView}>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
              <Text style={styles.title1}>{I18n.t("terms")}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
              <View>
                <Icon name="ios-arrow-forward" color="#969696" size={28} />
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate("privacy")} style={styles.fieldView}>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
              <Text style={styles.title1}>{I18n.t("privacy")}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
              <View>
                <Icon name="ios-arrow-forward" color="#969696" size={28} />
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.logOutPress()} style={{ alignSelf: "center", paddingVertical: 10 }}>
            <Text
              style={{
                color: color.primary,
                fontWeight: "400",
                fontSize: 17
              }}
            >
              {I18n.t("logout")}
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>;
  }
}

function mapStateToProps(state, props) {
  return {
    userInfo: state.user.userInfo,
    myToken: state.user.userToken,
    pinCode: state.user.pinCode,
    isPinCodeSet: state.user.isPinCodeSet
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {

    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "4%",
    marginVertical: 20,
    paddingLeft: 20,
    color: "#969696"
  },
  title2: {
    fontSize: 20,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: 6,
    paddingLeft: 30
  },
  iconView: {


  },
  title1: {
    fontSize: 15,
    fontWeight: "400",

    color: "#373535",
    letterSpacing: 0.5,
    //paddingHorizontal: 10
  },
  fieldView: {

    flexDirection: "row",
    width: "91%",
    alignItems: "center",
    alignSelf: 'center',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: 'transparent',
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: 'white',
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 11,
    paddingVertical: 10,
    marginTop: 16,



  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
      borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
      borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsScreen);
