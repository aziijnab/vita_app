import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from '../../i18n/i18n';
import Spinner from "react-native-loading-spinner-overlay";
import Route from "../../network/route.js";
import moment from "moment";

const route = new Route("http://192.168.100.18:8000/api/");
class AccountHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accountHistory: [],

      data:[
        {name:'Mediveron Clinic',price:200},
        {name:'Health Clinic',price:400},
        {name:'Bjourn Health',price:600}

      ]

    };
  }

  componentDidMount() {
    this.fetchAccountHistory();
  }


  fetchAccountHistory(){
    this.setState({ loading: true })
    route
      .getAuthenticated("account-history", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          console.log("account history",response)
          this.setState({ loading: false });
          this.setState({accountHistory:response.data})
        }
      });
  }
  changeDateFormat(date) {
    var newDate = moment(date).format("DD MMM YYYY");
    return newDate
  }

  render() {
    return (
      <View style={{ flex: 1,backgroundColor:'white' }}>
        <Spinner
          visible={this.state.loading}
          textContent={I18n.t('please_wait')}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Header
          label={I18n.t('account_history')}
         onbackPress={()=>this.props.navigation.pop()}
        />
        <ScrollView style={{ flex: 1,marginVertical:13,backgroundColor:'white' }}>
        <Text style={styles.title3}>{I18n.t('all_transactions')}</Text>
      {this.state.accountHistory.map((item,index)=>{

      
       return <View key={index} style={styles.fieldView}>
            <View style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-start",
                width: "100%"}}>
                <View>
                <Text style={styles.title2}>{this.changeDateFormat(item.date)}</Text>
                <Text style={styles.title1}>{item.clinic.name}</Text>

                </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-end",
                width: "100%",
                alignItems: "center"
              }}>
              
              
                <View>
                <Text style={styles.titlePrice}>RM{item.amount}</Text>
                </View>
              </View>
          
        </View>
        })}
              
        <View style={{alignSelf:'center',paddingVertical:10}}>

        </View>
              
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles  = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: "flex-start"
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-around"
    },
    titlePrice: {
      fontSize: 15,
      fontWeight: "400",
      
      color: color.primary,
      letterSpacing: 0.5,
      //paddingHorizontal: 10
    },
    title2: {
        fontSize: 13,
        fontWeight: "400",
        letterSpacing: 0.5,
        paddingVertical:2,
        color:'#969696'
    },
    title3: {
      fontSize: 16,
      fontWeight: "600",
      letterSpacing: 0.5,
      paddingVertical:2,
      color:'#969696',
      paddingLeft:10
  },
    iconView: {
        
    
    },
    title1: {
        fontSize: 16,
        fontWeight: "400",
        
        color: "#373535",
        letterSpacing: 0.5,
        //paddingHorizontal: 10
    },
    fieldView: {
        
        flexDirection: "row",
        width:"91%",
        alignItems:"center",
        alignSelf:'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: 'transparent',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: 'white',
    elevation: 2, // Android
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 1 },
        // shadowOpacity: 0.20,
        // shadowRadius: 1.41,
        // elevation: 2,
        paddingHorizontal:11,
        paddingVertical: 10,
        marginTop: 16,
        


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex:1,
        flexDirection:"row",
        paddingHorizontal:20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountHistory);
