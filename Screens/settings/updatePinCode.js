import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Button from '../../components/Button';
import Icon from "react-native-vector-icons/Ionicons";
import I18n from '../../i18n/i18n';
import Toast, {DURATION} from 'react-native-easy-toast'
class UpdatePinCode extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      pinCode:'',
      fromMain:false
    };
  }

  componentDidMount() { 
    const { params } = this.props.navigation.state;
    if(typeof params !== "undefined"){
        this.setState({fromMain : params.fromMain})
    }

    console.log("from main",this.state.fromMain)

  }

  pinCodeSet(){
    console.log("pin code is set",this.state.pinCode);
    if (this.props.pinCode == ''){
      this.props.setPinCode(this.state.pinCode)
      this.props.isSetPinCode(true)
      this.props.navigation.pop()
    }else{
      if(this.state.pinCode == this.props.pinCode){
        if(this.props.myToken != ""){
          if(this.props.user.user_profile != null){
            if(this.props.user.user_question_answer.length > 0){
              this.props.navigation.navigate("App")
              
            }else{
              console.log("yaha chal raha ha")
              this.props.navigation.navigate("medicalHistory")
            }
          }else{
            this.props.navigation.navigate("createProfileScreen");
          }
        }else{
          console.log(this.props.boarDone)
          if(this.props.boarDone == true){
            this.props.navigation.navigate('getStarted');
    
          }else{
            this.props.navigation.navigate('Auth');
    
          }
        }
      }else{
        this.refs.toast.show(I18n.t('your_pin_code_is_wrong'),DURATION.SHORT);
      }
    }
    
  }

  updateUserPinCode(){
    this.props.setPinCode(this.state.pinCode)
    this.props.navigation.pop()
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        {this.state.fromMain == false &&
            <Header
            label={I18n.t('update_pin_code')}
            onbackPress={() => this.props.navigation.pop()}
          />

        }
        <Toast
          ref="toast"
          style={{backgroundColor:'red'}}
          position='top'

          positionValue={80}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{color:'white'}}
        />
        <ScrollView style={{ flex: 1, marginVertical: 13, backgroundColor: 'white' }}>
          <View style={styles.header}>
            <Text style={[styles.title,{textAlign:'center'}]}>{I18n.t('update_pin_code')}</Text>
          </View>

          
          <View style={styles.fieldView}>
            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              maxLength={4}
              onChangeText={text => this.setState({pinCode:text})}
              keyboardType={'phone-pad'}
              underlineColorAndroid="transparent"
              //placeholderTextColor="#969696"
              blurOnSubmit={false}
            />
          </View>



        

          <View style={{ alignSelf: 'center', paddingVertical: 10 }} />
          <Button buttonStyle={styles.button} label={I18n.t('done')} onPress={() => this.updateUserPinCode()} />

        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    boarDone: state.user.boardDone,
    selectedLanguage: state.user.selectedLanguage,
    pinCode:state.user.pinCode
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",


    backgroundColor: color.primary
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {

    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,

  },

  header: {
    alignSelf: "center",
    width: '80%',
    flexWrap: 'wrap',
    paddingVertical: 30
  },
  fieldView: {

    flexDirection: "row",
    alignItems: "center",
    //justifyContent: 'space-evenly',
    alignSelf: 'center',
    paddingVertical: 20,


  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {

    letterSpacing:20,
    width: "80%",
    textAlign: 'center',
    flexDirection: "row",
    backgroundColor: '#e8e8e8',
    color: 'black',
    alignItems: "center",
    fontSize: 20,
    padding: 13,
    marginHorizontal: 7,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: "#e8e8e8"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
      borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
      borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdatePinCode);
