import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/MaterialIcons";
import ImagePicker from 'react-native-image-crop-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import Route from "../../network/route.js";
const route = new Route("http://192.168.100.18:8000/api/");
import { RNCamera } from 'react-native-camera';
import { NavigationEvents } from 'react-navigation';


import I18n from '../../i18n/i18n';
class Myuploads extends Component {
  constructor(props) {
    super(props);
    this.state = {
        userUploads:[],
        loading:false,
        photoUrl:'',
        uploads:[
            {image:'https://placeimg.com/640/480/tech'},
            {image:'https://placeimg.com/640/480/animals'},
            {image:'https://placeimg.com/640/480/nature'},
            
        ]

    };
  }

  componentDidMount() {
      this.fetchUserUploads();
  }

  fetchUserUploads(){
    this.setState({loading:true})
    route.getAuthenticated('get-user-upload',this.props.myToken).then(async(response)=>{
      if(response.error){
        console.log(response.error)
        this.setState({loading:false})
      }
      else {

          
          this.setState({userUploads:response.upload},()=>{
            console.log("my uploads",this.state.userUploads)
          })
          this.setState({loading:false})
          // this.setState({inboxCount:response.data.unreadMsg})    

          }
      })
  }

  deleteUserUploadsAlert(clinic_id) {
        
    Alert.alert(
        `${I18n.t('delete_my_upload')}`,
        `${I18n.t('are_you_sure_to_delete_this_upload')}`,
        [
            { text: `${I18n.t('cancel')}`, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'OK', onPress: () => this.deleteUserUploads(clinic_id) },
        ],
        { cancelable: false }
    )
}
  deleteUserUploads(upload_id){
    let data = {upload_id:upload_id}
    this.setState({loading:true})
    route.updateData(`delete-user-upload`,data,this.props.myToken).then(async response => {
      if(response.error){
          console.log("response rror",response.error)
          this.setState({loading:false})

      }else{
          console.log("user upload delete delete",response)
          this.fetchUserUploads()
          this.setState({loading:false})
      }
    })
  }
pickImage(){
    ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true
      }).then(image => {
        this.setState({image:image.path},()=>{
            console.log("image path",this.state.image)
            this.setState({loading:true})
            var data = new FormData();
            data.append('image', {
            uri: this.state.image, // your file path string
            name: 'image.jpg',
            type: 'image/jpg'
            })
            let userImage = {'image': this.state.image}
            route.postfile('add-user-upload',data,this.props.myToken).then(async response => {
            if (response.error) {
                console.log(response.error)
                this.props.stopLoading();
                this.setState({loading:false})
            } else {
                    console.log("user image upload",response);
                    console.log(response);
                    this.fetchUserUploads()
                    this.setState({loading:false})
                }
            });
        })
      });

}


  render() {
    return (
      <View style={{flex:1,height:'100%',width:'100%',paddingBottom:6,backgroundColor:'white' }}>
        <Header
          label={I18n.t('my_uploads')}
          
          onbackPress ={() => this.props.navigation.pop()}
        />
        <NavigationEvents
                    onDidFocus={()=>this.fetchUserUploads()}
                />
        <Spinner
            visible={this.state.loading}
            textContent={`${I18n.t('please_wait')}...`}
            textStyle={{color:color.primary}}
            cancelable={true}
            color="#F16638"
        />
        <ScrollView style={{ flex: 1,paddingVertical:13,height:'100%',backgroundColor:'white' }}>
        <View style={styles.header}>
            <Text style={styles.title}>{I18n.t('your_uploads')}</Text>
        </View>
        <View style={{width:'80%', paddingBottom:20 ,alignSelf:'center',flexDirection:'row',justifyContent:"flex-end"}}>
            <View >
            <TouchableOpacity onPress={()=>this.pickImage()} style={styles.iconTop}>
                <Icon name='folder' color='white' size={24}  />
            </TouchableOpacity>
            </View>
            <View style={{paddingLeft:20}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('cameraScreen')} style={styles.iconTop}>
                <Icon name='camera-alt' color='white' size={24}  />
            </TouchableOpacity>
            </View>
        </View>


        <View style={{flex:1}}>
        <View style={styles.media}>
        
            
            

        {this.state.userUploads.map((item,index)=>{
            return (
                <View key={index} style={styles.rowInner}>

        <Image source={{uri:item.image}} style={{width:'100%',height:125}} resizeMode='cover' />
        <TouchableOpacity onPress={() => this.deleteUserUploadsAlert(item.id)} style={{backgroundColor:color.primary,width:'100%',alignItems:'center',justifyContent:'center',paddingVertical:3}}>
             <Text style={{color:'white'}}>{I18n.t('delete')}</Text>
        </TouchableOpacity>
        </View>
            )
        })}
                

        </View>
        
        </View>
        <View style={{alignSelf:'center',paddingVertical:10}} />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles  = StyleSheet.create({
    media:{
        width: "90%",
    //height: "35%",
    alignSelf: "center",

    paddingVertical: 8,
    flexWrap: "wrap",
    flexDirection: "row"
    }
    ,
    rowView:{
        flexDirection:'row',
        width:'100%',
        alignItems:'center'
    },
    header:{
        alignSelf:'center',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
,        width:'90%',
        paddingVertical:20

    },
    rowInner:{
       marginHorizontal:3,
       marginBottom:10,
        height:150,
        width:100,
        // justifyContent:'flex-end',
        backgroundColor:'grey',
    },
    iconTop:{
        width:50,
        height:50,
        borderRadius:25,
        backgroundColor:color.primary,
        borderColor:'transparent',
       
        alignItems:'center',
        justifyContent:'center'
    },
    break: {
        width:'100%',
        borderColor: '#dadfe1',
        borderBottomWidth: 0.5,
        marginVertical:10,
    },
    editTitle:{
        alignSelf:'center',
        flexDirection:'row',
        alignItems:'center',
        paddingBottom:12,
    },
    customImage: {
        flexDirection: "column",
        width: 150,
        height:150,
        borderRadius:75,
        marginVertical:20,
        alignSelf:'center'
       
        
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-around"
    },
    title: {

        fontSize: 25,
        fontWeight: "700",
        letterSpacing: 0.5,
        
    },
    title2: {
        fontSize: 20,
        fontWeight: "400",
        letterSpacing: 0.5,
       
    },
    iconView: {
        
    
    },
    title1: {
        fontSize: 15,
        fontWeight: "400",
        
        color: "#373535",
        letterSpacing: 0.5,
        //paddingHorizontal: 10
    },
    breakSeparator:{
        
        borderLeftWidth: 0.6,
        borderColor: '#dadfe1',
        height: '100%',
        backgroundColor:'transparent',
        bottom:0
     
   
  },
    editPersonal:{
        width:"91%",
        alignSelf:'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: 'transparent',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal:11,
        marginVertical:13,
        paddingVertical: 15,

    },
    fieldView: {
        
        flexDirection: "row",
        width:"91%",
        alignItems:"center",
        alignSelf:'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: 'transparent',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal:11,
        paddingVertical: 10,
        marginTop: 16,
        


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex:1,
        flexDirection:"row",
        paddingHorizontal:20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Myuploads);
