import React, { Component } from 'react';
import {
  View, ActivityIndicator, StatusBar, AsyncStorage,
} from 'react-native';
import {color, font, apiURLs} from '../components/Constant';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions'; //Import your actions


class MainApp extends Component {

  constructor(props) {
    super(props);
    this.state = {
        userDetail : this.props.userDetail,
      };
    this._bootstrapAsync();  
  }
  
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userLogin');
    // this.props.setUser({token:"",userInfo:null})
    this.props.navigation.navigate("Auth")
    
      
    
    
    // this.props.navigation.navigate(userToken == 'isLoggedIn' ? 'App' : 'Auth');
    
  };

  componentDidMount(){
    console.log('pin code here did mount',this.props.pinCode)
    
  }

  render() {
    return (
      <View style={{ flex:1, justifyContent:'center', alignItems:'center' }}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }

}

function mapStateToProps(state, props) {
  // console.log('map start proos',state,props);
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    boarDone: state.user.boardDone,
    pinCode: state.user.pinCode
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MainApp);
