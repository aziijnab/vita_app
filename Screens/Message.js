import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    FlatList,
    Alert,
    KeyboardAvoidingView,
    Platform
} from "react-native";
import { color, font, apiURLs } from "../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as ReduxActions from "../actions"; //Import your actions;
import Header from "../components/Header";
import Icon from "react-native-vector-icons/Feather";
import { GiftedChat, Actions, Bubble, SystemMessage, Send, Composer } from 'react-native-gifted-chat'
import Route from "../network/route.js";
import Spinner from 'react-native-loading-spinner-overlay';
import SendMessageButton from "../components/SendMessageButton";
import InputToolbar from "../components/InputToolbar"
// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");
import I18n from '../i18n/i18n';
class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            allMessages:[],
            messages: [],
            clinic_id:this.props.navigation.state.params.clinic_id,
            clinic_name:this.props.navigation.state.params.clinic_name,
            data: [
                { name: 'Mediveron', message: 'Big brown fox jumped over the lazy dog, hascal newton ploto neil thompson, lotem ipsum dolar sit amit.', time: 8, price: 200 },
                { name: 'Health Clinic', message: 'TTD health clinic has sent you a message. Please respond at the earliest', time: 2, price: 400 },
                { name: 'Bjourn Health', message: 'TTD health clinic has sent you a message.', time: 9, price: 600 }

            ]

        };
        this.renderComposer = this.renderComposer.bind(this);
        this.renderSend = this.renderSend.bind(this);
        this.renderBubble = this.renderBubble.bind(this);
        this.renderInputToolbar = this.renderInputToolbar.bind(this);
        this.renderCustomActions = this.renderCustomActions.bind(this);
    }


    fetchChatMessages(){
        console.log("Message did mount called",this.state.chat_id)
        this.setState({loading:true})
        route.updateData('get-chat-msg',{clinic_id:this.state.clinic_id},this.props.myToken).then(async(response)=>{
        if(response.error){
          console.log(response.error)
          this.setState({loading:false})
        }
        else {

            console.log(response)
            this.props.setInboxCount(0)

            this.setState({loading:false})
            route.checkTokenExpire(response)
            var messages = []
            var user_id = null
            response.msgs.map((value,index)=>{
                if(value.sender_id == this.props.user.id){
                    user_id = 1
                }else{
                    user_id = value.receiver.id
                }
                let message = {
                        _id:value.id,
                        text:value.msg, 
                        createdAt: value.created_at,
                        user :{
                            _id:user_id,
                            name:value.sender.name,
                            avatar:'https://placeimg.com/140/140/any'
                        }
                    }
                    messages.push(message)
            })
            messages.reverse()
            this.setState({ messages: messages})
            console.log("user All Messages",this.state.messages);          
            }
        })
     }

    componentDidMount() {

        this.fetchChatMessages()
    }



    renderSend(props) {
        return (
            <SendMessageButton {...props}>
                <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center', backgroundColor: color.primary }}>
                    <Icon name='send' color='white' style={{ padding: 3 }} size={26} />
                </View>
            </SendMessageButton>
        );
    }

    renderComposer(props) {
        return <Composer {...props}
            textInputStyle={{ alignSelf: 'flex-start', backgroundColor: color.primary, color: "white", }}
            placeholderTextColor='white'
            placeholder='Type something'
        />
    }


    renderBubble(props) {
        return (
            <Bubble
                {...props}
                wrapperStyle={{
                    right: {
                        backgroundColor: color.primary,
                        borderRadius: 4,
                    },
                    left: {
                        backgroundColor: 'white',
                        borderColor:'#ececec',
                        borderWidth:0.5,
                        borderRadius: 4,
                    }
                }}
            />
        )
    }

   



    renderInputToolbar(props) {
        // Here you will return your custom InputToolbar.js file you copied before and include with your stylings, edits.
        return (
            <InputToolbar {...props}

                containerStyle={{ backgroundColor: color.primary }}

            />
        )
    }
    showCameraIcon(){
        return(
              <Icon name='plus-circle' color='white'  size={26}/>
          
        )
      }
    renderCustomActions(props) {

        return (
            <Actions
              {...props}
              // options={options}
              onPressActionButton={()=>console.log('efr')}
              icon={this.showCameraIcon}
              containerStyle={{backgroundColor:color.primary,width:30}}
            />
            
          );
        }


    onSend(messages = []) {
        // messages.map((value,index)=>{
        //     value.user._id = this.props.user.id
        // })
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
        var message = {}
        messages.map((value,index)=>{
            message.clinic_id = this.state.clinic_id
            message.msg= value.text
        })
        console.log('send message',message)
        this.sendMessageToServer(message)

    }

    sendMessageToServer(message){
        console.log("Message sent to server",message)
        this.setState({loading:true})
        route.updateData('save-chat-msg',message,this.props.myToken).then(async(response)=>{
        if(response.error){
          console.log(response.error)
          this.setState({loading:false})
        }
        else {

            console.log("send message",response)
            this.setState({loading:false})   
            route.checkTokenExpire(response)    
            }
        })
     }
    removeItem(props) {
        var arr = this.state.data
        arr.splice(props, 1)
        this.setState({ data: arr })

    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <Spinner
                    visible={this.state.loading}
                    textContent={`${I18n.t('please_wait')}...`}
                    textStyle={{color:color.primary}}
                    cancelable={true}
                    color="#F16638"
                />
                <Header
                    label={this.state.clinic_name}
                    onbackPress={() => this.props.navigation.pop()}
                />


                <View style={styles.container}>

                    <GiftedChat
                        messages={this.state.messages}
                        onSend={messages => this.onSend(messages)}
                        renderComposer={this.renderComposer}
                        renderSend={this.renderSend}
                        alwaysShowSend={true}
                        renderBubble={this.renderBubble}
                        renderInputToolbar={this.renderInputToolbar}

                        //renderActions={this.renderCustomActions}

                        user={{
                            _id: 1,
                        }}
                    />
                </View>

            </View>
        );
    }
}



const styles = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: "flex-start"
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-around"
    },
    titlePrice: {
        fontSize: 15,
        fontWeight: "400",

        color: color.primary,
        letterSpacing: 0.5,
        //paddingHorizontal: 10
    },
    title2: {
        fontSize: 16,
        fontWeight: "400",
        letterSpacing: 0.5,
        paddingVertical: 2,
        color: '#969696'
    },
    title3: {
        fontSize: 16,
        fontWeight: "400",
        letterSpacing: 0.5,
        paddingVertical: 2,
        color: '#131313'
    },
    iconView: {


    },
    title1: {
        fontSize: 18,
        fontWeight: "400",

        color: "#131313",

        //paddingHorizontal: 10
    },
    fieldView: {

        flexDirection: "row",
        width: "91%",
        flexWrap: 'wrap',
        alignItems: "center",
        alignSelf: 'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: 'transparent',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
        shadowOffset: { height: 1, width: 1 }, // IOS
        shadowOpacity: 1, // IOS
        shadowRadius: 1, //IOS
        backgroundColor: 'white',
        elevation: 2, // Android
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 1 },
        // shadowOpacity: 0.20,
        // shadowRadius: 1.41,
        // elevation: 2,
        paddingHorizontal: 11,
        paddingVertical: 10,
        marginTop: 16,



    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },

})


function mapStateToProps(state, props) {
    return {
        user: state.user.userInfo,
        myToken: state.user.userToken,
        questionAnswer: state.userQuestionAnswer
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ReduxActions, dispatch);
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Message);
