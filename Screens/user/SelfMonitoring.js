import React, { Component } from "react";
import {
  Text,
  View,
  Alert,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  FlatList
} from "react-native";
// import { LineChart } from '../../components/react-native-chart-kit'
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import Header from "../../components/Header";
import ActionButton from "react-native-circular-action-menu";
import Icon from "react-native-vector-icons/Feather";
import I18n from "../../i18n/i18n";
import Route from "../../network/route.js";
import Spinner from "react-native-loading-spinner-overlay";
import Button from "../../components/Button";
import moment, { ISO_8601 } from "moment";
import lodash from "lodash";
import Toast, { DURATION } from "react-native-easy-toast";
import * as d3Scale from "d3-scale";
import { LineChart, Grid, YAxis, XAxis } from "react-native-svg-charts";
// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

// create a component
const chartWidth = Dimensions.get("window").width;

class SelfMonitoring extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      loading: false,
      selected: null,
      monitoringParameters: [],
      monitoringHistoryData: [],
      chartData: null,
      parameterCartData: [],
      parameterCartData2: null,
      parameterName: "",
      parameter_id: null,
      parameterNewValue: "",
      parameterCartDate: [],
      diastolicBloodPressure: "",
      systolicBloodPressure: "",
      showChart: false,
      selectedBlood: false,
      bloodParameterId: {},
      diastolicBloodSet: true,
      systolicBloodSet: false,
      reversHistoryData: [],
      abc: [],
      imageLink: require("../../asset/images/heartbeat.png"),
      listData: [
        { name: "CIRCUMFERENCE", selected: false },
        { name: "BLOOD PRESSURE", selected: true },
        { name: "WEIGHT", selected: false },
        { name: "DIABETES", selected: false }
      ]
    };
  }
  componentDidMount() {
    this.fetchSelfMonitoringParameters();
  }

  setMedicalHistoryDataToDelete(parameter_id, name) {
    // alert(parameter_id)
    if (name == "diastolic") {
      this.setState({ diastolicBloodSet: true });
      this.setState({ systolicBloodSet: false });
    }

    if (name == "systolic") {
      this.setState({ systolicBloodSet: true });
      this.setState({ diastolicBloodSet: false });
    }
    var parameterData = { test_parameter_id: parameter_id };
    console.log("parameterData", parameterData);
    route
      .updateData("parameter-history", parameterData, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.props.stopLoading();
          this.setState({ loading: false });
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          console.log("Parameter history data", response);
          var reversHistoryData = this.reverseArray(response.data);

          this.setState({ reversHistoryData: reversHistoryData });
          this.setState({ monitoringHistoryData: response.data }, () => {
            console.log(
              "moitoringHisotryData set",
              this.state.monitoringHistoryData
            );
          });
          this.setState({ loading: false });
        }
      });
  }

  async fetchSelfMonitoringParameters() {
    this.setState({ loading: true });
    route
      .getAuthenticated("self-monitor-parameter", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
          console.log("parameter data", response.data);
          var parameterArray = [];
          var parameterBloodData = {
            title: "Blood Pressure",
            test_parameter_id: {}
          };
          var counterForBlood = 0;
          response.data.map((value, index) => {
            if (value.title == "Diastolic Blood pressure") {
              parameterBloodData.test_parameter_id.parameter_diastolic_id =
                value.test_parameter_id;
              if (counterForBlood == 1) {
                parameterArray.push(parameterBloodData);
              }
              counterForBlood = 1;
            } else if (value.title == "Systolic Blood pressure") {
              parameterBloodData.test_parameter_id.parameter_systolic_id =
                value.test_parameter_id;
              if (counterForBlood == 1) {
                parameterArray.push(parameterBloodData);
              }
              counterForBlood = 1;
            } else {
              parameterArray.push(value);
            }
          });
          this.setState({ monitoringParameters: parameterArray }, () => {
            console.log(
              "monitoring parameter data ",
              this.state.monitoringParameters
            );
          });
          await this.fetchParameterHistoryData(
            response.data[0].test_parameter_id,
            response.data[0].title
          );
        }
      });
  }
  // changeDateFormatWithYear(date) {
  //   var date = moment(date).format("DD/MM");
  //   return date;
  // }
  changeDateFormatWithYear(date){
    var date = moment(date).format("DD/MM/YY");
    return date;
  }
  getDayFormat(date) {
    var newDate = moment(date).format("DD");
    return parseInt(newDate);
  }
  getMonthFormat(date) {
    var newDate = moment(date).format("MM");
    return parseInt(newDate);
  }
  getYearFormat(date) {
    var newDate = moment(date).format("Y");
    return parseInt(newDate);
  }
  reverseArray(arr) {
    var newArray = [];
    for (var i = arr.length - 1; i >= 0; i--) {
      newArray.push(arr[i]);
    }
    return newArray;
  }

  fetchSingleParameterHistoryData(parameter_id, parameterName, showchart) {
    this.setState({ showChart: showchart });
    this.setState({ parameterName: parameterName });
    this.setState({ parameter_id: parameter_id });
    this.setState({ parameterCartData: [] });
    this.setState({ parameterCartDate: [] });
    this.setState({ parameterCartData2: null });
    this.setState({ loading: true });
    this.setState({ selected: parameter_id });
    var parameterData = { test_parameter_id: parameter_id };
    var parameterDataReturn = {};
    var newParameterDate = "";
    console.log("parameterData", parameterData);
    var parameterCartData = [];
    var parameterCartDate = [];
    route
      .updateData("parameter-history", parameterData, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.props.stopLoading();
          this.setState({ loading: false });
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          console.log("Parameter history data", response);
          this.setState({ monitoringHistoryData: response.data }, () => {
            var reversHistoryData = this.reverseArray(response.data);

            this.setState({ reversHistoryData: reversHistoryData }, () => {
              console.log(
                "moitoringHisotryData set",
                this.state.monitoringHistoryData
              );
              console.log(
                "reversHistoryData set",
                this.state.reversHistoryData
              );
            });

            if (this.state.monitoringHistoryData != []) {
              var valueSum = 0;
              var firstTimearray = true;
              this.state.monitoringHistoryData.map((value, index) => {
                console.log("moitoringHisotryData set value", value);
                console.log("new day", this.getDayFormat(value.created_at));
                console.log("get day", this.getDayFormat(value.created_at));
                
                var findIndex = parameterCartDate.findIndex(
                  (value, index) => this.changeDateFormatWithYear(value) === this.changeDateFormatWithYear(value.created_at)
                );
                if (findIndex < 0 || firstTimearray == true) {
                console.log("parameter cart date", parameterCartDate),
                parameterCartData.push(parseInt(value.user_value));
                console.log("adate", this.changeDateFormatWithYear(value.created_at));
                parameterCartDate.push(value.created_at);
                firstTimearray = false;
                }
              });
              newParameterDate = lodash.sortedUniq(parameterCartDate);
              this.setState({ loading: false });
            }
          });
          this.setState({ loading: false });
        }
      });
    parameterDataReturn = {
      parameterCartData: parameterCartData,
      parameterCartDate: parameterCartDate
    };
    console.log("return from method", parameterDataReturn);
    return parameterDataReturn;
  }

  fetchParameterHistoryData(parameter_id, parameterName) {
    this.setState({ parameterCartData2: null });
    var multiParameterData = {};
    console.log("id object or not", parameter_id);
    if (typeof parameter_id == "object") {
      this.setState({ bloodParameterId: parameter_id, selectedBlood: true });
      console.log(
        "parameter_id length",
        this.fetchSingleParameterHistoryData(
          parameter_id.parameter_diastolic_id,
          parameterName
        )
      );
      multiParameterData.parameter_diastolic_blood = this.fetchSingleParameterHistoryData(
        parameter_id.parameter_diastolic_id,
        parameterName
      );
      multiParameterData.parameter_systolic_blood = this.fetchSingleParameterHistoryData(
        parameter_id.parameter_systolic_id,
        parameterName
      );
      this.setState({ parameterCartData2: multiParameterData });
    } else {
      console.log(
        "return parameter history data",
        this.fetchSingleParameterHistoryData(parameter_id, parameterName)
      );
      var parameterDataValue = this.fetchSingleParameterHistoryData(
        parameter_id,
        parameterName
      );
      this.setState(
        {
          parameterCartData: parameterDataValue.parameterCartData,
          parameterCartDate: parameterDataValue.parameterCartDate,
          selectedBlood: false
        },
        () => {
          console.log("para meter cart data", this.state.parameterCartData);
        }
      );
    }
  }

  sendDataToApi() {
    this.setState({ loading: true });
    if (this.state.parameterNewValue != "") {
      var parameterData = {
        test_parameter_id: this.state.parameter_id,
        value: this.state.parameterNewValue
      };
      route
        .updateData("self-monitor-save", parameterData, this.props.myToken)
        .then(async response => {
          if (response.error) {
            console.log(response.error);
            this.props.stopLoading();
            this.setState({ loading: false });
            Alert.alert("Error", JSON.stringify(response.error));
          } else {
            console.log("new value responsee", response);
            this.fetchParameterHistoryData(
              this.state.parameter_id,
              this.state.parameterName,
              this.state.showChart
            );
            this.setState({ loading: false });
          }
        });
    } else {
      this.setState({ loading: false });
      this.refs.toast.show(I18n.t("please_enter_your_data"), DURATION.SHORT);
    }
  }

  addBloodParameterEntry() {
    console.log("this.state.bloodParameterId", this.state.bloodParameterId);
    this.setState({ loading: true });
    var validateBloodData = true;
    if (
      parseFloat(this.state.systolicBloodPressure) < 70 ||
      parseFloat(this.state.systolicBloodPressure) > 180
    ) {
      this.refs.toast.show(I18n.t("systolic_blood_error"), DURATION.SHORT);
      validateBloodData = false;
      this.setState({ loading: false });
    } else if (
      parseFloat(this.state.diastolicBloodPressure) < 40 ||
      parseFloat(this.state.diastolicBloodPressure) > 120
    ) {
      this.refs.toast.show(I18n.t("diastolic_blood_error"), DURATION.SHORT);
      validateBloodData = false;
      this.setState({ loading: false });
    }

    if (validateBloodData == true) {
      var parameterData1 = {
        test_parameter_id: this.state.bloodParameterId.parameter_systolic_id,
        value: this.state.systolicBloodPressure
      };
      route
        .updateData("self-monitor-save", parameterData1, this.props.myToken)
        .then(async response => {
          if (response.error) {
            console.log(response.error);
            this.props.stopLoading();
            this.setState({ loading: false });
            Alert.alert("Error", JSON.stringify(response.error));
          } else {
            console.log("new value responsee", response);
            console.log("para meter id is a boject or not", this.state.param);
            this.fetchParameterHistoryData(
              this.state.bloodParameterId,
              this.state.parameterName,
              this.state.showChart
            );
            this.setState({ loading: false });
          }
        });

      var parameterData2 = {
        test_parameter_id: this.state.bloodParameterId.parameter_diastolic_id,
        value: this.state.diastolicBloodPressure
      };
      route
        .updateData("self-monitor-save", parameterData2, this.props.myToken)
        .then(async response => {
          if (response.error) {
            console.log(response.error);
            this.props.stopLoading();
            this.setState({ loading: false });
            Alert.alert("Error", JSON.stringify(response.error));
          } else {
            console.log("new value responsee", response);
            this.fetchParameterHistoryData(
              this.state.bloodParameterId,
              this.state.parameterName,
              this.state.showChart
            );
            this.setState({ loading: false });
          }
        });
    }
  }

  deleteUserValue(id) {
    Alert.alert(
      `${I18n.t("delete_user_value")}`,
      `${I18n.t("are_you_sure_to_this_value")}`,
      [
        {
          text: `${I18n.t("cancel")}`,
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => this.deleteValue(id) }
      ],
      { cancelable: false }
    );
  }
  async deleteValue(id) {
    let data = { id: id };
    this.setState({ loading: true });
    route
      .updateData(`delete-self-entry`, data, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log("response rror", response.error);
          this.setState({ loading: false });
        } else {
          console.log("audit delete", response);
          await this.fetchSelfMonitoringParameters();
          // this.setState({loading:false})
        }
      });
  }

  addParameterEntry() {
    console.log("parameter_id", this.state.parameter_id);
    if (this.state.parameter_id === 7) {
      console.log("blood sugar", this.state.parameterNewValue);
      if (
        parseFloat(this.state.parameterNewValue) < 2 ||
        parseFloat(this.state.parameterNewValue) > 25
      ) {
        this.refs.toast.show(I18n.t("blood_suger_error"), DURATION.SHORT);
      } else {
        this.sendDataToApi();
      }
    } else {
      this.sendDataToApi();
    }
  }
  replaceSpaceToUnderScore(string) {
    let lowerCaseString = string.toLowerCase();
    let underScoreString = lowerCaseString.split(" ").join("_");
    let brackitLess = underScoreString.split("(").join("");
    let brackitLess2 = brackitLess.split(")").join("");
    let removeQuestionMark = brackitLess2.split("?").join("");
    let removeQoma = removeQuestionMark.split(",").join("");
    let removeDot = removeQoma.split(".").join("");
    let removeColon = removeDot.split(":").join("");
    let removeSemiColon = removeColon.split(";").join("");
    let singleUnderScore = removeSemiColon.split("__").join("_");
    singleUnderScore = singleUnderScore.split("-").join("_");
    var newString = singleUnderScore;
    console.log("string length", newString.length);

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length;
      newString = singleUnderScore.substring(0, 57);
      console.log("new sting with cut", newString);
    }
    console.log("under score string", newString);
    return newString;
  }
  render() {
    let date = [1, 20, 50, 60];
    var greenValue = 0;
    var orangeValue = 0;
    var redValue = 0;
    var onePrec = 0;
    var yAxisData = [0];
    if (this.state.parameterCartData2 == null) {
      this.state.parameterCartData.map((value, index) => {
        yAxisData.push(value);
      });
    } else {
      this.state.parameterCartData2.parameter_diastolic_blood.parameterCartData.map(
        (value, index) => {
          yAxisData.push(value);
        }
      );

      {
        yAxisData.push(180 + 50);
      }
    }

    console.log("chart data last value", Math.max.apply(null, yAxisData));
    var bloodMax = yAxisData[yAxisData.length - 1];
    var maxValue = 0;
    var minValue = 0;

    console.log("value dif", maxValue);
    if (this.state.monitoringHistoryData.length > 0) {
      if (
        this.state.monitoringHistoryData[0].normal_range != "" &&
        this.state.monitoringHistoryData[0].normal_range != null
      ) {
        var valueRang = this.state.monitoringHistoryData[0].normal_range.split(
          "-"
        );
        yAxisData.push(
          this.state.parameterCartData[
            this.state.parameterCartData.length - 1
          ] + parseFloat(valueRang[1])
        );
        maxValue = Math.max.apply(null, yAxisData);
        minValue = Math.min.apply(null, yAxisData);

        onePrec = maxValue / 100;
        console.log("one Prec", onePrec);
        console.log("value rangc", valueRang);
        console.log("y axis data", yAxisData);
        console.log("min value dif", parseFloat(valueRang[0]) - minValue);
        greenValue = (parseFloat(valueRang[0]) - minValue) / onePrec;
        console.log("green value", greenValue);
        console.log(
          "min orange value dif",
          parseFloat(valueRang[1]) - parseFloat(valueRang[0])
        );
        orangeValue =
          (parseFloat(valueRang[1]) - parseFloat(valueRang[0])) / onePrec;
        console.log("orange value", orangeValue);
        redValue = (parseFloat(maxValue) - parseFloat(valueRang[1])) / onePrec;
        console.log("red value", redValue);
      }
    }
    return (
      <View style={styles.container}>
        <Header
          label={I18n.t("self_monitoring")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: "red" }}
          position="top"
          positionValue={80}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{ color: "white" }}
        />
        <View style={{ flex: 1, backgroundColor: "white" }}>
          <View>
            <FlatList
              style={{ paddingVertical: 10 }}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              data={this.state.monitoringParameters}
              horizontal={true}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  style={{ paddingHorizontal: 10 }}
                  onPress={() =>
                    this.fetchParameterHistoryData(
                      item.test_parameter_id,
                      item.title
                    )
                  }
                >
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: "400",
                      color:
                        this.state.parameterName == item.title
                          ? color.primary
                          : "#696969"
                    }}
                  >
                    {I18n.t(this.replaceSpaceToUnderScore(item.title))}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <View style={styles.dotView}>
            {this.state.monitoringParameters.map((item, index) => {
              return (
                <TouchableOpacity
                  onPress={() =>
                    this.fetchParameterHistoryData(
                      item.test_parameter_id,
                      item.title
                    )
                  }
                  style={{
                    marginHorizontal: 7,
                    width: 14,
                    height: 14,
                    borderRadius: 7,
                    backgroundColor:
                      this.state.parameterName == item.title
                        ? color.primary
                        : "#696969"
                  }}
                />
              );
            })}
          </View>
          <ScrollView>
            {console.log(
              "mulitpul blod values hre",
              this.state.parameterCartData2
            )}
            {this.state.parameterCartData2 == null ? (
              <View>
                <View>
                  {this.state.monitoringHistoryData.length > 0 &&
                  this.state.monitoringHistoryData[0].normal_range === null ? (
                    <View>
                      <View
                        style={{
                          height: 200,
                          width: "88%",
                          alignSelf: "center",
                          marginVertical: 10,
                          marginRight: 20,
                          flexDirection: "row"
                        }}
                      >
                        {console.log(
                          "parameter of normal null",
                          this.state.parameterCartData
                        )}
                        {/* <View
                          style={{
                            width: "92%",
                            left: "10%",
                            height: "85%",
                            position: "absolute",
                            top: "7%"
                          }}
                        >
                          <View
                            style={{
                              width: "100%",
                              height: `${redValue}%`,
                              backgroundColor: "#CB0C12",
                              opacity: 0.4
                            }}
                          />
                          <View
                            style={{
                              width: "100%",
                              height: `${orangeValue}%`,
                              backgroundColor: "#F2A700",
                              opacity: 0.4
                            }}
                          />
                          <View
                            style={{
                              width: "100%",
                              height: `${greenValue}%`,
                              backgroundColor: "#87D400",
                              opacity: 0.4
                            }}
                          />
                        </View> */}
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <YAxis
                            // style={{ width: '85%', alignSelf: 'center', }}

                            data={yAxisData}
                            contentInset={{ top: 20, bottom: 20 }}
                            numberOfTicks={10}
                            formatLabel={value => `${value}`}
                          />
                        </View>

                        <View style={{ flex: 9 }}>
                          <LineChart
                            yMin={0}
                            yMax={Math.max.apply(null, yAxisData)}
                            style={{
                              height: 200,
                              width: "95%",
                              marginLeft: 10
                            }}
                            data={this.state.parameterCartData}
                            svg={{ stroke: color.primary }}
                            contentInset={{ top: 20, bottom: 20 }}
                          >
                            <Grid />
                          </LineChart>
                          {this.state.parameterCartDate.length <= 5 && (
                            <XAxis
                              data={this.state.parameterCartDate}
                              scale={d3Scale.scaleBand}
                              xAccessor={({ item }) => item}
                              formatLabel={(value, index) => {
                                console.log(value);
                                return this.changeDateFormatWithYear(value);
                              }}
                              contentInset={{ left: -20, right: -20 }}
                              svg={{ fontSize: 10, fill: "black" }}
                            />
                          )}
                        </View>
                      </View>
                    </View>
                  ) : (
                    <View>
                      <View
                        style={{
                          height: 200,
                          width: "88%",
                          alignSelf: "center",
                          marginVertical: 10,
                          marginRight: 20,
                          flexDirection: "row"
                        }}
                      >
                        {/* <View
                          style={{
                            width: "92%",
                            left: "10%",
                            height: "85%",
                            position: "absolute",
                            top: "7%"
                          }}
                        >
                          <View
                            style={{
                              width: "100%",
                              height: `${redValue}%`,
                              backgroundColor: "#CB0C12"
                              // opacity: 0.4
                            }}
                          />
                          <View
                            style={{
                              width: "100%",
                              height: `${orangeValue}%`,
                              backgroundColor: "#F2A700"
                              // opacity: 0.4
                            }}
                          />
                          <View
                            style={{
                              width: "100%",
                              height: `${greenValue}%`,
                              backgroundColor: "#87D400"
                              // opacity: 0.4
                            }}
                          />
                        </View> */}

                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <YAxis
                            // style={{ width: '85%', alignSelf: 'center', }}

                            data={yAxisData}
                            contentInset={{ top: 20, bottom: 20 }}
                            numberOfTicks={10}
                            formatLabel={value => `${value}`}
                          />
                        </View>

                        <View style={{ flex: 9 }}>
                          <LineChart
                            yMin={0}
                            yMax={maxValue}
                            xMin={0}
                            style={{
                              height: 200,
                              width: "95%",
                              marginLeft: 10
                            }}
                            data={this.state.parameterCartData}
                            svg={{ stroke: color.primary }}
                            contentInset={{ top: 20, bottom: 20 }}
                          >
                            <Grid />
                          </LineChart>
                          {this.state.parameterCartDate.length <= 5 && (
                            <XAxis
                              data={this.state.parameterCartDate}
                              scale={d3Scale.scaleBand}
                              xAccessor={({ item }) => item}
                              formatLabel={(value, index) => {
                                console.log(value);
                                return this.changeDateFormatWithYear(value);
                              }}
                              contentInset={{ left: -20, right: -20 }}
                              svg={{ fontSize: 10, fill: "black" }}
                            />
                          )}
                        </View>
                      </View>
                    </View>
                  )}
                </View>
                <View
                  style={{
                    width: "80%",
                    flexWrap: "wrap",
                    alignSelf: "center"
                  }}
                >
                  {this.state.monitoringHistoryData.length != 0 && (
                    <Text style={styles.title}>
                      {this.state.monitoringHistoryData[0].description}
                    </Text>
                  )}

                  <Text style={styles.title2}>{I18n.t("add_new_entry")} </Text>
                </View>
                <View
                  style={{
                    width: "80%",
                    flexWrap: "wrap",
                    alignSelf: "center"
                  }}
                >
                  <TextInput
                    style={styles.input1}
                    selectionColor={"#dfe6e9"}
                    onChangeText={text =>
                      this.setState({ parameterNewValue: text })
                    }
                    placeholder={this.state.parameterName}
                    autoCapitalize="words"
                    autoCorrect={true}
                    underlineColorAndroid="transparent"
                    //placeholderTextColor="#969696"
                  />
                </View>

                <View
                  style={{
                    width: "80%",
                    flexWrap: "wrap",
                    alignSelf: "center"
                  }}
                >
                  <Button
                    label={I18n.t("submit")}
                    buttonStyle={styles.button}
                    onPress={() => this.addParameterEntry()}
                  />
                </View>
              </View>
            ) : (
              <View>
                <View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      width: "80%",
                      alignSelf: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <View
                        style={{
                          backgroundColor: "blue",
                          width: 10,
                          height: 10,
                          marginRight: 10
                        }}
                      />
                      <View>
                        <Text style={{ fontSize: 8 }}>
                          {I18n.t("diastolic_blood_pressure")}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                      <View
                        style={{
                          backgroundColor: color.primary,
                          width: 10,
                          height: 10,
                          marginRight: 10
                        }}
                      />
                      <View>
                        <Text style={{ fontSize: 8 }}>
                          {I18n.t("systolic_blood_pressure")}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      height: 200,
                      width: "90%",
                      alignSelf: "center",
                      marginVertical: 10,
                      marginRight: 20,
                      flexDirection: "row"
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <YAxis
                        // style={{ width: '85%', alignSelf: 'center', }}

                        data={yAxisData}
                        contentInset={{ top: 20, bottom: 20 }}
                        numberOfTicks={10}
                        formatLabel={value => `${value}`}
                      />
                    </View>

                    <View style={{ flex: 9 }}>
                      <LineChart
                        yMin={0}
                        yMax={bloodMax}
                        style={{ height: 200, width: "95%", marginLeft: 10 }}
                        data={
                          this.state.parameterCartData2.parameter_systolic_blood
                            .parameterCartData
                        }
                        svg={{ stroke: color.primary }}
                        contentInset={{ top: 20, bottom: 20 }}
                      >
                        <Grid />
                      </LineChart>
                      <LineChart
                        yMin={0}
                        yMax={bloodMax}
                        style={{
                          height: 200,
                          width: "95%",
                          position: "absolute",
                          left: 10
                        }}
                        data={
                          this.state.parameterCartData2
                            .parameter_diastolic_blood.parameterCartData
                        }
                        svg={{ stroke: "blue" }}
                        contentInset={{ top: 20, bottom: 20 }}
                      />
                      {this.state.parameterCartData2.parameter_diastolic_blood
                        .parameterCartDate.length < 6 && (
                        <XAxis
                          scale={d3Scale.scaleBand}
                          data={
                            this.state.parameterCartData2
                              .parameter_diastolic_blood.parameterCartDate
                          }
                          xAccessor={({ item }) => item}
                          formatLabel={(value, index) => {
                            console.log(value);
                            return this.changeDateFormatWithYear(value);
                          }}
                          contentInset={{ left: -20, right: -20 }}
                          svg={{ fontSize: 10, fill: "black" }}
                        >
                          <Text
                            style={{
                              color: "transparent",
                              opacity: 0,
                              fontSize: 0
                            }}
                          />
                        </XAxis>
                      )}
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    width: "80%",
                    flexWrap: "wrap",
                    alignSelf: "center"
                  }}
                >
                  {this.state.monitoringHistoryData.length != 0 && (
                    <Text style={styles.title}>
                      {this.state.monitoringHistoryData[0].description}
                    </Text>
                  )}

                  <Text style={styles.title2}>{I18n.t("add_new_entry")} </Text>
                </View>
                <View
                  style={{
                    width: "80%",
                    flexWrap: "wrap",
                    alignSelf: "center"
                  }}
                >
                  <TextInput
                    style={styles.input1}
                    selectionColor={"#dfe6e9"}
                    onChangeText={text =>
                      this.setState({ systolicBloodPressure: text })
                    }
                    placeholder={I18n.t("systolic_blood_pressure")}
                    autoCapitalize="words"
                    autoCorrect={true}
                    underlineColorAndroid="transparent"
                    //placeholderTextColor="#969696"
                  />

                  <TextInput
                    style={styles.input1}
                    selectionColor={"#dfe6e9"}
                    onChangeText={text =>
                      this.setState({ diastolicBloodPressure: text })
                    }
                    placeholder={I18n.t("diastolic_blood_pressure")}
                    autoCapitalize="words"
                    autoCorrect={true}
                    underlineColorAndroid="transparent"
                    //placeholderTextColor="#969696"
                  />
                </View>

                <View
                  style={{
                    width: "80%",
                    flexWrap: "wrap",
                    alignSelf: "center"
                  }}
                >
                  <Button
                    label={I18n.t("submit")}
                    buttonStyle={styles.button}
                    onPress={() => this.addBloodParameterEntry()}
                  />
                </View>
              </View>
            )}

            {this.state.parameterCartData2 != null && (
              <View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    width: "90%",
                    alignSelf: "center",

                    // paddingVertical:10,
                    marginTop: 10,
                    borderBottomWidth: 1,
                    borderBottomColor: "#EEE"
                  }}
                >
                <View style={{ flex: 1, height: "100%" }}>
                    <TouchableOpacity
                      style={{
                        backgroundColor: "#dfe6e9",
                        flex: 1,
                        width: "100%",
                        height: "100%",
                        alignContent: "center",
                        paddingVertical: 10
                      }}
                      onPress={() =>
                        this.setMedicalHistoryDataToDelete(
                          this.state.bloodParameterId.parameter_diastolic_id,
                          "diastolic"
                        )
                      }
                    >
                      <Text
                        style={{
                          color:
                            this.state.diastolicBloodSet == true
                              ? color.primary
                              : "black",
                          alignSelf: "center"
                        }}
                      >
                        {I18n.t("diastolic_blood")}
                      </Text>
                    </TouchableOpacity>
                  </View>
                <View style={{ flex: 1 }}>
                    <TouchableOpacity
                      style={{
                        backgroundColor: "#dfe6e9",
                        flex: 1,
                        width: "100%",
                        height: "100%",
                        alignContent: "center",
                        paddingVertical: 10
                      }}
                      onPress={() =>
                        this.setMedicalHistoryDataToDelete(
                          this.state.bloodParameterId.parameter_systolic_id,
                          "systolic"
                        )
                      }
                    >
                      <Text
                        style={{
                          color:
                            this.state.systolicBloodSet == true
                              ? color.primary
                              : "black",
                          alignSelf: "center"
                        }}
                      >
                        {I18n.t("systolic_blood")}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  

                  
                </View>
              </View>
            )}

            {this.state.reversHistoryData.length > 0
              ? this.state.reversHistoryData.map((item, index) => {
                  return (
                    <TouchableOpacity
                      activeOpacity={0.9}
                      style={styles.fieldView}
                    >
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-start",
                          width: "100%"
                        }}
                      >
                        <Text style={styles.title1}>
                          {this.changeDateFormatWithYear(item.created_at)}
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          width: "100%",
                          alignItems: "center"
                        }}
                      >
                        <View>
                          <Text style={styles.title1}>
                            {item.user_value} {item.measuring_meter}
                          </Text>
                        </View>
                        <TouchableOpacity
                          style={{ paddingLeft: 20 }}
                          onPress={() => this.deleteUserValue(item.id)}
                        >
                          <Text style={styles.titleRemove}>
                            {I18n.t("remove")}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </TouchableOpacity>
                  );
                })
              : null}
          </ScrollView>
        </View>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 15,
    backgroundColor: "white",
    marginBottom: 20
  },
  dotView: {
    paddingVertical: 13,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  titleRemove: {
    fontSize: 15,
    fontWeight: "400",

    color: color.primary,
    letterSpacing: 0.5
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "100%",
    marginVertical: 10,
    backgroundColor: color.primary
  },
  title2: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingVertical: 10,

    color: "#969696"
  },
  title3: {
    fontSize: 22,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingVertical: 10,
    color: "#969696"
  },
  title4: {
    fontSize: 22,
    paddingLeft: 10,
    letterSpacing: 0.5,
    paddingVertical: 10
  },
  title: {
    fontSize: 16,
    color: "#969696",
    fontWeight: "700",
    alignSelf: "center",
    letterSpacing: 0.5
  },
  fieldView: {
    flexDirection: "row",
    width: "91%",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: "white",
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 11,
    paddingVertical: 10,
    marginTop: 16
  }
});

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    questionAnswer: state.userQuestionAnswer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelfMonitoring);

//make this component available to the app
// export default SelfMonitoring;
