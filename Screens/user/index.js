import React, { Component } from "react";
import {
  Text,
  View,
  Alert,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  FlatList,
  ImageBackground,
  AsyncStorage
} from "react-native";
import Overlay from "../../components/Overlay";
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType
} from "react-native-fcm";
import Config from "react-native-config";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import NotificationHeader from "../../components/NotificationHeader";
import ActionButton from "react-native-circular-action-menu";
import Icon from "react-native-vector-icons/Feather";
import testList from "../../components/Dummydata/testList";
import dashBoardImages from "../../components/Dummydata/testdata";
import Route from "../../network/route.js";
import I18n from "../../i18n/i18n";
import Spinner from "react-native-loading-spinner-overlay";
import { NavigationEvents } from "react-navigation";

const route = new Route("http://192.168.100.18:8000/api/");

class UserScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modalVisible: false,
      inboxCount: null,
      dashBoardData: [],
      rightData: [],
      kidenyLevel: 0,
      mentalLevel: 0,
      heartLevel: 0,
      boneLevel: 0,
      liverLevel: 0,
      lungsLevel: 0,
      mentalData: null,
      kidenyData: null,
      heartData: null,
      boneData: null,
      liverData: null,
      lungsData: null,
      imageLink: require("../../asset/images/heartbeat.png")
    };
  }

  async componentDidMount() {
    FCM.subscribeToTopic("latest_update");
    this.notificationLis();
    this.fetchAllChats();
    // this.fetchAllNotification()
    this.fetchDashBoardData();
    // await AsyncStorage.setItem('showUserDiscount', "false");
    this.showFirstDiscount();

    console.log(".env varables", Config.API_URL);
  }

  replaceSpaceToUnderScore(string) {
    console.log("real string", string);
    var string = string.trim();
    var lowerCaseString = string.toLowerCase();
    var underScoreString = lowerCaseString.split(" ").join("_");
    var brackitLess = underScoreString.split("(").join("");
    var brackitLess2 = brackitLess.split(")").join("");
    var removeQuestionMark = brackitLess2.split("?").join("");
    var removeQoma = removeQuestionMark.split(",").join("");
    var removeDot = removeQoma.split(".").join("");
    var removeColon = removeDot.split(":").join("");
    var removeSemiColon = removeColon.split(";").join("");

    var singleUnderScore = removeSemiColon.split("-").join("_");
    singleUnderScore = singleUnderScore.split("__").join("_");
    singleUnderScore = singleUnderScore.split("/").join("_");
    singleUnderScore = singleUnderScore.split("\n").join("_");
    singleUnderScore = singleUnderScore.split("'").join("_");
    singleUnderScore = singleUnderScore.split("%").join("_");
    singleUnderScore = singleUnderScore.split("<").join("_");
    singleUnderScore = singleUnderScore.split(">").join("_");
    var newString = singleUnderScore;

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length;
      newString = singleUnderScore.substring(0, 57);
    }
    console.log("traslated string", newString);
    return newString;
  }

  async notificationLis() {
    try {
      let result = await FCM.requestPermissions({
        badge: false,
        sound: true,
        alert: true
      });
      console.log(result);
    } catch (e) {
      console.error(e);
    }
    FCM.getFCMToken().then(token => {
      console.log("TOKEN (getFCMToken)", token);
    });
    // FCM.subscribeToTopic('allEvents');
    FCM.getInitialNotification().then(notif => {
      console.log("INITIAL NOTIFICATION", notif);
      if (notif.from) {
        // NavigationService.navigate("notifications");
        this.props.navigation.navigate("notifications");
        // console.log("props here",this.props)
      }
    });
    console.log("FCM Event: ", NotificationType);
    FCM.on(FCMEvent.Notification, notif => {
      console.log("App Notification", notif);
      this.props.setNotificationCount(this.props.notificationCount + 1);
      // this.props.addNotification(1);
      if (notif.local_notification) {
        return;
      }

      if (Platform.OS === "ios") {
        //optional
        //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
        //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
        //notif._notificationType is available for iOS platfrom
        switch (notif._notificationType) {
          case NotificationType.Remote:
            notif.finish(RemoteNotificationResult.NewData); //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
            break;
          case NotificationType.NotificationResponse:
            notif.finish();
            break;
          case NotificationType.WillPresent:
            notif.finish(WillPresentNotificationResult.All); //other types available: WillPresentNotificationResult.None
            break;
        }
      }
      this.showLocalNotification(notif);
    });

    this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, token => {
      console.log("TOKEN (refreshUnsubscribe)", token);
    });
  }

  showLocalNotification(notif) {
    FCM.presentLocalNotification({
      title: notif.title,
      body: notif.body,
      priority: "high",
      click_action: notif.click_action,
      show_in_foreground: true,
      local: true
    });
  }

  fetchAllChats() {
    console.log("Message did mount called");
    route
      .getAuthenticated("get-all-chat-msg", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
        } else {
          console.log(response);
          this.props.setInboxCount(response.data.unreadMsg);
          // this.setState({inboxCount:response.data.unreadMsg})
        }
      })
      .catch(e => {
        console.log("catch error", e);
      });
  }

  fetchDashBoardData() {
    this.setState({ loading: true });
    route
      .getAuthenticated("get-dashboard-data", this.props.myToken)
      .then(async response => {
        if (response.error) {
          this.setState({ loading: false });
          console.log(response.error);
        } else {
          var rightData = [];

          response.data.map((value, index) => {
            console.log("data value", value);
            if (value.isBody === 0) {
              rightData.push(value);
            } else {
              if (value.name == "Mental Health/Stress") {
                this.setState({ mentalData: value });
                if (value.riskLevel == 0) {
                  this.setState({ mentalLevel: 0 });
                }
                if (value.riskLevel == 1) {
                  this.setState({ mentalLevel: 1 });
                }
                if (value.riskLevel == 2) {
                  this.setState({ mentalLevel: 2 });
                }
                if (value.riskLevel == null) {
                  this.setState({ mentalLevel: null });
                }
              }

              if (value.name == "Heart") {
                this.setState({ heartData: value });
                if (value.riskLevel == 0) {
                  this.setState({ heartLevel: 0 });
                }
                if (value.riskLevel == 1) {
                  this.setState({ heartLevel: 1 });
                }
                if (value.riskLevel == 2) {
                  this.setState({ heartLevel: 2 });
                }
                if (value.riskLevel == null) {
                  this.setState({ heartLevel: null });
                }
              }

              if (value.name == "Lungs") {
                this.setState({ lungsData: value });
                if (value.riskLevel == 0) {
                  this.setState({ lungsLevel: 0 });
                }
                if (value.riskLevel == 1) {
                  this.setState({ lungsLevel: 1 });
                }
                if (value.riskLevel == 2) {
                  this.setState({ lungsLevel: 2 });
                }
                if (value.riskLevel == null) {
                  this.setState({ lungsLevel: null });
                }
              }

              if (value.name == "Liver") {
                this.setState({ liverData: value });
                if (value.riskLevel == 0) {
                  this.setState({ liverLevel: 0 });
                }
                if (value.riskLevel == 1) {
                  this.setState({ liverLevel: 1 });
                }
                if (value.riskLevel == 2) {
                  this.setState({ liverLevel: 2 });
                }
                if (value.riskLevel == null) {
                  this.setState({ liverLevel: null });
                }
              }

              if (value.name == "Kidney and electrolytes") {
                this.setState({ kidenyData: value });
                if (value.riskLevel == 0) {
                  this.setState({ kidenyLevel: 0 });
                }
                if (value.riskLevel == 1) {
                  this.setState({ kidenyLevel: 1 });
                }
                if (value.riskLevel == 2) {
                  this.setState({ kidenyLevel: 2 });
                }
                if (value.riskLevel == null) {
                  this.setState({ kidenyLevel: null });
                }
              }
              if (value.name == "Bone and joint Health") {
                this.setState({ boneData: value });
                if (value.riskLevel == 0) {
                  this.setState({ boneLevel: 0 });
                }
                if (value.riskLevel == 1) {
                  this.setState({ boneLevel: 1 });
                }
                if (value.riskLevel == 2) {
                  this.setState({ boneLevel: 2 });
                }
                if (value.riskLevel == null) {
                  this.setState({ boneLevel: null });
                }
              }
            }
          });
          this.setState({ rightData: rightData }, () => {
            console.log("right Data", this.state.rightData);
          });
          this.setState({ loading: false });
        }
      });
  }

  async showFirstDiscount() {
    let data = {};
    route
      .updateData("check-first-order-discount", data, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.props.stopLoading();
          this.setState({ loading: false });
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          console.log("user discount", response);

          const firstDiscountValue = await AsyncStorage.getItem(
            "showUserDiscount"
          );
          console.log("first discount value", firstDiscountValue);
          // if(firstDiscountValue == null){

          //     if(response.message != "not applicable for first order discount"){
          //         // alert(response.message + "\n" + response.discount)
          //         await AsyncStorage.setItem('showUserDiscount', "true");
          //         Alert.alert(
          //             `${I18n.t('welcome')}!`,
          //             `${I18n.t('as_you_are_new_user')} ${response.discount} ${I18n.t('check_out_discount')}`,
          //             [
          //               {text: 'OK', onPress: () => console.log('OK Pressed')},
          //             ],
          //             { cancelable: false }
          //           )
          //     }
          // }
        }
      });
  }

  fetchAllNotification() {
    console.log("all notifications");
    route
      .getAuthenticated("get-all-notification", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
        } else {
          console.log(response);
          var notificationLength =
            response.notification.newUnreadMsgs.length +
            response.notification.appointmentIn48Hour.length;
          this.props.setNotificationCount(notificationLength);
        }
      });
  }
  openTab(props) {
    if (props == "tests") {
      this.setState({ modalVisible: !this.state.modalVisible });
      this.props.navigation.navigate("Tests");
    } else if (props == "monitoring") {
      this.setState({ modalVisible: !this.state.modalVisible });
      this.props.navigation.navigate("selfMonitoring");
    } else if (props == "uploadresults") {
      this.setState({ modalVisible: !this.state.modalVisible });
      this.props.navigation.navigate("myuploads");
    }
  }

  riskColor(riskLevel) {
    if (riskLevel == 0) {
      return "#5dd31d";
    }
    if (riskLevel == 1) {
      return "#ffa21e";
    }
    if (riskLevel == 2) {
      return "#e3001c";
    }
    return null;
  }
  riskLevelImage(dashBoardImage, riskLevel) {
    if (riskLevel == 0) {
      return dashBoardImage.image_green;
    }
    if (riskLevel == 1) {
      return dashBoardImage.image_yellow;
    }
    if (riskLevel == 2) {
      return dashBoardImage.image_red;
    }
    return dashBoardImage.image;
  }

  leftTestClick(id, name, path, riskLevel) {
    console.log("risk level ", riskLevel);
    this.props.navigation.navigate("testResults", {
      medical_test_id: id,
      resultName: name,
      imagePath: path
    });
  }

  render() {
    console.log("language selected", this.props.selectedLanguage);
    return (
      <View style={{ flex: 1 }}>
        <NavigationEvents onDidFocus={() => this.fetchDashBoardData()} />
        {/* <PushNotificationController /> */}
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <NotificationHeader
          label={I18n.t("vita")}
          inboxCount={this.props.inboxCount}
          notificationCount={this.props.notificationCount}
          onMailPress={() => this.props.navigation.navigate("inbox")}
          onNotificationPress={() =>
            this.props.navigation.navigate("notifications")
          }
        />
        <View
          style={{ flex: 1, flexDirection: "row", backgroundColor: "white" }}
        >
          <Overlay
            visible={this.state.modalVisible}
            animationType="zoomIn"
            closeOnTouchOutside={true}
            onClose={() => this.setState({ modalVisible: false })}
            containerStyle={{ backgroundColor: "rgba(0, 0, 0, 0.68)" }}
            childrenWrapperStyle={{
              backgroundColor: "transparent",
              width: "100%"
            }}
            animationDuration={400}
          >
            <TouchableOpacity
              onPress={() => this.openTab("tests")}
              style={{
                alignSelf: "center",
                paddingVertical: 10,
                flexDirection: "row",
                alignItems: "center",
                top: -10,
                right: -10
              }}
            >
              <Text style={styles.subTitle}>{I18n.t("browse_test")}</Text>
              <View style={styles.iconInner}>
                <Icon name="life-buoy" color="white" size={23} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.openTab("monitoring")}
              style={{
                alignSelf: "center",
                paddingVertical: 10,
                flexDirection: "row",
                alignItems: "center",
                top: -10,
                right: 30
              }}
            >
              <Text style={styles.subTitle}>{I18n.t("self_monitoring")}</Text>
              <View style={styles.iconInner}>
                <Icon name="calendar" color="white" size={23} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.openTab("uploadresults")}
              style={{
                alignSelf: "center",
                paddingVertical: 10,
                flexDirection: "row",
                alignItems: "center",
                top: -10,
                right: 0
              }}
            >
              <Text style={styles.subTitle}>{I18n.t("upload_results")}</Text>
              <View style={styles.iconInner}>
                <Icon name="camera" color="white" size={23} />
              </View>
            </TouchableOpacity>
          </Overlay>
          <View style={{ width: "63%", height: "100%" }}>
            <View
              style={{
                width: "100%",
                paddingVertical: 10,
                alignItems: "center",
                flexWrap: "wrap",
                alignSelf: "center"
              }}
            >
              <Text style={styles.title}>{I18n.t("your_health_summary")}</Text>
            </View>
            <View style={{ flex: 1, flexWrap: "wrap" }}>
              <View
                style={{
                  position: "absolute",
                  bottom: 0,
                  left: 0,
                  width: 230,
                  height: "100%"
                }}
              >
                {/* <Image source={require('../../asset/images/human.png')} style={{ bottom: 0, left: 0, width: '100%', height: '100%' }} resizeMode='stretch' /> */}
                <Image
                  source={require("../../asset/images/body.png")}
                  style={{
                    position: "absolute",
                    bottom: 0,
                    left: 0,
                    width: "100%",
                    height: "100%"
                  }}
                  resizeMode="stretch"
                />
                {console.log("mental image", this.state.mentalImage)}

                {this.state.mentalLevel == null && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.mentalData.id,
                        resultName: this.state.mentalData.name,
                        imagePath: require("../../asset/images/mental_grey.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 70,
                      top: "1.5%",
                      left: "9%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/mental_grey.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: this.props.selectedLanguage != "bm" ? 9 : 7,
                          alignSelf: "center",
                          color: "white",
                          marginTop: 7
                        }}
                      >
                        {I18n.t("mental")}
                      </Text>
                      <Text
                        style={{
                          fontSize: this.props.selectedLanguage != "bm" ? 9 : 7,
                          alignSelf: "center",
                          color: "white"
                        }}
                      >
                        {I18n.t("health_stress")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.mentalLevel == 0 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.mentalData.id,
                        resultName: this.state.mentalData.name,
                        imagePath: require("../../asset/images/mental_green.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 70,
                      top: "1.5%",
                      left: "9%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/mental_green.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: this.props.selectedLanguage != "bm" ? 9 : 7,
                          alignSelf: "center",
                          color: "white",
                          marginTop: 7
                        }}
                      >
                        {I18n.t("mental")}
                      </Text>
                      <Text
                        style={{
                          fontSize: this.props.selectedLanguage != "bm" ? 9 : 7,
                          alignSelf: "center",
                          color: "white"
                        }}
                      >
                        {I18n.t("health_stress")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.mentalLevel == 1 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.mentalData.id,
                        resultName: this.state.mentalData.name,
                        imagePath: require("../../asset/images/mental_orange.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 70,
                      top: "1.5%",
                      left: "9%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/mental_orange.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: this.props.selectedLanguage != "bm" ? 9 : 7,
                          alignSelf: "center",
                          color: "white",
                          marginTop: 7
                        }}
                      >
                        {I18n.t("mental")}
                      </Text>
                      <Text
                        style={{
                          fontSize: this.props.selectedLanguage != "bm" ? 9 : 7,
                          alignSelf: "center",
                          color: "white"
                        }}
                      >
                        {I18n.t("health_stress")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}
                {this.state.mentalLevel == 2 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.mentalData.id,
                        resultName: this.state.mentalData.name,
                        imagePath: require("../../asset/images/mental_red.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 70,
                      top: "2%",
                      left: "9%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/mental_red.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: this.props.selectedLanguage != "bm" ? 9 : 7,
                          alignSelf: "center",
                          color: "white",
                          marginTop: 7
                        }}
                      >
                        {I18n.t("mental")}
                      </Text>
                      <Text
                        style={{
                          fontSize: 9,
                          alignSelf: "center",
                          color: "white"
                        }}
                      >
                        {I18n.t("healt_stress")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.lungsLevel == null && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.lungsData.id,
                        resultName: this.state.lungsData.name,
                        imagePath: require("../../asset/images/lungs_grey.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 108,
                      width: 115,
                      top: "27%",
                      left: 0
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/lungs_grey.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          bottom: 15,
                          position: "absolute",
                          left: 10
                        }}
                      >
                        {I18n.t("lungs")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.lungsLevel == 0 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.lungsData.id,
                        resultName: this.state.lungsData.name,
                        imagePath: require("../../asset/images/lungs_green.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 108,
                      width: 115,
                      top: "27%",
                      left: 0
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/lungs_green.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          bottom: 15,
                          position: "absolute",
                          left: 10
                        }}
                      >
                        {I18n.t("lungs")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.lungsLevel == 1 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.lungsData.id,
                        resultName: this.state.lungsData.name,
                        imagePath: require("../../asset/images/lungs_orange.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 108,
                      width: 115,
                      top: "27%",
                      left: 0
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/lungs_orange.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          bottom: 15,
                          position: "absolute",
                          left: 10
                        }}
                      >
                        {I18n.t("lungs")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.lungsLevel == 2 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.lungsData.id,
                        resultName: this.state.lungsData.name,
                        imagePath: require("../../asset/images/lungs_red.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 108,
                      width: 115,
                      top: "27%",
                      left: 0
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/lungs_red.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          bottom: 15,
                          position: "absolute",
                          left: 10
                        }}
                      >
                        {I18n.t("lungs")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.heartLevel == null && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.heartData.id,
                        resultName: this.state.heartData.name,
                        imagePath: require("../../asset/images/heart_grey.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 40,
                      top: "33.5%",
                      left: "18%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/heart_grey.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 20,
                          position: "absolute",
                          left: 7
                        }}
                      >
                        {I18n.t("heart")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.heartLevel == 0 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.heartData.id,
                        resultName: this.state.heartData.name,
                        imagePath: require("../../asset/images/heart_green.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 40,
                      top: "33.5%",
                      left: "18%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/heart_green.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 20,
                          position: "absolute",
                          left: 7
                        }}
                      >
                        {I18n.t("heart")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.heartLevel == 1 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.heartData.id,
                        resultName: this.state.heartData.name,
                        imagePath: require("../../asset/images/heart_orange.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 40,
                      top: "33.5%",
                      left: "18%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/heart_orange.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 20,
                          position: "absolute",
                          left: 7
                        }}
                      >
                        {I18n.t("heart")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.heartLevel == 2 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.heartData.id,
                        resultName: this.state.heartData.name,
                        imagePath: require("../../asset/images/heart_red.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 40,
                      top: "33.5%",
                      left: "18%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/heart_red.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 20,
                          position: "absolute",
                          left: 7
                        }}
                      >
                        {I18n.t("heart")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.boneLevel == null && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.boneData.id,
                        resultName: this.state.boneData.name,
                        imagePath: require("../../asset/images/bone_grey.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 112,
                      width: 50,
                      top: "31%",
                      left: "62%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/bone_grey.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "grey",
                          top: -12,
                          position: "absolute",
                          left: -15
                        }}
                      >
                        {I18n.t("bone")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.boneLevel == 0 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.boneData.id,
                        resultName: this.state.boneData.name,
                        imagePath: require("../../asset/images/bone_green.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 112,
                      width: 50,
                      top: "31%",
                      left: "62%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/bone_green.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "grey",
                          top: -12,
                          position: "absolute",
                          left: -15
                        }}
                      >
                        {I18n.t("bone")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}
                {this.state.boneLevel == 1 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.boneData.id,
                        resultName: this.state.boneData.name,
                        imagePath: require("../../asset/images/bone_orange.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 112,
                      width: 50,
                      top: "31%",
                      left: "62%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/bone_orange.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "grey",
                          top: -12,
                          position: "absolute",
                          left: -15
                        }}
                      >
                        {I18n.t("bone")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}
                {this.state.boneLevel == 2 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.boneData.id,
                        resultName: this.state.boneData.name,
                        imagePath: require("../../asset/images/bone_red.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 112,
                      width: 50,
                      top: "31%",
                      left: "62%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/bone_red.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "grey",
                          top: -12,
                          position: "absolute",
                          left: -15
                        }}
                      >
                        {I18n.t("bone")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.liverLevel == null && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.liverData.id,
                        resultName: this.state.liverData.name,
                        imagePath: require("../../asset/images/liver_grey.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 85,
                      top: "50%",
                      left: 2
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/liver_grey.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 10,
                          position: "absolute",
                          left: 15
                        }}
                      >
                        {I18n.t("liver")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.liverLevel == 0 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.liverData.id,
                        resultName: this.state.liverData.name,
                        imagePath: require("../../asset/images/liver_green.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 85,
                      top: "50%",
                      left: 2
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/liver_green.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 10,
                          position: "absolute",
                          left: 15
                        }}
                      >
                        {I18n.t("liver")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.liverLevel == 1 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.liverData.id,
                        resultName: this.state.liverData.name,
                        imagePath: require("../../asset/images/liver_orange.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 85,
                      top: "50%",
                      left: 2
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/liver_orange.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 10,
                          position: "absolute",
                          left: 15
                        }}
                      >
                        {I18n.t("liver")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.liverLevel == 2 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.liverData.id,
                        resultName: this.state.liverData.name,
                        imagePath: require("../../asset/images/liver_red.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 50,
                      width: 85,
                      top: "50%",
                      left: 2
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/liver_red.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 10,
                          position: "absolute",
                          left: 15
                        }}
                      >
                        {I18n.t("liver")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.kidenyLevel == null && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.kidenyData.id,
                        resultName: this.state.kidenyData.name,
                        imagePath: require("../../asset/images/kidneys_grey.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 55,
                      width: 45,
                      top: "56%",
                      left: "25%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/kidneys_grey.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 8,
                          position: "absolute",
                          left: 7
                        }}
                      >
                        {I18n.t("kidneys")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.kidenyLevel == 0 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.kidenyData.id,
                        resultName: this.state.kidenyData.name,
                        imagePath: require("../../asset/images/kidneys_green.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 55,
                      width: 45,
                      top: "56%",
                      left: "25%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/kidneys_green.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 8,
                          position: "absolute",
                          left: 7
                        }}
                      >
                        {I18n.t("kidneys")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.kidenyLevel == 1 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.kidenyData.id,
                        resultName: this.state.kidenyData.name,
                        imagePath: require("../../asset/images/kidneys_orange.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 55,
                      width: 45,
                      top: "56%",
                      left: "25%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/kidneys_orange.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 8,
                          position: "absolute",
                          left: 7
                        }}
                      >
                        {I18n.t("kidneys")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                {this.state.kidenyLevel == 2 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("testResults", {
                        medical_test_id: this.state.kidenyData.id,
                        resultName: this.state.kidenyData.name,
                        imagePath: require("../../asset/images/kidneys_red.png")
                      })
                    }
                    style={{
                      position: "absolute",
                      height: 55,
                      width: 45,
                      top: "56%",
                      left: "25%"
                    }}
                  >
                    <ImageBackground
                      source={require("../../asset/images/kidneys_red.png")}
                      style={{ width: "100%", height: "100%" }}
                      resizeMode="stretch"
                    >
                      <Text
                        style={{
                          fontSize: 9,
                          fontWeight: "bold",
                          alignSelf: "center",
                          color: "white",
                          top: 8,
                          position: "absolute",
                          left: 7
                        }}
                      >
                        {I18n.t("kidneys")}
                      </Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}

                <ImageBackground
                  // source={require("../../asset/images/optimal.png")}
                  style={{
                    position: "absolute",
                    backgroundColor:"#5dd31d",
                    height: 9,
                    width: 100,
                    top: "71%",
                    left: 0
                  }}
                  resizeMode="stretch"
                >
                  <Text style={{fontSize:7,color:"white",marginLeft:5}}>{I18n.t('optimal')}</Text>
                </ImageBackground>
                <ImageBackground
                  // source={require("../../asset/images/monitoring.png")}
                  style={{
                    position: "absolute",
                    backgroundColor:"#ffa21e",
                    height: 9,
                    width: 130,
                    top: "73.1%",
                    left: 0
                  }}
                  resizeMode="stretch"
                >
                  <Text style={{fontSize:7,color:"white",marginLeft:5}}>{I18n.t('monitoring_advised')}</Text>
                </ImageBackground>
                <ImageBackground
                  // source={require("../../asset/images/priority.png")}
                  style={{
                    position: "absolute",
                    backgroundColor:"#e3001c",
                    height: 9,
                    width: 145,
                    top: "75.1%",
                    left: 0
                  }}
                  resizeMode="stretch"
                >
                  <Text style={{fontSize:7,color:"white",marginLeft:5}}>{I18n.t('priority_close_monitoring')}</Text>
                </ImageBackground>
              </View>
            </View>
          </View>
          <View style={{ width: "37%", paddingVertical: 30, height: "100%" }}>
            <TouchableOpacity
              style={styles.iconTop}
              onPress={() => this.setState({ modalVisible: true })}
            >
              {/* <Icon name='plus' color='white' size={24} /> */}
              <Text
                style={{
                  textAlign: "center",
                  alignSelf: "center",
                  color: "white",
                  fontSize: this.props.selectedLanguage != "bm" ? 14 : 8,
                  
                }}
              >
                {I18n.t("add_new")}
              </Text>
            </TouchableOpacity>
            <ScrollView>
              <View
                style={{
                  alignItems: "center",
                  paddingHorizontal: 6,
                  height: "100%",
                  justifyContent: "space-evenly",
                  paddingVertical: 30
                }}
              >
                {this.state.rightData.length > 0
                  ? this.state.rightData.map((item, index) => {
                      let imageIndex = dashBoardImages.findIndex(
                        (value, index) => value.name == item.icone
                      );
                      let path = this.riskLevelImage(
                        dashBoardImages[imageIndex],
                        item.riskLevel
                      );
                      return (
                        <TouchableOpacity
                          key={index}
                          style={[styles.listItem,{paddingVertical:this.props.selectedLanguage == "ms" ? 6 : 2}]}
                          onPress={() =>
                            this.leftTestClick(
                              item.id,
                              item.name,
                              path,
                              item.riskLevel
                            )
                          }
                        >
                          <Image
                            style={{ width: 29, height: 29 }}
                            resizeMode="contain"
                            source={path}
                          />

                          <View style={{ flex: 1, flexWrap: "wrap" }}>
                            <Text
                              style={[
                                styles.listText,
                                {
                                  color: this.riskColor(item.riskLevel),
                                  
                                }
                              ]}
                            >
                              {I18n.t(this.replaceSpaceToUnderScore(item.name))}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })
                  : null}
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    paddingLeft: 10,
    fontSize: 25,
    fontWeight: "400",
    letterSpacing: 0.5,

    color: "#969696"
  },
  header: {
    height: 52,

    alignItems: "center",
    //marginTop: Platform.OS == "ios" ? 20 : 0,
    flexDirection: "row",
    width: "100%",

    backgroundColor: "transparent"
  },
  listItem: {
    flexDirection: "row",
    flexWrap: "wrap",
    
    alignItems: "center",
    justifyContent: "center"
  },
  listText: {
    fontSize: 14,
    paddingLeft: 5,
    paddingVertical: 5,
    color: "#969696"
  },
  subTitle: {
    color: "white",
    fontSize: 14,
    paddingHorizontal: 10
  },
  iconTop: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginHorizontal: 2,
    backgroundColor: color.primary,
    borderColor: "transparent",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  iconInner: {
    width: 40,

    height: 40,
    borderRadius: 20,
    backgroundColor: color.primary,
    borderColor: "transparent",
    marginRight: 10,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center"
  }
});

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    selectedLanguage: state.user.selectedLanguage,
    inboxCount: state.user.inboxCount,
    notificationCount: state.user.notificationCount,
    firstTimeDiscount: state.user.firstTimeDiscount
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserScreen);
