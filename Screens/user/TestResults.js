import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  Platform,
  WebView,
  PermissionsAndroid
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Feather";
import Header from "../../components/Header";
import Spinner from "react-native-loading-spinner-overlay";
import Route from "../../network/route.js";
import moment from "moment";
import RNFetchBlob from "rn-fetch-blob";
import FileViewer from "react-native-file-viewer";
import RNFS from "react-native-fs";
import HTML from "react-native-render-html";
import AutoResizeHeightWebView from "react-native-autoreheight-webview";
const route = new Route("http://192.168.100.18:8000/api/");

const htmlStyle = `<style>
                    *{
                      padding:0px;
                      margin:0px;
                      font-size:13px !important;
                    }
                    p{
                      font-size:13px;
                      padding:0 !important;
                      margin:0 !important;
                      color:#969696;
                      font-weight:bold;
                    }
                  </style>`;
                  const htmlStyle2 = `<style>
                    *{
                      padding:0px;
                      margin:0px;
                      font-size:18px !important;
                    }
                    p{
                      font-size:18px;
                      padding:0 !important;
                      margin:0 !important;
                      color:#969696;
                      font-weight:bold;
                    }
                  </style>`;
class TestResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usertest: {},
      testData: [],
      loading: false,
      testResultName: "",
      imagePath: "",
      indicatorPosition: "100%",
      medical_test_id: ""
    };
  }

  replaceSpaceToUnderScore(string) {
    console.log("real string", string);
    var string = string.trim();
    var lowerCaseString = string.toLowerCase();
    var underScoreString = lowerCaseString.split(" ").join("_");
    var brackitLess = underScoreString.split("(").join("");
    var brackitLess2 = brackitLess.split(")").join("");
    var removeQuestionMark = brackitLess2.split("?").join("");
    var removeQoma = removeQuestionMark.split(",").join("");
    var removeDot = removeQoma.split(".").join("");
    var removeColon = removeDot.split(":").join("");
    var removeSemiColon = removeColon.split(";").join("");

    var singleUnderScore = removeSemiColon.split("-").join("_");
    singleUnderScore = singleUnderScore.split("__").join("_");
    singleUnderScore = singleUnderScore.split("/").join("_");
    singleUnderScore = singleUnderScore.split("\n").join("_");
    singleUnderScore = singleUnderScore.split("'").join("_");
    singleUnderScore = singleUnderScore.split("%").join("_");
    singleUnderScore = singleUnderScore.split("<").join("_");
    singleUnderScore = singleUnderScore.split(">").join("_");
    var newString = singleUnderScore;

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length;
      newString = singleUnderScore.substring(0, 57);
    }
    console.log("traslated string", newString);
    return newString;
  }

  componentDidMount() {
    var string =
      "Reduce salt intake to 2g per day. Reduce consumption of fat in diet. Increase consumption of fruit and vegetables. Aerobic exercise of 30mins per day.Target BMI to be below 25";
    this.replaceSpaceToUnderScore(string);
    const { params } = this.props.navigation.state;
    console.log("params", params);
    this.setState({
      testResultName: params.resultName,
      imagePath: params.imagePath,
      medical_test_id: params.medical_test_id
    });
    this.fetchTestResult(params.medical_test_id);
  }

  fetchTestResult(medical_test_id) {
    this.setState({ loading: true });
    data = { medical_test_id: medical_test_id };
    route
      .updateData(`get-medicaltest-analysis`, data, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log("response rror", response.error);
          this.setState({ loading: false });
        } else {
          console.log("user test result", response);
          this.setState({
            usertest: response.apiData,
            testData: response.apiData.testData
          });
          this.setState({ loading: false });
        }
      });
  }
  changeDateFormat(date) {
    if (date != "" && date != null) {
      var newDate = moment(date).format("MMM, DD YYYY");
      return newDate + " - " + this.state.usertest.clinic;
    }
    return "";
  }

  async downloadPdf() {
    if (Platform.OS == "ios") {
      this.fetchPdf();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "Vita App Storage Permission",
            message: "Vita App needs access to your Storage ",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the storage");
          this.fetchPdf();
        } else {
          console.log("Storage permission denied");
          alert("you can not donwload pdf write now");
        }
      } catch (err) {
        console.warn(err);
      }
    }
  }

  async fetchPdf() {
    // await this.requestStoragePermission()
    //    this.setState({ loading: true });
    var now = new Date();
    let dirs = "";
    if (Platform.OS == "android") {
      dirs = RNFetchBlob.fs.dirs.DownloadDir;
    } else {
      dirs = RNFetchBlob.fs.dirs.DocumentDir;
    }

    this.setState({ loading: true });
    route
      .getAuthenticated(
        "get-medicaltest-analysis-pdf?medical_test_id=" +
          this.state.medical_test_id,
        this.props.myToken
      )
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.props.stopLoading();
          this.setState({ loading: false });
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          console.log("PDF url", response);
          this.setState({ loading: false });

          console.log("dirs", RNFetchBlob.fs);
          RNFetchBlob.config({
            // add this option that makes response data to be stored as a file,
            // this is much more performant.
            fileCache: false,
            useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
            notification: true,
            path: dirs + "/MedicalTestAnalysis" + now + ".pdf",
            IOSBackgroundTask: true,
            indicator: true,
            addAndroidDownloads: {
              useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
              notification: true,
              path: dirs + "/MedicalTestAnalysis" + now + ".pdf", // this is the path where your downloaded file will live in
              description: "Downloading file."
            }
          })
            .fetch("GET", response.pdf.pdf_url, {})
            .then(res => {
              // the temp file path
              this.setState({ loading: false });
              console.log("The file saved to ", res);
              console.log("doument directory path", RNFS.DocumentDirectoryPath);
              FileViewer.open(res.data);
              // RNFetchBlob.openDocument(res.path());
            })
            .catch((errorMessage, statusCode) => {
              this.setState({ loading: false });
              // alert(errorMessage);
            });
        }
      });
  }

  selectRangeColor(range) {
    if (range.risk_level == 0) {
      return "#87D400";
    } else if (range.risk_level == 1) {
      return "#F2A700";
    } else if (range.risk_level == 2) {
      return "#CB0C12";
    }
  }
   reverseArray(arr) {
    var newArray = [];
    for (var i = arr.length - 1; i >= 0; i--) {
      newArray.push(arr[i]);
    }
    return newArray;
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          label={I18n.t("results")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />

        <ScrollView keyboardShouldPersistTaps={"always"}>
          <View style={styles.header}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image
                source={this.state.imagePath}
                style={{ width: 39, height: 39 }}
                resizeMode="contain"
              />
              <View
                style={{
                  flexWrap: "wrap",
                  display: "flex",
                  paddingHorizontal: 4,
                  flex: 1
                }}
              >
                <Text style={styles.title}>
                  {I18n.t(
                    this.replaceSpaceToUnderScore(this.state.testResultName)
                  )}{" "}
                </Text>
                {this.state.usertest.testdate != "" && (
                  <Text style={styles.title2}>
                    {this.changeDateFormat(this.state.usertest.testDate)}
                  </Text>
                )}
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() => this.downloadPdf()}
              >
                <Icon name="arrow-down" color="white" size={23} />
                <Text style={{ color: "white" }}>PDF</Text>
              </TouchableOpacity>
            </View>
          </View>
          {console.log("test result data", this.state.testData)}
          {this.state.testData.length > 0 ? (
		  
			
            this.state.testData.map((value, index) => {
			if(value.measuring_meter == null)
				value.measuring_meter = "";
			
              if (value.normal_range == "" || value.normal_range == null) {
                return (
                  <View>
                    <View style={styles.custom2}>
                      <View>
                        <Text style={styles.title6}>{value.title} :</Text>
                      </View>

                      <View>
                        <Text style={styles.title4}>{value.user_value}</Text>
                      </View>
                      <WebView
                        style={{
                          width: 150,
                          height: 50,
                          marginTop: 12,
                          marginLeft:5,   
                          backgroundColor: "transparent"
                        }}
                        automaticallyAdjustContentInsets={true}
                        javaScriptEnabled={false}
                        scalesPageToFit={false}
                        scrollEnabled={false}
                        source={{ html: htmlStyle2 + value.measuring_meter }}
                      />
                    </View>
                  </View>
                );
              } else {
				try{
					minValue = parseFloat(value.range[0].min);
				}
				catch(e){
					minValue = 0;
				}
				
				try{
					maxValue = parseFloat(value.range[value.range.length - 1].max);
				}
				catch(e){
					maxValue = 0;
				}
                
                totalValue = maxValue - minValue;
                onePrec = totalValue / 100;
                userValue = value.user_value - minValue;
                var indecotorPosition = 0;
				
				try{
					indecotorPosition = parseFloat(userValue) / onePrec;
				}
				catch(e){
					indecotorPosition = 0;
				}
				
                if (indecotorPosition < 0) {
                  indecotorPosition = 0;
                }
                if (indecotorPosition > 100) {
                  indecotorPosition = 100;
                }
                return (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("history", {
                        test_parameter_id: value.test_parameter_id,
                        testName: value.title,
                        parameterRange:this.reverseArray(value.range),
                        parameterValue:value
                      })
                    }
                    style={styles.custom}
                  >
                    <View>
                      <View style={styles.custom}>
                        {console.log("indecotor postion", indecotorPosition)}

                        <View
                          style={{
                            width: "100%",
                            flexDirection: "row",
                            backgroundColor: "pink",
                            justifyContent: "flex-end"
                          }}
                        >
                          {indecotorPosition < 80 ? (
                            <View
                              style={{
                                flexDirection: "row",
                                alignItems: "center",
                                left: `${indecotorPosition - 6}%`,
                                position: "absolute",
                                color: color.primary
                              }}
                            >
                              <Icon
                                name="corner-right-down"
                                color="black"
                                size={27}
                              />

                              <Text
                                style={{
                                  fontWeight: "500",
                                  color: color.primary
                                }}
                              >
                                {value.user_value + " "}
                              </Text>
                              <WebView
                                style={{
                                  width: 150,
                                  height: 30,
                                  backgroundColor: "transparent"
                                }}
                                automaticallyAdjustContentInsets={true}
                                javaScriptEnabled={false}
                                scalesPageToFit={false}
                                scrollEnabled={false}
                                source={{
                                  html: htmlStyle + value.measuring_meter
                                }}
                              />

                              {/* <HTML tagsStyles={{ p: {color:"green",borderWidth:1,height:50,paddingTop:15} , sup:{top:-10}}} html={value.measuring_meter} /> */}
                            </View>
                          ) : (
                            <View
                              style={{
                                flexDirection: "column",
                                top: -30,
                                alignItems: "center",
                                left: `${indecotorPosition -22}%`,
                                position: "absolute",
                                color: color.primary
                              }}
                            >
                              <View style={{ flexDirection: "row" }}>
                                <Text
                                  style={{
                                    fontWeight: "500",
                                    marginLeft: -50,
                                    color: color.primary
                                  }}
                                >
                                  {value.user_value + " "}
                                </Text>
                                <WebView
                                  style={{
                                    width: 150,
                                    height: 30,
                                    backgroundColor: "transparent"
                                  }}
                                  automaticallyAdjustContentInsets={true}
                                  javaScriptEnabled={false}
                                  scalesPageToFit={false}
                                  scrollEnabled={false}
                                  source={{
                                    html: htmlStyle + value.measuring_meter
                                  }}
                                />
                              </View>

                              <Icon
                                name="corner-right-down"
                                color="black"
                                size={27}
                                style={{ marginLeft: -12 }}
                              />
                            </View>
                          )}
                        </View>
                        <View
                          style={{
                            height: 38,
                            width: "100%",
                            flexDirection: "row",
                            backgroundColor: color.primary,
                            marginTop: 27
                          }}
                        >
                          {value.range.map((range, index) => {
							try{
								minValue = parseFloat(value.range[0].min);
							}
							catch(e){
								minValue = 0;
							}
							
							try{
								maxValue = parseFloat(value.range[value.range.length - 1].max);
							}
							catch(e){
								maxValue = 0;
							}
                            totalValue = maxValue - minValue;
                            rangeDif = 0;
                            if (index == 0) {
								try{
									rangeDif =
                                parseFloat(value.range[index].max) -
                                parseFloat(value.range[index].min);
								}
								catch(e){
									rangeDif = 0;
								}
                              
                            } else {
								try{
									rangeDif =
                                parseFloat(value.range[index].max) -
                                parseFloat(value.range[index - 1].max);
								}
								catch(e){
									rangeDif = 0;
								}
                              
                            }
                            onePrec = totalValue / 100;
                            let viewWidth = 0;
							
							try{
								viewWidth = parseFloat(rangeDif) / onePrec;
							}
							catch(e){
								viewWidth = 0;
							}
                            console.log("range width", totalValue);
                            return (
                              <View
                                style={{
                                  height: "100%",
                                  width: viewWidth + "%",
                                  backgroundColor: this.selectRangeColor(range)
                                }}
                              />
                            );
                          })}
                        
                        </View>

                        <View
                          style={{
                            flexDirection: "row",
                            width: "100%",
                            alignItems: "center",
                            paddingVertical: 10
                          }}
                        >
                          <View
                            style={{
                              flex: 1,
                              width: "100%",
                              flexDirection: "row",
                              justifyContent: "flex-start"
                            }}
                          >
                            <Text style={{ color: "#373535", fontSize: 18 }}>
                              {value.title}
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              width: "100%",
                              flexDirection: "row",
                              justifyContent: "flex-end",
                              color: color.primary
                            }}
                          >
                            <View style={{ flexDirection: "row" }}>
                              <Text style={{ color: "#969696", fontSize: 14 }}>
                                Normal: {value.normal_range + " "}
                              </Text>
                              <WebView
                                style={{
                                  width: 150,
                                  height: 30,
                                  backgroundColor: "transparent"
                                }}
                                automaticallyAdjustContentInsets={true}
                                javaScriptEnabled={false}
                                scalesPageToFit={false}
                                scrollEnabled={false}
                                source={{
                                  html: htmlStyle + value.measuring_meter
                                }}
                              />

                              {/* <View style={{width:50,height:10,backgroundColor:'transparent'}}>
                                <AutoResizeHeightWebView
                                    defaultHeight={20}
                                    needAutoResetHeight={true}
                                    style={{backgroundColor:'transparent'}}
                                    automaticallyAdjustContentInsets={false}
                                    source={{ html: htmlStyle+value.measuring_meter  }}
                                  />
                                </View> */}
                            </View>
                          </View>
                        </View>

                        <View
                          style={{
                            flex: 1,
                            width: "100%",
                            justifyContent: "flex-start"
                          }}
                        >
                          <Text style={{ color: "#969696", fontSize: 18 }}>
                            {/* {value.recommend} */}
                            {value.recommend != null
                              ? I18n.t(
                                  this.replaceSpaceToUnderScore(value.recommend),
                                  {defaultValue: value.recommend}
                                )
                              : ""}
                          </Text>
                        </View>
                      </View>
                      <View style={styles.break} />
                    </View>
                  </TouchableOpacity>
                );
              }
            })
          ) : (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignContent: "center"
              }}
            >
              <Text
                style={{ fontSize: 18, alignSelf: "center", color: "#969696" }}
              >
                {I18n.t("no_result_found")}
              </Text>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  custom: {
    backgroundColor: "#f9f9f9",
    width: "100%",
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 18,
    flexWrap: "wrap"
  },
  custom2: {
    backgroundColor: "#f9f9f9",
    width: "100%",
    flex: 1,
    paddingHorizontal: 40,
    flexDirection: "row",
    paddingVertical: 18,
    flexWrap: "wrap"
  },
  break: {
    width: "100%",
    borderColor: "#dadfe1",
    borderBottomWidth: 0.5
  },
  custom1: {
    width: "100%",
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 28
  },
  title6: {
    fontSize: 22,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingVertical: 10,
    color: "#969696"
  },
  title4: {
    fontSize: 22,
    paddingLeft: 10,
    letterSpacing: 0.5,
    paddingVertical: 10
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,

    color: "#969696"
  },
  title2: {
    fontSize: 16,
    color: "#9b9b9b",
    paddingVertical: 2
  },
  title3: {
    fontSize: 14,
    fontWeight: "500",
    letterSpacing: 0.5,
    paddingVertical: 5,
    color: "#9b9b9b"
  },
  iconView: {},
  title1: {
    fontSize: 15,
    fontWeight: "300",
    marginVertical: 15,
    color: "#969696",
    letterSpacing: 0.5,
    paddingHorizontal: 10
  },
  fieldView: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "transparent",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    paddingHorizontal: 8,
    paddingVertical: 10,
    marginTop: 12,
    marginBottom: 3
  },
  button: {
    width: 50,
    height: 50,
    paddingVertical: 2,
    borderRadius: 25,
    backgroundColor: color.primary,
    alignItems: "center",
    justifyContent: "center"
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
              borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingBottom: 15

    /* borderColor: "red",
              borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    width: "90%",
    alignSelf: "center",
    flexDirection: "row",
    paddingBottom: 23,
    paddingTop: 30,
    alignItems: "center"
  }
});
function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestResults);
