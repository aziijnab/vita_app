import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ScrollView,
  Platform,
  PermissionsAndroid,
  WebView,
  Alert
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/Feather";
import {
  LineChart,
  AreaChart,
  Grid,
  YAxis,
  XAxis
} from "react-native-svg-charts";
import Spinner from "react-native-loading-spinner-overlay";
import Route from "../../network/route.js";
import moment from "moment";
import lodash from "lodash";
import RNFetchBlob from "rn-fetch-blob";
import FileViewer from "react-native-file-viewer";
import * as d3Scale from "d3-scale";
// import { LineChart } from '../../components/react-native-chart-kit'
import { Circle, Path, G, Rect, Text as SvgText } from "react-native-svg";
import { NavigationEvents } from 'react-navigation';
const htmlStyle = `<style>
                    *{
                      padding:0px;
                      margin:0px;
                    }
                    p{
                      font-size:12;
                      padding:0 !important;
                      margin:0 !important;
                      color:#969696;
                      font-weight:bold;
                    }
                  </style>`;
const chartWidth = Dimensions.get("window").width;

const route = new Route("http://192.168.100.18:8000/api/");
import I18n from "../../i18n/i18n";
import { average } from "paths-js/ops";
import { Dropdown } from "../../components/customLib/react-native-material-dropdown";
import Button from "../../components/Button";

class Dass21 extends Component {  
  constructor(props) {
    super(props);
	console.log("current prop is", props);
    this.state = {dassQuestions:[], answers:[], dassQuestion:{question : ''}, questionIndex: -1, questionAnswered: false};
  }
  
  fetchDASS21Questions(){
	this.setState({loading:true})
	route.getAuthenticated('questions/mental_health',this.props.myToken).then(async(response)=>{
		if(response.error){
		  console.log(response.error)
		  this.setState({loading:false});

		}
		else {		  
		  this.setState({dassQuestions:response.questions});
		  this.setSingleQuestion(true);
		  this.setState({loading:false});
		}
	});
  }
  
  setSingleQuestion(isNext){
	this.state.selectedButton = "";
	if(isNext){
		if(!this.state.questionAnswered){
			Alert.alert("You must answer this question before you can proceed!")
		}
		else{
			if((this.state.questionIndex + 1) < this.state.dassQuestions.length){
				this.state.questionIndex++;
			}
		}
	}
	else{
		if((this.state.questionIndex - 1) >= 0){
			this.state.questionIndex--;
		}
	}
	this.setState({dassQuestion:this.state.dassQuestions[this.state.questionIndex]});
	this.answerBackground(this.state.answers[this.state.questionIndex].answer);
  }
  
  answerDASS21(){
	this.state.questionAnswered = true;
  }
  
  postAnswerDASS21(){//this is to submit to server
	this.setState({loading:true});	
	let answers = {"answers": this.state.answers};
	console.log("i'm trying to submit", answers);
	
	route.updateData('dass', answers, this.props.myToken).then(async(response)=>{
		if(response.error){
		  console.log("answerDASS21 failure", response.error);
		  this.setState({loading:false});
		}
		else {
		  console.log("answerDASS21 success", response);
		  //this.setState({dassQuestions:response.questions});
		  this.setState({loading:false});
		  this.state.dassQuestions = [];
		  this.fetchDASS21Questions();
		}
	}); 
  }
  
  updateAnswers(question_id, value){
	  console.log(`i expect to update question id ${question_id} with value ${value}`);
	  
	  if(this.state.answers.length > 0){
		  let foundIndex = this.state.answers.findIndex(item => item.question_id === question_id);
		  
		  if(foundIndex >= 0){
			  this.state.answers.splice(foundIndex, 1);
		  }
	  }
	  this.state.answers.push({"question_id": question_id, "answer": value});
	  this.setState({selectedButton: value});
	  //console.log("answers list", this.state.answers);
  }
  
  answerBackground(answer){
	  return this.state.selectedButton === answer
                                ? color.primary
                                : "#C0C0C0"
  }
  
  componentDidMount() {
	  this.fetchDASS21Questions();
  }
  
  render() {
	let dropdownData= [
		{"value":0, "label": I18n.t("DASS_21_did_not_apply_to_me")},
		{"value":1, "label": I18n.t("DASS_21_apply_to_me_to_some_degree")},
		{"value":2, "label": I18n.t("DASS_21_apply_to_me_considerably")},
		{"value":3, "label": I18n.t("DASS_21_apply_to_me_very_much")}
	];
	return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <Header
          label={I18n.t("DASS_21")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <View
          style={{ flex: 1, marginVertical: 13, backgroundColor: "red" }}
        >
		  <View style={styles.form}>
		  { 
			//this.state.dassQuestions.map((item, index) => { 
				//return (
					<View style={{flex: 1, flexDirection: "column"}}>
						<View style={{flex:1, flexDirection: "row", margin: 10, flexWrap: 'wrap'}}>
							<Text style={styles.title2}>{`${(this.state.questionIndex + 1)}. ${this.state.dassQuestion.question}`}</Text> 
						</View>
						<View style={{flex:3, flexDirection: "column"}}>
							{
								dropdownData.map((item, key) => {
									return (
									<TouchableOpacity  onPress={() => this.updateAnswers(this.state.dassQuestion.id, item.value)} style={{flex:1, borderColor: color.primary, backgroundColor: this.answerBackground(item.value), borderBottomColor: 'white',
										borderBottomWidth: 3,}}>
										<Text style={styles.answer}>{item.label}</Text>
									</TouchableOpacity >
									);
								})
							}
						</View>
						<View style={{flex:1, flexDirection: "row"}}>
							<Button label={"<"} buttonStyle={styles.questionButton} onPress={() => this.setSingleQuestion(false) } />
							<Text style={styles.titleCount}>{(this.state.questionIndex + 1)} { "of" } {(this.state.dassQuestions.length)}</Text>
							<Button label={">"} buttonStyle={styles.questionButton} onPress={() => this.setSingleQuestion(true) } />
						</View>		
						<View style={{flex:1, flexDirection: "row"}}>
							<Button label={"Confirm"} buttonStyle={styles.submitButton} onPress={() => this.answerDASS21()} />		
						</View>				
					</View>
				//)
		 	//})
		   }
		   
		  </View>
		</View>
		
		
		
      </View>
	  );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 16,
    color: "#969696",
    fontWeight: "700",
    letterSpacing: 0.5
  },
  iconView: {},
  title1: {
    fontSize: 15,
    fontWeight: "400",

    color: "#373535",
    letterSpacing: 0.5
    //paddingHorizontal: 10
  },
  title2: {
    color: "#969696",
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "left",
    fontWeight: "700",
    paddingLeft: 2
  },
  title3: {
    fontSize: 15,
    fontWeight: "400",

    color: color.primary,
    letterSpacing: 0.5
  },
  header: {
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    width: "90%",
    paddingVertical: 20
  },
  fieldView: {
    flexDirection: "row",
    width: "91%",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: "white",
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 11,
    paddingVertical: 10,
    marginTop: 16
  },
  button: {
    width: 50,
    height: 50,
    paddingVertical: 2,
    borderRadius: 25,
    backgroundColor: color.primary,
    alignItems: "center",
    justifyContent: "center"
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 20,
	flex: 1,
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center",
	backgroundColor: "white"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  pickerSt: {
    marginVertical: 17,
    width: "100%",
    alignSelf: "center",
    //flexDirection: "row",
    paddingTop: 6,
    paddingHorizontal: 15,
    justifyContent: "center",
    //alignItems: "center",
    //paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  rectButton: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    margin: 10,
	flex: 1,
    backgroundColor: color.primary
  },
  submitButton: {
	alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    margin: 10,
	flex: 1,
    backgroundColor: color.primary
  },
  questionButton: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    alignSelf: "center",
    width: "30%",
    margin: 10,
	flex: 1,
	paddingVertical: 13,
    backgroundColor: color.primary,
	textAlignVertical: "bottom"
  },
  titleCount: {
	alignItems: "center",
    justifyContent: "center",
	color: "#969696",
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center",
    fontWeight: "700",
    paddingLeft: 2,
	flex: 2,
	textAlignVertical: "center"
  },
  answer: {
    color: "white",
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "left",
    fontWeight: "700",
    paddingLeft: 2
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dass21);
