import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ScrollView,
  Platform,
  PermissionsAndroid,
  WebView
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/Feather";
import {
  LineChart,
  AreaChart,
  Grid,
  YAxis,
  XAxis
} from "react-native-svg-charts";
import Spinner from "react-native-loading-spinner-overlay";
import Route from "../../network/route.js";
import moment from "moment";
import lodash from "lodash";
import RNFetchBlob from "rn-fetch-blob";
import FileViewer from "react-native-file-viewer";
import * as d3Scale from "d3-scale";
// import { LineChart } from '../../components/react-native-chart-kit'
import { Circle, Path, G, Rect, Text as SvgText } from "react-native-svg";
import { NavigationEvents } from 'react-navigation';


const htmlStyle = `<style>
                    *{
                      padding:0px;
                      margin:0px;
                    }
                    p{
                      font-size:12;
                      padding:0 !important;
                      margin:0 !important;
                      color:#969696;
                      font-weight:bold;
                    }
                  </style>`;
const chartWidth = Dimensions.get("window").width;

const route = new Route("http://192.168.100.18:8000/api/");
import I18n from "../../i18n/i18n";
import { average } from "paths-js/ops";
class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parameterCartData: [],
      parameterCartDate: [],
      testHistory: [],
      testName: "",
      userValueAvarage: 0,
      measuring_meter: "",
      optimum_target: "",
      medical_test_id: "",
      parameterRange: this.props.navigation.state.params.parameterRange,
      parameterValue: this.props.navigation.state.params.parameterValue,
      maxGraphRang: null,
      minGraphRang: null
    };
  }
  // changeDateFormat(date) {
  //   var date = moment(date).format("DD/MM");
  //   return date;
  // }
  getDayFormat(date) {
    var newDate = moment(date).format("DD");
	//returnyy
    return parseInt(newDate);
  }
  getMonthFormat(date) {
    var newDate = moment(date).format("MM");
    return parseInt(newDate);
  }
  getYearFormat(date) {
    var newDate = moment(date).format("Y");
    return parseInt(newDate);
  }
  componentDidMount() {
    console.log("I18n.locale", I18n.locale);
    var minGraphRang = this.state.parameterRange[
      this.state.parameterRange.length - 1
    ].min;
    var maxGraphRang = this.state.parameterRange[0].max;
    this.setState({ maxGraphRang: maxGraphRang, minGraphRang: minGraphRang });
    const { params } = this.props.navigation.state;
    this.setState({
      testName: params.testName,
      medical_test_id: params.test_parameter_id
    });
    this.fetchTestHistory(params.test_parameter_id);
  }

  getCorrectMTCLocale(){
	if(I18n.locale === "bm"){
		return this.state.mtc_description_bm;
	}
	else if(I18n.locale === "ms"){
		return this.state.mtc_description_ms;
	}
	else{
		return this.state.mtc_description;
	}
  }
  
  async downloadPdf() {
    if (Platform.OS == "ios") {
      this.fetchPdf();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "Vita App Storage Permission",
            message: "Vita App needs access to your Storage ",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the storage");
          this.fetchPdf();
        } else {
          console.log("Storage permission denied");
          alert("you can not donwload pdf write now");
        }
      } catch (err) {
        console.warn(err);
      }
    }
  }

  async fetchPdf() {
    var now = new Date();
    // await this.requestStoragePermission()
    //    this.setState({ loading: true });
    let dirs = "";
    if (Platform.OS == "android") {
      dirs = RNFetchBlob.fs.dirs.DownloadDir;
    } else {
      dirs = RNFetchBlob.fs.dirs.DocumentDir;
    }
    console.log("dirs", RNFetchBlob.fs);
    RNFetchBlob.config({
      // add this option that makes response data to be stored as a file,
      // this is much more performant.
      fileCache: false,
      useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
      notification: true,
      path: dirs + "/parameterHistory" + now + ".pdf",
      IOSBackgroundTask: true,
      indicator: true,
      addAndroidDownloads: {
        useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
        notification: true,
        path: dirs + "/parameterHistory" + now + ".pdf", // this is the path where your downloaded file will live in
        description: "Downloading file."
      }
    })
      .fetch(
        "GET",
        "http://vitahealthapp.com/api/parameter-history-pdf?test_parameter_id=" +
          this.state.medical_test_id,
        {
          Authorization: `Bearer ${this.props.myToken}`
          // more headers  ..
        }
      )
      .then(res => {
        // the temp file path
        this.setState({ loading: false });
        FileViewer.open(res.path());
        //   RNFetchBlob.openDocument(res.path())
      })
      .catch((errorMessage, statusCode) => {
        this.setState({ loading: false });
        // alert(errorMessage);
      });
  }

  changeDateFormatWithYear(date){
    var date = moment(date).format("DD/MM/YY");
    return date;
  }

  fetchTestHistory(test_parameter_id) {
    this.setState({ loading: true });
    data = { test_parameter_id: test_parameter_id };
    route
      .updateData(`parameter-history`, data, this.props.myToken)
      .then(async response => {
        if (response.error) {
          this.setState({ loading: false });
        } else {
          console.log("user test paramerter history", response.data);
          this.setState({ testHistory: response.data });
          this.setState({ measuring_meter: response.data[0].measuring_meter });
          this.setState({ optimum_target: response.data[0].normal_range });
		  this.setState({ mtc_description: response.data[0].description });
		  this.setState({ mtc_description_bm: response.data[0].description_bm });
		  this.setState({ mtc_description_ms: response.data[0].description_ms });
		  
		  this.localed_mtc_description = this.getCorrectMTCLocale();
	
          if (response.data != []) {
            var parameterCartData = [];
            var parameterCartDate = [];
            var valueSum = 0;
            var firstTimearray = true;
            response.data.map((value, index) => {
              var userValue  = 0
              var findIndex = parameterCartDate.findIndex(
                (value, index) => this.changeDateFormatWithYear(value) === this.changeDateFormatWithYear(value.created_at)
              );
              if (findIndex < 0 || firstTimearray == true) {
                 var userValue = 0;
				 try{
					userValue = value.user_value;
					valueSum += parseFloat(userValue);
				 }
				 catch(e){
					 
				 }
				 
                if(userValue != 0){
                   userValue = value.user_value.replace(/^0+/, "");
                }
				try{
					if(userValue != "")
						parameterCartData.push(parseFloat(userValue));
					parameterCartDate.push(value.created_at);
					firstTimearray = false;
				}
				catch(e){}
              } else {
              }
            });
            
            var newParameterDate = lodash.sortedUniq(parameterCartDate);

            this.setState({
              parameterCartData: parameterCartData,
              parameterCartDate,
              newParameterDate,
              
            },()=>{
              console.log("value of sum",valueSum)
              var average = 0;
			  try{
				average = parseFloat(valueSum / this.state.parameterCartData.length);
				console.log("average data is::::");
				console.log(average);
				
			  }
			  catch(e){}
              this.setState({userValueAvarage: average.toFixed(2)})
              console.log("parameterData",this.state.parameterCartData)
            });
          }
          this.setState({ loading: false });
        }
      });
  }

  selectRangeColor(range) {
    console.log("range color",range)

    if (range.risk_level == 0) {
      return "#87D400";
    } 
     if (range.risk_level == 1) {
      return "#F2A700";
    } 
     if (range.risk_level == 2) {
      return "#CB0C12";
    }
  }

  getColorOfUserInputCircle(value) {
    var color = "";
    console.log("range value",value)
    this.state.parameterRange.map((range, index) => {
      if (value >= range.min && value <= range.max) {
        
        if (range.risk_level == 0) {
         
          color = "#87D400";
        } else if (range.risk_level == 1) {
          
          color = "#F2A700";
        } else if (range.risk_level == 2) {
         
          color = "#CB0C12";
        }
      }
    })
    if (
      value >
      this.state.parameterRange[0].max
    ) {
      color = "#CB0C12";
    }
    return color;
  }

  render() {
    const Tooltip = ({
      x,
      y,
      tooltipX,
      tooltipY,
      black,
      index,
      dataLength
    }) => {
      let xAxis = 4;
      if (dataLength > 4) {
        if (index < 2) {
          xAxis = 35;
        } else if (index > dataLength - 2) {
          xAxis = 0;
        } else {
          xAxis = 20;
        }
      }
      return (
        <G x={x(tooltipX) - 35} y={y(tooltipY)}>
          {index == 0 ? (
            <G y={tooltipY > 9 ? 20 : 15} x={40}>
              <Rect
                x={10}
                y={-25}
                height={22}
                width={(tooltipY+"").length > 1 ? 35 : 20}
                fill={this.getColorOfUserInputCircle(tooltipY)}
                ry={10}
                rx={10}
              />
              <SvgText x={17} y={-10} stroke="#fff">
                {tooltipY}
              </SvgText>
            </G>
          ) : index == this.state.parameterCartData.length - 1 ? (
            <G y={tooltipY > 9 ? 20 : -29} x={xAxis}>
              <Rect
                x={0}
                y={-10}
                height={22}
                width={(tooltipY+"").length > 1 ? 35 : 20}
                fill={this.getColorOfUserInputCircle(tooltipY)}
                ry={10}
                rx={10}
              />
              <SvgText x={8} y={6} stroke="#fff">
                {tooltipY}
              </SvgText>
            </G>
          ) : (
            <G y={tooltipY > 9 ? 20 : -29} x={xAxis}>
              <Rect
                x={10}
                y={-10}
                height={22}
                width={(tooltipY+"").length > 1 ? 35 : 20}
                fill={this.getColorOfUserInputCircle(tooltipY)}
                ry={10}
                rx={10}
              />
              <SvgText x={17} y={6} stroke="#fff">
                {tooltipY}
              </SvgText>
            </G>
          )}
        </G>
      );
    };

    const Decorator = ({ x, y, data }) => {
      return this.state.parameterCartData.map((value, index) => (
        <Circle
          key={index}
          cx={x(index)}
          cy={y(value)}
          r={6}
          stroke={color.primary}
          fill={this.getColorOfUserInputCircle(value)}
        />
      ));
    };

    const Line = ({ line }) => (
      <Path d={line} stroke={color.primary} fill={"none"} />
    );
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <Header
          label={I18n.t("history")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <ScrollView
          style={{ flex: 1, marginVertical: 13, backgroundColor: "white" }}
        >
          <View style={styles.header}>
            <View
              style={{
                flexWrap: "wrap",
                display: "flex",
                paddingHorizontal: 4,
                flex: 1
              }}
            >
              <Text style={styles.title}>
                {I18n.t("all_results_for")} {this.state.testName}
              </Text>
            </View>

            <TouchableOpacity
              style={styles.button}
              onPress={() => this.downloadPdf()}
            >
              <Icon name="arrow-down" color="white" size={23} />
              <Text style={{ color: "white" }}>PDF</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: "78%",
              alignSelf: "center",
              alignItems: "center",
              paddingVertical: 6
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{ color: "#969696", fontSize: 18, paddingVertical: 2 }}
              >
                {I18n.t("local_average")}: {this.state.userValueAvarage}{" "}
              </Text>
              <WebView
                style={{
                  width: 150,
                  height: 30,
                  backgroundColor: "transparent"
                }}
                automaticallyAdjustContentInsets={true}
                javaScriptEnabled={false}
                scalesPageToFit={false}
                scrollEnabled={false}
                source={{ html: htmlStyle + this.state.measuring_meter }}
              />
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{ color: "#969696", fontSize: 18, paddingVertical: 2 }}
              >
                {I18n.t("optimum_target")}: {this.state.optimum_target}{" "}
              </Text>
              <WebView
                style={{
                  width: 150,
                  height: 30,
                  backgroundColor: "transparent"
                }}
                automaticallyAdjustContentInsets={true}
                javaScriptEnabled={false}
                scalesPageToFit={false}
                scrollEnabled={false}
                source={{ html: htmlStyle + this.state.measuring_meter }}
              />
            </View>
          </View>

          <View
            style={{
              height: 200,
              width: "88%",
              alignSelf: "center",
              marginVertical: 10,
              marginRight: 20,
              marginBottom:30
              
            }}
          >
            {/* <View
              style={{
                width: "92%",
                left: "10%",
                height: "85%",
                position: "absolute",
                top: "7%"
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: `${redValue}%`,
                  backgroundColor: "#CB0C12",
                  // opacity: 0.7
                }}
              />
              <View
                style={{
                  width: "100%",
                  height: `${orangeValue}%`,
                  backgroundColor: "#F2A700",
                  // opacity: 0.7
                }}
              />
              <View
                style={{
                  width: "100%",
                  height: `${greenValue}%`,
                  backgroundColor: "#87D400",
                  // opacity: 0.7
                }}
              />
            </View> */}

           
              {/* <YAxis
                // style={{ width: '85%', alignSelf: 'center', }}

                data={yAxisData}
                contentInset={{ top: 20, bottom: 20 }}
                numberOfTicks={10}
                formatLabel={value => `${value}`}
              /> */}

              <View style={{flexDirection:'row',height:200}}>
              <View
                style={{
                  height: 200,
                  width: 25,
                  marginLeft: 15,
                  backgroundColor: color.primary,
                  marginTop: 20
                }}
              >
                {this.state.parameterRange.map((range, index,rangeArray) => {
                  console.log('range array', rangeArray)
                  minValue = 0;
				  try{
					  minValue = this.state.parameterValue.range[0].min;
					  if(minValue != null)
						minValue = parseFloat(minValue);
				  }
				  catch(e){}
				  
                  maxValue = 0;
				  try{
					  maxValue = this.state.parameterValue.range[this.state.parameterValue.range.length - 1].max;
					  if(maxValue != null)
						maxValue = parseFloat(maxValue);
				  }
				  catch(e){}
				
                  console.log("min and max value",minValue,maxValue)
                  totalValue = maxValue- minValue  ;
                  rangeDif = 0;
                  if (index == 0) {
					  try{
						rangeDif = parseFloat(rangeArray[index].max) - parseFloat(rangeArray[index].min);
					  }
					  catch(e){
						  
					  }
                       
                      
                  } else {
					  try{
						rangeDif = parseFloat(rangeArray[index - 1].min) - parseFloat(rangeArray[index].min);
                     }
					  catch(e){
						  
					  }
                  }
                  onePrec = totalValue / 100;
                  let viewWidth = 0;
				  try{
					viewWidth = parseFloat(rangeDif) / onePrec;
				  }
				  catch(e){}
                  console.log("range width", viewWidth);
                  return (
                    <View
                      style={{
                        height: viewWidth + "%",
                        width: "100%",
                        backgroundColor: this.selectRangeColor(range)
                      }}
                    />
                  );
                })}
              </View>
              <View style={{ flex: 9,height:200}}>
                <AreaChart
                  yMin={this.state.minGraphRang}
                  yMax={this.state.maxGraphRang}
                  style={{ height: 240, width: "100%" }}
                  data={this.state.parameterCartData}
                  svg={{ fill: "rgba(241, 102, 56, 0)" }}
                  contentInset={{ top: 20, bottom: 20 }}
                >
                  <Grid />
                  <Line />
                  <Decorator />
                  {this.state.parameterCartData.map((value, index) => {
                    return (
                      <Tooltip
                        tooltipX={index}
                        tooltipY={value}
                        color="#003F5A"
                        index={index}
                        dataLength={this.state.parameterCartData.length}
                      />
                    );
                  })}
                </AreaChart>
              </View>
                {/* <LineChart
                yMin={0}
                yMax={maxValue}
                numberOfTicks={10}
                style={{ height: 200, width: "95%", marginLeft: 10 }}
                data={this.state.parameterCartData}
                svg={{ stroke: "black" }}
                contentInset={{ top: 20, bottom: 20 }}
              >
                <Grid />
                <Decorator/>
              </LineChart> */}
            </View>

            {this.state.parameterCartDate.length <= 5 && (
              <XAxis
                style={{ color: "transparent", opacity: 1, marginTop: 22 }}
                spacingOuter={0}
                spacingInner={0}
                data={this.state.parameterCartDate}
                // numberOfTicks={this.state.parameterCartDate.length+1}
                scale={d3Scale.scaleBand}
                xAccessor={({ item }) => item}
                formatLabel={(value, index) => this.changeDateFormatWithYear(value)}
                contentInset={{ left: -20, right: -20 }}
                svg={{ fontSize: 10, fill: "black" }}
              />
            )}
          </View>

		  <View
            style={{ alignSelf: "center", paddingVertical: 10, width: "80%" }}
          >
			<View style={{ flexDirection: "row" }}>
              <Text
                style={{ color: "#969696", fontSize: 16, paddingVertical: 2 }}
              >
					{this.localed_mtc_description}
				
              </Text>
              <WebView
                style={{
                  width: 150,
                  height: 30,
                  backgroundColor: "transparent"
                }}
                automaticallyAdjustContentInsets={true}
                javaScriptEnabled={false}
                scalesPageToFit={false}
                scrollEnabled={false}
                source={{ html: htmlStyle + this.state.measuring_meter }}
              />
            </View>
          </View>
		  
          {this.state.testHistory.length > 0
            ? this.state.testHistory.map((item, index) => {
                return (
                  <TouchableOpacity
                    activeOpacity={0.9}
                    /*onPress={() => this.props.navigation.pop()}*/ style={
                      styles.fieldView
                    }
                  >
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        width: "100%"
                      }}
                    >
                      <Text style={styles.title1}>
                        {this.changeDateFormatWithYear(item.created_at)}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "flex-end",
                        width: "100%",
                        alignItems: "center"
                      }}
                    >
                      <View style={{ flexDirection: "row" }}>
                        <Text style={styles.title3}>{item.user_value}</Text>
                        <View
                          style={{
                            width: 50,
                            height: 30,
                            marginLeft: 5,
                            backgroundColor: "transparent"
                          }}
                        >
                          <WebView
                            style={{
                              width: 150,
                              height: 30,
                              backgroundColor: "transparent"
                            }}
                            automaticallyAdjustContentInsets={true}
                            javaScriptEnabled={false}
                            scalesPageToFit={false}
                            scrollEnabled={false}
                            source={{
                              html: htmlStyle + this.state.measuring_meter
                            }}
                          />
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              })
            : null}
          <View
            style={{ alignSelf: "center", paddingVertical: 10, width: "80%" }}
          >
			
            {this.state.testHistory.length > 0 && (
              <Text>{this.state.testHistory[0].recommend}</Text>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 16,
    color: "#969696",
    fontWeight: "700",
    letterSpacing: 0.5
  },
  title2: {
    fontSize: 20,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: 6,
    paddingLeft: 30
  },
  iconView: {},
  title1: {
    fontSize: 15,
    fontWeight: "400",

    color: "#373535",
    letterSpacing: 0.5
    //paddingHorizontal: 10
  },
  title3: {
    fontSize: 15,
    fontWeight: "400",

    color: color.primary,
    letterSpacing: 0.5
  },
  header: {
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    width: "90%",
    paddingVertical: 20
  },
  fieldView: {
    flexDirection: "row",
    width: "91%",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: "white",
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 11,
    paddingVertical: 10,
    marginTop: 16
  },
  button: {
    width: 50,
    height: 50,
    paddingVertical: 2,
    borderRadius: 25,
    backgroundColor: color.primary,
    alignItems: "center",
    justifyContent: "center"
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(History);
