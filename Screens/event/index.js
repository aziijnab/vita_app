import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform,
  RefreshControl,
  PermissionsAndroid,
  ToastAndroid
} from "react-native";
import RNPickerSelect from "react-native-picker-select";
import { color, font, apiURLs } from "../../components/Constant";
import Button from "../../components/Button";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import NotificationHeader from "../../components/NotificationHeader";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from "../../i18n/i18n";
import Route from "../../network/route.js";
import moment from "moment";
import geolib from "geolib";
import underscore from "underscore";
import Spinner from "react-native-loading-spinner-overlay";
import { NavigationEvents } from "react-navigation";
import MapboxGL from "@react-native-mapbox-gl/maps";
import Geolocation from 'react-native-geolocation-service';

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
MapboxGL.setAccessToken("pk.eyJ1Ijoidml0YWhlYWx0aGFwcCIsImEiOiJjazNlM2lpcWQxM3gyM2VwYmtjem85ZXpxIn0.KqZKg2qal0UEy1cHbGj5xw");

class EventScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events_data: [],
      event_details: [],
      showDetails: false,
      coords_arr: [],
      pastAppointments: [],
      categories: [],
      tags: [],
      selected_tag: "",
      selected_category: "",
      date: "",
      selectedSensor: 1,
      refreshing: false,
      loading: false,
      allPackages: [],
      isDateTimePickerVisible: false,
      isLoading:true,
      region: {
        latitude: 3.1390,
        longitude: 101.6869,
    },
    centerLat:0,
    centerLng:0,
      data: [
        {
          name: "Mediveron",
          message: "Blood package with H Clinic",
          time: 8,
          price: 200
        },
        {
          name: "Health Clinic",
          message: "Lung Package with Dynamix",
          time: 2,
          price: 400
        },
        {
          name: "Bjourn Health",
          message: "Dermatolofy package with Metrix Clinic",
          time: 9,
          price: 600
        }
      ]
    };

  }
  renderAnnotation = (counter) => {
    const id = `pointAnnotation${counter}`;
    const coordinate = this.state.coordinates[counter];
    const title = `Longitude: ${this.state.coordinates[counter][0]} Latitude: ${this.state.coordinates[counter][1]}`;
// console.log("this")
    return (
      <MapboxGL.PointAnnotation
        key={id}
        id={id}
        title='Test'
        coordinate={coordinate}>

        <Image
        source={require('../../asset/icons/marker.png')}
        style={{
          resizeMode: 'contain',
          width: 25,
          height: 25
          }}/>
      </MapboxGL.PointAnnotation>
    );
  }
  renderAnnotations = () => {
    const items = [];

    for (let i = 0; i < this.state.coordinates.length; i++) {
      items.push(this.renderAnnotation(i));
    }
    // console.log(items)
    // return items;

  }
  
 
 
  async componentDidMount() {
    await this.hasLocationPermission()
   this.setState({ loading: true })

   Geolocation.getCurrentPosition(
       (position) => {
           console.log("wokeeey");
           //   alert(JSON.stringify(position) );
           let centerLat =0;
           let centerLng =0;
           console.log("Center Lat Lng")
           console.log(this.props.navigation.state.params)
           if(this.props.navigation.state.params){
             centerLat = this.props.navigation.state.params.lat;
             centerLng = this.props.navigation.state.params.lng;
           }
           else{
            centerLat = position.coords.latitude;
            centerLng = position.coords.longitude;
           }
           let region = {
               latitude: position.coords.latitude,
               longitude: position.coords.longitude,
           }
console.log("..........REGION...............")
console.log(region)
           this.setState({ region: region,
            centerLat: centerLat,
            centerLng: centerLng
           },()=>{
                this.fetchFilters();
    MapboxGL.setTelemetryEnabled(false);
               this.setState({ loading: false })
           })
       },
       (error) => {

           console.log("error postion", error)
           this.setState({ loading: false })

         
       },
       { enableHighAccuracy: true, timeout: 20000 },
   ); 

}
async hasLocationPermission () {
  if (Platform.OS === 'ios' ||
      (Platform.OS === 'android' && Platform.Version < 23)) {
  return true;
  }

  const hasPermission = await PermissionsAndroid.check(
  PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
  );

  if (hasPermission) return true;

  const status = await PermissionsAndroid.request(
  PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
  );

  if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

  if (status === PermissionsAndroid.RESULTS.DENIED) {
  ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
  } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
  ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
  }

  return false;
}
  // componentDidMount() {
 
   
  // }
  showDetails = (event) => {
    // console.log(event.properties.data);
    this.setState({event_details : event.properties.data, showDetails : true})
    console.log("-----------------");
    console.log(event.properties.data);
    console.log("-----------------");

  }
  fetchFilters() {
    this.setState({ loading: true });
    fetch("http://192.168.1.6:8080/api/event-filters", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        // console.log(response);
        // alert(response.response)
        if (response.responseCode == 200) {
          // alert("this")
          this.setState({ categories: response.categories, tags: response.tags });

        } 
         else {
          //  alert("that")
         console.log("error")
          this.setState({ loading: false });
        }
      })
      .catch(error => alert("Please check internet connection"));
  }
  fetchData = (category, tag, date) => {

    this.setState({ loading: true, events_data:[] });
    // console.log("before APi:")
    // console.log("Category = "+category)
    // console.log("tag = "+tag)
    // console.log("date = "+date)
    // console.log("before APi ends:")
    // console.log("http://192.168.1.6:8080/api/user-all-events?category="+category+"&tag="+tag+"&date="+date)
    
    fetch("http://192.168.1.6:8080/api/user-all-events?category="+category+"&tag="+tag+"&date="+date, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        if (response.responseCode == 200) {

          console.log("response.data")
          console.log(response.data)
          console.log("response.data")
          // ..................WORKING................
          // this.setState({ events_data: response.data})
          let data_arr = response.data;
          data_arr.forEach(element => {
            let distance = geolib.getDistance(
              { latitude: this.state.region.latitude, longitude: this.state.region.longitude },
              { latitude: element.latitude, longitude: element.longitude }
          );


          var distance1 = distance / 1000
          console.log("Math.round(distance1 * 10) / 10");
          console.log(Math.round(distance1 * 10) / 10);
          element["distance"] = Math.round(distance1 * 10) / 10;

          var sortedClinicsByDistanc = underscore.sortBy(data_arr, 'distance')
          this.setState({
            events_data: sortedClinicsByDistanc
          });
         
           
          }
          );
         

          this.setState({ loading: false });
        } 
         else {
          console.log("error")
          this.setState({ loading: false });
        }


      })
      .catch(error => alert("Please check internet connection"));
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    console.log("refreshing");
    
    this.fetchData("","","");
    this.setState({ refreshing: false, selected_category : "", selected_tag:"", date:"" });
  };
  
  render() {
    const placeholder_category = {
      label: "By Category...",
      value: null,
      color: "#9EA0A4",
      key: 0
    };
    const placeholder_tag = {
      label: "By Tag...",
      value: null,
      color: "#9EA0A4",
      key: 0
    };
    const placeholder_time = {
      label: "By Time...",
      value: null,
      color: "#9EA0A4",
      key: 0
    };
    const Categories = this.state.categories;
    const Tags = this.state.tags;
  
    // this.state.events_data.forEach(element => {
    //   console.log(element.id)
    // });
    let arr = [];
    this.state.events_data.forEach(element => {
      let lat = element.latitude;
      let lng = element.longitude;
      let cords_array = [];
      cords_array.push(lng)
      cords_array.push(lat)
let obj = {
  "type": "Feature",
  "properties": {
    icon: 'airport-15',
    data : element,
  },
  "geometry": {
    "type": "Point",
    "coordinates":cords_array
  }
};
// let parsed_obj = JSON.parse(obj)
arr.push(obj) 
});
    const featureCollection = {
      "type": "FeatureCollection",
      "features":arr
    };
 
    return (
      <View style={{ width: deviceWidth, backgroundColor: "white" }}>
        <NotificationHeader
          // label={"events"}
          label={I18n.t("health_compass")}
          inboxCount={this.props.inboxCount}
          notificationCount={this.props.notificationCount}
          onMailPress={() => this.props.navigation.navigate("inbox")}
          onNotificationPress={() =>
            this.props.navigation.navigate("notifications")
          }
        />
           <NavigationEvents onDidFocus={() => {
             this.setState({
               selected_category : "",
              selected_tag:"",
               date:""
               })
             this.fetchData("","","");
             }} />
        <Spinner
          visible={this.state.loading}
          textContent={I18n.t('please_wait')}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
         <View
          style={{
            borderRadius: 25,
            padding: 2,
            borderWidth: 2,
            borderColor: "",
            alignSelf: "center",
            borderColor: "#e5e5e5",
            width: "90%",
            height: 55,
            backgroundColor: "white",
            flexDirection: "row",
            marginVertical: 20
          }}
        >
          <TouchableOpacity
            onPress={() => this.setState({ selectedSensor: 0 })}
            activeOpacity={0.8}
            style={{
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
              backgroundColor:
                this.state.selectedSensor == 0 ? color.primary : "white",
              width: "100%",
              borderRadius: 23
            }}
          >
            <Text
              style={{
                color: this.state.selectedSensor == 0 ? "white" : "#373535",
                fontSize: 17
              }}
            >
              {I18n.t("list")}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.setState({ selectedSensor: 1 })}
            activeOpacity={0.8}
            style={{
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
              backgroundColor:
              this.state.selectedSensor == 1 ? color.primary : "white",
              width: "100%",
              borderRadius: 23
            }}
          >
            <Text
              style={{
                color: this.state.selectedSensor == 1 ? "white" : "#373535",
                fontSize: 17
              }}
            >
              {I18n.t("map")}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.setState({ selectedSensor: 2 })}
            activeOpacity={0.8}
            style={{
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
              backgroundColor:
              this.state.selectedSensor == 2 ? color.primary : "white",
              width: "100%",
              borderRadius: 23
            }}
          >
            <Text
              style={{
                color: this.state.selectedSensor == 2 ? "white" : "#373535",
                fontSize: 15
              }}
            >
              {I18n.t("recommended_for_you")}
            </Text>
          </TouchableOpacity>
        </View>
        {this.state.selectedSensor == 0 ? (
          <View>
          <View style={{width:"100%", alignItems:"center", alignContent:"center"}}>
<View>
  <Text style={{fontSize:22, fontWeight:"400"}}>{I18n.t("health_events_nearby")}</Text>
</View>

<View style={{ width:"90%"}}>
                      <RNPickerSelect
                        placeholder={placeholder_category}
                        items={Categories}
                        // onValueChange={value => this.selectCategory(value)}
                        onValueChange={(value) => {
                          this.setState({ selected_category : value });
                          console.log("category : " + value)
                          console.log("tag : " + this.state.selected_tag)
                          console.log("time : " + this.state.date)
                          this.fetchData(value,this.state.selected_tag,this.state.date);
                          }}
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 24
                          },
                        
                          width: 30
                        }}
                        value={this.state.selected_category}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Icon
                              name="arrow-down"
                              style={{
                              color:"#9EA0A4",
                              fontSize:18
                              }}
                            />
                          );
                        }}
                      />
                    </View>

            <View style={{flexDirection:"row", width:"90%"}}>
            
                    <View style={{ width:"50%"}}>
                      <RNPickerSelect
                        placeholder={placeholder_tag}
                        items={Tags}
                        // onValueChange={value => this.selectTag(value)}
                        onValueChange={(value) => {
                          this.setState({selected_tag : value});
                          console.log("category : " + this.state.selected_category)
                          console.log("tag : " + value)
                          console.log("time : " + this.state.date)
                          this.fetchData(this.state.selected_category,value,this.state.date);

                        }
                        }
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 24
                          },
                          width: 30
                        }}
                        value={this.state.selected_tag}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Icon
                              name="md-arrow-down"
                              style={{
                              color:"#9EA0A4",
                              fontSize:18
                              }}
                            />
                          );
                        }}
                      />
                    </View>
                   
                    <View style={{ width:"50%"}}>
                      <RNPickerSelect
                        placeholder={placeholder_time}
                        items={[
                { label: 'Today', value: 'today' },
                { label: 'Tomorrow', value: 'tomorrow' },
                { label: 'Next 1 Week', value: 'week' },
                { label: 'Next 1 Year', value: 'year' },
            ]}
                       
                        onValueChange={(value) => {
                        this.setState({date : value});
                        this.fetchData(this.state.selected_category,this.state.selected_tag,value);
                        }
                        }
                        style={{
                          ...smallpickerSelectStyles,
                          iconContainer: {
                            top: 10,
                            right: 24
                          },
                          width: 30
                        }}
                        value={this.state.date}
                        useNativeAndroidPickerStyle={false}
                        textInputProps={{ underlineColor: "#3B5998" }}
                        Icon={() => {
                          return (
                            <Icon
                              name="md-arrow-down"
                              style={{
                              color:"#9EA0A4",
                              fontSize:18
                              }}
                            />
                          );
                        }}
                      />
                    </View>
                   
                    
           
            </View>
          </View>
          <View style={{width:"100%",height:deviceHeight/2}}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
            style={{ width:"100%", marginTop: 13, backgroundColor: "white" }}
          >
            {this.state.events_data.length == 0 && (
              <View
                style={{
                  width: "100%"
                }}
              >
                <Text
                  style={{
                    textAlign: "center"
                  }}
                >
                  {I18n.t("no_events_found")}
                </Text>
              </View>
            )}
            {this.state.events_data.map((item, index) => {
             
                return (
                  <TouchableOpacity
                   key={index}
                  style={styles.fieldView}
                  onPress={() => this.props.navigation.navigate("EventDetails",{event_data:item})}
                  >
                  <View style={{flexDirection:"row"}}>
                  <View style={{width:"30%"}}>
                  {item.category_id == 1 ? 
                    <Image style={{width:"100%", height:80,borderRadius: 8, resizeMode:"contain"}}
           source={require("../../asset/icons/sports.png")}/>
                  : item.category_id == 2 ? 
                  <Image style={{width:"100%", height:80,borderRadius: 8, resizeMode:"contain"}}
           source={require("../../asset/icons/food.png")}/>
                  : item.category_id == 3 ? 
                  <Image style={{width:"100%", height:80,borderRadius: 8, resizeMode:"contain"}}
           source={require("../../asset/icons/charity.png")}/>
            : item.category_id == 4 ?
           <Image style={{width:"100%", height:80,borderRadius: 8, resizeMode:"contain"}}
           source={require("../../asset/icons/blood.png")}/>
            : item.category_id == 5 ?
            <Image style={{width:"100%", height:80,borderRadius: 8, resizeMode:"contain"}}
           source={require("../../asset/icons/health.png")}/>
           :
            <Image style={{width:"100%", height:80,borderRadius: 8, resizeMode:"contain"}}
           source={require("../../asset/icons/health.png")}/>
            }
         
      </View>
      <View style={{width:"50%", paddingHorizontal:10}}>

      <View style={{paddingVertical:3}}> 
      <Text style={{fontSize:16, fontWeight:"500"}}>{item.event_title}</Text>
      </View>
      <View style={{paddingVertical:3}}> 
      <Text style={{fontSize:14, fontWeight:"normal"}}>
      {moment(item.date_from).format(" Do MMM, YYYY")}
      </Text>
      </View>
      <View style={{paddingVertical:3}}> 
      <Text style={{fontSize:14, fontWeight:"normal"}}>
      {item.address}
      </Text>
      </View>
      </View>
      <View style={{width:"20%", paddingVertical:30}}>
      <Text style={{ fontSize: 16, color: "#808080", fontWeight:"bold" }}>
                      {item.distance} km away
                      </Text>
      </View>
                  </View>

      </TouchableOpacity> );
            
            })}

            <View style={{ alignSelf: "center", paddingVertical: 10 }} />
          </ScrollView>
        
          </View>
         
          </View>
         ) : this.state.selectedSensor == 1 ? (
          <View style={{width:"100%", alignItems:"center", alignContent:"center"}}>
         
          <MapboxGL.MapView style={styles.map}>
          <MapboxGL.Camera
            zoomLevel={13}
            centerCoordinate={[this.state.centerLng,this.state.centerLat]}
          />
          {/* <MapboxGL.Images images={{example: "../../asset/icons/marker.png", assets: ['pin']}} /> */}
          <MapboxGL.ShapeSource
            id="exampleShapeSource"
            onPress={(e)=> this.showDetails(e.nativeEvent.payload)}
            shape={featureCollection}>
            {/* e.nativeEvent.payload */}
            <MapboxGL.SymbolLayer id="exampleIconName" 
            style={{
    iconImage: ['get', 'icon'],

    iconSize: [
      'match',
      ['get', 'icon'],
     1,
    ],
  }} />
          </MapboxGL.ShapeSource>
        </MapboxGL.MapView>
        <View style={{width:"100%", position:"absolute", bottom:250, zIndex:9999}}>
<ScrollView style={{width:"100%"}} horizontal={true}>
{this.state.events_data.map((item, index) => {
             
             return (
             <TouchableOpacity
             key={index}
             onPress={() => this.props.navigation.navigate("EventDetails",{event_data:item})}
              style={{
                width: 100,
                backgroundColor: "#fff",
                borderRadius:15,
                borderColor:"#2f528f",
                borderWidth:2,
                marginLeft:10
              }}>
             
<View style={{width:"100%", alignItems:"center", alignContent:"center"}}>
{item.category_id == 1 ? 
                    <Image style={{width:60, height:60, resizeMode:"contain"}}
           source={require("../../asset/icons/sports.png")}/>
                  : item.category_id == 2 ? 
                  <Image style={{width:60, height:60, resizeMode:"contain"}}
           source={require("../../asset/icons/food.png")}/>
                  : item.category_id == 3 ? 
                  <Image style={{width:60, height:60, resizeMode:"contain"}}
           source={require("../../asset/icons/charity.png")}/>
            : item.category_id == 4 ?
           <Image style={{width:60, height:60, resizeMode:"contain"}}
           source={require("../../asset/icons/blood.png")}/>
            : item.category_id == 5 ?
            <Image style={{width:60, height:60, resizeMode:"contain"}}
           source={require("../../asset/icons/health.png")}/>
           :
            <Image style={{width:60, height:60, resizeMode:"contain"}}
           source={require("../../asset/icons/health.png")}/>
            }
</View>
<View style={{width:"100%", alignItems:"center", alignContent:"center"}}>
<Text style={{ fontSize: 12, fontWeight: "600", textAlign:"center" }}>
{item.event_title}
                      </Text>
</View>
<View style={{width:"100%", alignItems:"center", alignContent:"center"}}>
<Text style={{ fontSize: 14, color: "#808080" }}>
                      {item.distance} km away
                      </Text>
</View>
            
  
             </TouchableOpacity>
             )
})
}
          
{/* <View
              style={{
                width: "33%",
                backgroundColor: "#fff",
                alignContent: "center",
                alignItems: "center",
                borderRadius:15,
                borderColor:"#2f528f",
                borderWidth:2,
                paddingTop: 25
              }}
            >
           <View style={{ width: "96%" }}>
                  
                  <Image
                    style={{ width: "100%", height: 100 }}
                    source={require("../../asset/icons/health.png")}
                  />
               

                <View style={{ width: "100%" }}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ width: "50%", padding: 20 }}>
                      <Text style={{ fontSize: 18, fontWeight: "600" }}>
                        Title
                      </Text>
                      <Text style={{ fontSize: 14, color: "#808080" }}>
                      2 km away
                      </Text>
                    </View>
                     </View>
                 
                </View>
              </View>
          
            </View> */}

            
</ScrollView>
</View>
{this.state.showDetails ? (

 <View style={{width:"100%", alignContent:"center", alignItems:"center", position:"absolute", top:50}}>
   <View style={{minWidth:"65%", borderRadius:20, borderWidth:2, borderColor:"#F16638", alignItems:"center", alignContent:"center", justifyContent:"center", backgroundColor:"#fff", padding:10}}>
<Text style={{fontSize:22}}>{this.state.event_details.event_title}</Text>
   </View>
 </View>
) : null}
         </View>
         
         ) : this.state.selectedSensor == 2 ? null : null}
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    inboxCount: state.user.inboxCount,
    notificationCount: state.user.notificationCount,
	user: state.user.userInfo,
    myToken: state.user.userToken
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}
function elevationShadowStyle(elevation) {
  return {
    elevation,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0.5 * elevation },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation
  };
}
const styles = StyleSheet.create({
    shadow1: elevationShadowStyle(5),
    shadow2: elevationShadowStyle(10),
    shadow3: elevationShadowStyle(20),
    Bigbox: {
      width: "90%",
      paddingVertical:10,
      alignSelf:"center",
      borderRadius: 8,
      backgroundColor: "#eaeaea",
      marginTop: 30
    },
    map: {
     width:"100%",
     height:deviceHeight
    },
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  titlePrice: {
    fontSize: 15,
    fontWeight: "400",

    color: color.primary,
    letterSpacing: 0.5
    //paddingHorizontal: 10
  },
  title2: {
    fontSize: 13,
    fontWeight: "400",
    letterSpacing: 0.5,
    paddingVertical: 2,
    color: "#969696"
  },
  title3: {
    fontSize: 16,
    fontWeight: "400",
    paddingVertical: 6,
    color: color.primary
  },
  iconView: {},
  title1: {
    fontSize: 16,
    fontWeight: "400",

    color: "#373535"

    //paddingHorizontal: 10
  },
  fieldView: {
    flexDirection: "row",
    width: "91%",
    flexWrap: "wrap",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: "white",
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginTop: 19
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  buttonCancel: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.grey
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  }
});
const smallpickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#000",
    borderRadius: 10,
    color: "black",
    paddingRight: 40,
    backgroundColor: "#fff",
    width: "100%"
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#000",
    borderRadius: 10,
    color: "black",
    paddingRight: 40,
    backgroundColor: "#fff",
    width: "100%"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventScreen);
