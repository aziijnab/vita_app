import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  Linking,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform,
  RefreshControl
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Button from "../../components/Button";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from "../../i18n/i18n";
import Route from "../../network/route.js";
import moment from "moment";
import Spinner from "react-native-loading-spinner-overlay";
import { NavigationEvents } from "react-navigation";
import Modal from "react-native-modal";
import ImageViewer from "react-native-image-zoom-viewer";
import HTML from 'react-native-render-html';

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
class EventDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event_data: null,
      upcomingAppointments: [],
      pastAppointments: [],
      selectedSensor: 0,
      refreshing: false,
      loading: false,
      allPackages: [],
      isModalVisible: false,
      showDetails: false
    };
  }
  _toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
    console.log("Model => " + this.state.isModalVisible);
  };
  _toggleText = () => this.setState({ showDetails: !this.state.showDetails });

  componentDidMount() {
    this.setState({
      event_data: this.props.navigation.state.params.event_data
    });
    console.log("this.props.navigation.state.params.event_data");
    console.log(this.props.navigation.state.params.event_data);
  }

  navigateGoogle = () => {
    var url = `https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=${this.props.navigation.state.params.event_data.latitude},${this.props.navigation.state.params.event_data.longitude}`;
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          console.log("Can't handle url: " + url);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err => console.error("An error occurred", err));
  };
  _onRefresh = () => {
    this.setState({ refreshing: true });
    console.log("refreshing");

    this.setState({ refreshing: false });
  };
  render() {
    const images = [
      {
        // Simplest usage.
        url: this.props.navigation.state.params.event_data.photo,

        // width: number
        // height: number
        // Optional, if you know the image size, you can set the optimization performance

        // You can pass props to <Image />.
        props: {
          // headers: ...
        }
      }
    ];
    return (
      <View style={{ width: deviceWidth, height:deviceHeight, backgroundColor: "white" }}>
        <Header
          label={I18n.t("event_details")}
          onbackPress={() => this.props.navigation.pop()}
        />
         <ScrollView>
         <View
          style={{
            width: "100%",
            alignItems: "center",
            alignContent: "center",
            paddingBottom:100,
          }}
        >
        
          <View style={{ width: "100%", marginTop: 10 }}>
            {this.props.navigation.state.params.event_data.photo ? (
              <TouchableOpacity
                onPress={this._toggleModal}
                style={{ width: "100%", height: 200 }}
              >
                <Image
                  style={{
                    width: "100%",
                    height: "100%",
                    borderRadius: 8,
                    resizeMode: "contain"
                  }}
                  source={{
                    uri: this.props.navigation.state.params.event_data.photo
                  }}
                />
              </TouchableOpacity>
            ) : (
              <Image
                style={{
                  width: "100%",
                  height: 200,
                  borderRadius: 8,
                  resizeMode: "contain"
                }}
                source={{
                  uri:
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS42VxMCOJNsVew-Zw3x1mU9UrC62bw2cdra3JLrjESAVHJs11-"
                }}
              />
            )}
          </View>

          <View style={styles.fieldView}>
            <View
              style={{
                width: "100%",
                alignContent: "center",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: "50%",
                  borderRadius: 5,
                  backgroundColor: "#fbdfcc",
                  padding: 3
                }}
              >
                <Text
                  style={{
                    fontSize: 10,
                    textAlign: "center",
                    color: "#eb6b19"
                  }}
                >
                  {this.props.navigation.state.params.event_data.category_name}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", marginTop: 5 }}>
              <View style={{ width: "40%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                    Event Title :
                  </Text>
                </View>
              </View>
              <View style={{ width: "60%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {" "}
                    {this.props.navigation.state.params.event_data.event_title}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "row", marginTop: 5 }}>
              <View style={{ width: "40%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                    Event By :
                  </Text>
                </View>
              </View>
              <View style={{ width: "60%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {" "}
                    {this.props.navigation.state.params.event_data.event_by}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
            <View style={{ width: "50%",flexDirection: "row" }}>
           
        
              <View style={{ width: "30%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                    From :
                  </Text>
                </View>
              </View>
              <View style={{ width: "70%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {moment(
                      this.props.navigation.state.params.event_data.date_from
                    ).format(" Do MMM, YYYY")}
                  </Text>
                </View>
              </View>
            
            </View>
            <View style={{ width: "50%",flexDirection: "row" }}>
           
        
              <View style={{ width: "30%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                    To :
                  </Text>
                </View>
              </View>
              <View style={{ width: "70%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {moment(
                      this.props.navigation.state.params.event_data.date_to
                    ).format(" Do MMM, YYYY")}
                  </Text>
                </View>
              </View>
            
            </View>
              </View>
            {/* <View style={{flexDirection:"row"}}>

            <View style={{width:"40%"}}>
            <View style={{paddingVertical:3}}> 
        <Text style={{fontSize:14, fontWeight:"bold"}}>Time :</Text>
        </View>

            </View>
            <View style={{width:"60%"}}>
            <View style={{paddingVertical:3}}> 
        <Text style={{fontSize:14, fontWeight:"normal"}}>
        </Text>
        </View>

            </View>
        </View> */}
            <View style={{ flexDirection: "row" }}>
              <View style={{ width: "40%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                    Address :
                  </Text>
                </View>
              </View>
              <View style={{ width: "60%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {this.props.navigation.state.params.event_data.address}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={{ width: "40%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                   No. of Pax
                  </Text>
                </View>
              </View>
              <View style={{ width: "60%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {this.props.navigation.state.params.event_data.no_of_pax ? this.props.navigation.state.params.event_data.no_of_pax : 'N/A' }
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={{ width: "40%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                   Email
                  </Text>
                </View>
              </View>
              <View style={{ width: "60%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {this.props.navigation.state.params.event_data.email ? this.props.navigation.state.params.event_data.email : 'N/A'}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={{ width: "40%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                  Mobile No.
                  </Text>
                </View>
              </View>
              <View style={{ width: "60%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {this.props.navigation.state.params.event_data.mobile ? this.props.navigation.state.params.event_data.mobile : 'N/A'}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={{ width: "40%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                  Whatsapp
                  </Text>
                </View>
              </View>
              <View style={{ width: "60%" }}>
                <View style={{ paddingVertical: 3 }}>
                  <Text style={{ fontSize: 14, fontWeight: "normal" }}>
                    {this.props.navigation.state.params.event_data.whatsapp ? this.props.navigation.state.params.event_data.whatsapp : 'N/A'}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ width: "100%", paddingTop:3 }}>
                
                  <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                    Description :
                  </Text>
              
            </View>
              <View style={{ width: "100%" }}>
                <View style={{ paddingVertical: 3 }}>
                  {this.state.showDetails ? (
                    <HTML onLinkPress={(event, href)=>{
            Linking.openURL(href)
          }}
           html={this.props.navigation.state.params.event_data
                          .description} imagesMaxWidth={deviceWidth} />
                    
                  ) : this.props.navigation.state.params.event_data.description
                      .length < 80 ? (
                        <HTML html={this.props.navigation.state.params.event_data
                          .description} imagesMaxWidth={deviceWidth} />
                  ) : (
                    <View>
                    <HTML html={this.props.navigation.state.params.event_data.description.substring(
                        0,
                        80
                      )} imagesMaxWidth={deviceWidth} />
                  
                      <Text
                        onPress={this._toggleText}
                        style={{ color: "#ff0000" }}
                      >
                        More
                      </Text>
                  
                    </View>
                    
                  )}
                </View>
              </View>
           
          </View>
          {this.props.navigation.state.params.event_data.tags.length != 0 ? (
            <View style={styles.fieldView}>
              <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                {this.props.navigation.state.params.event_data.tags.map(
                  (item, index) => {
                    return (
                      <View
                        key={index}
                        style={{
                          backgroundColor: "#000",
                          padding: 5,
                          borderRadius: 5,
                          minWidth: "30%",
                          marginVertical: 5,
                          marginHorizontal: 2
                        }}
                      >
                        <Text
                          style={{
                            textAlign: "center",
                            fontSize: 12,
                            color: "#fff"
                          }}
                        >
                          {item}
                        </Text>
                      </View>
                    );
                  }
                )}
              </View>
            </View>
          ) : null}
          <View style={{ width: "70%", marginTop: 10 }}>
            <Button
              label={I18n.t("see_on_map")}
              buttonStyle={styles.button}
              onPress={() => this.props.navigation.push("eventScreen",{
                lat:this.props.navigation.state.params.event_data.latitude,
                lng:this.props.navigation.state.params.event_data.longitude,

                })}
            />
          </View>
          <View style={{ width: "70%" }}>
            <Button
              label={I18n.t("navigate_to")}
              buttonStyle={styles.button}
              onPress={this.navigateGoogle}
            />
          </View>
         
        </View>

</ScrollView>
        
        <Modal visible={this.state.isModalVisible} transparent={true}>
          <ImageViewer
            onCancel={() => this._toggleModal}
            imageUrls={images}
            renderFooter={currentImage => (
              <TouchableOpacity
                style={{
                  width: deviceWidth,
                  alignContent: "center",
                  alignItems: "center",
                  paddingVertical: 15,
                  backgroundColor: "#eb6b19"
                }}
                onPress={this._toggleModal}
              >
                <Text style={{ fontSize: 22, fontWeight: "bold", color:"#fff" }}>Close</Text>
              </TouchableOpacity>
            )}
          />
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    inboxCount: state.user.inboxCount,
    notificationCount: state.user.notificationCount,
    user: state.user.userInfo,
    myToken: state.user.userToken
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}
function elevationShadowStyle(elevation) {
  return {
    elevation,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0.5 * elevation },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation
  };
}
const styles = StyleSheet.create({
  shadow1: elevationShadowStyle(5),
  shadow2: elevationShadowStyle(10),
  shadow3: elevationShadowStyle(20),
  Bigbox: {
    width: "90%",
    paddingVertical: 10,
    alignSelf: "center",
    borderRadius: 8,
    backgroundColor: "#eaeaea",
    marginTop: 30
  },
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  titlePrice: {
    fontSize: 15,
    fontWeight: "400",

    color: color.primary,
    letterSpacing: 0.5
    //paddingHorizontal: 10
  },
  title2: {
    fontSize: 13,
    fontWeight: "400",
    letterSpacing: 0.5,
    paddingVertical: 2,
    color: "#969696"
  },
  title3: {
    fontSize: 16,
    fontWeight: "400",
    paddingVertical: 6,
    color: color.primary
  },
  iconView: {},
  title1: {
    fontSize: 16,
    fontWeight: "400",

    color: "#373535"

    //paddingHorizontal: 10
  },
  fieldView: {
    flexDirection: "row",
    width: "91%",
    flexWrap: "wrap",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: "white",
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginTop: 19
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(EventDetails);
