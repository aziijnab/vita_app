import React, { Component } from 'react';
import {
    Text, View, TouchableOpacity, Image, ScrollView, TextInput,
    FlatList,
    Dimensions,
    StyleSheet
} from 'react-native';
import { color, font, apiURLs } from '../../components/Constant';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions
import doctors from '../../components/Dummydata/doctors.json';
import clinics from '../../components/Dummydata/clinics.json'
import Header from '../../components/Header';
import Icon from 'react-native-vector-icons/Ionicons'

class Doctors extends Component {


    constructor(props) {
        super(props);
        this.state = {
            clinicList: clinics,
            doctorsList: doctors,


        };
    }

    componentDidMount() {
    }

    render() {
        const { navigation } = this.props;
        var index = navigation.getParam('clinicDesired', 'NO-ID')

        return (
            <View style={styles.container}>
                <Header label='Doctor' onbackPress={() => this.props.navigation.goBack()} />

                <View style={styles.title3}>
                    <Text style={styles.title2}>{I18n.t('found')}</Text>
                    <Text style={styles.titleBold}> {this.state.doctorsList.length} </Text>

                    <Text style={styles.title2}>doctors in</Text>



                    <Text style={styles.titleBold}> {this.state.clinicList[index].name}</Text>
                </View>


                <View style={styles.list}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.doctorsList}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity /* activeOpacity={0.9} */ onPress={() => this.props.navigation.navigate('information', { clinicDesired: index })} style={styles.listStyle}>


                                <Image source={{ uri: item.image }} style={{ width: 70, height: 70, borderRadius: 70 }} resizeMode='contain' />



                                <View style={styles.details}>

                                    <Text style={{ color: '#373535', fontSize: 17, fontWeight: '600' }}>{item.name}</Text>

                                    <Text style={{ color: '#969696', fontSize: 15 }}>{item.specialization}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Icon name='ios-pin' size={16} color="#373535" />
                                        <Text style={{ color: '#373535', fontSize: 13, paddingHorizontal: 3 }}>{item.distance} km away</Text>

                                    </View>
                                </View>

                                <View style={{ flex: 1, justifyContent: "flex-start" }}>
                                    <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", paddingHorizontal: 10 }}>
                                        <View  >
                                            <Icon name='ios-star-outline' size={24} color="#3F67E6" />
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: 18, color: "#3F67E6", fontWeight: '400', paddingHorizontal: 2 }}>{item.rating}</Text>
                                        </View>
                                    </View>

                                    <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "flex-end", paddingHorizontal: 12 }}>
                                        <Icon name='md-more' size={34} color="#969696" />
                                    </View>



                                </View>

                            </TouchableOpacity>
                        )} />



                </View>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    list: {
        width: '95%',
        paddingTop: 10,
        paddingBottom: 100,
        paddingHorizontal: 3,
        alignSelf: 'center'

    },
    details: {
        paddingHorizontal: 10
    },
    listStyle: {
        marginVertical: 10,

        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'transparent',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
        shadowOffset: { height: 1, width: 1 }, // IOS
        shadowOpacity: 1, // IOS
        shadowRadius: 1, //IOS
        backgroundColor: 'white',
        elevation: 2, // Android
        //         shadowColor: "#d2d7d3",
        //         shadowOffset: {
        // 	    width: 0,
        // 	    height: 1,
        //         },
        // shadowOpacity: 0.20,
        // shadowRadius: 4.21,

        // elevation: 2,
        paddingVertical: 13,
        paddingHorizontal: 12

    },
    title2: {
        color: "#969696",
        fontSize: 16,

        textAlign: 'center',
        letterSpacing: 0.2,
        fontWeight: "400",


    },
    titleBold: {
        color: "#373535",
        fontSize: 16,

        textAlign: 'center',
        letterSpacing: 0.2,
        fontWeight: "600",
    },
    title3: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 3,
        marginTop: 5
    },
    form: {
        paddingTop: 10,
        width: "90%",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    input1: {
        marginVertical: 14,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        fontSize: 19,
        fontStyle: 'italic',
        paddingLeft: 17,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    }
});
function mapStateToProps(state, props) {
    return {
        userDetail: state.user.userDetail,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Doctors);
