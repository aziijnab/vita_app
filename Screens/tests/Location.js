import Header from "../../components/Header";
import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    FlatList,
    Alert
} from "react-native";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import clinics from '../../components/Dummydata/clinics'

class Location extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clinicData: clinics
        };
    }



    componentDidMount() {




    }

    render() {
        const { navigation } = this.props;
        var index = navigation.getParam('clinicDesired', 'NO-ID')
        console.log(index)
        return (
            <View style={styles.container}>
                <Header
                    label={I18n.t("hours_location")}
                    onbackPress={() => this.props.navigation.pop()}
                />
                <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps={"always"}>









                    <View style={styles.fieldView}>


                        <View style={styles.custom1}>
                            <Text style={{ fontSize: 22, color: "#373535" }}>
                                {I18n.t("healer_hospital")}
                            </Text>
                        </View>

                        <View style={styles.address} >
                            <Text style={{ fontSize: 16 }}>
                                {this.state.clinicData[index].Address}
                            </Text>

                        </View>

                    
                        <View style={styles.location}>
                            <Icon name='ios-pin' size={16} color={color.primary} />
                            <Text style={{ color: color.primary, fontSize: 13, paddingHorizontal: 3 }}> Directions</Text>

                        </View>
                    

                        <View style={styles.custom1}>
                            <Text style={{ fontSize: 22, color: "#373535" }}>
                                {I18n.t("bussiness_hours")}
                            </Text>
                        </View>

                        <View style={styles.address} >
                            <Text style={{ fontSize: 16 }}>
                                09:00 - 17:00, Monday to Friday
                            </Text>
                        </View>


                    </View>

                    <Button
                        label={I18n.t("book_appointment")}
                        buttonStyle={styles.button}
                        onPress={() => this.props.navigation.navigate('review', { clinicDesired: index })}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginVertical: 20

    },
    iconTop: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: color.primary,
        borderColor: 'transparent',

        alignItems: 'center',
        justifyContent: 'center'
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginTop: 20,
        marginBottom: 6
    },

    address: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginTop: 10,
        marginBottom: 10
    },

    location: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginTop: 2,
        marginBottom: 10
    },

    topView: {
        width: '90%',
        justifyContent: 'space-evenly',
        paddingVertical: 30,
        flexDirection: "row",
        alignSelf: 'center',
        alignItems: 'center'
    },
    title: {

        fontSize: 18,
        fontWeight: "400",
        letterSpacing: 0.5,

        paddingBottom: 30,
        alignSelf: "center",
        textAlign: 'center',
        color: "#373535"
    },
    title2: {
        fontSize: 20,
        fontWeight: "700",
        letterSpacing: 0.5,
        paddingTop: 6,
        paddingLeft: 30
    },
    iconView: {


    },
    title1: {
        fontSize: 15,
        fontWeight: "300",
        marginVertical: 15,
        color: "#969696",
        letterSpacing: 0.5,
        paddingHorizontal: 20
    },
    fieldView: {

        width: "94%",
        alignItems: "center",
        alignSelf: "center",
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'transparent',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal: 12,
        paddingVertical: 10,
        marginTop: 50,
        marginBottom: 3


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    header: {
        height: 50,
        width: "100%",
        justifyContent: "flex-start",
        flexDirection: "row",
        alignSelf: "center",
        borderBottomColor: "#F4AE96",
        borderBottomWidth: 1,
        marginTop: Platform.OS == "ios" ? 20 : 0,
        backgroundColor: "white"
    }
});

function mapStateToProps(state, props) {
    return {
        userDetail: state.user.userDetail
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Location);
