import Header from "../../components/Header";
import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    FlatList,
    Alert,
    Linking,

} from "react-native";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Feather";
import Button from "../../components/Button";
import clinics from '../../components/Dummydata/clinics';
import Route from "../../network/route.js";
import Spinner from 'react-native-loading-spinner-overlay';
import getDirections from "react-native-google-maps-directions";
// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

class ClinicProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clinicData: {},
            selectedPackages:'',
            loading:false,
            region: {
                latitude: 3.1390,
                longitude: 101.6869,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            }
        };
    }



    componentDidMount() {
        
        const { params } = this.props.navigation.state;
        var clinicProfileId = params.clinicProfileId
        this.setState({selectedPackages:params.selectedPackages})
        console.log("clinic profile id",clinicProfileId)
        this.setState({loading:true})
        route.getAuthenticated(`single-clinic-profile/${clinicProfileId}`,this.props.myToken).then(async(response)=>{
            if(response.error){
              console.log(response.error)
              this.setState({loading:false})
    
            }
            else {
                this.setState({loading:false})
                route.checkTokenExpire(response)
              console.log("clicnic profile",response)
              this.setState({clinicData:response.clinicProfile},()=>{
                  navigator.geolocation.getCurrentPosition(
                      (position) => {
                          console.log("wokeeey");
                          //   alert(JSON.stringify(position) );
                          let region = {
                              latitude: position.coords.latitude,
                              longitude: position.coords.longitude,
                              latitudeDelta: LATITUDE_DELTA,
                              longitudeDelta: LONGITUDE_DELTA,
                          }
                          this.setState({ region: region }, () => {
                              console.log(this.state.region)
                              
                          })

                      },
                      (error) => {
                          console.log("error postion", error)
                        //   this.getLocationDirection(clinic)
                      },
                      { enableHighAccuracy: true, timeout: 20000 },
                  )
              })
            }
        })


        



    }

    handleGetDirections(clinic){
        var userLatitude = this.state.region.latitude
        var userLongitude = this.state.region.longitude
        var clinicLatitude = parseFloat(clinic.latitude);
        var clinicLongitude = parseFloat(clinic.longitude);

        console.log("clinic direction", this.state.clinicData.latitude,this.state.clinicData.longitude)
        const data = {
            source: {
                latitude: userLatitude,
                longitude: userLongitude
            },
            destination: {
                latitude: clinicLatitude,
                longitude: clinicLongitude
            },
            params: [
                {
                    key: "travelmode",
                    value: "driving"        // may be "walking", "bicycling" or "transit" as well
                },
                {
                    key: "dir_action",
                    value: "navigate"       // this instantly initializes navigation using the given travel mode 
                }
            ]
        }

        getDirections(data)
    }

    render() {
        
        return (
            <View style={styles.container}>
                <Header
                    label={I18n.t("clinic_profile")}
                    onbackPress={() => this.props.navigation.pop()}
                />
                <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps={"always"}>

                    <View style={styles.topView}>
                        <TouchableOpacity style={styles.iconTop}>
                            <Icon name='phone-call' color='white' size={24} onPress={()=>Linking.openURL('tel:'+this.state.clinicData.mobile+'')} />
                        </TouchableOpacity>

                        <Image source={{ uri: `${this.state.clinicData.image}` }} style={{ width: 130, height: 130 }} resizeMode='contain' />


                        <TouchableOpacity style={styles.iconTop} onPress={() => this.props.navigation.navigate('message',{clinic_id:this.state.clinicData.id,clinic_name:this.state.clinicData.name})}>
                            <Icon name='message-circle' color='white' size={24} />
                        </TouchableOpacity>

                    </View>

                    <Text style={styles.title}>{this.state.clinicData.name}</Text>
                    
                    <Button
                        label={I18n.t("book_appointment")}
                        buttonStyle={styles.button}
                        onPress={() => this.props.navigation.navigate('bookAppointment',{clinicData:this.state.clinicData,selectedPackages:this.state.selectedPackages})}
                    />

                    <View style={styles.fieldView}>
                        <View style={{ flex: 1, flexDirection: "row", width: "100%", marginTop:5 }}>
                            <View style={{ flex: 1, justifyContent: "flex-start", paddingHorizontal: 2 }}>
                                <Icon

                                    name='user'
                                    color={color.primary}
                                    size={28}

                                />
                            </View>

                            <View style={{ flex: 4, justifyContent: "flex-start" }}>
                                <Text style={{ fontSize: 20, color:"#373535" }} >{I18n.t("about_us")}</Text>
                            </View>
                        </View>

                        <View style={styles.custom} >
                            <Text style={{fontSize:16}}>
                                {this.state.clinicData.about}
                            </Text>
                        </View>
                    </View>

                    <View style={styles.fieldView}>
                        <View style={{ flex: 1, flexDirection: "row", width: "100%", marginTop:5 }}>
                            <View style={{ flex: 1, justifyContent: "flex-start", paddingHorizontal: 2 }}>
                                <Icon

                                    name='home'
                                    color={color.primary}
                                    size={30}

                                />
                            </View>

                            <View style={{ flex: 4, justifyContent: "flex-start" }}>
                                <Text style={{ fontSize: 20, color:"#373535" }} >{I18n.t("open_hours")}</Text>
                            </View>
                        </View>

                        <View style={styles.custom1}>
                            <Text style={{ fontSize: 18, color:"#373535"}}>
                                {I18n.t('address')}
                                </Text>
                        </View>

                        <View style={styles.address} >
                            <Text style={{fontSize:16}}>
                                {this.state.clinicData.address}
                            </Text>
                        </View>

                        <TouchableOpacity style={styles.location} onPress={() => this.handleGetDirections(this.state.clinicData)}>
                            <Icon name='map-pin' size={16} color={color.primary} />
                            <Text style={{ color: color.primary, fontSize: 16, paddingHorizontal: 3 }}> {I18n.t('directions')}</Text>

                        </TouchableOpacity>

                            <View style={styles.custom1}>
                            <Text style={{ fontSize: 18, color:"#373535"}}>
                            {I18n.t("bussiness_hours")}
                                </Text>
                        </View>

                        <View style={styles.address} >
                            <Text style={{fontSize:16}}>
                               {this.state.clinicData.open_hours} : {this.state.clinicData.avail}
                            </Text>
                        </View>

                            <View style={styles.custom1}>
                            <Text style={{ fontSize: 18, color:"#373535"}}>
                            {I18n.t("phone")}
                                </Text>
                        </View>

                        <View style={styles.address} >
                            <Text style={{fontSize:16}}>
                                {this.state.clinicData.mobile}
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginVertical:20

    },
    iconTop: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: color.primary,
        borderColor: 'transparent',

        alignItems: 'center',
        justifyContent: 'center'
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginTop:20,
        marginBottom:5
    },

    address: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginTop:5,
        marginBottom:10
    },
    topView: {
        width: '90%',
        justifyContent: 'space-evenly',
        paddingVertical: 30,
        flexDirection: "row",
        alignSelf: 'center',
        alignItems: 'center'
    },
    title: {

        fontSize: 18,
        fontWeight: "500",
        letterSpacing: 0.5,

        paddingBottom: 30,
        alignSelf: "center",
        textAlign: 'center',
        color: "#373535",
        marginBottom: 20
    },
    title2: {
        fontSize: 20,
        fontWeight: "700",
        letterSpacing: 0.5,
        paddingTop: 6,
        paddingLeft: 30
    },
    iconView: {


    },
    title1: {
        fontSize: 15,
        fontWeight: "300",
        marginVertical: 15,
        color: "#969696",
        letterSpacing: 0.5,
        paddingHorizontal: 20
    },
    fieldView: {

        width: "94%",
        alignItems: "center",
        alignSelf: "center",
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'transparent',
        borderBottomWidth: 0,
        backgroundColor:'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal: 12,
        paddingVertical: 10,
        marginTop: 20,
        marginBottom: 10


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    location: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginTop: 2,
        marginBottom: 10
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    header: {
        height: 50,
        width: "100%",
        justifyContent: "flex-start",
        flexDirection: "row",
        alignSelf: "center",
        borderBottomColor: "#F4AE96",
        borderBottomWidth: 1,
        marginTop: Platform.OS == "ios" ? 20 : 0,
        backgroundColor: "white"
    }
});

function mapStateToProps(state, props) {
    return {
        user: state.user.userInfo,
        myToken: state.user.userToken
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ClinicProfile);
