import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    FlatList,
    Alert,
    KeyboardAvoidingView,
    Platform,
    ToastAndroid
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { Dropdown } from 'react-native-material-dropdown';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import recommendedPackages from "../../components/Dummydata/recommendedPackages.json";
import addOns from "../../components/Dummydata/addOns.json";
import Header from "../../components/Header";
import Route from "../../network/route.js";
import moment, { now } from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast'
import IPay88, { Pay } from "ipay88-sdk";
import Config from 'react-native-config'

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

class PaymentMethod extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paymentMethod: "",
            appointmentDate :"",
            appointmentTime:"",
            clinicDetail:"",
            appointmentTotalPrice:null,
            loading:false,
            appointmentId:null
            
        };

    }

    successNotify = data => {
        let paymentData = {appointment_id:this.state.appointmentId,ref_no:data.referenceNo,transaction_id:data.transactionId,amount:data.amount,status:1}
        console.log("payment data",paymentData)
        route.updateData('save-ipay-transaction',paymentData,this.props.myToken).then(async response => {
            if (response.error) {
                console.log(response.error)
                this.props.stopLoading();
                this.setState({loading:false})
                Alert.alert("Error", JSON.stringify(response.error));
            } else {
                console.log("Payment APi");
                console.log(response);
                this.setState({loading:false})
                this.props.navigation.navigate("appointmentScreen");

            }
        });
      };
    
      cancelNotify = data => {
        const { transactionId, referenceNo, amount, remark, error } = data;
        this.setState({loading:false})
        if (Platform.OS === "ios") {
          Alert.alert("Cancel Message", `${error}`, { cancelable: true });
        } else {
            this.refs.toast.show(`Cancel Message: ${error}`,DURATION.SHORT);
        }
      };
    
      failedNotify = data => {
        this.setState({loading:false})
        const { transactionId, referenceNo, amount, remark, error } = data;
    
        if (Platform.OS === "ios") {
          Alert.alert("failed Message", `${error}`, { cancelable: true });
        } else {
          this.refs.toast.show(` failed Message: ${error}`,DURATION.SHORT);
        }
      };
      replaceSpaceToUnderScore(string) {
        let lowerCaseString = string.toLowerCase();
        let underScoreString = lowerCaseString.split(" ").join("_");
        let brackitLess = underScoreString.split("(").join("");
        let brackitLess2 = brackitLess.split(")").join("");
        let removeQuestionMark = brackitLess2.split("?").join("");
        let removeQoma = removeQuestionMark.split(",").join("");
        let removeDot = removeQoma.split(".").join("");
        let removeColon = removeDot.split(":").join("");
        let removeSemiColon = removeColon.split(";").join("");
        let singleUnderScore = removeSemiColon.split("-").join("_");
        singleUnderScore = singleUnderScore.split("__").join("_");
        singleUnderScore = singleUnderScore.split("/").join("_");
        var newString = singleUnderScore;
        console.log("string length", newString.length);
    
        if (singleUnderScore.length > 57) {
          let cutLength = 57 - singleUnderScore.length;
          newString = singleUnderScore.substring(0, 57);
          console.log("new sting with cut", newString);
        }
        console.log("under score string", newString);
        return newString;
      }


    componentDidMount() {
        console.log("user info",this.props.user)
        const { params } = this.props.navigation.state;
        this.setState({
            appointmentDate:params.appointmentDate,
            clinicDetail:params.clinicDetail,
            appointmentTime:params.appointmentTime,
            // appointmentEndTime: params.appointmentEndTime,
            appointmentTotalPrice:this.props.appointmentTotalPrice
        })
        console.log(params)
     }

     pay = (referenceNo) => {
        try {
          const data = {};
          data.paymentId = "198"; // refer to ipay88 docs
          data.merchantKey = `${Config.ipay88_merchant_key}`;
          data.merchantCode = `${Config.ipay88_merchant_code}`;
          data.referenceNo = referenceNo;
          data.amount = `${this.state.appointmentTotalPrice}`;
          data.currency = "MYR";
          data.productDescription = "Appointment";
          data.userName = `${this.props.user.user_profile.full_name}`;
          data.userEmail = `${this.props.user.email}`;
          data.userContact = `${this.props.user.user_profile.mobile}`;
          data.utfLang = "UTF-8";
          data.country = "MY";
          data.remark = "Appointment";
          data.backendUrl = `${Config.ipay88_backendUrl}`;
          console.log('payment data',data)
          const errs = Pay(data);
          if (Object.keys(errs).length > 0) {
            console.log(errs);
          }
        } catch (e) {
          console.log(e);
        }
      };
      saveAppointmentToApi(){
        console.log("packagespackages on appoientmend",this.props.userSelectedPackages)
        var errorMessages = []
        if(this.state.paymentMethod == ""){
            errorMessages.push(I18n.t("please_select_your_payment_method"))
        }
        if(this.state.paymentMethod == "iPay88"){
            if(this.props.userSelectedPackages.length == 0){
                errorMessages.push(I18n.t("please_select_any_package"))
            }
        }
        if(errorMessages.length == 0){
            var appointmentDetail = {
                clinic_id:this.state.clinicDetail.id,
                time: moment(this.state.appointmentTime, "hh:mm A").format("hh:mm A"),
                // endTime: moment(this.state.appointmentEndTime, "hh:mm A").format("HH:mm:ss"),
                date:this.state.appointmentDate,
                payment_method:this.state.paymentMethod,
                amount:this.state.appointmentTotalPrice,
                packages_id:this.props.userSelectedPackages.toString()
               }
               this.setState({loading:true})
               console.log("appointment detail on submit",appointmentDetail)
           route.updateData('user-appointment',appointmentDetail,this.props.myToken).then(async response => {
               if (response.error) {
                   console.log(response.error)
                   this.props.stopLoading();
                   this.setState({loading:false})
                   if(response.error == "Budget not available to book this appointment."){
                    this.refs.toast.show(I18n.t(this.replaceSpaceToUnderScore(response.error)),DURATION.SHORT);
                   }else{
                    Alert.alert("Error", JSON.stringify(response.error));
                   }
               } else {
                   console.log("appointment");
                   console.log(response);
                   this.setState({loading:false})
                   route.checkTokenExpire(response)
                   if(this.state.paymentMethod == "Cash"){
                    this.props.navigation.navigate("appointmentScreen");
                }
                if(this.state.paymentMethod == "iPay88"){
                    let appointmentId = response.appointment.id
                    this.setState({appointmentId,appointmentId},()=>{
                        let referenceNo = `${now()}${appointmentId} `
                        this.pay(referenceNo)
                    })
                    
                }
                   
               }
           });
         }else{
            let errorArray = errorMessages.join("\n")
            this.refs.toast.show(errorArray,DURATION.SHORT);
         }
      }
     appointmentSubmit(){
        this.saveAppointmentToApi();
     }

    render() {
        let paymentMethod = [
            { label: I18n.t('cash'), value: "Cash" },
            { label: I18n.t('credit_card___online_banking'), value: "iPay88" },
        ]
        return (
            <View style={styles.container}>
                <Header
                    label={I18n.t('payment_method')}
                    onbackPress={() => this.props.navigation.pop()}
                />
                <Spinner
                    visible={this.state.loading}
                    textContent={`${I18n.t('payment_method')}...`}
                    textStyle={{color:color.primary}}
                    cancelable={true}
                    color="#F16638"
                    />
                <Toast
                    ref="toast"
                    style={{backgroundColor:'red'}}
                    position='top'
                    positionValue={80}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    textStyle={{color:'white'}}
                />
                <ScrollView keyboardShouldPersistTaps={"always"}>

                    <Text style={styles.title}>{I18n.t("select_your")}</Text>
                    <Text style={styles.title2}>{I18n.t("payment_method")}</Text>

                    <Dropdown
                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                        onChangeText={(itemValue, itemIndex) =>
                            this.setState({ paymentMethod: itemValue })}
                        labelFontSize={15}
                        rippleOpacity={0.10}
                        dropdownMargins={{ min: 0, max: 0 }}
                        //temPadding={1}
                        //inputContainerStyle={{ height: 50, width: "100%" }}
                        //pickerStyle={{ paddingVertical: 6 }}
                        pickerStyle={{ paddingHorizontal: 40 }}
                        textColor={"#969696"}
                        dropdownOffset={{ top: 4, left: 0 }}
                        containerStyle={styles.pickerSt}
                        data={paymentMethod}
                        overlayStyle={{ borderColor: 'transparent', borderWidth: 1 }}
                        value={I18n.t('select_method')}
                        baseColor="#576574"
                    />

                    {/* <View style={styles.custom}>
                        <Text style={{ color: "#969696", textAlign: "center" }}>If you select online payment, you will be entitled to 5% discount </Text>
                    </View> */}
                    <IPay88
                        successNotify={this.successNotify}
                        failedNotify={this.failedNotify}
                        cancelNotify={this.cancelNotify}
                    />
                    <Button
                        label={I18n.t("done")}
                        buttonStyle={styles.button}
                        onPress={() => this.appointmentSubmit()}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        paddingHorizontal: 20,
        marginVertical: 30,
        justifyContent: "flex-start",
        alignSelf: "center"
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        justifyContent: "space-around"
    },
    title: {

        fontSize: 25,
        fontWeight: "700",
        letterSpacing: 0.5,
        paddingTop: "4%",
        marginVertical: 15,
        paddingLeft: 20,
        color: "#969696",
        textAlign: "center"
    },
    title2: {
        fontSize: 25,
        fontWeight: "700",
        letterSpacing: 0.5,
        paddingLeft: 20,
        color: "#969696",
        textAlign: "center"
    },
    iconView: {


    },
    title1: {
        fontSize: 15,
        fontWeight: "300",
        marginVertical: 15,
        color: "#969696",
        letterSpacing: 0.5,
        paddingHorizontal: 10
    },
    fieldView: {

        flexDirection: "row",
        width: "100%",
        alignItems: "center",
        borderWidth: 1,
        borderRadius: 2,
        borderColor: 'transparent',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal: 8,
        paddingVertical: 10,
        marginTop: 12,
        marginBottom: 3


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 30,
        width: "90%",
        flexDirection: "row",
        paddingVertical: 25,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9",
        alignSelf: "center"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    pickerSt: {
        marginVertical: 17,
        width: "90%",
        alignSelf: "center",
        //flexDirection: "row",
        paddingTop: 6,
        paddingHorizontal: 15,
        justifyContent: 'center',
        //alignItems: "center",
        //paddingLeft: 19,
        borderRadius: 27,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white",


        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    header: {
        height: 50,
        width: "100%",
        justifyContent: "flex-start",
        flexDirection: "row",
        alignSelf: "center",
        borderBottomColor: "#F4AE96",
        borderBottomWidth: 1,
        marginTop: Platform.OS == "ios" ? 20 : 0,
        backgroundColor: "white"
    }
});

function mapStateToProps(state, props) {
    return {
        user: state.user.userInfo,
        myToken: state.user.userToken,
        questionAnswer: state.user.userQuestionAnswer,
        userSelectedPackages:state.user.selectUserPackages,
        appointmentTotalPrice:state.user.appointmentTotalPrice,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PaymentMethod);
