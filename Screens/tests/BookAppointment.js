import Header from "../../components/Header";
import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    FlatList,
    Alert
} from "react-native";
import DateTimePicker from 'react-native-modal-datetime-picker';

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Feather";
import Button from "../../components/Button";
import clinics from '../../components/Dummydata/clinics';
import moment, { now } from 'moment';
import Toast, {DURATION} from 'react-native-easy-toast'

import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

class BookAppointment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clinicData: {},
            isDateTimePickerVisible: false,
            time: "00:00",
            date: '',
            selectedPackages:'',
            isStartTimeClick:false,
            isEndTimeClick:false,

        };
    }

    _showStartDateTimePicker = () => this.setState({ isDateTimePickerVisible: true});
    // _showEndDateTimePicker = () => this.setState({ isDateTimePickerVisible: true, isEndTimeClick: true, isStartTimeClick:false});
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        moment.locale('en-gb');
        console.log('A date has been picked: ', date);
        var time = moment(date, "HH:mm:ss").format("hh:00 A");
        //time = hours + ":" + minutes
        this._hideDateTimePicker();
        console.log("set time",time)
            this.setState({
                time: time
            }); 
    };


    componentDidMount() {
        moment.locale('en-gb');
        const { params } = this.props.navigation.state;
        this.setState({clinicData:params.clinicData})
        this.setState({selectedPackages:params.selectedPackages})
        console.log("selectedPackages",this.state.selectedPackages)
    }

    bookNowClick(){
        moment.locale('en-gb');
        var time = moment(this.state.time, "HH:mm A").format("HH");
        // var endTime = moment(this.state.endTime, "HH:mm A").format("HH");
        console.log("start time",time)
        var userStartDayOrNight = moment(this.state.time, "HH:mm A").format("A");
        // var userEndDayOrNight = moment(this.state.endTime, "HH:mm A").format("A");
        var clinicOpeningHours = this.state.clinicData.open_hours.split(" to ");
        var clinicStartHours = moment(clinicOpeningHours[0], "HH:mm A").format("HH")
        var clinicEndHours = moment(clinicOpeningHours[1], "HH:mm A").format("HH")
        var clinicStartDayOrNight = moment(clinicOpeningHours[0], "HH:mm A").format("A");
        var clinicEndDayOrNight = moment(clinicOpeningHours[1], "HH:mm A").format("A");
        console.log("open hours", clinicStartHours, clinicEndHours, clinicStartDayOrNight, clinicEndDayOrNight);
        
        console.log(" Time", time, clinicStartHours, clinicEndHours, userStartDayOrNight, clinicStartDayOrNight ,)
        // alert(parseInt(startTime) - parseInt(endTime));
        if (time >= clinicStartHours && time <= clinicEndHours) {
            var errorMessages = []
            if (this.state.date == "") {
                errorMessages.push(`${I18n.t('please_select_your_appointment_date')}`)
            }
            if (this.state.time == "00:00") {
                errorMessages.push(`${I18n.t('please_select_your_appointment_time')}`)
            }
            if (errorMessages.length == 0) {
                this.props.navigation.navigate('appointmentDetails', {
                    appointmentDetail: {
                        clinicData: this.state.clinicData,
                        date: this.state.date,
                        time: this.state.time,
                        selectedPackages: this.state.selectedPackages
                    }
                })
            } else {
                let errorArray = errorMessages.join("\n")
                this.refs.toast.show(errorArray, DURATION.SHORT);
            }
        }else{
            this.refs.toast.show(I18n.t("please_add_your_time_between_clinic_hours"), DURATION.SHORT);
        }


            
        
        
    }

    render() {
        var newDate = this.state.date;
        return (

            <View style={styles.container}>
                <Header
                    label={I18n.t('book_appointment')}
                    onbackPress={() => this.props.navigation.pop()}
                />
                <Toast
                    ref="toast"
                    style={{backgroundColor:'red'}}
                    position='top'
                    positionValue={80}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    textStyle={{color:'white'}}
                />
                <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps={"always"}>

                    <View style={styles.topView}>


                        <Image source={{uri : `${this.state.clinicData.image}`}} style={{ width: 130, height: 130 }} resizeMode='contain' />


                    </View>

                    <View style={styles.fieldView}>

                        <Text style={styles.title}>{this.state.clinicData.name}</Text>
                            
                        <Calendar

                        markedDates={{[this.state.date]: {selected: true, disableTouchEvent: true, selectedDotColor: color.primary}}}
                            // Specify style for calendar container element. Default = {}
                            style={{
                                height: 350,
                                width: "100%"
                            }}
                            // Specify theme properties to override specific styles for calendar parts. Default = {}
                            theme={{
                                backgroundColor: '#ffffff',
                                calendarBackground: '#ffffff',
                                textSectionTitleColor: color.primary,
                                selectedDayBackgroundColor: color.primary,
                                selectedDayTextColor: '#ffffff',
                                todayTextColor: color.primary,
                                dayTextColor: '#2d4150',
                                arrowColor: 'orange',
                                monthTextColor: '#969696',
                                // textDayFontFamily: 'monospace',
                                // textMonthFontFamily: 'monospace',
                                // textDayHeaderFontFamily: 'monospace',
                                textMonthFontWeight: 'bold',
                                textDayFontSize: 16,
                                textMonthFontSize: 16,
                                textDayHeaderFontSize: 16,
                                // 'stylesheet.calendar.header': {
                                //     week: {
                                //         backgroundColor:color.primary
                                //     //   marginTop: 5,
                                //     //   flexDirection: 'row',
                                //     //   justifyContent: 'space-between'
                                //     }
                                //   }
                            }}
                            // Initially visible month. Default = Date()

                            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                            minDate={now}
                            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                            // Handler which gets executed on day press. Default = undefined
                            onDayPress={(day) => {
                                this.setState({ date: moment(day.dateString).format('YYYY-MM-DD') })
                            }}
                            // Handler which gets executed on day long press. Default = undefined
                            //onDayLongPress={(day) => { console.log('selected day',day) }}
                            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                            monthFormat={'MMMM yyyy'}
                            // Handler which gets executed when visible month changes in calendar. Default = undefined
                            onMonthChange={(month) => { console.log('month changed', month) }}
                            // Hide month navigation arrows. Default = false
                            hideArrows={false}
                            // Replace default arrows with custom ones (direction can be 'left' or 'right')
                            //renderArrow={(direction) => (<Arrow />)}
                            // Do not show days of other months in month page. Default = false
                            hideExtraDays={true}
                            // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                            // day from another month that is visible in calendar page. Default = false
                            disableMonthChange={false}
                            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                            firstDay={1}
                            // Hide day names. Default = false
                            hideDayNames={false}
                            // Show week numbers to the left. Default = false
                            showWeekNumbers={false}
                            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                            onPressArrowLeft={substractMonth => substractMonth()}
                            // Handler which gets executed when press arrow icon left. It receive a callback can go next month
                            onPressArrowRight={addMonth => addMonth()}




                        />

                    </View>

                    <TouchableOpacity
                        onPress={this._showStartDateTimePicker}
                    >
                        <View style={styles.fieldView}>
                            <View style={{ flex: 1, flexDirection: "row", width: "100%" }}>
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
                                    <Text style={styles.title1}>{I18n.t("time")}</Text>
                                </View>
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
                                    <View style={{ paddingHorizontal: 2 }}>
                                        <Text style={styles.time}>{this.state.time}</Text>
                                    </View>


                                    <View >
                                        <Icon

                                            name="chevron-right"
                                            color="#969696"
                                            size={28}


                                        />
                                    </View>


                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>


                    {/* <TouchableOpacity
                        onPress={this._showEndDateTimePicker}
                    >
                        <View style={styles.fieldView}>
                            <View style={{ flex: 1, flexDirection: "row", width: "100%" }}>
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", width: "100%" }}>
                                    <Text style={styles.title1}>{I18n.t("appoinment_end_time")}</Text>
                                </View>
                                <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", width: "100%", alignItems: "center" }}>
                                    <View style={{ paddingHorizontal: 2 }}>
                                        <Text style={styles.time}>{this.state.endTime}</Text>
                                    </View>


                                    <View >
                                        <Icon

                                            name="chevron-right"
                                            color="#969696"
                                            size={28}


                                        />
                                    </View>


                                </View>
                            </View>
                        </View>
                    </TouchableOpacity> */}

                    <DateTimePicker
                        mode={'time'}
                        minuteInterval={60}
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        is24Hour={false}
                        datePickerModeAndroid={'spinner'}
                    />
                    

                    <Button
                        label={I18n.t('book_now')}
                        buttonStyle={styles.button}
                        onPress={() => this.bookNowClick()}
                    />

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginVertical: 20

    },
    iconTop: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: color.primary,
        borderColor: 'transparent',

        alignItems: 'center',
        justifyContent: 'center'
    },
    custom1: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginTop: 20,
        marginBottom: 6
    },

    address: {
        flexDirection: "row",
        width: "90%",
        flex: 1,
        justifyContent: "flex-start",
        marginTop: 10,
        marginBottom: 10
    },
    topView: {
        width: '90%',
        justifyContent: 'space-evenly',
        paddingVertical: 30,
        flexDirection: "row",
        alignSelf: 'center',
        alignItems: 'center'
    },
    title: {

        fontSize: 18,
        fontWeight: "400",
        letterSpacing: 0.5,

        paddingBottom: 30,
        alignSelf: "center",
        textAlign: 'center',
        color: "#373535"
    },
    title2: {
        fontSize: 20,
        fontWeight: "700",
        letterSpacing: 0.5,
        paddingTop: 6,
        paddingLeft: 30
    },
    iconView: {


    },
    title1: {
        fontSize: 18,
        fontWeight: "300",
        marginVertical: 15,
        color: "#696969",
        letterSpacing: 0.5,
        paddingHorizontal: 20
    },

    time: {
        fontSize: 18,
        fontWeight: "300",
        marginVertical: 15,
        color: color.primary,
        letterSpacing: 0.5,
        paddingHorizontal: 20
    },

    fieldView: {
        width: "94%",
        alignItems: "center",
        alignSelf: "center",
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'transparent',
        borderBottomWidth: 0,
        backgroundColor:"white",
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        paddingHorizontal: 12,
        paddingTop: 10,
        marginTop: 20,
        marginBottom: 3


    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 17,
        width: "40%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        flex: 1,
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: "90%",
        justifyContent: "space-between",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white",
        marginBottom:20,
      /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    header: {
        height: 50,
        width: "100%",
        justifyContent: "flex-start",
        flexDirection: "row",
        alignSelf: "center",
        borderBottomColor: "#F4AE96",
        borderBottomWidth: 1,
        marginTop: Platform.OS == "ios" ? 20 : 0,
        backgroundColor: "white"
    }
});

function mapStateToProps(state, props) {
    return {
        userDetail: state.user.userDetail
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BookAppointment);
