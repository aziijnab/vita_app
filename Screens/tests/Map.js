import React, { Component } from 'react';
import {
  Text, View,TouchableOpacity,Image,ScrollView,TextInput,Animated,
  FlatList,
  Dimensions,
  StyleSheet,
  Linking,
  PermissionsAndroid,
  Platform
} from 'react-native';
IMAGE = 'http://melissa.depperfamily.net/blog/wp-content/uploads/2014/09/emergency-clipart-12422396261783581009Cruz_Roja.svg_.med_-300x300.png';
import {color, font, apiURLs} from '../../components/Constant';
import mapoverlay from '../../components/mapStyle/mapoverlay';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions
import MapView from 'react-native-maps';
import IconHeader from '../../components/IconHeader';
import sortByDistance from "sort-by-distance";
const { width, height }  = window
import Geolocation from 'react-native-geolocation-service';
const WIDTH  = Dimensions.get('window').width;
const HEIGHT  = Dimensions.get('window').height;
LATITUDE_DELTA = 0.0122
const ASPECT_RATIO = WIDTH / HEIGHT;

 LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import Header from '../../components/Header';
import Icon from 'react-native-vector-icons/Feather';
import clinics from '../../components/Dummydata/clinics';
import Route from "../../network/route.js";
// const route = new Route("https://vita-health.herokuapp.com/api/");
import Spinner from "react-native-loading-spinner-overlay";
import I18n from "../../i18n/i18n";
import geolib from "geolib";
import underscore from "underscore";
const route = new Route("http://192.168.100.18:8000/api/");
import { NavigationEvents } from "react-navigation";

export async  function requestLocationPermission() 
{
    try {
        const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        if (granted) {
          console.log("You can use the location")
        } else {
          console.log("location permission denied")
          console.log(granted)
        }
      } catch (err) {
        console.warn(err)
      }
}

class Map extends Component {


    constructor(props) { 
        super(props);
        this.state = {
           clinicList: [],
           filterText: '',
           nearClinics:null,
           selectedPackages:'',
            placeStore:[],
           loading: false,
           region:{
            latitude: 3.1390,
            longitude: 101.6869,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
           }
        };
        
    }
       sortClinicByDistance(responseClinics){
           var clinics = []
           responseClinics.map((value, index) => {
             let distance = geolib.getDistance({ latitude: this.state.region.latitude, longitude: this.state.region.longitude }, { latitude: value.latitude, longitude: value.longitude });

             value.distance = distance / 1000;
             clinics.push(value);
           });

           var sortedClinicsByDistanc = underscore.sortBy(clinics, 'distance')
           console.log("geo distanc", sortedClinicsByDistanc);

           return sortedClinicsByDistanc
           
       }

        async fetchClinics(){
           this.props.startLoading()
           this.setState({loading:true})
           route.getAuthenticated('all-clinics?search-keyword=&packages=/' + this.props.userSelectedPackages, this.props.myToken).then(async (response) => {
               if (response.error) {
                   console.log(response.error)
                   this.props.startLoading()
                   this.setState({ loading: false })
               }
               else {

                   var sortedClinics = this.sortClinicByDistance(response.clinics)
                   sortedClinics.map((element, i) => {
                       console.log("this state clinicList", this.state.clinicList)
                       this.state.placeStore.push(
                           <MapView.Marker
                               key={i}
                               title={element.name}
                               description={element.about}
                               onPress={() => console.log(i)}
                               tracksViewChanges={false}

                               coordinate={{
                                   latitude: parseFloat(element.latitude),
                                   longitude: parseFloat(element.longitude),
                               }}
                           >
                               <Image source={{ uri: `${element.image}` }} style={{ width: 60, height: 60, borderRadius: 30, borderWidth: 2, borderColor: color.primary }} />

                           </MapView.Marker>
                       )
                   })
                   this.setState({ clinicList: sortedClinics })
                   this.setState({ loading: false })
                   console.log("clicnics list", this.state.clinicList)
                   console.log("clicnics playStor", this.state.nearClinics);
               }
           })
       }

       

         fetchAllClinics(){
            placeStore = [];
           this.setState({ loading: true })
           Geolocation.getCurrentPosition(
             position => {
               console.log("wokeeey");
               //   alert(JSON.stringify(position) );
               let region = {
                 latitude: position.coords.latitude,
                 longitude: position.coords.longitude,
                 latitudeDelta: LATITUDE_DELTA,
                 longitudeDelta: LONGITUDE_DELTA
               };

                this.state.placeStore.push(
                 <MapView.Marker
                   title={"You"}
                    tracksViewChanges={false}
                   //onPress={()=> console.warn('data:',this.state.markers[i],'index:',[i])}
                   //onPress={()=> this._toggleSubview(this.state.markers[i])}

                   coordinate={{
                     latitude: parseFloat(position.coords.latitude),
                     longitude: parseFloat(position.coords.longitude)
                   }}
                 >
                   
                 </MapView.Marker>
               );

               

               this.setState({ region: region }, () => {
                 console.log(this.state.region);
                 this.setState({ loading: false });
                  this.fetchClinics();
               });
             },
             error => {
               console.log("error postion", error);
               this.setState({ loading: false });
                this.fetchClinics();
             },
             {
               enableHighAccuracy: true,
               timeout: 20000,
               maximumAge: 1000
             }
           );
           

       }



        async hasLocationPermission () {
            if (Platform.OS === 'ios' ||
                (Platform.OS === 'android' && Platform.Version < 23)) {
            return true;
            }

            const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            );

            if (hasPermission) return true;

            const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            );

            if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

            if (status === PermissionsAndroid.RESULTS.DENIED) {
            ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
            } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
            }

            return false;
        }
     async componentDidMount() {
        await this.hasLocationPermission()
       await this.fetchAllClinics()
        
    }

    handlemapPress(item){
        console.log(item)
        var newRegion= {
            latitude:parseFloat(item.latitude),
            latitudeDelta: LATITUDE_DELTA,
            longitude:parseFloat(item.longitude),
            longitudeDelta:LONGITUDE_DELTA,
           
          }
        //   this.map.animateToRegion(newRegion, 5000)
        this.setState({region:newRegion})
    }
  render() {
   
    return (
        <View style={styles.container}>
            <NavigationEvents
                onDidFocus={() => this.fetchAllClinics()}
            />
            <IconHeader label={I18n.t("maps")} onbackPress={()=>this.props.navigation.goBack()} onIconPress={()=>this.props.navigation.navigate('clinics',{selectedPackages:this.state.selectedPackages})} iconRight='md-search' />
            {/* <Header label='Maps' onbackPress={()=>this.props.navigation.goBack()} /> */}
            <Spinner
                visible={this.state.loading}
                textContent={`${I18n.t('please_wait')}...`}
                textStyle={{ color: color.primary }}
                cancelable={true}
                color="#F16638"
            />
            <View style={{flex:1}}>
            <MapView 
                region={this.state.region}
                ref={ map => { this.map = map }}
                onRegionChangeComplete={(newregion)=>this.setState({region:newregion})}
                style={styles.map}>
                {this.state.placeStore}
            </MapView>
            <View style={styles.tileWrapper}>
            <FlatList
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                data={this.state.clinicList}
                horizontal={true}
                renderItem={({ item }) => (
                <TouchableOpacity onPress={()=>this.handlemapPress(item)} activeOpacity={0.9} style={styles.tileStyle}>
                <View style={styles.tilebody}>

                            <Image source={{ uri: `${item.image}` }} style={{width:70,height:70}} resizeMode='contain' />
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('clinicprofile',{clinicProfileId:item.id})}>
                    <Text numberOfLines={1} style={{color:'#373535',paddingVertical:3,fontSize:15}}>{item.name}</Text>
                </TouchableOpacity>
                
                <Text numberOfLines={1} style={{color:'#969696',paddingVertical:3,fontSize:13}}>{item.type}</Text>

                </View>
                <View style={styles.tilefooter}>
                    <TouchableOpacity onPress={()=>Linking.openURL('tel:'+item.mobile+'')} style={styles.innerfooter1}>
                        <Icon name='phone-call' size={18} color={color.primary} />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.innerfooter} onPress={() => this.props.navigation.navigate('message',{clinic_id:item.id,clinic_name:item.name})}>
                        <Icon name='message-circle' size={18} color={color.primary} />
                    </TouchableOpacity>

                </View>

                </TouchableOpacity>

            )} />
            </View>
            </View>

        </View>
        );
  }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },
    innerfooter1:{
        flex:1,
        width:'100%',
        alignItems:"center",
        justifyContent:'center',
        padding:5,
        borderRightColor:'#969696',
        borderRightWidth:0.5,
        

    },
    innerfooter:{
        flex:1,
        width:'100%',
        alignItems:"center",
        justifyContent:'center',
        padding:5,
        

    },

    tilefooter:{
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        borderTopColor:'#969696',
        borderTopWidth: 0.5  
    },
    tilebody:{
    width:'100%',
    flexWrap:'wrap',
    paddingHorizontal:16,
    paddingVertical:17,
    alignItems:'center'
    },
    tileWrapper:{
        position:'absolute',
        bottom:0,
        flex:1,
        marginBottom :10,
        paddingLeft:16,
        paddingBottom:20
    },
    tileStyle:{
        //height:HEIGHT/2.3,
        flexWrap:'wrap',
        width:WIDTH/2.5,
        //width:'39%',
        backgroundColor:'white',
        alignItems:"center",
        borderRadius:8,
        borderWidth:1,
        borderColor:'transparent',
        marginHorizontal:4,
    },  
    map:{
        top:0,
        left:0,
        right:0,
        bottom:0,
        position:'absolute'
    },
    list:{
        width:'95%',
        paddingVertical:10,
        paddingHorizontal:3,
        alignSelf:'center'

    },
    details:{
        paddingHorizontal:10
    },
    listStyle:{
        marginVertical:10,
        
        flexDirection:'row',
        alignItems:'center',
        width:'100%',
        borderRadius:10,
        borderWidth:1,
        borderColor:'transparent',
        shadowColor: "#d2d7d3",
shadowOffset: {
	width: 0,
	height: 1,
},
shadowOpacity: 0.20,
shadowRadius: 4.21,

elevation: 2,
paddingVertical:13,
paddingHorizontal:12

    },
    title2: {
        color: "#969696",
        fontSize: 16,
       
        textAlign:'center',
        letterSpacing: 0.2,
        fontWeight: "400",
        
       
    },
    titleBold:{
        color: "#373535",
        fontSize: 16,
       
        textAlign:'center',
        letterSpacing: 0.2,
        fontWeight: "400",
    },
    title3:{
       flexDirection:'row',
       alignItems:'center',
        alignSelf:'center',
        paddingVertical:3
    },
    form: {
        paddingTop: 10,
        width: "90%",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
      },
      input1: {
        marginVertical: 14,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        fontSize:19,
        fontStyle:'italic',
        paddingLeft: 17,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
      }
});
function mapStateToProps(state, props) {
    return {
        user: state.user.userInfo,
        myToken: state.user.userToken
    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}
  
export default connect(mapStateToProps, mapDispatchToProps)(Map);
