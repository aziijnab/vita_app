import React, { Component } from 'react';
import {
  Text, View,TouchableOpacity,Image,ScrollView,TextInput,
  FlatList,
  Dimensions,
  StyleSheet
} from 'react-native';
import {color, font, apiURLs} from '../../components/Constant';
import {bindActionCreators} from 'redux';
import I18n from "../../i18n/i18n";
import Button from "../../components/Button";
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions
import clinics from '../../components/Dummydata/clinics.json';
import reviews from '../../components/Dummydata/reviews.json'
import Header from '../../components/Header';
import Icon from 'react-native-vector-icons/Ionicons'

class Review extends Component {


    constructor(props) { 
        super(props);
        this.state = {
           clinicList: clinics,
           reviews: reviews
        };
    }

    componentDidMount() {
    }
    
  render() {
    const { navigation } = this.props;
    var index = navigation.getParam('clinicDesired', 'NO-ID')    

    return (
        <View style={styles.container}>
            <Header label={I18n.t("review")} onbackPress={()=>this.props.navigation.goBack()}  />
            
            

          <View style={styles.list}>
          <FlatList
                showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.reviews}
            renderItem={({ item,index }) => (
            <TouchableOpacity /* activeOpacity={0.9} */  style={styles.listStyle}>
                
                <View style = {{flex:1, flexDirection:"row"}}> 
                <Image source={{uri:item.image}} style={{width:70,height:70, borderRadius:70}} resizeMode='contain'  />
                
                

                <View style={styles.details}>
                
                <Text style={{color:'#373535',fontSize:17,fontWeight:'600'}}>{item.name}</Text>
                
                <Text style={{color:'#969696',fontSize:15}}>{item.date}</Text>
                
                </View>

                

                <View style={{flex:1, justifyContent:"flex-start"}}>
                    <View style={{flex:1, flexDirection:"row", justifyContent:"flex-end", paddingHorizontal: 10}}>
                        
                            
                            <Icon name='ios-star-outline' size={24} color={color.primary}/>
                            <Icon name='ios-star-outline' size={24} color={color.primary}/>
                            <Icon name='ios-star-outline' size={24} color={color.primary}/>
                            <Icon name='ios-star-outline' size={24} color={color.primary}/>
                            <Icon name='ios-star-outline' size={24} color={color.primary}/>        
                    </View>
                </View>
               
                </View>
                
                
                <View style={styles.custom} >
                            <Text style={{fontSize:16, textAlign:"center"}}>
                                {item.review}
                            </Text>
                        </View>
                        
            </TouchableOpacity>
            )} />


      
          </View>
          <Button
                        label={I18n.t("book_appointment")}
                        buttonStyle={styles.button}
                        onPress={() => this.props.navigation.navigate('clinicprofile', { clinicDesired: index })}
                    />
        
        </View>
        );
  }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },
    list:{
        width:'95%',
        paddingTop:10,
        paddingBottom:100,
        paddingHorizontal:3,
        alignSelf:'center',
        paddingBottom: 50

    },
    details:{
        paddingHorizontal:10
    },
    listStyle:{
        marginVertical:10,
        
        
        
        width:'100%',
        borderRadius:10,
        borderWidth:1,
        borderColor:'transparent',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: 'white',
    elevation: 2, // Android
//         shadowColor: "#d2d7d3",
//         shadowOffset: {
// 	    width: 0,
// 	    height: 1,
//         },
// shadowOpacity: 0.20,
// shadowRadius: 4.21,

// elevation: 2,
paddingVertical:13,
paddingHorizontal:12

    },
    title2: {
        color: "#969696",
        fontSize: 16,
       
        textAlign:'center',
        letterSpacing: 0.2,
        fontWeight: "400",
        
       
    },
    titleBold:{
        color: "#373535",
        fontSize: 16,
       
        textAlign:'center',
        letterSpacing: 0.2,
        fontWeight: "600",
    },
    title3:{
       flexDirection:'row',
       alignItems:'center',
        alignSelf:'center',
        paddingVertical:3,
        marginTop:5
    },
    form: {
        paddingTop: 10,
        width: "90%",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
      },

      custom: {
        flex:1,
        flexDirection: "row",
        width: "90%",
        justifyContent: "flex-start",
        marginVertical:20

    },

      input1: {
        marginVertical: 14,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        fontSize:19,
        fontStyle:'italic',
        paddingLeft: 17,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
      }
});
function mapStateToProps(state, props) {
    return {
        userDetail: state.user.userDetail,
    }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}
  
export default connect(mapStateToProps, mapDispatchToProps)(Review);
