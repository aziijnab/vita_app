import Header from '../../components/Header'
import React, { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  FlatList,
  Alert
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { color, font, apiURLs } from '../../components/Constant'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions' //Import your actions
import I18n from '../../i18n/i18n'
const HEIGHT = Dimensions.get('window').height
import Icon from 'react-native-vector-icons/Feather'
import Button from '../../components/Button'
import clinics from '../../components/Dummydata/clinics'
import moment from 'moment'
import Spinner from 'react-native-loading-spinner-overlay'
import Toast, { DURATION } from 'react-native-easy-toast'
import Route from '../../network/route.js'

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route('http://192.168.100.18:8000/api/')

class AppointmentDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      clinicData: {},
      appointmentStartDate: '',
      appointmentEndDate: '',
      appointmentTime: '',
      selectedPackages: '',
      selectUserPackagesDetail: [],
      appointmentTotalPrice: null,
      refrelCode: '',
      discountAccept:false
    }
  }

  componentDidMount() {
    const { params } = this.props.navigation.state

    var newDate = moment(params.appointmentDetail.date).format('YYYY-MM-DD')
    this.setState({
      clinicData: params.appointmentDetail.clinicData,
      appointmentDate: newDate,
      appointmentTime: params.appointmentDetail.time,
      // appointmentEndTime: params.appointmentDetail.endTime,
      selectedPackages: params.appointmentDetail.selectedPackages,
      selectUserPackagesDetail: this.props.selectUserPackagesDetail,
      appointmentTotalPrice: this.props.appointmentTotalPrice
    })
    console.log('appointment detail', params)
    console.log(
      'appointmentdetail in componnent',
      this.props.selectUserPackagesDetail
    )
  }

  replaceSpaceToUnderScore(string) {
    let lowerCaseString = string.toLowerCase()
    let underScoreString = lowerCaseString.split(' ').join('_')
    let brackitLess = underScoreString.split('(').join('')
    let brackitLess2 = brackitLess.split(')').join('')
    let removeQuestionMark = brackitLess2.split('?').join('')
    let removeQoma = removeQuestionMark.split(',').join('')
    let removeDot = removeQoma.split('.').join('')
    let removeColon = removeDot.split(':').join('')
    let removeSemiColon = removeColon.split(';').join('')

    let singleUnderScore = removeSemiColon.split('-').join('_')
    singleUnderScore = singleUnderScore.split('__').join('_')
    singleUnderScore = singleUnderScore.split('/').join('_')
    var newString = singleUnderScore
    console.log('string length', newString.length)

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length
      newString = singleUnderScore.substring(0, 57)
      console.log('new sting with cut', newString)
    }
    console.log('under score string', newString)
    return newString
  }

  checkRefrelCode() {
    if (this.state.refrelCode != '') {
      this.setState({ loading: true })
      let data = { referral_code: this.state.refrelCode }
      route
        .updateData('check-referral-code', data, this.props.myToken)
        .then(async response => {
          if (response.error) {
            this.setState({ loading: false })
            this.refs.toast.show(
              I18n.t(this.replaceSpaceToUnderScore(response.error)),
              DURATION.SHORT
            )
          }

          if (response.message) {
            alert(I18n.t(this.replaceSpaceToUnderScore(response.message)))
            this.setState({ loading: false })
          } else {
            this.setState({ loading: false })
            if (response.referral.expiry != null && this.state.discountAccept == false) {
              var expireDate = moment(response.referral.expiry).format(
                'MMM, DD YYYY'
              )
              var currentDate = moment().format('MMM, DD YYYY')
              if (currentDate > expireDate) {
                this.refs.toast.show(
                  I18n.t('your_referal_code_has_been_expire'),
                  DURATION.SHORT
                )
              }else {
                var totalPrice = this.state.appointmentTotalPrice - parseFloat(response.referral.discount)
                console.log("total price after discount",totalPrice)
                this.setState({ appointmentTotalPrice: totalPrice,discountAccept : true }, () => {
                  this.props.setAppointmentTotalPrice(
                    this.state.appointmentTotalPrice
                  )
                })
              }
            } 
          }
        })
    } else {
      this.refs.toast.show(
        I18n.t('please_add_your_referal_code'),
        DURATION.SHORT
      )
    }
  }

  bookAppointment() {
    if (this.props.isChoseAtClinicPayment === true) {
      var appointmentDetail = {
        clinic_id: this.state.clinicData.id,
        time: moment(this.state.appointmentTime, 'hh:mm A').format('hh:mm A'),
        // endTime: moment(this.state.appointmentEndTime, "hh:mm A").format("HH:mm:ss"),
        date: this.state.appointmentDate
      }
      this.setState({ loading: true })
      route
        .updateData('user-appointment', appointmentDetail, this.props.myToken)
        .then(async response => {
          if (response.error) {
            console.log(response.error)
            this.props.stopLoading()
            this.setState({ loading: false })
            if (
              response.error == 'Budget not available to book this appointment.'
            ) {
              this.refs.toast.show(
                I18n.t(this.replaceSpaceToUnderScore(response.error)),
                DURATION.SHORT
              )
            } else {
              Alert.alert('Error', JSON.stringify(response.error))
            }
          } else {
            console.log('appointment')
            console.log(response)
            this.setState({ loading: false })
            route.checkTokenExpire(response)
            this.props.isChoseAtClinic(false)

            this.props.navigation.navigate('appointmentScreen')
          }
        })
    } else {
      this.props.navigation.navigate('paymentMethod', {
        appointmentDate: this.state.appointmentDate,
        clinicDetail: this.state.clinicData,
        appointmentTime: this.state.appointmentTime
      })
    }
  }

  replaceSpaceToUnderScore(string) {
    var string = string.trim()
    var lowerCaseString = string.toLowerCase()
    var underScoreString = lowerCaseString.split(' ').join('_')
    var brackitLess = underScoreString.split('(').join('')
    var brackitLess2 = brackitLess.split(')').join('')
    var removeQuestionMark = brackitLess2.split('?').join('')
    var removeQoma = removeQuestionMark.split(',').join('')
    var removeDot = removeQoma.split('.').join('')
    var removeColon = removeDot.split(':').join('')
    var removeSemiColon = removeColon.split(';').join('')

    var singleUnderScore = removeSemiColon.split('-').join('_')
    singleUnderScore = singleUnderScore.split('__').join('_')
    singleUnderScore = singleUnderScore.split('/').join('_')
    singleUnderScore = singleUnderScore.split('\n').join('_')
    singleUnderScore = singleUnderScore.split("'").join('_')
    singleUnderScore = singleUnderScore.split('%').join('_')
    singleUnderScore = singleUnderScore.split('<').join('_')
    singleUnderScore = singleUnderScore.split('>').join('_')
    singleUnderScore = singleUnderScore.split('+').join('_')
    var newString = singleUnderScore

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length
      newString = singleUnderScore.substring(0, 57)
    }
    return newString
  }
  render() {
    return (
      <View style={styles.container}>
        <Header
          label={I18n.t('appointment_details')}
          onbackPress={() => this.props.navigation.pop()}
        />
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t('please_wait')}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: 'red' }}
          position="top"
          positionValue={80}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{ color: 'white' }}
        />
        <KeyboardAwareScrollView>
          <View style={styles.fieldView}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                width: '100%',
                marginTop: 5
              }}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: 'flex-start',
                  paddingHorizontal: 2
                }}
              >
                <Image
                  source={{ uri: `${this.state.clinicData.image}` }}
                  style={{ width: 70, height: 70 }}
                  resizeMode="contain"
                />
              </View>

              <View style={{ flex: 4, justifyContent: 'flex-start' }}>
                <Text style={styles.title}>{this.state.clinicData.name}</Text>
              </View>
            </View>
          </View>

          <View style={styles.fieldView}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                width: '100%',
                marginTop: 5
              }}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: 'flex-start',
                  paddingHorizontal: 2
                }}
              >
                <Icon name="calendar" color={'#373535'} size={32} />
              </View>

              <View style={{ flex: 4, justifyContent: 'flex-start' }}>
                <Text style={{ fontSize: 18, color: '#373535' }}>
                  {moment(this.state.appointmentDate).format('MMM DD, YYYY')}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.fieldView}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                width: '100%',
                marginTop: 5
              }}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: 'flex-start',
                  paddingHorizontal: 2
                }}
              >
                <Icon name="clock" color={'#373535'} size={32} />
              </View>

              <View style={{ flex: 4, justifyContent: 'flex-start' }}>
                <Text style={{ fontSize: 18, color: '#373535' }}>
                  {this.state.appointmentTime}
                </Text>
              </View>
            </View>
          </View>

          {this.state.selectUserPackagesDetail.length > 0 && (
            <View style={styles.fieldView}>
              {console.log(
                'selected user Packages',
                this.state.selectUserPackagesDetail
              )}
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  width: '100%',
                  marginTop: 5,
                  paddingVertical: 10
                }}
              >
                <View
                  style={{
                    flex: 4,
                    justifyContent: 'flex-start',
                    paddingHorizontal: 2
                  }}
                >
                  <Text style={{ fontSize: 18, color: '#373535' }}>
                    {I18n.t('ordered_services')}
                  </Text>
                </View>
              </View>
              <View style={styles.break} />
              {this.state.selectUserPackagesDetail.map((item, index) => {
                return (
                  <View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        width: '100%',
                        marginTop: 5,
                        paddingVertical: 6
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'flex-start',
                          paddingHorizontal: 2
                        }}
                      >
                        <Text style={{ fontSize: 18, color: '#696969' }}>
                          {I18n.t(this.replaceSpaceToUnderScore(item.name))}
                        </Text>
                      </View>

                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'flex-end',
                          alignItems: 'flex-end'
                        }}
                      >
                        <Text style={{ fontSize: 18, color: '#373535' }}>
                          RM{item.discount_price}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        width: '100%',
                        marginTop: 2
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'flex-start',
                          paddingHorizontal: 2
                        }}
                      >
                        <Text style={{ fontSize: 18, color: color.primary }}>
                          {I18n.t('package')}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.break} />
                  </View>
                )
              })}

              {/* <View style={{ flex: 1, flexDirection: "row", width: "100%", marginTop: 5, paddingVertical: 6 }}>
                            <View style={{ flex: 1, justifyContent: "flex-start", paddingHorizontal: 2 }}>
                                <Text style={{ fontSize: 18, color: "#696969" }} >{I18n.t('fasting_blood_sugar')}</Text>
                            </View>

                            <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "flex-end" }}>
                                <Text style={{ fontSize: 18, color: "#373535" }} >RM50</Text>
                            </View>
                        </View>

                        <View style={{ flex: 1, flexDirection: "row", width: "100%", marginTop: 2 }}>
                            <View style={{ flex: 1, justifyContent: "flex-start", paddingHorizontal: 2 }}>
                                <Text style={{ fontSize: 18, color: color.primary }} >{I18n.t('individual_test')}</Text>
                            </View>
                        </View> */}

              <View style={styles.break} />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  width: '100%',
                  marginTop: 5,
                  paddingVertical: 4,
                  alignItems: 'center'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    paddingHorizontal: 2
                  }}
                >
                  <TextInput
                    style={styles.input1}
                    selectionColor={'#dfe6e9'}
                    onChangeText={text => this.setState({ refrelCode: text })}
                    placeholder={I18n.t('have_a_referal_code')}
                    autoCapitalize="words"
                    autoCorrect={true}
                    underlineColorAndroid="transparent"
                    //placeholderTextColor="#696969"
                  />
                </View>

                <TouchableOpacity
                  onPress={() => this.checkRefrelCode()}
                  style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end'
                  }}
                >
                  <Text style={{ fontSize: 18, color: color.primary }}>
                    {I18n.t('apply')}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={styles.break} />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  width: '100%',
                  marginTop: 5,
                  paddingVertical: 6
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    paddingHorizontal: 2
                  }}
                >
                  <Text style={{ fontSize: 18, color: '#373535' }}>
                    {I18n.t('total')}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end'
                  }}
                >
                  <Text style={{ fontSize: 18, color: '#373535' }}>
                    RM{this.state.appointmentTotalPrice}
                  </Text>
                </View>
              </View>
            </View>
          )}

          <Button
            label={I18n.t('confirm')}
            buttonStyle={styles.button}
            onPress={() => this.bookAppointment()}
          />
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: 'row',
    width: '90%',
    flex: 1,
    justifyContent: 'flex-start',
    marginVertical: 20
  },
  iconTop: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: color.primary,
    borderColor: 'transparent',

    alignItems: 'center',
    justifyContent: 'center'
  },
  custom1: {
    flexDirection: 'row',
    width: '90%',
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 20,
    marginBottom: 6
  },

  address: {
    flexDirection: 'row',
    width: '90%',
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 10,
    marginBottom: 10
  },
  break: {
    width: '100%',
    borderColor: '#dadfe1',
    borderBottomWidth: 0.5,
    marginVertical: 10
  },
  topView: {
    width: '90%',
    justifyContent: 'space-evenly',
    paddingVertical: 30,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 18,
    fontWeight: '400',
    letterSpacing: 0.5,

    marginTop: 20,
    paddingHorizontal: 15,

    color: '#373535'
  },
  title2: {
    fontSize: 20,
    fontWeight: '700',
    letterSpacing: 0.5,
    paddingTop: 6,
    paddingLeft: 30
  },
  iconView: {},
  title1: {
    fontSize: 15,
    fontWeight: '300',
    marginVertical: 15,
    color: '#696969',
    letterSpacing: 0.5,
    paddingHorizontal: 20
  },
  fieldView: {
    width: '94%',

    alignSelf: 'center',
    borderWidth: 1,
    borderRadius: 4,
    borderColor: 'transparent',
    borderBottomWidth: 0,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    paddingHorizontal: 12,
    paddingVertical: 10,
    marginTop: 25,
    marginBottom: 3
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: 'center',
    width: '90%',
    marginTop: 15,
    marginBottom: 30,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'center',
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#dfe6e9'
  },
  touchable3: {
    marginVertical: 17,
    width: '40%',
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'center',
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#dfe6e9'
  },
  input1: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 8,
    alignItems: 'center',
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#dfe6e9'
  },

  remember: {
    alignSelf: 'center',
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: '90%',
    justifyContent: 'space-between',
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: 'white'

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    marginBottom: 10,
    alignSelf: 'center'
  },
  header: {
    height: 50,
    width: '100%',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignSelf: 'center',
    borderBottomColor: '#F4AE96',
    borderBottomWidth: 1,
    marginTop: Platform.OS == 'ios' ? 20 : 0,
    backgroundColor: 'white'
  }
})

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    selectUserPackagesDetail: state.user.selectUserPackagesDetail,
    appointmentTotalPrice: state.user.appointmentTotalPrice,
    user: state.user.userInfo,
    myToken: state.user.userToken,
    isChoseAtClinicPayment: state.user.isChoseAtClinicPayment
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppointmentDetails)
