import React, { Component } from 'react';
import {
  Text, View,TouchableOpacity,Image,ScrollView,TextInput,
  FlatList,
  Dimensions,
  StyleSheet,
  Platform,
  PermissionsAndroid
} from 'react-native';
import {color, font, apiURLs} from '../../components/Constant';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions
import clinics  from '../../components/Dummydata/clinics';
import IconHeader from '../../components/IconHeader';
import Icon from 'react-native-vector-icons/Ionicons'
import Route from "../../network/route.js";
import Spinner from 'react-native-loading-spinner-overlay';
import I18n from "../../i18n/i18n";
// const route = new Route("https://vita-health.herokuapp.com/api/");
import geolib from "geolib"
import underscore from "underscore";
const route = new Route("http://192.168.100.18:8000/api/");
import Geolocation from 'react-native-geolocation-service';


class Clinics extends Component {


    constructor(props) { 
        super(props);
        this.state = {
           clinicList: [],
           filterText: '',
           selectedPackages:'',
           loading:false,
            region: {
                latitude: 3.1390,
                longitude: 101.6869,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            }
        };
    }

    fetchClinics(){
        this.setState({ loading: true })
        route.getAuthenticated('all-clinics?search-keyword=&packages=/' + this.props.userSelectedPackages, this.props.myToken).then(async (response) => {
            if (response.error) {
                console.log(response.error)
                this.setState({ loading: false })
            }
            else {
                this.setState({ loading: false })
                var clinics = []
                response.clinics.map((value, index) => {
                    let distance = geolib.getDistance(
                        { latitude: this.state.region.latitude, longitude: this.state.region.longitude },
                        { latitude: value.latitude, longitude: value.longitude }
                    );


                    var distance1 = distance / 1000
                    value.distance = Math.round(distance1 * 10) / 10;
                    clinics.push(value);
                })

                var sortedClinicsByDistanc = underscore.sortBy(clinics, 'distance')
                console.log("geo distanc", sortedClinicsByDistanc);
                this.setState({ clinicList: sortedClinicsByDistanc })

                console.log("clicnics list", this.state.clinicList)
            }
        })
    }

 async hasLocationPermission () {
            if (Platform.OS === 'ios' ||
                (Platform.OS === 'android' && Platform.Version < 23)) {
            return true;
            }

            const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            );

            if (hasPermission) return true;

            const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            );

            if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

            if (status === PermissionsAndroid.RESULTS.DENIED) {
            ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
            } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
            }

            return false;
        }

    async componentDidMount() {
         await this.hasLocationPermission()
        this.setState({ loading: true })

        Geolocation.getCurrentPosition(
            (position) => {
                console.log("wokeeey");
                //   alert(JSON.stringify(position) );
                let region = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }

                this.setState({ region: region },()=>{
                    this.setState({ loading: false })

                    this.fetchClinics()
                })

            },
            (error) => {

                console.log("error postion", error)
                this.setState({ loading: false })

                this.fetchClinics();
            },
            { enableHighAccuracy: true, timeout: 20000 },
        ); 

    }
    _onChangeFilterText = (filterText) => {
        this.setState({filterText:filterText});
      };
    
  render() {
    const HEIGHT = Dimensions.get('window').height;
    const filterRegex = new RegExp(String(this.state.filterText), 'i');
    console.log("clinics list in render",this.state.clinicList)
    const filter = (item) => (
      filterRegex.test(item.name) 
    );
    const filteredData = this.state.clinicList.filter(filter);

    return (
        <View style={styles.container}>
            <Spinner
                visible={this.state.loading}
                textContent={I18n.t('please_wait')}
                textStyle={{color:color.primary}}
                cancelable={true}
                color="#F16638"
            />
            <IconHeader label='Clinics' onbackPress={()=>this.props.navigation.goBack()} onIconPress={()=>this.props.navigation.navigate('mapview')} iconRight='md-pin' />
            <View style={styles.form}>
            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={this._onChangeFilterText}
              placeholder={I18n.t('search_by_clinic_name')}
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />
            </View>
            <View style={styles.title3}>
            <Text style={styles.title2}>{I18n.t('found')}</Text>
          <Text style={styles.titleBold}> {this.state.clinicList.length} </Text>

          <Text style={styles.title2}>{I18n.t('clinics_that_offer_this')}</Text>



          <Text style={styles.titleBold}> {I18n.t('package')}.</Text>
          </View>


          <View style={styles.list}>
          <FlatList
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            data={filteredData}
            renderItem={({ item,index }) => (
            <TouchableOpacity /* activeOpacity={0.9} */ onPress={()=>this.props.navigation.navigate('clinicprofile',{clinicProfileId:item.id,selectedPackages:this.state.selectedPackages})} style={styles.listStyle}>
                
                <Image source={{ uri: `${item.image}` }} style={{width:70,height:70}} resizeMode='contain' />
                

                <View style={styles.details}>
                <Text style={{color:'#373535',fontSize:18,}}>{item.name}</Text>
                <Text style={{color:'#969696',fontSize:16}}>{item.type}</Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                <Icon name='ios-pin' size={16} color="#696969"/>
                            <Text style={{ color: '#696969', fontSize: 14, paddingHorizontal: 3 }}>{item.distance} {I18n.t('km_away')}</Text>
                    
                </View>
                </View>


            </TouchableOpacity>
            )} />


      
          </View>

        
        </View>
        );
  }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },
    list:{
        width:'95%',
        paddingTop:10,
        paddingBottom:16,
        paddingHorizontal:3,
        alignSelf:'center',
        paddingBottom: 50

    },
    details:{
        paddingHorizontal:10
    },
    listStyle:{
        marginVertical:10,
        
        flexDirection:'row',
        alignItems:'center',
        width:'100%',
        borderRadius:10,
        borderWidth:1,
        borderColor:'transparent',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: 'white',
    elevation: 2, // Android
    paddingVertical:13,
    paddingHorizontal:12

    },
    title2: {
        color: "#969696",
        fontSize: 16,
       
        textAlign:'center',
        letterSpacing: 0.2,
        fontWeight: "400",
        
       
    },
    titleBold:{
        color: "#373535",
        fontSize: 16,
       
        textAlign:'center',
        letterSpacing: 0.2,
        fontWeight: "600",
    },
    title3:{
       flexDirection:'row',
       alignItems:'center',
        alignSelf:'center',
        paddingVertical:3
    },
    form: {
        paddingTop: 10,
        width: "90%",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
      },
      input1: {
        marginVertical: 14,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        fontSize:19,
        fontStyle:'italic',
        paddingLeft: 17,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
      }
});
function mapStateToProps(state, props) {
    return {
        user: state.user.userInfo,
        myToken: state.user.userToken,
        userSelectedPackages:state.user.selectUserPackages,
    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}
  
export default connect(mapStateToProps, mapDispatchToProps)(Clinics);
