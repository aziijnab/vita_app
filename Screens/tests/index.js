import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import recommendedPackages from "../../components/Dummydata/recommendedPackages.json";
import addOns from "../../components/Dummydata/addOns.json";
import NotificationHeader from "../../components/NotificationHeader";
import Spinner from "react-native-loading-spinner-overlay";
import Route from "../../network/route.js";
import { NavigationEvents } from "react-navigation";

const route = new Route("http://192.168.100.18:8000/api/");

class TestsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addOn: addOns,
      status: false,
      total: 0,
      selectedPackagesDetail: [],
      packages: [],
      loading: false,
      selectedPackages: [],
      recommendedPackages: [],
      addOnPackages: [],
      allPackages: [],
      selectAdvanceRecommendedPackage: false,
      selectBasicRecommendedPackage: false
    };
    this.addToCart = this.addToCart.bind(this);
    this.deleteFromCart = this.deleteFromCart.bind(this);
    this.recommendedHandlePress = this.recommendedHandlePress.bind(this);
    this.addOnHandlePress = this.addOnHandlePress.bind(this);
  }


  replaceSpaceToUnderScore(string) {
    var string = string.trim()
    var lowerCaseString = string.toLowerCase();
    var underScoreString = lowerCaseString.split(" ").join("_");
    var brackitLess = underScoreString.split("(").join("");
    var brackitLess2 = brackitLess.split(")").join("");
    var removeQuestionMark = brackitLess2.split("?").join("");
    var removeQoma = removeQuestionMark.split(",").join("");
    var removeDot = removeQoma.split(".").join("");
    var removeColon = removeDot.split(":").join("");
    var removeSemiColon = removeColon.split(";").join("");

    var singleUnderScore = removeSemiColon.split("-").join("_");
    singleUnderScore = singleUnderScore.split("__").join("_");
    singleUnderScore = singleUnderScore.split("/").join("_");
    singleUnderScore = singleUnderScore.split("\n").join("_");
    singleUnderScore = singleUnderScore.split("'").join("_");
    singleUnderScore = singleUnderScore.split("%").join("_");
    singleUnderScore = singleUnderScore.split("<").join("_");
    singleUnderScore = singleUnderScore.split(">").join("_");
    singleUnderScore = singleUnderScore.split("+").join("_");
    var newString = singleUnderScore;

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length;
      newString = singleUnderScore.substring(0, 57);
    }
    return newString;
  }
  componentDidMount() {
	this.fetchData();
  }

  fetchData(){
    console.log("packages component");
    this.setState({ loading: true });
    route
      .getAuthenticated("all-packages", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          console.log("packages response", response);
          this.setState({ loading: false });
          route.checkTokenExpire(response);
          var allPackages = this.state.allPackages;
          this.setState(
            { recommendedPackages: response.data.recommended },
            () => {
              console.log(
                "recommendedpackages",
                this.state.recommendedPackages
              );
              allPackages.push(this.state.recommendedPackages);
              this.state.recommendedPackages.map((item, index) => {
                item.discount_price = item.total_price;
              });
            }
          );
          this.setState({ addOnPackages: response.data.addon }, () => {
            console.log("addOnPackages", this.state.addOnPackages);
            allPackages.push(this.state.addOnPackages);
            this.state.addOnPackages.map((item, index) => {
              item.discount_price = item.total_price;
            });
          });
        }
      });
  }
  
  addOnHandlePress(index, id) {
    var selectedPackages = this.state.selectedPackages;
    var packageFind = selectedPackages.findIndex(value => value == id);
    if (packageFind == -1) {
      const packageDetail = this.state.addOnPackages[index].detail;
      console.log("package Detail ", packageDetail);
      var packageDetailText = "";
      packageDetail.map((value, index) => {
        packageDetailText += value.test.name + "\n";
      });
      console.log("package detail", packageDetailText);
      Alert.alert(
        this.state.addOnPackages[index].name,
        packageDetailText,
        [
          {
            text: "Add to cart",
            onPress: () => this.addToCart(index, id, this.state.addOnPackages)
          },
          {
            text: "Back",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          }
        ],
        { cancelable: false }
      );
    } else {
      this.deleteFromCart(index, packageFind, this.state.addOnPackages);
    }
    //  else if (category == "addOn" && this.state.addOn[index].status == true) {
    //   this.deleteFromCart( index);
    // }
  }

  recommendedHandlePress(index, id) {

    let recommendedAddedIndex = this.state.selectedPackagesDetail.findIndex((value,index)=> value.internal_type == "recommended")

    console.log("recommended package founde",recommendedAddedIndex)
    if(recommendedAddedIndex == -1){
      var selectedPackages = this.state.selectedPackages;
        var packageFind = selectedPackages.findIndex(value => value == id);
        if (packageFind == -1) {
          this.packageAlert(index,id)
        } else {


          this.deleteFromCart(index, packageFind, this.state.recommendedPackages);
        }
    }else{

      if(this.state.selectedPackagesDetail[recommendedAddedIndex].id == id){
        this.deleteFromCart(index, recommendedAddedIndex, this.state.recommendedPackages);
      }else{
        this.deleteFromCart(index, recommendedAddedIndex, this.state.recommendedPackages);
        this.packageAlert(index,id)
      }
      // alert(I18n.t('you_already_select_a_recommended_package'))
    }
  }
  packageAlert(index,id){
    const packageDetail = this.state.recommendedPackages[index].detail;
    console.log("package Detail ", packageDetail);
    var packageDetailText = "";
    packageDetail.map((value, index) => {
      if(value.test != null){
        packageDetailText += value.test.name + "\n";
      }
    });
    console.log("package detail", packageDetailText);
    Alert.alert(
      this.state.recommendedPackages[index].name,
      packageDetailText,
      [
        {
          text: "Add to cart",
          onPress: () =>
            this.addToCart(index, id, this.state.recommendedPackages)
        },
        {
          text: "Back",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  }

  addToCart(index, id, packageArray) {
    console.log("add to card press", this.state.selectBasicRecommendedPackage,this.state.selectAdvanceRecommendedPackage);
    var selectedPackages = this.state.selectedPackages;
    var selectedPcakgesDetail = this.state.selectedPackagesDetail;
    if (packageArray[index].internal_type == "recommended") {
      if (packageArray[index].type == "Advanced") {
        this.state.addOnPackages.map((item, key) => {
          if (packageArray[index].on_advanced_selection_dis != null) {
            alert(packageArray[index].on_advanced_selection_dis)
            item.discount_price =
              item.total_price - item.on_advanced_selection_dis;
          }
        });
      }
      if (packageArray[index].type == "Basic") {
        this.state.addOnPackages.map((item, key) => {
          if (packageArray[index].on_basic_selection_dis != null) {
            item.discount_price =
              item.total_price - packageArray[index].on_basic_selection_dis;
          }
        });
      }
    }
    let recommendedAddedIndex = this.state.selectedPackagesDetail.findIndex((value,index)=> value.internal_type == "recommended")
    if( packageArray[index].internal_type != "recommended"){
      if(recommendedAddedIndex != -1){
       
        selectedPcakgesDetail.push(packageArray[index]);
        this.setState({ selectedPackagesDetail: selectedPcakgesDetail }, () => {
          console.log(
            "selected packages detail",
            this.state.selectedPackagesDetail
          );
        });
        const totalPrice = this.state.total + packageArray[index].discount_price;
        selectedPackages.push(id);
        this.setState({
          selectedPackages: selectedPackages,
          total: totalPrice
        });
        console.log("total price", this.state.total);
      }else{
        alert(I18n.t('please_select_any_recommended_package'))
        
      }
    }else{
      //var recInd = selectedPackages.findIndex((pac,ind)=>pac.internal_type == "recommended")
      console.log("recommended package founde",recommendedAddedIndex)
        console.log("package array",packageArray)
       this.state.addOnPackages.map((item,key)=>{
        if(item.on_advanced_selection_dis != null){
          item.discount_price = item.total_price - item.on_advanced_selection_dis
          console.log("package array",packageArray)
          
        }
        if(item.on_basic_selection_dis != null){
          item.discount_price = item.total_price - item.on_basic_selection_dis
          console.log("package array",packageArray)
          
        }
       })
      selectedPcakgesDetail.push(packageArray[index]);
      this.setState({ selectedPackagesDetail: selectedPcakgesDetail }, () => {
        console.log(
          "selected packages detail",
          this.state.selectedPackagesDetail
        );
      });
      const totalPrice = this.state.total + packageArray[index].discount_price;
      selectedPackages.push(id);
      this.setState({
        selectedPackages: selectedPackages,
        total: totalPrice
      });
      console.log("total price", this.state.total);
    }
      var totalPriceNew = 0
      this.state.selectedPackagesDetail.map((pack,index)=>{
          totalPriceNew += pack.discount_price
      })
      this.setState({total:totalPriceNew})

    
  }

  deleteFromCart(index, packageFind, packageArray) {
    var selectedPackages = this.state.selectedPackages;
    const totalPrice = this.state.total - packageArray[index].discount_price;
    var selectedPcakgesDetail = this.state.selectedPackagesDetail;
    selectedPcakgesDetail.splice(packageFind, 1);
    selectedPackages.splice(packageFind, 1);
    this.setState(
      {
        selectedPackages: selectedPackages,
        total: totalPrice,
        selectedPackagesDetail: selectedPcakgesDetail
      },
      () => {
        console.log("remove package for array", this.state.selectedPackagesDetail);
        if (this.state.selectedPackagesDetail.length == 0) {
          console.log("hellow world");
          var addOnPackages = this.state.addOnPackages;
          addOnPackages.map((item, index) => {
            console.log("item total price", item.total_price);
            item.discount_price = item.total_price;
          });
          this.setState({ addOnPackages, addOnPackages });
        } else {
          var advanceFind = false;
          var basicFind = false;
          this.state.selectedPackagesDetail.map((selectedPakcage) => {
            if (selectedPakcage.internal_type == "recommended") {
              if (selectedPakcage.type == "Advanced") {
                this.setState({
                  selectAdvanceRecommendedPackage: true
                });
              }
              if (selectedPakcage.type == "Basic") {
                this.setState({
                  selectAdvanceRecommendedPackage: true
                });
              }
            }
            if(packageArray[index].internal_type == "recommended"){
              this.setState({selectedPackages:[]})
              this.state.addOnPackages.map((item,key)=>{
                item.discount_price = item.total_price
              })
              this.setState({total:0,selectedPackagesDetail:[]})
            }
           
            console.log("selected packages detail from remove",this.state.selectedPackagesDetail)
          });
        }

        console.log("total price after remove package", this.state.total);
      }
    );

    var totalPriceNew = 0
    this.state.selectedPackagesDetail.map((pack,index)=>{
        totalPriceNew += pack.discount_price
    })
    this.setState({total:totalPriceNew})
  }

  proceedAppointment() {
    console.log("selected packages", this.state.selectedPackages);
    this.props.setUserSeletedPackages(this.state.selectedPackages);
    this.props.setUserSeletedPackagesDetail(this.state.selectedPackagesDetail);
    this.props.setAppointmentTotalPrice(this.state.total);
    this.props.navigation.navigate("mapview", {
      selectedPackages: this.state.selectedPackage
    });
  }

  choseAtClinicPress() {
    console.log("selected packages", this.state.selectedPackages);
    this.props.setUserSeletedPackages(this.state.selectedPackages);
    this.props.setUserSeletedPackagesDetail(this.state.selectedPackagesDetail);
    this.props.setAppointmentTotalPrice(this.state.total);
    this.props.isChoseAtClinic(true)
    this.props.navigation.navigate("mapview",{
      selectedPackages: this.state.selectedPackage
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <NotificationHeader
          label={I18n.t("select_package")}
          notificationCount={5}
          onMailPress={() => this.props.navigation.navigate("inbox")}
          onNotificationPress={() =>
            this.props.navigation.navigate("notifications")
          }
        />
		<NavigationEvents onDidFocus={() => this.fetchData()} />
        <ScrollView keyboardShouldPersistTaps={"always"}>
          <View>
            <Text style={styles.title}>{I18n.t("recommended_packages")} </Text>

            <View style={{ width: "90%", alignSelf: "center" }}>
              <FlatList
                data={this.state.recommendedPackages}
                showsHorizontalScrollIndicator={false}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {
                  var packageFind = this.state.selectedPackages.findIndex(
                    value => value == item.id
                  );
                  return (
                    <TouchableOpacity
                      onPress={() =>
                        this.recommendedHandlePress(index, item.id)
                      }
                    >
                      <View style={styles.fieldView}>
                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContent: "flex-start",
                            width: "100%"
                          }}
                        >
                          <Text style={styles.title1}>{I18n.t(this.replaceSpaceToUnderScore(item.name),{defaultValue: item.name})}</Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContent: "flex-end",
                            width: "100%",
                            alignItems: "center"
                          }}
                        >
                          <View style={{ paddingHorizontal: 2 }}>
                            <Text style={styles.title1}>
                              RM
                              {item.total_price}
                            </Text>
                          </View>

                          {packageFind >= 0 ? (
                            <View>
                              <Icon
                                name="ios-checkmark-circle-outline"
                                color={color.primary}
                                size={28}
                              />
                            </View>
                          ) : (
                            <View>
                              <Icon
                                name="ios-arrow-forward"
                                color="#969696"
                                size={28}
                              />
                            </View>
                          )}
                        </View>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            </View>
          </View>

          <Text style={styles.title}>ADD-ONS & {I18n.t("basic_packages")}</Text>

          <View style={{ width: "90%", alignSelf: "center" }}>
            <FlatList
              data={this.state.addOnPackages}
              showsHorizontalScrollIndicator={false}
              extraData={this.state}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) => {
                var packageFind = this.state.selectedPackages.findIndex(
                  value => value == item.id
                );
                return (
                  <TouchableOpacity
                    style={{}}
                    onPress={() => this.addOnHandlePress(index, item.id)}
                  >
                    <View style={styles.fieldView}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-start",
                          width: "100%"
                        }}
                      >
                        <Text style={styles.title1}>{I18n.t(this.replaceSpaceToUnderScore(item.name))}</Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          width: "100%",
                          alignItems: "center"
                        }}
                      >
                        <View style={{ paddingHorizontal: 2 }}>
                          <Text style={styles.title1}>
                            RM
                            {item.discount_price}
                          </Text>
                        </View>

                        {packageFind >= 0 ? (
                          <View>
                            <Icon
                              name="ios-checkmark-circle-outline"
                              color={color.primary}
                              size={28}
                            />
                          </View>
                        ) : (
                          <View>
                            <Icon
                              name="ios-arrow-forward"
                              color="#969696"
                              size={28}
                            />
                          </View>
                        )}
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
          </View>

          <View style={styles.form}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-start",
                width: "100%"
              }}
            >
              <Text
                style={{ fontSize: 18, color: "#373535", fontWeight: "bold" }}
              >
                {I18n.t('total')}:
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-end",
                width: "100%",
                alignItems: "center"
              }}
            >
              <View style={{ paddingHorizontal: 4 }}>
                <Text
                  style={{
                    fontSize: 18,
                    color: color.primary,
                    fontWeight: "bold"
                  }}
                >
                  RM
                  {this.state.total}
                </Text>
              </View>
            </View>
          </View>

          <Button
            label={I18n.t('proceed')}
            buttonStyle={styles.button}
            onPress={() => this.proceedAppointment()}
          />

          <Button
            label={I18n.t('chose_at_clinic')}
            buttonStyle={styles.button1}
            onPress={() => this.choseAtClinicPress()}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "4%",
    marginVertical: 20,
    paddingLeft: 20,
    color: "#969696"
  },
  title2: {
    fontSize: 20,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: 6,
    paddingLeft: 30
  },
  iconView: {},
  title1: {
    fontSize: 15,
    fontWeight: "300",
    marginVertical: 15,
    color: "#969696",
    letterSpacing: 0.5,
    paddingHorizontal: 10
  },
  fieldView: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "transparent",
    borderBottomWidth: 0,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    marginVertical: 2,
    borderWidth: 1.5,
    borderColor: "rgba(0,0,0,0.1)",
    elevation: 2,
    paddingHorizontal: 8,
    paddingVertical: 10,
    marginTop: 12,
    marginBottom: 3
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },

  button1: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 10,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingBottom: 20

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    borderBottomColor: "#F4AE96",
    borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    questionAnswer: state.userQuestionAnswer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestsScreen);
