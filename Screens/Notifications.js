import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { color, font, apiURLs } from "../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions"; //Import your actions;
import NotificationHeader from "../components/NotificationHeader";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from '../i18n/i18n';
import Route from "../network/route.js";
import moment, { now } from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");
class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {

      notifications:[],
      loading:false,
      allMessages:[],


      data: [
        { name: 'Mediveron', message: 'TTD health clinic has sent you a message.', time: 8, price: 200 },
        { name: 'Health Clinic', message: 'TTD health clinic has sent you a message.', time: 2, price: 400 },
        { name: 'Bjourn Health', message: 'TTD health clinic has sent you a message.', time: 9, price: 600 }

      ]

    };
  }

  componentDidMount() {
    this.fetchAllChats()
   }

 

  fetchAllChats(){
    console.log("Message did mount called")

    this.setState({loading:true})
    route.getAuthenticated('get-all-notification',this.props.myToken).then(async(response)=>{
    if(response.error){
      console.log(response.error)
    }
    else {
      console.log("notification response",response)
      this.props.setNotificationCount(0)

        var notification = response.notification.appointmentIn48Hour
        notification.map((value)=>{
          value.date = this.changeDateFormat(value.appointment_date)
          value.time = this.changeTimeFormat(value.appointment_date)
        })
        this.setState({notifications:notification,allMessages:response.notification.newUnreadMsgs},()=>{
          console.log("notification set",this.state.notifications)
        })
        this.setState({loading:false})
        console.log(response)    

        }
    })
 }
 changeTimeFormatChat(time){
  var currentDate = moment();
  var currentTime = moment().hours();
  var newDate = moment(time).format('MMM, DD YYYY');
  var newTime = moment(time, "HH").format("hh");
  console.log("current date",currentDate.format('MMM, DD YYYY'))
  console.log(" newDate",newDate)
  console.log("new and currend date match", newDate == currentDate)
  if(newDate == currentDate.format('MMM, DD YYYY')){
      console.log("hours diffrent", currentTime-newTime)
      hoursDifference = currentTime-newTime+" hrs ago"
      return hoursDifference
  }else{
      var dayDiffrence = currentDate.diff(newDate,'day') + " day ago"
      console.log("day diffrence",currentDate.diff(newDate,'day'))
      return dayDiffrence

  }
  console.log("new data and time",newDate,newTime)
  return newTime
}

 changeTimeFormat(time){
  var newTime = moment(time, "HH:mm:ss").format("hh:mm A");
  return newTime
}
changeDateFormat(date){
  var newDate = moment(date).format('MMM, DD YYYY');
  return newDate
}
  removeItem(props) {
    var arr = this.state.data
    arr.splice(props, 1)
    this.setState({ data: arr })

  }

  showChatMessages(id){

    console.log("clinic id",id)
    this.props.navigation.navigate('message',{clinic_id:id})
}
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <NotificationHeader
          label={I18n.t('notifications')}
          onMailPress={() => this.props.navigation.navigate('inbox')}
          onNotificationPress={() => console.log('notif')}
        />
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t('please_wait')}...`}
          textStyle={{color:color.primary}}
          cancelable={true}
          color="#F16638"
        />
        <ScrollView style={{ flex: 1, marginVertical: 13, backgroundColor: 'white' }}>
        {this.state.allMessages.length > 0 ?
                        this.state.allMessages.map((item, index) => {


                            return <TouchableOpacity onPress={() => this.showChatMessages(item.clinic_id)} key={index} style={styles.fieldView}>
    
                                <View
                                    style={{
                                        flex: 1,
                                        paddingVertical: 5,
                                        flexDirection: "row",
    
                                        width: "100%",
                                        alignItems: "flex-start"
                                    }}>
                                    <View style={{ width: 40, height: 40, borderRadius: 20, }}>
                                        <Image source={require('../asset/images/tom.jpg')} style={{ width: 40, height: 40, borderRadius: 20 }} resizeMode='cover' />
                                    </View>
    
                                    <View style={{ paddingHorizontal: 5, flexWrap: 'wrap', width: '100%', }}>
    
                                        <View style={{ width: '100%', paddingRight: 4 }}>
                                            <Text style={styles.title1}>{item.clinic.name}</Text>
                                        </View>
                                        <Text style={styles.title2}>{item.msg}</Text>
                                        <Text style={styles.title3}>{this.changeTimeFormatChat(item.updated_at)}</Text>
    
                                    </View>
                                </View>
                            </TouchableOpacity>
    
                        })
                    :
                    null

                    }
          {this.state.notifications.length > 0 ?
            this.state.notifications.map((item, index) => {


              return <View key={index} style={styles.fieldView}>
  
                <View
                  style={{
                    flex: 1,
                    paddingVertical: 5,
                    flexDirection: "row",
  
                    width: "100%",
                    alignItems: "flex-start"
                  }}>
                  <View style={{ width: 40, height: 40, borderRadius: 20, }}>
                    <Image source={require('../asset/images/tom.jpg')} style={{ width: 40, height: 40, borderRadius: 20 }} resizeMode='cover' />
                  </View>
  
                  <View style={{ paddingHorizontal: 5, flexWrap: 'wrap', width: '100%', flex: 1 }}>
  
                    <View style={{ flex: 1, width: '100%', flexWrap: 'wrap' }}>
                      <Text style={styles.title1}>You have appointment with {item.clinic} on {item.date} at {item.time}</Text>
                    </View>
  
                    {/* <Text style={styles.title2}>{item.time} hours ago</Text> */}
                  </View>
  
  
  
  
                </View>
  
              </View>
            })

            :

            null
            
        }

        {(this.state.notifications.length == 0 && this.state.allMessages.length == 0) && 
        <View style={{flex:1,justifyContent:'center',alignContent:'center'}}>
        <Text style={{fontSize:18,alignSelf:"center",color: "#969696"}}>
            {I18n.t('no_notification_found')}
        </Text>
    </View>

      }


          

          <View style={{ alignSelf: 'center', paddingVertical: 10 }}>

          </View>

        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    selectedLanguage: state.user.selectedLanguage,
    inboxCount: state.user.inboxCount,
    notificationCount: state.user.notificationCount
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  titlePrice: {
    fontSize: 15,
    fontWeight: "400",

    color: color.primary,
    letterSpacing: 0.5,
    //paddingHorizontal: 10
  },
  title2: {
    fontSize: 13,
    fontWeight: "400",
    letterSpacing: 0.5,
    paddingVertical: 2,
    color: '#969696'
  },
  title3: {
    fontSize: 16,
    fontWeight: "600",
    letterSpacing: 0.5,
    paddingVertical: 2,
    color: '#969696',
    paddingLeft: 10
  },
  iconView: {


  },
  title1: {
    fontSize: 16,
    fontWeight: "400",

    color: "#373535",

    //paddingHorizontal: 10
  },
  fieldView: {

    flexDirection: "row",
    width: "91%",
    flexWrap: 'wrap',
    alignItems: "center",
    alignSelf: 'center',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: 'transparent',
    shadowColor: 'rgba(0,0,0, .4)', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: 'white',
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 11,
    paddingVertical: 10,
    marginTop: 16,



  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
      borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
      borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notifications);
