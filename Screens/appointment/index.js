import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform,
  RefreshControl
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import NotificationHeader from "../../components/NotificationHeader";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from "../../i18n/i18n";
import Route from "../../network/route.js";
import moment from "moment";
import Spinner from "react-native-loading-spinner-overlay";
import { NavigationEvents } from "react-navigation";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");
class AppointmentScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      upcomingAppointments: [],
      pastAppointments: [],
      selectedSensor: 0,
      refreshing: false,
      loading: false,
      allPackages: [],

      data: [
        {
          name: "Mediveron",
          message: "Blood package with H Clinic",
          time: 8,
          price: 200
        },
        {
          name: "Health Clinic",
          message: "Lung Package with Dynamix",
          time: 2,
          price: 400
        },
        {
          name: "Bjourn Health",
          message: "Dermatolofy package with Metrix Clinic",
          time: 9,
          price: 600
        }
      ]
    };
  }
  fetchPackages() {
    this.setState({ loading: true });
    route
      .getAuthenticated("all-packages", this.props.myToken)
      .then(async response => {
        if (response.error) {
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
          var allPackages = this.state.allPackages;
          response.data.recommended.map((value, index) => {
            allPackages.push(value);
          });
          response.data.addon.map((value, index) => {
            allPackages.push(value);
          });
          this.setState({ allPackages: allPackages });
        }
      });
  }
  fetchData() {
    this.setState({ loading: true });
    route
      .getAuthenticated("user-all-appointment", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          console.log("Appointments", response);
          this.setState({ loading: false });
          route.checkTokenExpire(response);
          this.setState({
            upcomingAppointments: response.appointments.upcomming
          });
          this.setState({ pastAppointments: response.appointments.old });
          console.log(
            "upcomming appointments",
            this.state.pastAppointments.length
          );
        }
      });
  }
  getPackageName(packages_id) {
     
    if (packages_id != null && this.state.allPackages.length != 0) {
      var packagesArray = packages_id.split(",");
      var packageIndex = this.state.allPackages.findIndex(
        (value, index) => value.id == packagesArray[0]
      );
      if (packageIndex > 0) {
        return `${this.state.allPackages[packageIndex].name} ${I18n.t(
          "package_with"
        )}`;
      } else {
        return "";
      }
    }
  }
  componentDidMount() {
    this.fetchData();
    this.fetchPackages();
  }
  removeItem(props) {
    var arr = this.state.data;
    arr.splice(props, 1);
    this.setState({ data: arr });
  }
  changeTimeFormat(time) {
    var newTime = moment(time, "HH:mm:ss").format("hh:mm A");
    return newTime;
  }
  changeDateFormat(date) {
    var newDate = moment(date).format("MMM, DD YYYY");
    return newDate;
  }
  cencelAppointmentSubmit(id) {
    const data = "";
    route
      .updateData(`user-cencel-appointment/${id}`, data, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.props.stopLoading();
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          this.setState({ loading: false });
          route.checkTokenExpire(response);
          if (response == true) {
            this.fetchData();
            this.fetchPackages();
          }
        }
      });
  }
  cancelAppointment(id) {
    Alert.alert(
      `${I18n.t("cancel_appointment")}`,
      `${I18n.t("are_you_sure_to_cancel_appointment")}`,
      [
        {
          text: `${I18n.t("cancel")}`,
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => this.cencelAppointmentSubmit(id) }
      ],
      { cancelable: false }
    );
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    console.log("refreshing");
    this.fetchData();
    this.setState({ refreshing: false });
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <NotificationHeader
          label={I18n.t("appointments")}
          inboxCount={this.props.inboxCount}
          notificationCount={this.props.notificationCount}
          onMailPress={() => this.props.navigation.navigate("inbox")}
          onNotificationPress={() =>
            this.props.navigation.navigate("notifications")
          }
        />
        <NavigationEvents onDidFocus={() => this.fetchData()} />
        <Spinner
          visible={this.state.loading}
          textContent={I18n.t('please_wait')}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />

        <View
          style={{
            borderRadius: 25,
            padding: 2,
            borderWidth: 2,
            borderColor: "",
            alignSelf: "center",
            borderColor: "#e5e5e5",
            width: "80%",
            height: 55,
            backgroundColor: "white",
            flexDirection: "row",
            marginVertical: 20
          }}
        >
          <TouchableOpacity
            onPress={() => this.setState({ selectedSensor: 0 })}
            activeOpacity={0.8}
            style={{
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
              backgroundColor:
                this.state.selectedSensor == 0 ? color.primary : "white",
              width: "100%",
              borderRadius: 23
            }}
          >
            <Text
              style={{
                color: this.state.selectedSensor == 0 ? "white" : "#373535",
                fontSize: 17
              }}
            >
              {I18n.t("upcoming")}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.setState({ selectedSensor: 1 })}
            activeOpacity={0.8}
            style={{
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
              backgroundColor:
                this.state.selectedSensor == 1 ? color.primary : "white",
              width: "100%",
              borderRadius: 23
            }}
          >
            <Text
              style={{
                color: this.state.selectedSensor == 1 ? "white" : "#373535",
                fontSize: 17
              }}
            >
              {I18n.t("past")}
            </Text>
          </TouchableOpacity>
        </View>

        {this.state.selectedSensor == 1 ? (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
            style={{ flex: 1, marginVertical: 13, backgroundColor: "white" }}
          >
            {this.state.pastAppointments.length == 0 && (
              <View
                style={{
                  width: "100%"
                }}
              >
                <Text
                  style={{
                    textAlign: "center"
                  }}
                >
                  {I18n.t("no_past_appointments_found")}
                </Text>
              </View>
            )}
            {this.state.pastAppointments.map((item, index) => {
              if (item.clinic_detail != null) {
                return (
                  <TouchableOpacity
                    key={index}
                    style={styles.fieldView}
                    onPress={() => this.props.navigation.navigate("results",{appointment_id:item.id,clinicName:item.clinic_detail.name,appointmentDate:item.date})}
                  >
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        width: "100%",
                        alignItems: "flex-start"
                      }}
                    >
                      <View style={{ width: 25, height: 25 }}>
                        <Image
                          source={require("../../asset/images/clipboard.png")}
                          style={{ width: 25, height: 25 }}
                          resizeMode="contain"
                        />
                      </View>

                      <View style={{ flexWrap: "wrap", paddingHorizontal: 5 }}>
                        <Text style={styles.title1}>
                          {this.getPackageName(item.packages_id)}{" "}
                          {item.clinic_detail.name}
                        </Text>
                        <Text style={styles.title2}>
                          {this.changeTimeFormat(item.time)},{" "}
                          {this.changeDateFormat(item.date)}
                        </Text>

                        {/* <TouchableOpacity onPress={() => this.cancelAppointment()}>
                                        <Text style={styles.title3}>Request cancellation</Text>
                                    </TouchableOpacity> */}
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              }
            })}

            <View style={{ alignSelf: "center", paddingVertical: 10 }} />
          </ScrollView>
        ) : (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
            style={{ flex: 1, marginVertical: 13, backgroundColor: "white" }}
          >
            {this.state.upcomingAppointments.length == 0 && (
              <View
                style={{
                  width: "100%"
                }}
              >
                <Text
                  style={{
                    textAlign: "center"
                  }}
                >
                  {I18n.t("no_upcoming_appointments_found")}
                </Text>
              </View>
            )}
            {this.state.upcomingAppointments.map((item, index) => {
              if (item.clinic_detail != null) {
                return (
                  <View key={index} style={styles.fieldView}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        width: "100%",
                        alignItems: "flex-start"
                      }}
                    >
                      <View style={{ width: 25, height: 25 }}>
                        <Image
                          source={require("../../asset/images/clipboard.png")}
                          style={{ width: 25, height: 25 }}
                          resizeMode="contain"
                        />
                      </View>

                      <View style={{ flexWrap: "wrap", paddingHorizontal: 5 ,paddingRight:10}}>
                        <Text style={styles.title1}>
                          {this.getPackageName(item.packages_id)}{" "}
                          {item.clinic_detail.name}
                        </Text>
                        <Text style={styles.title2}>
                          {this.changeTimeFormat(item.time)},{" "}
                          {this.changeDateFormat(item.date)}
                        </Text>
                        {item.status == "Canceled" && (
                          <Text style={styles.title3}>
                            {I18n.t("appointment_cencelled")}
                          </Text>
                        )}
                        {item.status != "Canceled" && (
                          <TouchableOpacity
                            onPress={() => this.cancelAppointment(item.id)}
                          >
                            <Text style={styles.title3}>
                              {I18n.t("request_cancellation")}
                            </Text>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </View>
                );
              }
            })}

            <View style={{ alignSelf: "center", paddingVertical: 10 }} />
          </ScrollView>
        )}
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    inboxCount: state.user.inboxCount,
    notificationCount: state.user.notificationCount,
	user: state.user.userInfo,
    myToken: state.user.userToken
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  titlePrice: {
    fontSize: 15,
    fontWeight: "400",

    color: color.primary,
    letterSpacing: 0.5
    //paddingHorizontal: 10
  },
  title2: {
    fontSize: 13,
    fontWeight: "400",
    letterSpacing: 0.5,
    paddingVertical: 2,
    color: "#969696"
  },
  title3: {
    fontSize: 16,
    fontWeight: "400",
    paddingVertical: 6,
    color: color.primary
  },
  iconView: {},
  title1: {
    fontSize: 16,
    fontWeight: "400",

    color: "#373535"

    //paddingHorizontal: 10
  },
  fieldView: {
    flexDirection: "row",
    width: "91%",
    flexWrap: "wrap",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: "white",
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginTop: 19
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppointmentScreen);
