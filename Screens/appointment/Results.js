import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  Alert,
  KeyboardAvoidingView,
  Platform,
  PermissionsAndroid
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions;
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/Ionicons";
import I18n from "../../i18n/i18n";
import RNFetchBlob from "rn-fetch-blob";
import FileViewer from "react-native-file-viewer";
import RNFS from "react-native-fs";
import Route from "../../network/route.js";
const route = new Route("http://192.168.100.18:8000/api/");
import moment from "moment";

class Results extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appointment_id: "",
      data: [
        { name: "Mediveron", price: 200 },
        { name: "Health Clinic", price: 400 },
        { name: "Bjourn Health", price: 600 }
      ]
    };
  }

  componentDidMount() {
    this.setState({
      appointment_id: this.props.navigation.state.params.appointment_id
    });
  }
  removeItem(props) {
    var arr = this.state.data;
    arr.splice(props, 1);
    this.setState({ data: arr });
  }

  async downloadPdf() {
    if (Platform.OS == "ios") {
      this.fetchPdf();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "Vita App Storage Permission",
            message: "Vita App needs access to your Storage ",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the storage");
          this.fetchPdf();
        } else {
          console.log("Storage permission denied");
          alert("you can not donwload pdf write now");
        }
      } catch (err) {
        console.warn(err);
      }
    }
  }

  async fetchPdf() {
    // await this.requestStoragePermission()
    //    this.setState({ loading: true });
    var now = new Date();
    let dirs = "";
    if (Platform.OS == "android") {
      dirs = RNFetchBlob.fs.dirs.DownloadDir;
    } else {
      dirs = RNFetchBlob.fs.dirs.DocumentDir;
    }

    this.setState({ loading: true });
    route
      .updateData(
        "get-appontment-pdf",
        { appointment_id: this.state.appointment_id },
        this.props.myToken
      )
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.props.stopLoading();
          this.setState({ loading: false });
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          console.log("PDF url", response);
          this.setState({ loading: false });

          console.log("dirs", RNFetchBlob.fs);
          RNFetchBlob.config({
            // add this option that makes response data to be stored as a file,
            // this is much more performant.
            fileCache: false,
            useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
            notification: true,
            path: dirs + "/AppointmentDetail" + now + ".pdf",
            IOSBackgroundTask: true,
            indicator: true,
            addAndroidDownloads: {
              useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
              notification: true,
              path: dirs + "/AppointmentDetail" + now + ".pdf", // this is the path where your downloaded file will live in
              description: "Downloading file."
            }
          })
            .fetch("GET", response.pdf.pdf_url, {})
            .then(res => {
              // the temp file path
              this.setState({ loading: false });
              console.log("The file saved to ", res);
              console.log("doument directory path", RNFS.DocumentDirectoryPath);
              FileViewer.open(res.data);
              // RNFetchBlob.openDocument(res.path());
            })
            .catch((errorMessage, statusCode) => {
              this.setState({ loading: false });
              // alert(errorMessage);
            });
        }
      });
  }
  changeDateFormat(date) {
    var newDate = moment(date).format("MMM, DD YYYY");
    return newDate;
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <Header
          label={I18n.t("results")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <ScrollView
          style={{ flex: 1, marginVertical: 13, backgroundColor: "white" }}
        >
          <View style={{ width: "85%", flexWrap: "wrap", paddingLeft: 3 }}>
            <Text style={styles.title3}>
              {I18n.t("appointment_with_the")} {this.props.navigation.state.params.clinicName}{" "}
              {I18n.t("was_completed_on")} {this.changeDateFormat(this.props.navigation.state.params.appointmentDate)}{" "}
              {I18n.t("below_are_your_details")}
            </Text>
          </View>

          <View style={styles.fieldView}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                width: "100%",
                alignItems: "flex-start"
              }}
            >
              <View style={{ width: 25, height: 25 }}>
                <Image
                  source={require("../../asset/images/clipboard.png")}
                  style={{ width: 25, height: 25 }}
                  resizeMode="contain"
                />
              </View>

              <View style={{ flexWrap: "wrap", paddingHorizontal: 5 }}>
                <Text style={styles.title1}>{I18n.t("lab_results")}</Text>
                <TouchableOpacity
                  style={{ paddingVertical: 8 }}
                  onPress={() => this.downloadPdf()}
                >
                  <Text style={styles.title2}>{I18n.t("download_pdf")}</Text>
                </TouchableOpacity>

                {/* <TouchableOpacity onPress={() => this.cancelAppointment()}>
                                        <Text style={styles.title3}>Request cancellation</Text>
                                    </TouchableOpacity> */}
              </View>
            </View>
          </View>
          {/* <View style={{ width: '85%', flexWrap: 'wrap', paddingHorizontal: 3, paddingVertical: 8 }}>
                        <Text style={{
                            fontSize: 16,
                            fontWeight: "400",
                            color: "#969696",
                            letterSpacing: 0.5,
                        }}>Notes</Text>
                    </View>

                    <View style={styles.fieldView}>

                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                width: "100%",
                                alignItems: "flex-start"
                            }}>
                            <View style={{ width: 40, height: 40, borderRadius: 20, borderWidth: 1, borderColor: 'transparent' }}>
                                <Image source={require('../../asset/images/elon.jpg')} style={{ width: 40, height: 40, borderRadius: 20 }} resizeMode='cover' />
                            </View>

                            <View style={{ flexWrap: 'wrap', paddingHorizontal: 7, flex: 1 }}>
                                <Text style={styles.title1}>{I18n.t('doctors_remark')}</Text>
                                <View style={{
                                    paddingVertical: 8, flexWrap: 'wrap', flex: 1
                                }}>
                                    <Text style={{
                                        fontSize: 13,
                                        fontWeight: "400",
                                        letterSpacing: 0.5,
                                        color: '#969696'
                                    }}>Lorem ipsum dolor sit amet, consectetuer adipiscing.Lorem ipsum dolor sit amet, consectetuer adipiscing.</Text>
                                </View>


                                

                            </View>




                        </View>

                    </View> */}
          {/* <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.pop()} style={styles.fieldView}>
                        <View style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContenst: "flex-start",
                            width: "100%"
                        }}>
                            <Text style={styles.title1}>{I18n.t('previous_tests')}</Text>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "flex-end",
                                width: "100%",
                                alignItems: "center"
                            }}>


                            <View>
                                <Icon name="ios-arrow-forward" color="#969696" size={28} />
                            </View>
                        </View>

                    </TouchableOpacity> */}

          <View style={{ alignSelf: "center", paddingVertical: 10 }} />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  titlePrice: {
    fontSize: 15,
    fontWeight: "400",

    color: color.primary,
    letterSpacing: 0.5
    //paddingHorizontal: 10
  },
  title2: {
    fontSize: 13,
    fontWeight: "400",
    letterSpacing: 0.5,
    color: color.primary
  },
  title3: {
    fontSize: 16,
    fontWeight: "600",
    letterSpacing: 0.5,
    paddingVertical: 2,
    color: "#969696",
    paddingLeft: 10
  },
  iconView: {},
  title1: {
    fontSize: 16,
    fontWeight: "400",

    color: "#373535",
    letterSpacing: 0.5
    //paddingHorizontal: 10
  },
  fieldView: {
    flexDirection: "row",
    width: "91%",
    flexWrap: "wrap",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "transparent",
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    backgroundColor: "white",
    elevation: 2, // Android
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingHorizontal: 12,
    paddingVertical: 10,
    marginTop: 19
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Results);
