import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ScrollView,
  Platform,
  PermissionsAndroid,
  Alert
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Feather";
import Route from "../../network/route.js";
import NotificationHeader from "../../components/NotificationHeader";
import Spinner from "react-native-loading-spinner-overlay";
import HTML from "react-native-render-html";
import RNFetchBlob from "rn-fetch-blob";
const route = new Route("http://192.168.100.18:8000/api/");
import FileViewer from "react-native-file-viewer";
import RNFS from "react-native-fs";
import AutoResizeHeightWebView from "react-native-autoreheight-webview";
import { NavigationEvents } from "react-navigation";
import {BoxShadow} from 'react-native-shadow';
import { TouchableHighlight } from 'react-native';

const htmlStyle = `<style>
                    *{
                      padding:0px;
                      margin:0px;
                    }
                    p{
                      font-size:12;
                      padding:0 !important;
                      margin:0 !important;
                      color:#969696;
                      font-weight:bold;
                    }
                  </style>`
                  const htmlStyleLeft = `<style>
                    *{
                      padding:0px;
                      margin:0px;
                    }
                    p{
                      font-size:8;
                      padding:0 !important;
                      margin:0 !important;
                      color:#969696;
                      margin-bottom:10;
                      font-weight:bold;
                    }
                  </style>`
				  
  const width1 = Dimensions.get('window').width * 0.7;
  const width2 = Dimensions.get('window').width * 0.025;
  const width3 = Dimensions.get('window').width * 0.225;
  const EMPTY = " ";
  
  const shadowOpt = {		
	width:width1,
	height:55,
	color:"#000",
	border:4,
	radius:3,
	opacity:0.2,
	x:0,
	y:2,
	style:{marginVertical:5}
  }
	
  const shadowOpt1 = {		
	width:Dimensions.get('window').width * 0.95,
	height:55,
	color:"#000",
	border:4,
	radius:3,
	opacity:0.2,
	x:0,
	y:2,
	style:{marginVertical:5}
  }
	
  const shadowOpt2 = {
	width:width3,
	height:55,
	color:"#000",
	border:4,
	radius:3,
	opacity:0.2,
	x:0,
	y:2,
	style:{marginVertical:5}
  }
  
class AnalysisScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      summaryData: null,
      heartRiskScore: "",
      loading: false,
      diabetesRiskScore: "",
      riskScore: "",
      indicatorPosition: "100%",
      otherRecommendation: []
    };
	
	this.riskScoreArr = [];
	this.otherRecommendationArr = [];
	this.riskScoreUI = "";
	this.otherRecommendationUI = "";
	this.isHeartScoreTitleDisplayed = false;
	this.isRecommendationTitleDisplayed = false;
  }
  riskScoreAndRecommendation = [];
  
  fetchData() {
    this.setState({ loading: true });
    route
      .getAuthenticated("recommendation-summary", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          console.log("recommendation-summary response", response);
          this.setState({ summaryData: response.apiData });
          this.setState({ riskScore: response.apiData.riskScore });
          this.setState({
            otherRecommendation: response.apiData.recommendation
          });
		  this.setState({ obesityScore: response.apiData.obesityScore });
          this.setState({ loading: false });
          console.log("recommendation summary", this.state.summaryData);
        }
      });
  }


  replaceSpaceToUnderScore(string) {
    console.log("real string",string)
    var string = string.trim()
    var lowerCaseString = string.toLowerCase();
    var underScoreString = lowerCaseString.split(" ").join("_");
    var brackitLess = underScoreString.split("(").join("");
    var brackitLess2 = brackitLess.split(")").join("");
    var removeQuestionMark = brackitLess2.split("?").join("");
    var removeQoma = removeQuestionMark.split(",").join("");
    var removeDot = removeQoma.split(".").join("");
    var removeColon = removeDot.split(":").join("");
    var removeSemiColon = removeColon.split(";").join("");

    var singleUnderScore = removeSemiColon.split("-").join("_");
    singleUnderScore = singleUnderScore.split("__").join("_");
    singleUnderScore = singleUnderScore.split("/").join("_");
    singleUnderScore = singleUnderScore.split("\n").join("_");
    singleUnderScore = singleUnderScore.split("'").join("_");
    singleUnderScore = singleUnderScore.split("%").join("_");
    singleUnderScore = singleUnderScore.split("<").join("_");
    singleUnderScore = singleUnderScore.split(">").join("_");
    var newString = singleUnderScore;

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length;
      newString = singleUnderScore.substring(0, 57);
    }
    console.log('traslated string',newString)
    return newString;
  }

  async downloadPdf() {
    if (Platform.OS == "ios") {
      this.fetchPdf();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "Vita App Storage Permission",
            message: "Vita App needs access to your Storage ",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the storage");
          this.fetchPdf();
        } else {
          console.log("Storage permission denied");
          alert("you can not donwload pdf write now");
        }
      } catch (err) {
        console.warn(err);
      }
    }
  }
  

  async fetchPdf() {
    // await this.requestStoragePermission()
    //    this.setState({ loading: true });
    var now = new Date();
    let dirs = "";
    if (Platform.OS == "android") {
      dirs = RNFetchBlob.fs.dirs.DownloadDir;
    } else {
      dirs = RNFetchBlob.fs.dirs.DocumentDir;
    }
    console.log("dirs", RNFetchBlob.fs);
    RNFetchBlob.config({
      // add this option that makes response data to be stored as a file,
      // this is much more performant.
      fileCache: false,
      useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
      notification: true,
      path: dirs + "/RecommendationSummary"+now+".pdf",
      IOSBackgroundTask: true,
      indicator: true,
      addAndroidDownloads: {
        useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
        notification: true,
        path: dirs + "/RecommendationSummary"+now+".pdf", // this is the path where your downloaded file will live in
        description: "Downloading file."
      }
    })
      .fetch("GET", "http://vitahealthapp.com/api/recommendation-summary-pdf", {
        Authorization: `Bearer ${this.props.myToken}`
        // more headers  ..
      })
      .then(res => {
        // the temp file path
        this.setState({ loading: false });
        console.log("The file saved to ", res.path());
        console.log("doument directory path", RNFS.DocumentDirectoryPath);
        FileViewer.open(res.path());
        // RNFetchBlob.openDocument(res.path());
      })
      .catch((errorMessage, statusCode) => {
        this.setState({ loading: false });
        // alert(errorMessage);
      });
  }
  componentDidMount() {
    this.fetchData();
  }

  calculateRangeWidth(range, index) {
    minValue = parseFloat(range[0].min);
    maxValue = parseFloat(range[range.length - 1].max);
    totalValue = maxValue - minValue;
    rangeDif = parseFloat(range[index].max) - parseFloat(range[index].min);
    onePrec = totalValue / 100;
    let viewWidth = parseFloat(rangeDif) / onePrec;
    var width = `${viewWidth}%`;
    return width;
  }

  selectRangeColor(range) {
    if (range.risk_level == 0) {
      return "#87D400";
    } else if (range.risk_level == 1) {
      return "#F2A700";
    } else if (range.risk_level == 2) {
      return "#CB0C12";
    }
  }
  	
  returnBlankWhenUndefined(val){
	  return val == "" || val == "undefined" || val === undefined  || val == "null" || val == null ? " " : val;
  }
  
  formatRiskScore(){
	  this.riskScoreAndRecommendation = this.state.riskScore.split('_split_section_');
	  let tmpRiskScore = this.riskScoreAndRecommendation[0] + "";
	  let values;
	  this.riskScoreArr = tmpRiskScore.split('<br/><br/>');
	  let viewTemplate = [];
	  let isHeartScoreTitleDisplayed = false;
	  
	  viewTemplate.push(this.riskScoreArr.map((data) => {
		values = data.split('<chd_value>');
		
		if(values.length > 1){  
			if(!isHeartScoreTitleDisplayed){
				viewTemplate.push(<HTML style={{flex:1}} html='<span style="color: rgb(255, 0, 0); font-weight: bold;font-size:18px">HEART Score:</span>' />);
				isHeartScoreTitleDisplayed = true;
			}
			
			return (
				<View style={{flex:1, flexDirection:"row"}}>
				  <BoxShadow setting={shadowOpt}>
					<TouchableHighlight style={{
						position:"relative",
						flex:1,
						height: 70,
						backgroundColor: "#fff",
						borderRadius:3,
						overflow:"hidden"}}>
						<HTML html={values[0]} />
					</TouchableHighlight>
				  </BoxShadow>
				  <Text style={{width:width2}}>  </Text>
				  <BoxShadow setting={shadowOpt2}>
						<TouchableHighlight style={{
							position:"relative",
							flex:1,
							height: 70,
							backgroundColor: "#fff",
							borderRadius:3,
							overflow:"hidden"}}> 
							<HTML html={values[1]} />
						</TouchableHighlight>
				  </BoxShadow>
				  </View>
			)
		}
		else{
			values[0] = this.returnBlankWhenUndefined(values[0]);
			
			if(values[0] == EMPTY){
				return (null)
			}
			else{
				return (
					<View style={{flex:1, flexDirection:"row"}}>
					  <BoxShadow setting={shadowOpt1}>
						<TouchableHighlight style={{
							position:"relative",
							flex:1,
							height: 70,
							backgroundColor: "#fff",
							borderRadius:3,
							overflow:"hidden"}}>
							<HTML html={values[0]} />
						</TouchableHighlight>
					  </BoxShadow>
					 </View>
				)
			}
		}
	  }));	
	  return viewTemplate;
  }
  
  formatRecommendation(){
	  let tmpOtherRecommendation = this.riskScoreAndRecommendation[1] + "";
	  let values;
	  this.otherRecommendationArr = tmpOtherRecommendation.split('<br/><br/>');
	  let viewTemplate = [];
	  let isRecommendationTitleDisplayed = false;
	  
	  viewTemplate.push(this.otherRecommendationArr.map((data) => {
		values = data.split('<chd_value>');
		
		if(values.length > 1){
			if(!isRecommendationTitleDisplayed){
				viewTemplate.push(<HTML style={{flex:1}} html='<span style="font-weight:bold;text-decoration: underline;font-size:18px"><u>Recommendations</u></span>' />);
				isRecommendationTitleDisplayed = true;
			}
			
			return (
				<View style={{flex:1, flexDirection:"row"}}>
				  <BoxShadow setting={shadowOpt}>
					<TouchableHighlight style={{
						position:"relative",
						flex:1,
						height: 70,
						backgroundColor: "#fff",
						borderRadius:3,
						overflow:"hidden"}}>
						<HTML html={values[0]} />
					</TouchableHighlight>
				  </BoxShadow>
				  <Text style={{width:width2}}>  </Text>
				  <BoxShadow setting={shadowOpt2}>
						<TouchableHighlight style={{
							position:"relative",
							flex:1,
							height: 70,
							backgroundColor: "#fff",
							borderRadius:3,
							overflow:"hidden"}}> 
							<HTML html={values[1]} />
						</TouchableHighlight>
				  </BoxShadow>
				  </View>
			)
		}
		else{
			values[0] = this.returnBlankWhenUndefined(values[0]);
			
			if(values[0] == EMPTY){
				return (null)
			}
			else{
				return (
				<View style={{flex:1, flexDirection:"row"}}>
				  <BoxShadow setting={shadowOpt1}>
					<TouchableHighlight style={{
						position:"relative",
						flex:1,
						height: 70,
						backgroundColor: "#fff",
						borderRadius:3,
						overflow:"hidden"}}>
						<HTML html={values[0]} />
					</TouchableHighlight>
				  </BoxShadow>
				 </View>)
			}
		}
	  }));	  
	  return viewTemplate;
  }
  
  formatObesity(){
	  console.log("format obesity");
	  let tmpObesityScore = this.state.obesityScore +"";
	  let values;
	  this.obesityScoreArr = tmpObesityScore.split('<br/><br/>');
	  let viewTemplate = [];
	  let isObesityTitleDisplayed = false;
	  
	  viewTemplate.push(this.obesityScoreArr.map((data) => {
		values = data.split('<chd_value>');
		
		if(values.length > 1){  
			if(!isObesityTitleDisplayed){
				viewTemplate.push(<HTML style={{flex:1}} html='<span style="color: rgb(255, 0, 0); font-weight: bold;font-size:18px">Measurement:</span>' />);
				isObesityTitleDisplayed = true;
			}
			
			return (
				<View style={{flex:1, flexDirection:"row"}}>
				  <BoxShadow setting={shadowOpt}>
					<TouchableHighlight style={{
						position:"relative",
						flex:1,
						height: 70,
						backgroundColor: "#fff",
						borderRadius:3,
						overflow:"hidden"}}>
						<HTML html={values[0]} />
					</TouchableHighlight>
				  </BoxShadow>
				  <Text style={{width:width2}}>  </Text>
				  <BoxShadow setting={shadowOpt2}>
						<TouchableHighlight style={{
							position:"relative",
							flex:1,
							height: 70,
							backgroundColor: "#fff",
							borderRadius:3,
							overflow:"hidden"}}> 
							<HTML html={values[1]} />
						</TouchableHighlight>
				  </BoxShadow>
				  </View>
			)
		}
		else{
			values[0] = this.returnBlankWhenUndefined(values[0]);
			
			if(values[0] == EMPTY){
				return (null)
			}
			else{
				return (
					<View style={{flex:1, flexDirection:"row"}}>
					  <BoxShadow setting={shadowOpt1}>
						<TouchableHighlight style={{
							position:"relative",
							flex:1,
							height: 70,
							backgroundColor: "#fff",
							borderRadius:3,
							overflow:"hidden"}}>
							<HTML html={values[0]} />
						</TouchableHighlight>
					  </BoxShadow>
					 </View>
				)
			}
		}
	  }));	
	  return viewTemplate;
  }
  
  render() {
    return (
      <View style={styles.container}>
        <NotificationHeader
          label={I18n.t('analysis')}
          inboxCount={this.props.inboxCount}
          notificationCount={this.props.notificationCount}
          onMailPress={() => this.props.navigation.navigate("inbox")}
          onNotificationPress={() =>
            this.props.navigation.navigate("notifications")
          }
        />
		<NavigationEvents onDidFocus={() => {this.fetchData(); this.state.loading = true;}} />
        <Spinner
          visible={this.state.loading}
          textContent={I18n.t('please_wait')}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />

        <ScrollView keyboardShouldPersistTaps={"always"}>
          <View style={styles.header}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.downloadPdf()}
            >
              <Icon name="arrow-down" color="white" size={23} />
              <Text style={{ color: "white" }}>PDF</Text>
            </TouchableOpacity>
            <View
              style={{
                flexWrap: "wrap",
                display: "flex",
                paddingHorizontal: 4,
                flex: 1
              }}
            >
              <Text style={styles.title}>
                {I18n.t("recommendation_summary")}
              </Text>
              <Text style={styles.title2}>
                {I18n.t("list_of_all_abnormal_result")}{" "}
              </Text>
			</View>
		  </View>
		  <View style={styles.header}>
			<TouchableOpacity
              style={styles.refreshButton}
              onPress={() => this.fetchData()}
            >
              <Text style={{ color: "white", fontSize: 14}}>{I18n.t("refresh")}</Text>
            </TouchableOpacity>
			
			{this.formatRiskScore()}
			{this.formatObesity()}
			{this.formatRecommendation()}
			
          </View>

          {this.state.summaryData != null
            ? this.state.summaryData.riskyTest.map((value, index) => {
                minValue = parseFloat(value.range[0].min);
                maxValue = parseFloat(value.range[value.range.length - 1].max);
                totalValue = maxValue - minValue;
                onePrec = totalValue / 100;
                userValue = value.user_value - minValue;
                var indecotorPosition = parseFloat(userValue) / onePrec;
                if(indecotorPosition < 0){
                    indecotorPosition = 0
                }
                console.log("indecotorPosition", userValue);
                return (
                  <View>
                    <View style={styles.custom}>
                      {console.log("indecotor postion", indecotorPosition)}

                        <View
                          style={{
                            width: "100%",
                            flexDirection: "row",
                            backgroundColor: "pink",
                            justifyContent: "flex-end",
                            marginTop:30
                          }}
                        >
                          {indecotorPosition < 80 ? (
                            <View
                              style={{
                                flexDirection: "row",
                                alignItems: "center",
                                left: `${indecotorPosition - 5}%`,
                                position: "absolute",
                                top:-30
                              }}
                            >
                              <Icon
                                name="corner-right-down"
                                color="black"
                                size={27}
                              />

                              <Text
                                style={{
                                  fontWeight: "500",
                                  color: color.primary
                                }}
                              >
                                {value.user_value + " "}
                              </Text>
                              <View style={{width:100,height:20,marginTop:-10,backgroundColor:'transparent'}}>
                                <AutoResizeHeightWebView
                                    needAutoResetHeight={true}
                                    style={{backgroundColor:'transparent',marginTop:5}}
                                    automaticallyAdjustContentInsets={false}
                                    source={{ html:htmlStyle+ value.measuring_meter  }}
                                  />
                                </View>                            
                                </View>
                          ) : (
                            <View
                              style={{
                                flexDirection: "column",
                                alignItems: "center",
                                left: `${indecotorPosition-5}%`,
                                position: "absolute",
                                top:-35
                              }}
                            >
                              <View style={{flexDirection:'row',marginTop:-10}}>
                              <Text
                                style={{
                                  fontWeight: "500",
                                  marginLeft: -50,
                                  color: color.primary
                                }}
                              >
                                {value.user_value + " "} 
                              </Text>
                              <View style={{width:100,height:20,backgroundColor:'transparent'}}>
                                <AutoResizeHeightWebView
                                    needAutoResetHeight={true}
                                    style={{backgroundColor:'transparent',marginTop:5,}}
                                    automaticallyAdjustContentInsets={false}
                                    source={{ html:htmlStyle+ value.measuring_meter  }}
                                  />
                                </View>
                              </View>

                              <Icon
                                name="corner-right-down"
                                color="black"
                                size={27}
                                style={{marginLeft:-55}}
                              />
                            </View>
                          )}
                        </View>
                        <View
                          style={{
                            height: 38,
                            width: "100%",
                            flexDirection: "row",
                            backgroundColor: color.primary
                          }}
                        >
                          {/* <View style={{ height: '100%', width: `0%`, backgroundColor: '#87D400' }}>

                                        </View>
                                        <View style={{ height: '100%', width: `0%`, backgroundColor: '#F2A700' }}>

                                        </View>
                                        <View style={{ height: '100%', width: '100%', backgroundColor: '#CB0C12' }}>

                                        </View> */}
                          {value.range.map((range, index) => {
                            minValue = parseFloat(value.range[0].min);
                            maxValue = parseFloat(
                              value.range[value.range.length - 1].max
                            );
                            totalValue = maxValue - minValue;
                            rangeDif = 0;
                            if (index == 0) {
                              rangeDif =
                                parseFloat(value.range[index].max) -
                                parseFloat(value.range[index].min);
                            } else {
                              rangeDif =
                                parseFloat(value.range[index].max) -
                                (parseFloat(value.range[index-1].max));
                            }
                            onePrec = totalValue / 100;
                            let viewWidth = parseFloat(rangeDif) / onePrec;
                            console.log("range width", totalValue);
                            return (
                              <View
                                style={{
                                  height: "100%",
                                  width: viewWidth + "%",
                                  backgroundColor: this.selectRangeColor(range)
                                }}
                              />
                            );
                          })}
                        </View>
                      

                      <View
                        style={{
                          flexDirection: "row",
                          width: "100%",
                          alignItems: "center",
                          paddingVertical: 10
                        }}
                      >
                        <View
                          style={{
                            flex: 1,
                            width: "100%",
                            flexDirection: "row",
                            justifyContent: "flex-start"
                          }}
                        >
                          <Text style={{ color: "#373535", fontSize: 18 }}>
                            {value.title}
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            width: "100%",
                            flexDirection: "row",
                            justifyContent: "flex-end"
                          }}
                        >
                          <View style={{flexDirection:"row"}}>
                              <Text style={{ color: "#969696", fontSize: 14 }}>
                                Normal: {value.normal_range + " "} 
                              </Text>
                              <View style={{width:60,height:10,backgroundColor:'transparent'}}>
                                <AutoResizeHeightWebView
                                    defaultHeight={20}
                                    needAutoResetHeight={true}
                                    style={{backgroundColor:'transparent'}}
                                    automaticallyAdjustContentInsets={false}
                                    source={{ html: htmlStyle+value.measuring_meter  }}
                                  />
                                </View>
                              </View>
                        </View>
                      </View>

                      <View
                        style={{
                          flex: 1,
                          width: "100%",
                          justifyContent: "flex-start"
                        }}
                      >
                        <Text style={{ color: "#969696", fontSize: 18 }}>
                        {value.recommend != null ? I18n.t(this.replaceSpaceToUnderScore(value.recommend),{defaultValue: value.recommend}) : ""}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.break} />
                  </View>
                );
              })
            : null}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  custom: {
    backgroundColor: "#f9f9f9",
    width: "100%",
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 18,
    flexWrap: "wrap"
  },
  break: {
    width: "100%",
    borderColor: "#dadfe1",
    borderBottomWidth: 0.5
  },
  custom1: {
    width: "100%",
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 28
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,

    color: "#969696"
  },
  title2: {
    fontSize: 14,
    fontWeight: "400",
    paddingVertical: 5
  },
  title3: {
    fontSize: 14,
    fontWeight: "500",
    letterSpacing: 0.5,
    paddingVertical: 5,
    color: "#9b9b9b"
  },
  iconView: {},
  title1: {
    fontSize: 15,
    fontWeight: "300",
    marginVertical: 15,
    color: "#969696",
    letterSpacing: 0.5,
    paddingHorizontal: 10
  },
  fieldView: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "transparent",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    paddingHorizontal: 8,
    paddingVertical: 10,
    marginTop: 12,
    marginBottom: 3
  },
  button: {
    width: 50,
    height: 50,
    paddingVertical: 2,
    borderRadius: 25,
    backgroundColor: color.primary,
    alignItems: "center",
    justifyContent: "center",
	
  },
  refreshButton: {
	width: 100,
    height: 50,
    paddingVertical: 2,
    borderRadius: 25,
    backgroundColor: color.primary,
    alignItems: "center",
    justifyContent: "center",
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: "90%",
    justifyContent: "space-between",
    /* borderColor: "pink",
              borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingBottom: 15

    /* borderColor: "red",
              borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    width: "95%",
    alignSelf: "center",
    flexDirection: "column",
    marginVertical: 23,
	flex:1
  }
});
function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
	selectedLanguage: state.user.selectedLanguage,
    inboxCount: state.user.inboxCount,
    notificationCount: state.user.notificationCount
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnalysisScreen);
