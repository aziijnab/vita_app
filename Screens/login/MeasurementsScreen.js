import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform
} from "react-native";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";

class MeasurementsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weight: "",
      height: "",
      waistCircumference: "",
      bloodPressure: "",
      capillaryGlucose: ""
    };
  }

  componentDidMount() {}

  render() {
    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps={"always"}>
          <View
            style={{
              width: "90%",
              flexDirection: "row",
              justifyContent: "flex-end",
              top: "10%"
            }}
          >
            <Text style={{ color: "#e5e5e5", fontSize: 17 }}>5 OF 5</Text>
          </View>
          <Text style={styles.title}>VITALS MEASUREMENT</Text>

          <View style={styles.form}>
            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={text => console.log(text)}
              placeholder="Weight (kg)"
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />

            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={text => console.log(text)}
              placeholder="Height (cm)"
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />

            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={text => console.log(text)}
              placeholder="Waist Circumference (cm)"
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />

            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={text => console.log(text)}
              placeholder="Blood Pressure (optional)"
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />

            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={text => console.log(text)}
              placeholder="Capillary Glucose (optional)"
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />
            
          </View>


          <Button
            label="SUBMIT"
            buttonStyle={styles.button}
            onPress={() => this.props.navigation.navigate("lifeStyleScreen")}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "15%",
    paddingLeft: 30
  },
  iconView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingHorizontal: 15
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center"
    //paddingHorizontal: "20%"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 10,
    width: "90%",
    /* borderColor: "pink",
      borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
      borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    borderBottomColor: "#F4AE96",
    borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MeasurementsScreen);
