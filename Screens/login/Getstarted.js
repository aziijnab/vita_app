import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  Platform
} from "react-native";
import { Dropdown } from "react-native-material-dropdown";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from "react-native-google-signin";
import Route from "../../network/route.js";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

import Spinner from "react-native-loading-spinner-overlay";
import Toast, { DURATION } from "react-native-easy-toast";
class Getstarted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      google_token: "",
      user_role: "user",
      role: "user",
      remember: false
    };
  }

  componentDidMount() {
    if (Platform.OS == "android") {
      GoogleSignin.configure({
        webClientId:
          "272191391624-q112ide48k1srjo9dmt11o9e40dktq28.apps.googleusercontent.com",
        // WebClientSecret:'ynpStqYhmrMrxJPR-3Cr8EOX',
        offlineAccess: false
      });
    } else {
      GoogleSignin.configure({
        webClientId:
          "272191391624-lnn28972i58sbdajsrag4e7t8872hq8b.apps.googleusercontent.com",
        // WebClientSecret:'ynpStqYhmrMrxJPR-3Cr8EOX',
        offlineAccess: false
      });
    }
  }

  _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      // this.setState({ userInfo });
      console.log("user gmail ", userInfo);
      var emailSplit = userInfo.user.email.split("@");
      var username = emailSplit[0];
      this.setState(
        {
          username: username,
          email: userInfo.user.email,
          google_token: userInfo.accessToken
        },
        () => {
          this.setState({ loading: true });
          let credentials = {
            username: this.state.username,
            email: this.state.email,
            google_token: this.state.google_token,
            user_role: this.user_role,
            role: this.state.role
          };
          route.postdata("google-login", credentials).then(async response => {
            if (response.error) {
              this.setState({ loading: false });
              this.refs.toast.show(response.error, DURATION.SHORT);
            } else {
              console.log("user login response", response);
              this.props.setUser(response);
              // await this.props.alterUser(response.loggedUser)

              // console.log(this.props.user);
              // response.loggedUser.is_profile_setup = 0
              if(response.loggedUser.user_profile.full_name != null){
                this.setState({loading:false})
                this.props.navigation.navigate("App")
                this.props.isProfileSetup(true);
              
              } else {
                this.setState({ loading: false });
                this.props.navigation.navigate("createProfileScreen");
              }
            }
          });
        }
      );
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login
        alert("user cancelled the login ");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        alert("operation (f.e. sign in) is in progress already");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log("play services not available or outdated");
      } else {
        // some other error happened
        console.log("error", error);
        alert(JSON.stringify(error));
      }
    }
  };
  render() {
    let maxpdata = [
      { label: "No max", value: "" },
      { label: "$50K", value: "50000" },
      { label: "$74K", value: "75000" },
      { label: "$100K", value: "100000" },
      { label: "$125K", value: "125000" },
      { label: "$150K", value: "150000" },
      { label: "$175K", value: "175000" },
      { label: "$200K", value: "200000" },
      { label: "$225K", value: "225000" },
      { label: "$250K", value: "250000" },
      { label: "$275K", value: "275000" },
      { label: "$300K", value: "300000" },
      { label: "$325K", value: "325000" },
      { label: "$350K", value: "350000" },
      { label: "$375K", value: "375000" },
      { label: "$400K", value: "400000" },
      { label: "$425K", value: "425000" }
    ];
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: "red" }}
          position="top"
          positionValue={80}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{ color: "white" }}
        />
        <Text style={styles.title}>{I18n.t("get_started_heading")}</Text>

        <View
          style={{
            width: "85%",
            alignItems: "center",
            flexWrap: "wrap",
            paddingTop: 50,
            paddingBottom: 60,
            alignSelf: "center"
            //paddingHorizontal: "20%"
          }}
        >
          <Text style={styles.title1}>{I18n.t("getstarted")}</Text>
        </View>
        <Button
          label={I18n.t("sign_up_with_email")}
          buttonStyle={styles.button}
          onPress={() => this.props.navigation.navigate("signupScreen")}
        />

        {/* <TouchableOpacity
          activeOpacity={0.9}
          onPress={this._signIn}
          style={{
            flexDirection: "row",
            height: "8%",
            backgroundColor: "#4285F4",
            alignSelf: "center",
            padding: 3,
            width: "90%",
            borderRadius: 4
          }}
        >
          <View
            style={{
              height: "100%",
              flex: 0.6,
              width: "100%",
              padding: 10,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "white"
            }}
          >
            <Image
              style={{ width: "100%", height: "100%" }}
              resizeMode="contain"
              source={require("../../asset/images/search.png")}
            />
          </View>

          <View
            style={{
              flex: 4,
              backgroundColor: "#4285F4",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text style={{ color: "white", fontSize: 20, fontWeight: "400" }}>
              {I18n.t("sign_in_with_google")}
            </Text>
          </View>
        </TouchableOpacity> */}

        <View style={styles.footer}>
          <Text style={{ color: "#969696" }}>{I18n.t("have_an_account")}</Text>
          <Text
            onPress={() => this.props.navigation.navigate("loginScreen")}
            style={{ color: color.primary, paddingHorizontal: 3 }}
          >
            {I18n.t("login_bm_get_start")}
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    alignSelf: "center",
    paddingTop: "20%"
  },
  title1: {
    fontSize: 24,
    letterSpacing: 0.5,
    fontWeight: "700",
    color: "#b2bec3"

    //paddingHorizontal: "20%"
  },
  pickerSt: {
    marginVertical: 7,
    width: "80%",
    alignSelf: "center",
    //flexDirection: "row",
    paddingTop: 6,
    paddingHorizontal: 8,
    justifyContent: "center",
    //alignItems: "center",
    //paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginVertical: 30,

    backgroundColor: color.primary
  },
  input: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 6,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: "10%",
    width: "90%",
    /* borderColor: "pink",
    borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    flexWrap: "wrap"
    /* borderColor: "red",
    borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  }
});

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Getstarted);
