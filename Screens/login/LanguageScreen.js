import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Platform,
  FlatList
} from "react-native";
import { color, font, apiURLs } from "../../components/Constant";
import Button from "../../components/Button";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import SplashScreen from "react-native-splash-screen";
import I18n from "../../i18n/i18n";
import moment from "moment";
import 'moment/locale/en-gb'
import 'moment/locale/zh-cn'
import 'moment/locale/ms'

class LanguageScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPinCodeSet:this.props.isPinCodeSet,
      languages: [
        { name: I18n.t("english"), selected: true },
        { name: I18n.t("mandarin"), selected: false },
        { name: I18n.t("bm"), selected: false }
      ]
    };
    
    // this.props.navigation.navigate("createProfileScreen");
    I18n.locale = this.props.selectedLanguage;
  }

  componentDidMount() {
    //SplashScreen.hide();
    //console.log('is pin code set',this.props.isPinCodeSet)
  }

  markCheck(name) {
    if (name == I18n.t("english")) {
      this.props.changeLanguage('en')
      I18n.locale = "en";
      moment.locale('en-gb');
      this.setState({
        languages: [
          { name: I18n.t("english"), selected: true },
          { name: I18n.t("mandarin"), selected: false },
          { name: I18n.t("bm"), selected: false }
        ]
      });
    } else if (name == I18n.t("mandarin")) {
      this.props.changeLanguage('ms')
      moment.locale('zh-cn');

      I18n.locale = "ms";
      this.setState({
        languages: [
          { name: I18n.t("english"), selected: false },
          { name: I18n.t("mandarin"), selected: true },
          { name: I18n.t("bm"), selected: false }
        ]
      });
    } else if (name == I18n.t("bm")) {
      I18n.locale = "bm";
      moment.locale('ms');

      this.props.changeLanguage('bm')
      this.setState({
        languages: [
          { name: I18n.t("english"), selected: false },
          { name: I18n.t("mandarin"), selected: false },
          { name: I18n.t("bm"), selected: true }
        ]
      });
    }
  }

  render() {
    console.log("language selected", this.props.selectedLanguage)
    return (
      <View style={styles.container}>
        <View style={styles.header} />
        <View style={styles.inner}>
          <Text style={styles.title}>{I18n.t("select_language")}</Text>

          <View style={styles.list}>
            <View>
              <FlatList
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                data={this.state.languages}
                showsVerticalScrollIndicator={false}
                renderItem={({ item, index }) => (
                  <TouchableOpacity
                    onPress={() => this.markCheck(item.name)}
                    activeOpacity={0.9}
                    style={{
                      paddingVertical: 27,
                      backgroundColor: item.selected ? "#373535" : "white",
                      paddingHorizontal: 30
                    }}
                  >
                    <Text
                      style={{
                        color: item.selected ? "white" : "#373535",
                        fontSize: 17
                      }}
                    >
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
          <View style={styles.button}>
            <Button 
              label={I18n.t('done')}
              onPress={() => this.props.navigation.navigate("settingsScreen")}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column"
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingLeft: 30
  },
  inner: {
    flex: 1,

    paddingTop: 50,
    paddingBottom: 20
  },

  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 15,
    paddingVertical: 10,
    alignSelf: "center",
    width: "90%",
    bottom: 50
  },
  break: {
    width: "100%",
    borderColor: "#dadfe1",
    borderBottomWidth: 0.5
  },
  header: {
    /*     shadowOpacity: 0.45,
    shadowRadius: 5,

    shadowColor: "black",
    shadowOffset: { height: 5, width: 0 }, */
    height: 20,
    width: "95%",
    alignSelf: "center",
    // borderBottomColor: "#F4AE96",
    // borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  },
  list: {
    flex: 1,
    flexDirection: "column",

    justifyContent: "center",
    width: "100%"
  }
});

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    boarDone: state.user.boardDone,
    selectedLanguage: state.user.selectedLanguage,
    pinCode:state.user.pinCode,
    isPinCodeSet: state.user.isPinCodeSet,
    // selectedLanguage:state.user.selectedLanguage.length,
    isProfileSetUp:state.user.isProfileSetUp,
    isQuestionAnswerSetUp:state.user.isQuestionAnswerSetUp
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LanguageScreen);
