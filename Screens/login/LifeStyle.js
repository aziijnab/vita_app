import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    Picker
} from "react-native";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";

class LifeStyle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activityLevel: "",
            regularSmoking: "",
            yearEnded: "",
            alcoholConsumption: "",
            drinkingRoutine: "",
            dailyAlcoholIntake: "",
            dietPortion:"",
            supplementsIntake:"",
            sexualPartners:"",
            dialysis:"",
        };
    }

    componentDidMount() { }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView keyboardShouldPersistTaps={"always"}>
                    <View
                        style={{
                            width: "90%",
                            flexDirection: "row",
                            justifyContent: "flex-end",
                            top: "10%"
                        }}
                    >
                        <Text style={{ color: "#e5e5e5", fontSize: 17 }}>3 OF 5</Text>
                    </View>
                    <Text style={styles.title}>{I18n.t('lifestyle')}</Text>

                    <View style={styles.form}>
                        <View style={styles.fieldView}>
                            <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.activityLevel}
                                    style={{ height: 50, width: "100%" }}
                                    
                                    onValueChange={(itemValue, itemIndex) => this.setState({ activityLevel: itemValue })}>
                                    <Picker.Item color={"#969696"} label="Select your activity level" disabled selected />
                                    <Picker.Item color={"#969696"} label="1" value="1" />
                                    <Picker.Item color={"#969696"} label="2" value="2" />
                                    <Picker.Item color={"#969696"} label="3" value="3" />
                                    <Picker.Item color={"#969696"} label="4" value="4" />
                                    <Picker.Item color={"#969696"} label="5" value="5" />
                                </Picker>
                            </View>
                        </View>

                        <View style={styles.fieldView}>
                            <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.regularSmoking}
                                    style={{ height: 50, width: "100%" }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ regularSmoking: itemValue })}>
                                    <Picker.Item color={"#969696"} label="Are you regularly smoking?" disabled selected />
                                    <Picker.Item color={"#969696"} label="Yes" value="yes" />
                                    <Picker.Item color={"#969696"} label="No" value="no" />
                                </Picker>
                            </View>


                            <View style={styles.custom1}>
                                <TextInput style={styles.touchable3}
                                    selectionColor={"#dfe6e9"}
                                    onChangeText={text => console.log(text)}
                                    placeholder={I18n.t('age_started')}
                                    autoCapitalize="words"
                                    autoCorrect={true}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="#969696"
                                />



                                <View style={styles.touchable3}>
                                    <Picker
                                        mode="dialog"
                                        selectedValue={this.state.yearEnded}
                                        style={{ height: 50, width: "100%" }}
                                        onValueChange={(itemValue, itemIndex) => this.setState({ yearEnded: itemValue })}>
                                        <Picker.Item color={"#969696"} label="Year Ended" disabled selected />
                                        <Picker.Item color={"#969696"} label="2015" value="2015" />
                                        <Picker.Item color={"#969696"} label="2016" value="2016" />
                                        <Picker.Item color={"#969696"} label="2017" value="2017" />
                                        <Picker.Item color={"#969696"} label="2018" value="2018" />
                                    </Picker>
                                </View>
                            </View>
                        </View>

                        <View style={styles.fieldView}>
                            <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.alcoholConsumption}
                                    style={{ height: 50, width: "100%" }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ alcoholConsumption: itemValue })}>
                                    <Picker.Item color={"#969696"} label="Do you consume alcohol?" disabled selected/>
                                    <Picker.Item color={"#969696"} label="Yes" value="yes" />
                                    <Picker.Item color={"#969696"} label="No" value="no" />
                                </Picker>
                            </View>
                        </View>

                        <View style={styles.textView}>
                            <View style={styles.custom}>
                                <Text style={{ color: "#969696", fontWeight: "800" }}>
                                    During the last 12 months, how often did you usually have any kind
                                    of drink containing alcohol? By a drink we mean half an ounce of
                                    absolute alcohol (e.g. a 330ml can or glass of beer or cooler, a
                                    glass of wine, or a drink containig 1 shot of liquor).
                                </Text>
                            </View>
                        </View>    

                        <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.drinkingRoutine}
                                    style={{ height: 50, width: "100%" }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ drinkingRoutine: itemValue })}>
                                    <Picker.Item color={"#969696"} label="Daily" value="daily" />
                                    <Picker.Item color={"#969696"} label="Weekly" value="weekly" />
                                    <Picker.Item color={"#969696"} label="Monthly" value="monthly" />
                                </Picker>
                            </View>

                        <View style={styles.textView}>
                            <View style={styles.custom}>
                                <Text style={{ color: "#969696", fontWeight: "800" }}>
                                    How many alcoholic drinks did you have on a typical day when you drank alcohol?
                                </Text>
                            </View>
                        </View>

                        <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.dailyAlcoholIntake}
                                    style={{ height: 50, width: "100%" }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ dailyAlcoholIntake: itemValue })}>
                                    <Picker.Item color={"#969696"} label="25" value="25" />
                                    <Picker.Item color={"#969696"} label="more then 25" value="more then 25" />
                                    
                                </Picker>
                            </View>
                        <View style = {styles.fieldView}></View>

                        <View style={styles.textView}>
                            <View style={styles.custom}>
                                <Text style={{ color: "#969696", fontWeight: "800" }}>
                                        How many portions of fruit or vegetable per day (one portion:
                                        one apple, 4 spoons of veg)?
                                </Text>
                            </View>
                        </View>

                        <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.dietPortion}
                                    style={{ height: 50, width: "100%" , }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ dietPortion: itemValue })}>
                                    <Picker.Item color={"#969696"} label="Select 0-10" disabled selected />
                                    <Picker.Item color={"#969696"} label="0" value="0" />
                                    <Picker.Item color={"#969696"} label="1" value="1" />
                                    <Picker.Item color={"#969696"} label="2" value="2" />
                                    <Picker.Item color={"#969696"} label="3" value="3" />
                                    <Picker.Item color={"#969696"} label="4" value="4" />
                                    <Picker.Item color={"#969696"} label="5" value="5" />
                                    <Picker.Item color={"#969696"} label="6" value="6" />
                                    <Picker.Item color={"#969696"} label="7" value="7" />
                                    <Picker.Item color={"#969696"} label="8" value="8" />
                                    <Picker.Item color={"#969696"} label="9" value="9" />
                                    <Picker.Item color={"#969696"} label="10" value="10" />
                                    
                                    
                                    
                                </Picker>
                            </View>
                            <View style={styles.textView}>
                            <View style={styles.custom}>
                                <Text style={{ color: "#969696", fontWeight: "800" }}>
                                        Are you taking any herbal or pharmaceutical supplements regularly?
                                </Text>
                            </View>
                        </View>

                        <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.supplementsIntake}
                                    style={{ height: 50, width: "100%" }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ supplementsIntake: itemValue })}>
                                    <Picker.Item color={"#969696"} label="Yes/No" disabled selected />
                                    <Picker.Item color={"#969696"} label="Yes" value="yes" />
                                    <Picker.Item color={"#969696"} label="No" value="no" />
                                </Picker>
                            </View>

                            <View style={styles.textView}>
                            <View style={styles.custom}>
                                <Text style={{ color: "#969696", fontWeight: "800" }}>
                                       Have you had more than 2 sexula partners in the past 6 months?
                                </Text>
                            </View>
                        </View>

                        <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.sexualPartners}
                                    style={{ height: 50, width: "100%" }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ sexualPartners: itemValue })}>
                                    <Picker.Item color={"#969696"} label="Yes/No" disabled selected />
                                    <Picker.Item color={"#969696"} label="Yes" value="yes" />
                                    <Picker.Item color={"#969696"} label="No" value="no" />
                                </Picker>
                            </View>

                            <View style={styles.textView}>
                            <View style={styles.custom}>
                                <Text style={{ color: "#969696", fontWeight: "800" }}>
                                        Do you have any history of dialysis/blood tansfusions?
                                </Text>
                            </View>
                        </View>

                        <View style={styles.pickerview}>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.state.dialysis}
                                    style={{ height: 50, width: "100%" }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ dialysis: itemValue })}>
                                    <Picker.Item color={"#969696"} label="Yes/No" disabled selected />
                                    <Picker.Item color={"#969696"} label="Yes" value="yes" />
                                    <Picker.Item color={"#969696"} label="No" value="no" />
                                </Picker>
                            </View>
                    </View>


                    <Button
                        label="NEXT"
                        buttonStyle={styles.button}
                        onPress={() => this.props.navigation.navigate("currentSymptomsScreen")}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "100%",
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: "flex-start"
    },
    custom1: {
        flexDirection: "row",
        width: "96%",
        justifyContent: "space-around"
    },
    title: {
        fontSize: 25,
        fontWeight: "700",
        letterSpacing: 0.5,
        paddingTop: "15%",
        paddingLeft: 30
    },
    iconView: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "flex-end",
        paddingHorizontal: 15
    },
    title1: {
        fontSize: 15,
        letterSpacing: 0.5,
        textAlign: "center"
        //paddingHorizontal: "20%"
    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 5,
        width: "48%",
        flexDirection: "row",
        paddingVertical: 2,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    fieldView: {
        marginVertical: 20
    },

    textView:{
        marginVertical: 15
    },

    pickerview: {
        marginVertical: 7,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 2,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 27,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        paddingVertical: 20,
        width: "90%",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    header: {
        height: 50,
        width: "100%",
        justifyContent: "flex-start",
        flexDirection: "row",
        alignSelf: "center",
        borderBottomColor: "#F4AE96",
        borderBottomWidth: 1,
        marginTop: Platform.OS == "ios" ? 20 : 0,
        backgroundColor: "white"
    }
});

function mapStateToProps(state, props) {
    return {
        userDetail: state.user.userDetail
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LifeStyle);
