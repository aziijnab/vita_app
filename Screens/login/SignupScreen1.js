import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  Alert,
  ScrollView,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import Route from "../../network/route.js";
import CheckBox from "react-native-check-box";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Spinner from "react-native-loading-spinner-overlay";
import Toast, { DURATION } from "react-native-easy-toast";
import Header from "../../components/Header";

class SignupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNum: "",
      address: "",
      companyName: "",
      remember: false,
      showConfirmation: false,
      loading: false,
	  isChecked: false,
	  isChecked2: false
    };
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    console.log(params.basicData);
  }

  registerWithData() {
    const { params } = this.props.navigation.state;
    var basicdata = params.basicData;
    var credentials = {
      mobile: this.state.mobileNum,
      address: this.state.address,
      username: basicdata.username,
      email: basicdata.email,
      password: basicdata.password,
	  ic_number: basicdata.ic_number,
	  fullname: basicdata.fullname,
      user_role: "user",
      role: "user"
    };
	
    //this.props.userLogin(credentials);
    if (this.state.isChecked && this.state.isChecked2) {
      var requestData = { email: this.state.email };
      this.setState({ loading: true });
	 
      route.postdata("register", credentials).then(async response => {
        if (response.error) {
          this.setState({ loading: false });
          this.refs.toast.show(response.error, DURATION.SHORT);
        } else {
          console.log(response);
          // this.props.setUser({token:"",userInfo:null})
          this.setState({ loading: false });
          if (response.message) {
            this.refs.toast.show(response.message, DURATION.SHORT);
          } else {
            this.setState({ showConfirmation: true });
          }
        }
      });
    } else {
      this.refs.toast.show(
        `${I18n.t("please_check_terms_conditions")}`,
        DURATION.SHORT
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Header label="" onbackPress={() => this.props.navigation.pop()} />
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: "red" }}
          position="top"
          positionValue={60}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{ color: "white" }}
        />
        {this.state.showConfirmation ? (
          <View style={{ flex: 1, backgroundColor: "white" }}>
            <View style={styles.header}>
              <TouchableOpacity
                style={{
                  paddingVertical: 3,
                  paddingHorizontal: 8,
                  alignItems: "center",
                  justifyContent: "center"
                }}
                activeOpacity={0.9}
                onPress={
                  () => this.props.navigation.navigate("loginScreen")

                  //this.props.navigation.navigate("createProfileScreen")
                }
              >
                <Icon name="md-arrow-back" size={27} color="#969696" />
              </TouchableOpacity>
            </View>
            <Text style={styles.title}>{I18n.t("varification")}</Text>
            <View
              style={
                {
                  width: "78%",
                  alignItems: "center",
                  flexWrap: "wrap",
                  paddingVertical: 80,
                  alignSelf: "center"
                }
                //paddingHorizontal: "20%"
              }
            >
              <Text style={styles.title1}>
                {I18n.t("activation_link_send_to_your_mail")}
              </Text>
              <Text style={styles.title1}>{I18n.t("thanks")}</Text>
            </View>
          </View>
        ) : (
          <View style={{ flex: 1 }}>
            <KeyboardAwareScrollView>
              <Text style={styles.title}>{I18n.t("sign_up")}</Text>

              <View style={styles.form}>
                <View style={styles.input}>
                  <Icon
                    name="ios-phone-portrait"
                    size={24}
                    color="#969696"
                    style={{ padding: 10 }}
                  />
                  <TextInput
                    style={{
                      /* paddingLeft: 8, fontSize: 17, width: "100%" */
                      //width: "100%",
                      flex: 1,
                      marginRight: 10,
                      paddingTop: 10,
                      paddingRight: 10,
                      paddingBottom: 10,
                      paddingLeft: 0,
                      backgroundColor: "#fff",
                      marginBottom: 2,
                      color: "#424242"
                    }}
                    selectionColor={"#dfe6e9"}
                    onChangeText={mobileNum => this.setState({ mobileNum })}
                    multiline={false}
                    placeholder={I18n.t("mobile_number")}
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="phone-pad"
                    returnKeyType="next"
                    underlineColorAndroid="transparent"
                    placeholderTextColor="#969696"
                  />
                </View>

                <View style={styles.remember}>
                  
				  <CheckBox style={{padding:1}} 
						onClick={()=>{
							this.setState({
								isChecked:!this.state.isChecked
							})
						}}
						isChecked={this.state.isChecked}
					/>
					
				  <Text style={{paddingLeft: 1, color: "#969696" }}>
                      {I18n.t("accept_all")}{" "}
                    </Text>
                    <Text
                      onPress={() =>
                        this.props.navigation.navigate("termsView")
                      }
                      style={{ color: color.primary }}
                    >
                      {I18n.t("terms_and_conditions")}
					  
					  
                    </Text>
					

                    
                </View>
				
				<View style={styles.remember}>
				  <CheckBox style={{padding:1}} 
						onClick={()=>{
							this.setState({
								isChecked2:!this.state.isChecked2
							})
						}}
						isChecked={this.state.isChecked2}

					/>
					
				  <Text style={{paddingLeft: 1, color: "#969696" }}>
                      {I18n.t("accept_all")}{" "}
                    </Text>
                    <Text
                      onPress={() =>
                        this.props.navigation.navigate("privacyPolicy")
                      }
                      style={{color: color.primary }}
                    >
                      {I18n.t("privacy_policy")}
					  
					  
                    </Text>
                  </View>
              </View>

              <Button
                label={I18n.t("sign_up")}
                buttonStyle={styles.button}
                onPress={() => this.registerWithData()}
              />
            </KeyboardAwareScrollView>

            <View style={styles.footer}>
              <Text style={{ color: "#969696" }}>
                {I18n.t("have_an_account")}?
              </Text>
              <Text
                onPress={() => this.props.navigation.navigate("loginScreen")}
                style={{ color: color.primary, paddingHorizontal: 3 }}
              >
                {I18n.t("login")}
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    alignSelf: "center",
    paddingTop: "20%"
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center"
    //paddingHorizontal: "20%"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  input: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingLeft: 9,
    paddingRight: 12,
    borderWidth: 1,
    borderColor: "#dfe6e9",
    marginVertical: 13,
    borderRadius: 25
  },
  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 20,
    width: "90%",
    /* borderColor: "pink",
    borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
    borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    /* borderBottomColor: "#F4AE96",
    borderBottomWidth: 1, */
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupScreen);
