//import liraries
import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import Onboarding from "react-native-onboarding-swiper";
import { color, font, apiURLs } from "../../components/Constant";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/Ionicons";
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";

// create a component
class Onboard extends React.Component {
  /* Next() {
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={{
          width: 80,
          height: 80,
          borderRadius: 40,
          backgroundColor: color.primary,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Icon name="ios-arrow-forward" size={23} color="white" />
      </TouchableOpacity>
    );
  } */
  boardScreenSubmit(){

    this.props.boardScreenDone()
    this.props.navigation.navigate('getStarted');
  }

  render() {
    //const color = color.primary;

    const Next = ({ isLight, ...props }) => (
      <Button
        boolean={false}
        clear
        icon={<Icon name="ios-arrow-forward" size={25} color="white" />}
        title=""
        containerStyle={{
          marginTop: 5,
          marginRight: 5,
          marginBottom: 5,
          width: 50,
          height: 50,
          borderRadius: 25,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: color.primary
        }}
        buttonStyle={{
          backgroundColor: "transparent",

          width: 50,
          height: 50,
          alignSelf: 'center',
        }}
        {...props}
      />
    );

    const Square = ({ isLight, ...props }) => {
      return (
        <Button
          boolean={false}
          clear
          icon={<Icon name="md-checkmark" size={25} color="white" />}
          title=""
          containerStyle={{
            marginTop: 5,
            marginRight: 5,
            marginBottom: 5,
            width: 50,
            height: 50,
            borderRadius: 25,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: color.primary
          }}
          buttonStyle={{
            backgroundColor: "transparent",
            alignSelf: 'center',
            width: 50,
            height: 50,
          }}
          {...props}
        />
      );
    };

    return (
      <View style={styles.container}>
        <Onboarding
          controlStatusBar={false}
          showSkip={false}
          bottomBarHighlight={false}
          onDone={() => this.boardScreenSubmit()}
          //nextLabel={Next}
          DoneButtonComponent={Square}
          NextButtonComponent={Next}
          pages={[
            {
              backgroundColor: "#fff",
              image: <Image source={require("../../asset/images/ob1.png")} />,
              title: I18n.t('doctor_guided_advice'),
              subtitle: I18n.t('doctor_guided_advice_text')
            },
            {
              backgroundColor: "#fff",
              image: <Image source={require("../../asset/images/ob2.png")} />,
              title: I18n.t('accessibility_and_convenience'),
              subtitle: I18n.t('accessibility_and_convenience_text')
            },
            {
              backgroundColor: "#fff",
              image: <Image source={require("../../asset/images/ob4.png")} />,
              title: I18n.t('accurate_lab_results'),
              subtitle: I18n.t('accurate_lab_results_text')
            }
          ]}
        />
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  // console.log('map start proos',state,props);
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    boardScreenDone: state.user.boardDone
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}
//make this component available to the app
export default connect(mapStateToProps, mapDispatchToProps)(Onboard);
