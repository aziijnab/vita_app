import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  Alert,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator
} from "react-native";
import Route from "../../network/route.js";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast'

class SignupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      repassword:'',
      remember: false,
      loading:false,
	  ic_number:'',
	  fullname:''
    };
  }

  validateEmail(email) 
  {
      var re = /\S+@\S+\.\S+/;
      console.log("email valide method",re.test(email))
      return re.test(email);
  }

  componentDidMount() { }

  signUpNext(){
    var errorMessages = []
    /*if(this.state.username.length == 0){
      errorMessages.push(`${I18n.t('please_enter_your_name')}`)
    }*/
    if(this.state.email == '' && this.validateEmail(this.state.email) == false){
      console.log("email is not valid")
      errorMessages.push(`${I18n.t('email_is_not_valid_or_empty')}`)
    }
    if(this.state.password.length < 6){
      errorMessages.push(`${I18n.t('password_must_have_6_characters')}`)
    }
    if(this.state.password != this.state.repassword){
      errorMessages.push(`${I18n.t('password_not_match')}`)
    }
    if(errorMessages.length == 0){
      var emailData = {email:this.state.email}
      this.setState({loading:true})
      route.postdata("emailExist", emailData)
      .then(async (response) => {

        if (response.error) {
          this.setState({loading:false})
          this.refs.toast.show(response.error,DURATION.SHORT);
        }
        else {
              console.log(response);
              // this.props.setUser({token:"",userInfo:null})
              if(response == false){
                this.setState({loading:false})
                this.props.navigation.navigate("signupScreen1",{basicData:
                  {
                    username:this.state.username,
                    email:this.state.email,
                    password:this.state.password,
					ic_number:this.state.ic_number,
					fullname:this.state.fullname
                  }
                })
              }else{
                this.setState({loading:false})
                this.refs.toast.show(`${I18n.t('email_exist')}`,DURATION.SHORT);
              }
            }

      })


      
    }else{
      let errorArray = errorMessages.join("\n")
      this.refs.toast.show(errorArray,DURATION.SHORT);
    }
  
  }

  render() {
    return (
      <View style={styles.container}>
      <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t('please_wait')}...`}
          textStyle={{color:color.primary}}
          cancelable={true}
          color="#F16638"
        />
      <Toast
        ref="toast"
        style={{backgroundColor:'red'}}
        position='top'
        positionValue={80}
        fadeInDuration={750}
        fadeOutDuration={1000}
        textStyle={{color:'white'}}
      />
        <KeyboardAwareScrollView>
          <Text style={styles.title}>{I18n.t("sign_up")}</Text>
          {this.props.showIndicator && <ActivityIndicator size="large" color="red" animating={true} />}

          <View style={styles.form}>
			      <View style={styles.input}>
              <Icon
                name="ios-person"
                size={24}
                color="#969696"
                style={{ padding: 10 }}
              />
              			  
			  <TextInput
                style={{
                  /* paddingLeft: 8, fontSize: 17, width: "100%" */
                  //width: "100%",
                  flex: 1,
                  marginRight: 10,
                  paddingTop: 10,
                  paddingRight: 10,
                  paddingBottom: 10,
                  paddingLeft: 0,
                  backgroundColor: "#fff",
                  marginBottom: 2,

                  color: "#424242"
                }}
                selectionColor={"#dfe6e9"}
                onChangeText={(fullname) => this.setState({ fullname })}

                multiline={false}
                placeholder={I18n.t('fullname')}
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType="next"
                underlineColorAndroid="transparent"
                placeholderTextColor="#969696"
              />
            </View>
			
			<View style={styles.input}>
              <Icon
                name="ios-person"
                size={24}
                color="#969696"
                style={{ padding: 10 }}
              />
              			  
			  <TextInput
                style={{
                  /* paddingLeft: 8, fontSize: 17, width: "100%" */
                  //width: "100%",
                  flex: 1,
                  marginRight: 10,
                  paddingTop: 10,
                  paddingRight: 10,
                  paddingBottom: 10,
                  paddingLeft: 0,
                  backgroundColor: "#fff",
                  marginBottom: 2,

                  color: "#424242"
                }}
                selectionColor={"#dfe6e9"}
                onChangeText={ic_number => this.setState({ ic_number })}

                multiline={false}
                placeholder={I18n.t('ic_number')}
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType="next"
                onSubmitEditing={(event) => {
                  this.refs.SecondInput.focus();
                }}
                underlineColorAndroid="transparent"
                placeholderTextColor="#969696"
              />
            </View>

            <View style={styles.input}>
              <Icon
                name="md-unlock"
                size={24}
                color="#969696"
                style={{ padding: 10 }}
              />
              <TextInput
                style={{
                  /* paddingLeft: 8, fontSize: 17, width: "100%" */
                  //width: "100%",
                  flex: 1,
                  marginRight: 10,
                  paddingTop: 10,
                  paddingRight: 10,
                  paddingBottom: 10,
                  paddingLeft: 0,
                  backgroundColor: "#fff",
                  marginBottom: 2,

                  color: "#424242"
                }}
                selectionColor={"#dfe6e9"}
                onChangeText={(password) => this.setState({ password })}
                ref='SecondInput'
                multiline={false}
                placeholder={I18n.t('password')}
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType="next"
                secureTextEntry
                onSubmitEditing={(event) => {
                  this.refs.ThirdInput.focus();
                }}
                underlineColorAndroid="transparent"
                placeholderTextColor="#969696"
              />
            </View>

            <View style={styles.input}>
              <Icon
                name="md-unlock"
                size={24}
                color="#969696"
                style={{ padding: 10 }}
              />
              <TextInput
                style={{
                  /* paddingLeft: 8, fontSize: 17, width: "100%" */
                  //width: "100%",
                  flex: 1,
                  marginRight: 10,
                  paddingTop: 10,
                  paddingRight: 10,
                  paddingBottom: 10,
                  paddingLeft: 0,
                  backgroundColor: "#fff",
                  marginBottom: 2,

                  color: "#424242"
                }}
                selectionColor={"#dfe6e9"}
                onChangeText={(repassword) => this.setState({ repassword })}
                ref='ThirdInput'
                multiline={false}
                placeholder={I18n.t('re_password')}
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType="next"
                secureTextEntry
                onSubmitEditing={(event) => {
                  this.refs.FourthInput.focus();
                }}
                underlineColorAndroid="transparent"
                placeholderTextColor="#969696"
              />
            </View>

            <View style={styles.input}>
              <Icon
                name="ios-mail"
                size={24}
                color="#969696"
                style={{ padding: 10 }}
              />
              <TextInput
                style={{
                  /* paddingLeft: 8, fontSize: 17, width: "100%" */
                  //width: "100%",
                  flex: 1,
                  marginRight: 10,
                  paddingTop: 10,
                  paddingRight: 10,
                  paddingLeft: 0,
                  backgroundColor: "#fff",
                  color: "#424242"
                }}
                selectionColor={"#dfe6e9"}
                onChangeText={(email) => this.setState({ email })}
                ref='FourthInput'
                multiline={false}
                placeholder={I18n.t('email')}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                returnKeyType="done"
                underlineColorAndroid="transparent"
                placeholderTextColor="#969696"
              />
            </View>
          </View>

          <Button
            label={I18n.t('next')}
            buttonStyle={styles.button}
            //onPress={() => this.registerWithData()}
           onPress={() => this.signUpNext()}
          />
		  
		  <View style={styles.footer}>
			  <Text style={{ color: "#969696" }}>{I18n.t('have_an_account')}?</Text>
			  <Text onPress={() => this.props.navigation.pop()} style={{ color: color.primary, paddingHorizontal: 3 }}>
				{I18n.t('login')}
			  </Text>
			</View>
        </KeyboardAwareScrollView>

        
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    alignSelf: "center",
    paddingTop: "20%"
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center"
    //paddingHorizontal: "20%"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginVertical: 5,
    backgroundColor: color.primary
  },
  input: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingLeft: 9,
    paddingRight: 12,
    borderWidth: 1,
    borderColor: "#dfe6e9",
    marginVertical: 13,
    borderRadius: 25
  },
  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 10,
    width: "90%",
    /* borderColor: "pink",
    borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
    borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
	width: "100%",
    marginVertical: 5
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    borderBottomColor: "#F4AE96",
    borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    showIndicator: state.user.loading,

  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupScreen);
