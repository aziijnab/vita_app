//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, WebView } from 'react-native';

// create a component
class TermsView extends Component {
    static navigationOptions = {
        header: null
    }
    render() {
        return (
            <WebView source={{ uri: 'https://www.apple.com/legal/privacy/en-ww/' }} />
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default TermsView;
