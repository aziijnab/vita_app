import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  Alert,
  KeyboardAvoidingView,
  Platform,
  Picker
} from "react-native";

import DateTimePicker from "react-native-modal-datetime-picker";
import { Dropdown } from "react-native-material-dropdown";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Button from "../../components/Button";
import Header from "../../components/Header";
import InputField from "../../components/InputField";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast, {DURATION} from 'react-native-easy-toast'
import moment, { now } from 'moment';

class CreateProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: "",
      dateOfBirth: "Date of Birth",
      gender: "",
      ethnicity: "",
      contactNumber: "",
      age:null,
      guardianName:"",
      passportNumber:"",
      isDateTimePickerVisible: false,
      showAgeExtraInfo:false,
    };
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    console.log("date selected",date);
    
    // var day = date.getDate();
    // var month = date.getMonth();
    // var year = date.getFullYear();
    // var newDate = year + "-" + month + "-" + day;
    var newDate = moment(date).format('Y/MM/DD')
    this.setState({
      dateOfBirth: newDate
    });
    this._hideDateTimePicker();
    var currentDate = moment();
    var dateDiffrence = currentDate.diff(date,'year')
    console.log("date Diffrence in age",dateDiffrence)
    this.setState({age:dateDiffrence},()=>{
      console.log("age of users",this.state.age)
    })
    if(dateDiffrence < 18 ){
      this.setState({showAgeExtraInfo:true})
    }else{
      this.setState({showAgeExtraInfo:false})
    }



  };

  componentDidMount() {
    
  }

  profileValidation(){

    var errorMessages = []
    if(this.state.fullName.length == 0){
      errorMessages.push("Please enter your full name")
    }

    if(this.state.dateOfBirth == ""){
      errorMessages.push("Please select your date of birth")
    }

    if(this.state.gender == ""){
      errorMessages.push("Please select your gender")
    }

    if(this.state.contactNumber == ""){
      errorMessages.push("Please enter you contact number")
    }

    if(this.state.age == null){
      errorMessages.push("Please enter your age")
    }

    if(this.state.showAgeExtraInfo == true){
      if(this.state.passportNumber == ""){
        errorMessages.push("Please enter your IC/Passport No.")
      }
      if(this.state.guardianName == ""){
        errorMessages.push("Parents/Guardian Name")
      }
    }

    if(errorMessages.length == 0){
      this.props.navigation.navigate("startScreen", {
        basicData: {
          name: this.state.fullName,
          dob: this.state.dateOfBirth,
          gender: this.state.gender,
          contact: this.state.contactNumber,
          age:this.state.age,
          passportNumber:this.state.passportNumber,
          guardianName:this.state.guardianName
        }
      })
    }else{
      let errorArray = errorMessages.join("\n")
      this.refs.toast.show(errorArray,DURATION.SHORT);
    }
    
  }

  render() {
    let genderData = [
      { label: "Gender", value: "" },
      { label: "Male", value: "Male" },
      { label: "Female", value: "Female" }
    ];  
    console.log(this.props.user);
    return (
      <View style={styles.container}>
      <Toast
        ref="toast"
        style={{backgroundColor:'red'}}
        position='top'
        positionValue={60}
        fadeInDuration={750}
        fadeOutDuration={1000}
        textStyle={{color:'white'}}
      />
      <Header
          label={I18n.t("your_details")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <KeyboardAwareScrollView>
          <Text style={styles.title}>{I18n.t('your_details')}</Text>
          {this.state.showAgeExtraInfo == true && 
          <Text style={styles.title1}>
            {I18n.t('you_are_required_to_get_parental_consent')}
          </Text>
          }
          <View style={styles.form}>
            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={fullName => this.setState({ fullName })}
              placeholder={I18n.t('your_full_name')}
              autoCorrect={true}
              returnKeyType="done"
              underlineColorAndroid="transparent"
            />
            <TouchableOpacity
              style={styles.touchable}
              onPress={this._showDateTimePicker}
            >
              <Text style={{ color: "#969696" }}>
              {this.state.dateOfBirth == "" &&
                `${I18n.t('date_of_birth')}`
               }
               {this.state.dateOfBirth != "" &&
                this.state.dateOfBirth
               }
            </Text>
            </TouchableOpacity>
            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
              maximumDate={new Date()}
            />
            {this.state.showAgeExtraInfo == true && 
                <View>
                  <TextInput
                    style={styles.input1}
                    selectionColor={"#dfe6e9"}
                    onChangeText={guardianName => this.setState({ guardianName })}
                    placeholder={I18n.t('guardian_name')}
                    autoCorrect={true}
                    returnKeyType="done"
                    underlineColorAndroid="transparent"
                  />
                  <TextInput
                    style={styles.input1}
                    selectionColor={"#dfe6e9"}
                    onChangeText={passportNumber => this.setState({ passportNumber })}
                    placeholder={I18n.t('passport_number')}
                    autoCorrect={true}
                    returnKeyType="done"
                    underlineColorAndroid="transparent"
                />
                </View>
            }
            <Dropdown
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              onChangeText={(itemValue, itemIndex) =>
                this.setState({ gender: itemValue })
              }
              labelFontSize={15}
              rippleOpacity={0.1}
              dropdownMargins={{ min: 0, max: 0 }}
              textColor={"#969696"} 
              pickerStyle={{ paddingHorizontal: 10 }}
              dropdownOffset={{ top: 4, left: 0 }}
              containerStyle={styles.pickerSt}
              data={genderData}
              overlayStyle={{ borderColor: "yellow", borderWidth: 1 }}
              value={I18n.t('gender')}
              baseColor="#576574"
            />
            
            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={contactNumber => this.setState({ contactNumber })}
              placeholder={I18n.t('contact_number')}
              autoCorrect={true}
              returnKeyType="done"
              keyboardType={"phone-pad"}
              underlineColorAndroid="transparent"
            />
            
            
             {/* <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={age => this.setState({ age })}
              placeholder={I18n.t('age')}
              autoCorrect={true}
              returnKeyType="done"
              keyboardType={"phone-pad"}
              underlineColorAndroid="transparent"
            /> */}
          </View>
          

          <Button
            label={I18n.t('submit')}
            buttonStyle={styles.button}
            onPress = {() => this.profileValidation()}
  />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "20%",
    paddingLeft: 30
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    // textAlign: "center",
    paddingTop: 10,
    paddingLeft: 30
    //paddingHorizontal: "20%"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 15,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  pickerview: {
    marginVertical: 7,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 2,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  pickerSt: {
    marginVertical: 17,
    width: "100%",
    alignSelf: "center",
    //flexDirection: "row",
    paddingTop: 6,
    paddingHorizontal: 15,
    justifyContent: "center",
    //alignItems: "center",
    //paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },

  dropdown: {
    width: "100%",
    alignItems: "flex-start"
  },
  form: {
    paddingVertical: 30,
    width: "90%",
    /* borderColor: "pink",
    borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white"

    /* borderColor: "red",
    borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    borderBottomColor: "#F4AE96",
    borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});
const pickerStyle = {
  inputIOS: {
    color: "white",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12
  },
  inputAndroid: {
    color: "red"
  },

  placeholderColor: "grey",
  underline: { borderTopWidth: 0 },
  icon: {
    position: "absolute",
    backgroundColor: "transparent",
    borderTopWidth: 5,
    borderTopColor: "#00000099",
    borderRightWidth: 5,
    borderRightColor: "transparent",
    borderLeftWidth: 5,
    borderLeftColor: "transparent",
    width: 0,
    height: 0,
    top: 20,
    right: 15
  }
};

function mapStateToProps(state, props) {
  console.log('====================================');
  console.log(state);
  console.log('====================================');
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateProfileScreen);




/*  onPress={() =>
              this.props.navigation.navigate("startScreen", {
                basicData: {
                  name: this.state.fullName,
                  dob: this.state.dateOfBirth,
                  gender: this.state.gender,
                  contact: this.state.contactNumber
                }
              })
            } */