import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  Alert,
  KeyboardAvoidingView,
  ActivityIndicator,
  Platform,
  
} from "react-native";
import Route from "../../network/route.js";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import Spinner from "react-native-loading-spinner-overlay";
import Toast, { DURATION } from "react-native-easy-toast";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType
} from "react-native-fcm";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from "react-native-google-signin";

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      remember: false,
      loading: false
    };
  }
  componentWillMount() {
    console.log(this.props.showIndicator), console.log(this.props.testDat);
    if (Platform.OS == 'android') {
      GoogleSignin.configure({
        webClientId:
          "272191391624-q112ide48k1srjo9dmt11o9e40dktq28.apps.googleusercontent.com",
        // WebClientSecret:'ynpStqYhmrMrxJPR-3Cr8EOX',
        offlineAccess: false
      });
    } else {
      GoogleSignin.configure({
        webClientId:
          "272191391624-lnn28972i58sbdajsrag4e7t8872hq8b.apps.googleusercontent.com",
        // WebClientSecret:'ynpStqYhmrMrxJPR-3Cr8EOX',
        offlineAccess: false
      });
    }
  }

  validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      // this.setState({ userInfo });
      // alert( JSON.stringify(userInfo))
      var emailSplit = userInfo.user.email.split("@");
      var username = emailSplit[0];
      this.setState(
        {
          username: username,
          email: userInfo.user.email,
          google_token: userInfo.accessToken
        },
        () => {
          this.setState({ loading: true });
          let credentials = {
            username: this.state.username,
            email: this.state.email,
            google_token: this.state.google_token,
            user_role: this.user_role,
            role: this.state.role
          };
          route.postdata("google-login", credentials).then(async response => {
            if (response.error) {
              this.setState({ loading: false });
              this.refs.toast.show(response.error, DURATION.SHORT);
            } else {
               console.log(response)
              this.props.setUser(response);
              // await this.props.alterUser(response.loggedUser)

              console.log("google-login",response);
              // this.props.navigation.navigate("createProfileScreen");
               if(response.loggedUser.user_profile.full_name != null){
                   this.setState({loading:false})
                   this.props.navigation.navigate("App")
                 
               }else{
                 this.setState({loading:false})
                 this.props.navigation.navigate("createProfileScreen");
               }
            }
          });
        }
      );
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login
        alert("user cancelled the login ");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        alert("operation (f.e. sign in) is in progress already");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log("play services not available or outdated");
      } else {
        // some other error happened
        console.log("error", error);
        alert(JSON.stringify(error));
      }
    }
  };

  sendDeviceTokentToApi() {
    FCM.getFCMToken().then(token => {
      console.log("TOKEN (getFCMToken)", token);
      let deviceTokect = { device_id: token };
      route
        .updateData("register-device", deviceTokect, this.props.myToken)
        .then(async tokenResponse => {
          if (tokenResponse.error) {
            console.log("user Error", tokenResponse.error);
          } else {
            console.log("Send User Token");
            console.log(tokenResponse);
          }
        });
    });
  }
  async loginWithData() {
    var credentials = {
      email: this.state.email,
      password: this.state.password
    };
    var errorMessages = [];
    console.log("email valid or not", this.validateEmail(credentials.email));
    if (
      credentials.email.length == 0 &&
      this.validateEmail(credentials.email) == false
    ) {
      errorMessages.push(`${I18n.t("email_is_not_valid_or_empty")}`);
    }
    if (credentials.password.length < 6) {
      errorMessages.push(`${I18n.t("password_must_have_6_characters")}`);
    }

    if (errorMessages.length == 0) {
      this.setState({ loading: true });
      route.postdata("login", credentials).then(async response => {
        if (response.error) {
          this.setState({ loading: false });
          this.refs.toast.show(response.error, DURATION.SHORT);
        } else {
          console.log("logged user", response);
          this.props.setUser(response);
          // await this.props.alterUser(response.loggedUser)
          await this.sendDeviceTokentToApi();

          // console.log(this.props.user);
          //  this.setState({loading:false})
          //  this.props.navigation.navigate("createProfileScreen");
          if(response.loggedUser.is_profile_setup != 0){
            this.setState({loading:false})
            this.props.navigation.navigate("App")
            this.props.isQuestionAnswerSetup(true);
        }else{
          this.setState({loading:false})
          this.props.navigation.navigate("createProfileScreen");
        }
        }
      });
    } else {
      let errorArray = errorMessages.join("\n");
      this.refs.toast.show(errorArray, DURATION.SHORT);
    }
  }
  render() {
    console.log(this.props.testDat);

    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: "red" }}
          position="top"
          positionValue={80}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{ color: "white" }}
        />
        
        <Text style={styles.title}>{I18n.t("login")}</Text>
        <View style={styles.form}>
          <View style={styles.input}>
            <Icon
              name="ios-person"
              size={24}
              color="#969696"
              style={{ padding: 10 }}
            />
            <TextInput
              style={{
                /* paddingLeft: 8, fontSize: 17, width: "100%" */
                //width: "100%",
                flex: 1,
                marginRight: 10,
                paddingTop: 10,
                paddingRight: 10,
                paddingBottom: 10,
                paddingLeft: 0,
                backgroundColor: "#fff",
                marginBottom: 2,
                color: "#424242"
              }}
              selectionColor={"#dfe6e9"}
              onChangeText={email => this.setState({ email })}
              keyboardType="email-address"
              multiline={false}
              placeholder={I18n.t("email")}
              autoCapitalize="none"
              autoCorrect={false}
              returnKeyType="next"
              onSubmitEditing={event => {
                this.refs.SecondInput.focus();
              }}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />
          </View>

          <View style={styles.input}>
            <Icon
              name="md-unlock"
              size={24}
              color="#969696"
              style={{ padding: 10 }}
            />
            <TextInput
              style={{
                /* paddingLeft: 8, fontSize: 17, width: "100%" */
                //width: "100%",
                flex: 1,
                marginRight: 10,
                paddingTop: 10,
                paddingRight: 10,
                paddingBottom: 10,
                paddingLeft: 0,
                backgroundColor: "#fff",
                marginBottom: 2,
                color: "#424242"
              }}
              selectionColor={"#dfe6e9"}
              onChangeText={password => this.setState({ password })}
              ref="SecondInput"
              multiline={false}
              placeholder={I18n.t("password")}
              autoCapitalize="none"
              autoCorrect={false}
              returnKeyType="done"
              secureTextEntry
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />
          </View>
          {/* {this.props.showIndicator && <ActivityIndicator size="large" color="red" animating={true} />} */}
          <View style={styles.remember}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                width: "100%",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                style={{ flexDirection: "row" }}
                onPress={() =>
                  this.setState({ remember: !this.state.remember })
                }
                activeOpacity={0.9}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      remember: !this.state.remember
                    })
                  }
                >
                  <View
                    style={{
                      height: 24,
                      width: 24,
                      borderRadius: 12,
                      borderWidth: 2,
                      borderColor: "#d8d8d8",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    {this.state.remember ? (
                      <View
                        style={{
                          height: 12,
                          width: 12,
                          borderRadius: 6,
                          backgroundColor: "#d8d8d8"
                        }}
                      />
                    ) : null}
                  </View>
                </TouchableOpacity>

                <Text style={{ paddingLeft: 10, color: "#969696" }}>
                  {I18n.t("remember_me")}
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flex: 1,
                width: "100%",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <TouchableOpacity
                style={{
                  height: 24,
                  width: 24,
                  borderRadius: 12,
                  backgroundColor: "#666060",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Icon name="ios-help" size={20} color="white" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Button
          label={I18n.t("login")}
          buttonStyle={styles.button}
          /* onPress={() => this.props.navigation.navigate("App")} */ onPress={() =>
            this.loginWithData()
          }
        />

        {/* <TouchableOpacity
          activeOpacity={0.9}
          onPress={this._signIn}
          style={{
            flexDirection: "row",
            height: "8%",
            backgroundColor: "#4285F4",
            alignSelf: "center",
            padding: 3,
            width: "90%",
            borderRadius: 4
          }}
        >
          <View
            style={{
              height: "100%",
              flex: 0.6,
              width: "100%",
              padding: 10,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "white"
            }}
          >
            <Image
              style={{ width: "100%", height: "100%" }}
              resizeMode="contain"
              source={require("../../asset/images/search.png")}
            />
          </View>

          <View
            style={{
              flex: 4,
              backgroundColor: "#4285F4",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text style={{ color: "white", fontSize: 20, fontWeight: "400" }}>
              {I18n.t("sign_in_with_google")}
            </Text>
          </View>
        </TouchableOpacity> */}

        {/* <GoogleSigninButton
          style={{ width: 48, height: 48 }}
          size={GoogleSigninButton.Size.Icon}
          color={GoogleSigninButton.Color.Dark}
          onPress={this._signIn}
          disabled={this.state.isSigninInProgress} /> */}

        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => this.props.navigation.navigate("forgetPasswordScreen")}
          style={{ paddingVertical: 4, alignSelf: "center" }}
        >
          <Text style={{ color: "#969696", fontSize: 18 }}>
            {I18n.t("forgot_password_small")}?
          </Text>
        </TouchableOpacity>
        <View style={styles.footer}>
          <Text style={{ color: "#969696" }}>
            {I18n.t("dont_have_an_account")}
          </Text>
          <Text
            onPress={() => this.props.navigation.navigate("signupScreen")}
            style={{ color: color.primary, paddingHorizontal: 3 }}
          >
            {I18n.t("sign_up")}
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    alignSelf: "center",
    paddingTop: "20%"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginVertical: 30,

    backgroundColor: color.primary
  },
  input: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingLeft: 9,
    paddingRight: 12,
    borderWidth: 1,
    borderColor: "#dfe6e9",
    marginVertical: 13,
    borderRadius: 25
  },
  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: "10%",
    width: "90%",
    /* borderColor: "pink",
    borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    flexWrap: "wrap"
    /* borderColor: "red",
    borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  }
});

function mapStateToProps(state, props) {
  console.log(state);
  return {
    userDetail: state.user.user,
    showIndicator: state.user.loading,
    testDat: state.user.testData,
    myToken: state.user.userToken
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);
