import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform
} from "react-native";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";

class PhoneConfirmation extends Component {
    constructor(props) {
        super(props);
        this.state = {
           
        };
    }

    componentDidMount() {
    }
    
  render() {
    
    return (
        <View style={styles.container}>
          <View style={styles.header}>
            
            <TouchableOpacity
              style={{
                paddingVertical: 3,
                paddingHorizontal: 8,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Icon
                name="md-arrow-back"
                onPress={() => this.props.navigation.pop()}
                size={27}
                color="white"
              />
            </TouchableOpacity>
            <Text style={styles.headertext}>{I18n.t('set_pin_code')}</Text>
          </View>
          <Text style={styles.title}>{I18n.t('enter_your_pin_code')}</Text>
          <View
            style={{
              width: "65%",
              flex:1,
              justifyContent: "space-evenly",
              flexDirection:"row",
              flexWrap: "wrap",
              paddingVertical: 80,
              alignSelf: "center"
              //paddingHorizontal: "20%"
            }}
          >

            <View style={{width: 40, height: 40, backgroundColor: '#E5E5E5'}} />
            <View style={{width: 40, height: 40, backgroundColor: '#E5E5E5'}} />
            <View style={{width: 40, height: 40, backgroundColor: '#E5E5E5'}} />
            <View style={{width: 40, height: 40, backgroundColor: '#E5E5E5'}} />
          </View>
  
          
  
          <Button
            label={I18n.t('done')}
            buttonStyle={styles.button}
            onPress={() => this.props.navigation.navigate("measurementsScreen")}
          />
  
        </View>
      );
  }
}

const styles = StyleSheet.create({
    title: {
      fontSize: 25,
      fontWeight: "700",
      letterSpacing: 0.5,
      alignSelf: "center",
      paddingTop: "20%"
    },
    title1: {
      fontSize: 15,
      letterSpacing: 0.5,
      textAlign: "center"
      //paddingHorizontal: "20%"
    },
    button: {
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 25,
      paddingVertical: 13,
      alignSelf: "center",
      width: "90%",
      marginVertical: 40,
  
      backgroundColor: color.primary
    },

    headertext:{
        
        fontSize: 20,
        color: "white",
        textAlign: "center",
        paddingVertical: 13,
        
    },
    input: {
      marginVertical: 17,
      width: "100%",
      flexDirection: "row",
      paddingVertical: 6,
      alignItems: "center",
      paddingLeft: 19,
      borderRadius: 25,
      borderWidth: 1,
      borderColor: "#dfe6e9"
    },
    remember: {
      alignSelf: "center",
      width: "90%",
      flexDirection: "row",
      alignItems: "center",
      paddingVertical: 4
    },
    form: {
      paddingVertical: 20,
      width: "90%",
      /* borderColor: "pink",
      borderWidth: 2, */
      alignSelf: "center"
    },
    container: {
      flex: 1,
      backgroundColor: "white",
      flexWrap: "wrap"
      /* borderColor: "red",
      borderWidth: 2 */
    },
    footer: {
      flexDirection: "row",
      justifyContent: "center",
      position: "absolute",
      bottom: 0,
      marginBottom: 10,
      alignSelf: "center"
    },
    header: {
      height: 50,
      width: "100%",
      justifyContent: "flex-start",
      flexDirection: "row",
      alignSelf: "center",
      borderBottomColor: "#F4AE96",
      borderBottomWidth: 1,
      marginTop: Platform.OS == "ios" ? 20 : 0,
      backgroundColor: color.primary
    }
  });

function mapStateToProps(state, props) {
    return {
        userDetail: state.user.userDetail,
    }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}
  
export default connect(mapStateToProps, mapDispatchToProps)(PhoneConfirmation);
