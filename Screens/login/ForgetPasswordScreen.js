import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform
} from "react-native";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast'
import Route from "../../network/route.js";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

class ForgetPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      remember: false,
      loading:false
    };
  }

  forgetPasswordSubmit(){
    errorMessages = []
    if(this.state.email == ""){
      errorMessages.push(`${I18n.t('please_enter_your_email')}`)
    }
    if(errorMessages.length == 0){
      this.setState({loading:true})
      var emailData = {email:this.state.email}
      route.postdata("emailExist", emailData)
      .then(async (response) => {

        if (response.error) {
          this.setState({loading:false})
          this.refs.toast.show(response.error,DURATION.SHORT);
        }
        else {
              console.log(response);
              // this.props.setUser({token:"",userInfo:null})
              if(response == false){
                this.setState({loading:false})
                this.refs.toast.show(`${I18n.t('email_not_exist')}`,DURATION.SHORT);
              }else{
                // this.setState({loading:false})
                // this.refs.toast.show("Email exist",DURATION.SHORT);
                route.postdata("forgot-password", emailData)
                .then(async (response) => {

                  if (response.error) {
                    this.setState({loading:false})
                    this.refs.toast.show(response.error,DURATION.SHORT);
                  }
                  else {
                        console.log(response);
                        this.setState({loading:false})
                        if(response.success){
                          this.refs.toast.show(response.success,DURATION.SHORT);
                        }else{
                          this.refs.toast.show(response,DURATION.SHORT);                          
                        }
                       
                      }

                })
                
              }
            }

      })
    }else{
      let errorArray = errorMessages.join("\n")
      this.refs.toast.show(errorArray,DURATION.SHORT);
    }
  }
  componentDidMount() { }

  render() {
    return (
      <View style={styles.container}>
      <Spinner
          visible={this.state.loading}
          textContent={I18n.t('please_wait')}
          textStyle={{color:color.primary}}
          cancelable={true}
          color="#F16638"
        />
      <Toast
        ref="toast"
        style={{backgroundColor:'red'}}
        position='top'
        positionValue={80}
        fadeInDuration={750}
        fadeOutDuration={1000}
        textStyle={{color:'white'}}
      />
        <View style={styles.header}>
          <TouchableOpacity
            style={{
              paddingVertical: 3,
              paddingHorizontal: 8,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Icon
              name="md-arrow-back"
              onPress={() => this.props.navigation.pop()}
              size={27}
              color="#969696"
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.title}>{I18n.t("forget_password")}</Text>
        <View
          style={{
            width: "65%",
            alignItems: "center",
            flexWrap: "wrap",
            paddingVertical: 50,
            alignSelf: "center"
            //paddingHorizontal: "20%"
          }}
        >
          <Text style={styles.title1}>
            {I18n.t('send_password_rest_intructions')}
          </Text>
        </View>

        <View style={styles.form}>
          <InputField
            label={I18n.t('email')}
            onChangeText={(email) =>this.setState({ email })}
            icon="ios-mail"
            returnKeyType="default"
            //iconSize={27}
            //iconColor="#0984e3"
            keyboardType="email-address"
            autoCorrect={true}
          />
        </View>

        <Button
          label={I18n.t('reset_password')}
          buttonStyle={styles.button}
          onPress={() => this.forgetPasswordSubmit()}
        />

        <View style={styles.footer}>
          <Text style={{ color: "#969696" }}>{I18n.t('dont_have_an_account')}</Text>
          <Text onPress={() => this.props.navigation.navigate("signupScreen")} style={{ color: color.primary, paddingHorizontal: 3 }}>
            {I18n.t('sign_up')}
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    alignSelf: "center",
    paddingTop: "20%"
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center"
    //paddingHorizontal: "20%"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginVertical: 30,

    backgroundColor: color.primary
  },
  input: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 6,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 20,
    width: "90%",
    /* borderColor: "pink",
    borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    flexWrap: "wrap"
    /* borderColor: "red",
    borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",

    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgetPasswordScreen);
