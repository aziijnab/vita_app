import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    Picker,
    FlatList
} from "react-native";
import symptomsData from "../../components/symptomsData.json";


import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";

class CurrentSymptoms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            symptoms: symptomsData,
        };
        this.checkBoxPress = this.checkBoxPress.bind(this);
    }

    componentDidMount() { }

    checkBoxPress(props){
        console.log('index', props);
        //console.log(this.state.symptoms);
        const symptom = this.state.symptoms;
        const result = symptom.findIndex((item, index) => index === props );
        console.log(result);
        if(result>=0){
            symptom[result].status = ! symptom[result].status;
            //console.log('Symptom', symptom); 
            this.setState({
                symptoms: symptom
            },function(){
                console.log('State symptoms', this.state.symptoms);
            })
        }
        
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView keyboardShouldPersistTaps={"always"}>
                    <View
                        style={{
                            width: "90%",
                            flexDirection: "row",
                            justifyContent: "flex-end",
                            top: "10%"
                        }}
                    >
                        <Text style={{ color: "#e5e5e5", fontSize: 17 }}>4 OF 5</Text>
                    </View>
                    <Text style={styles.title}>{I18n.t('current_syptoms')}</Text>

                    <View style={styles.textView}>
                        <View style={styles.custom}>
                            <Text style={{ color: "#969696", fontWeight: "800" }}>
                               {I18n.t('please_tick_all_that_apply')}
                                </Text>
                        </View>
                    </View>

                    <View style={styles.form}>
                        <FlatList
                            data={this.state.symptoms}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) =>
                                <View style={styles.custom}>
                                    <View style={styles.checkboxview}>
                                        <TouchableOpacity
                                            key = {index}
                                            onPress={() => this.checkBoxPress(index)}
                                        >
                                            <View
                                                style={{
                                                    height: 24,
                                                    width: 24,
                                                    borderRadius: 12,
                                                    borderWidth: 2,
                                                    borderColor: "#d8d8d8",
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                
                                                }}
                                            >
                                                {item.status && (
                                                    <View
                                                        style={{
                                                            height: 12,
                                                            width: 12,
                                                            borderRadius: 6,
                                                            backgroundColor: "#d8d8d8"
                                                        }}
                                                    />
                                                ) }
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <Text style={styles.item}>{item.title}</Text>
                                    </View>
                                </View>
                            }
                        />
                    </View>


                    <Button
                        label="NEXT"
                        buttonStyle={styles.button}
                        onPress={() => console.log("Detais Submitted")}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "100%",
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: "flex-start"
    },
    custom1: {
        flexDirection: "row",
        width: "96%",
        justifyContent: "space-around"
    },

    checkboxview:{
        marginVertical: 5,
        paddingVertical: 5,
        paddingHorizontal: 4
    },

    item: {
        marginVertical: 5,
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginHorizontal:5,
        fontSize: 15,
    },

    title: {
        fontSize: 25,
        fontWeight: "700",
        letterSpacing: 0.5,
        paddingTop: "15%",
        textAlign: "center"
    },
    iconView: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "flex-end",
        paddingHorizontal: 15
    },
    title1: {
        fontSize: 15,
        letterSpacing: 0.5,
        textAlign: "center"
        //paddingHorizontal: "20%"
    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 5,
        width: "48%",
        flexDirection: "row",
        paddingVertical: 2,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    fieldView: {
        marginVertical: 20
    },

    textView: {
        marginVertical: 15
    },

    pickerview: {
        marginVertical: 7,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 2,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 27,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 8,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        paddingVertical: 20,
        width: "90%",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white"

        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    header: {
        height: 50,
        width: "100%",
        justifyContent: "flex-start",
        flexDirection: "row",
        alignSelf: "center",
        borderBottomColor: "#F4AE96",
        borderBottomWidth: 1,
        marginTop: Platform.OS == "ios" ? 20 : 0,
        backgroundColor: "white"
    }
});

function mapStateToProps(state, props) {
    return {
        userDetail: state.user.userDetail
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CurrentSymptoms);
