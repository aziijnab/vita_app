import React, { Component } from "react";
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    Picker,
    FlatList
} from "react-native";
import symptomsData from "../../components/Dummydata/symptomsData";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Header from "../../components/Header";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import Route from "../../network/route.js";
import Spinner from 'react-native-loading-spinner-overlay';


// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

class Symptoms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      symptoms: symptomsData,
      survyQuestions: [],
      userAnswers: [],
      selectedCheckBox: [],
      fromProfile: false,
      loading: false
    };
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    if (typeof params !== "undefined") {
      this.setState({ fromProfile: params.fromProfile });
    }
    this.setState({ loading: true });
    route
      .getAuthenticated("questions", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
          route.checkTokenExpire(response);
          console.log(response);
          var allData = response.allQuestions;
          var questions = [];
          allData.map((item, index) => {
            if (item.question_type == "symptoms") {
              questions.push(item);
              if (item.user_answer.length > 0) {
                item.user_answer.map(ans => {
                  this.state.selectedCheckBox.push(ans.option_id);
                });
              }
            }
          });
          this.setState({ loading: false });
          this.setState({ survyQuestions: questions });
          //console.log(medicalQuestions);

          this.state.survyQuestions.forEach((value, index) => {
            if (value.answer_type == "single") {
              value.question_option.forEach((item, index) => {
                Object.assign(item, { status: false });
              });
            }
            return value;
          });
        }
        true;
      });
  }

  checkBoxPress(option_id, question_id) {
    var selectedCheckBox = this.state.selectedCheckBox;
    var index = this.state.selectedCheckBox.findIndex(
      (value, index) => value === option_id
    );
    if (index >= 0) {
      selectedCheckBox.splice(index, 1);
      this.setState({ selectedCheckBox: selectedCheckBox });
    } else {
      selectedCheckBox.push(option_id);
      this.setState({ selectedCheckBox: selectedCheckBox });
    }
    // var findCheck = this.state.selectedCheckBox.findIndex((value,index) => value.id == index)
  }

  checkAllOption() {
    console.log("check all option are called");
    var selectedCheckBoxs = this.state.selectedCheckBox;
    selectedCheckBoxs = [];
    if (this.state.survyQuestions != []) {
      this.state.survyQuestions.map((symptoms, index) => {
        symptoms.question_option.map((option, index) => {
          selectedCheckBoxs.push(option.id);
        });
      });
      this.setState({ selectedCheckBox: selectedCheckBoxs });
    }
  }

  submitSymptoms() {
    this.setState({ loading: true });
    var userAnswerOption = [
      {
        question_id: this.state.survyQuestions[0].id,
        options: this.state.selectedCheckBox.toString()
      }
    ];
    var answer = { user_id: "", questions: userAnswerOption };
    route
      .updateData("user-answer", answer, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.props.stopLoading();
          this.setState({ loading: true });
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          this.setState({ loading: false });
          route.checkTokenExpire(response);
          console.log("Symptoms screen response");
          console.log(response);
          if (response) {
            if (this.state.fromProfile == false) {
              this.setState({ loading: false });
              this.props.navigation.navigate("measurementScreen");
            } else {
              this.setState({ loading: false });
              this.props.navigation.navigate("profile");
            }
          }

          //dispatch(NavigationActions.navigate({ routeName: 'App' }))
        }
      });
  }

  replaceSpaceToUnderScore(string) {
    let lowerCaseString = string.toLowerCase();
    let underScoreString = lowerCaseString.split(" ").join("_");
    let brackitLess = underScoreString.split("(").join("");
    let brackitLess2 = brackitLess.split(")").join("");
    let removeQuestionMark = brackitLess2.split("?").join("");
    let removeQoma = removeQuestionMark.split(",").join("");
    let removeDot = removeQoma.split(".").join("");
    let removeColon = removeDot.split(":").join("");
    let removeSemiColon = removeColon.split(";").join("");

    let singleUnderScore = removeSemiColon.split("-").join("_");
    singleUnderScore = singleUnderScore.split("__").join("_");
    singleUnderScore = singleUnderScore.split("/").join("_");
    var newString = singleUnderScore;
    console.log("string length", newString.length);

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length;
      newString = singleUnderScore.substring(0, 57);
      console.log("new sting with cut", newString);
    }
    console.log("under score string", newString);
    return newString;
  }
  render() {
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.loading}
          textContent={I18n.t('please_wait')}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Header
          label={I18n.t("current_syptoms")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <KeyboardAwareScrollView>
          <View
            style={{
              width: "90%",
              flexDirection: "row",
              justifyContent: "flex-end",
              top: "10%"
            }}
          >
            {this.state.fromProfile == false && (
              <Text style={{ color: "#e5e5e5", fontSize: 17 }}>
                4 {I18n.t("of")} 5
              </Text>
            )}
          </View>
          <Text style={styles.title}>{I18n.t("current_syptoms")}</Text>

          <View style={styles.textView}>
            <View style={styles.custom}>
              <Text
                onPress={() => this.checkAllOption()}
                style={{ color: "#969696", fontWeight: "800" }}
              >
                {I18n.t("please_tick_all_that_apply")}
              </Text>
            </View>
          </View>

          <View style={styles.form}>
            {console.log("survy Question", this.state.survyQuestions)}
            {this.state.survyQuestions != []
              ? this.state.survyQuestions.map((symptoms, index) => {
                  return (
                    <View>
                      {symptoms.answer_type == "single" && (
                        <FlatList
                          data={symptoms.question_option}
                          extraData={this.state}
                          showsHorizontalScrollIndicator={false}
                          keyExtractor={(item, index) => index.toString()}
                          renderItem={({ item, index }) => (
                            <TouchableOpacity
                              onPress={() =>
                                this.checkBoxPress(item.id, item.question_id)
                              }
                              key={index}
                              activeOpacity={0.9}
                              style={styles.custom}
                            >
                              <View style={styles.checkboxview}>
                                <View>
                                  <View
                                    style={{
                                      height: 24,
                                      width: 24,
                                      borderRadius: 12,
                                      borderWidth: 2,
                                      borderColor: "#d8d8d8",
                                      alignItems: "center",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <View
                                      style={{
                                        height: 12,
                                        width: 12,
                                        borderRadius: 6,
                                        backgroundColor:
                                          this.state.selectedCheckBox.findIndex(
                                            (value, index) => value === item.id
                                          ) >= 0
                                            ? "#d8d8d8"
                                            : "transparent"
                                      }}
                                    />
                                  </View>
                                </View>
                              </View>
                              <View>
                                <Text style={styles.item}>
                                  {I18n.t(
                                    this.replaceSpaceToUnderScore(item.value)
                                  )}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          )}
                        />
                      )}
                    </View>
                  );
                })
              : null}
          </View>

          {/* <TextInput
            style={styles.input1}
            selectionColor={"#dfe6e9"}
            onChangeText={text => console.log(text)}
            placeholder={I18n.t("other_relevant_information")}
            autoCapitalize="words"
            autoCorrect={true}
            underlineColorAndroid="transparent"
            //placeholderTextColor="#969696"
          /> */}

          {this.state.fromProfile == true && (
            <Button
              label={I18n.t("submit")}
              buttonStyle={[styles.button, { marginBottom: 15 }]}
              onPress={() => this.submitSymptoms()}
            />
          )}
          {this.state.fromProfile == false && (
            <Button
              label={I18n.t("next")}
              buttonStyle={styles.button}
              onPress={() => this.submitSymptoms()}
            />
          )}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    custom: {
        flexDirection: "row",
        width: "100%",
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: "flex-start"
    },
    custom1: {
        flexDirection: "row",
        width: "96%",
        justifyContent: "space-around"
    },

    checkboxview: {
        marginVertical: 5,
        paddingVertical: 5,
        paddingHorizontal: 4
    },

    item: {
        marginVertical: 5,
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginHorizontal: 5,
        fontSize: 15,
    },

    title: {
        fontSize: 25,
        fontWeight: "700",
        letterSpacing: 0.5,
        paddingTop: "15%",
        textAlign: "center"
    },
    iconView: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "flex-end",
        paddingHorizontal: 15
    },
    title1: {
        fontSize: 15,
        letterSpacing: 0.5,
        textAlign: "center"
        //paddingHorizontal: "20%"
    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
        paddingVertical: 15,
        alignSelf: "center",
        width: "90%",
        marginVertical: 10,

        backgroundColor: color.primary
    },
    touchable: {
        marginVertical: 17,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    touchable3: {
        marginVertical: 5,
        width: "48%",
        flexDirection: "row",
        paddingVertical: 2,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },

    fieldView: {
        marginVertical: 20
    },

    textView: {
        marginVertical: 15
    },

    pickerview: {
        marginVertical: 7,
        width: "100%",
        flexDirection: "row",
        paddingVertical: 2,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 27,
        borderWidth: 1,
        borderColor: "#dfe6e9"
    },
    input1: {
        marginVertical: 17,
        width: "90%",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: "center",
        paddingLeft: 19,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: "#dfe6e9",
        alignSelf: "center"
    },

    remember: {
        alignSelf: "center",
        width: "90%",
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 4
    },
    form: {
        paddingVertical: 20,
        width: "90%",
        /* borderColor: "pink",
          borderWidth: 2, */
        alignSelf: "center"
    },
    container: {
        flex: 1,
        backgroundColor: "white",
        marginBottom: 15,


        /* borderColor: "red",
          borderWidth: 2 */
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        position: "absolute",
        bottom: 0,
        marginBottom: 10,
        alignSelf: "center"
    },
    header: {
        height: 50,
        width: "100%",
        justifyContent: "flex-start",
        flexDirection: "row",
        alignSelf: "center",
        borderBottomColor: "#F4AE96",
        borderBottomWidth: 1,
        marginTop: Platform.OS == "ios" ? 20 : 0,
        backgroundColor: "white"
    }
});

function mapStateToProps(state, props) {
    return {
        user: state.user.userInfo,
        myToken: state.user.userToken,
        questionAnswer: state.userQuestionAnswer
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Symptoms);