import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  FlatList,
  Platform
} from "react-native";


import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import allergies from "../../components/Dummydata/allergies";
import disease from "../../components/Dummydata/disease";
import familyDisease from '../../components/Dummydata/familyDisease'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Route from "../../network/route.js";
import Spinner from 'react-native-loading-spinner-overlay';
import Header from "../../components/Header";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

class MedicalHistory extends Component {
  constructor(props) {
    super(props);

    //console.log(allergies);
    this.state = {
      fullName: "",
      dateOfBirth: "",
      gender: "",
      ethnicity: "",
      contactNumber: "",
      allergies: allergies,
      diseases: disease,
      familyDiseases: familyDisease,
      medicalQuestion:[],
      questionAnswer :[],
      fromProfile : false,
      currentMedicens :"",
      currentSurgeries:"",
      loading:false,
      inputAnswer:[],
      questionLength:null
    };
  }

 

  componentDidMount() { 
    const { params } = this.props.navigation.state;
    console.log("naviational params", params)

    if(typeof params !== "undefined"){
      this.setState({fromProfile : params.fromProfile})
    }
    this.setState({loading:true})
    route.getAuthenticated('questions',this.props.myToken).then(async(response)=>{
        if(response.error){
          console.log(response.error)
          this.setState({loading:false})

        }
        else {
          console.log("questions response",response)
          this.setState({loading:false})
          var allData = response.allQuestions
          console.log("all data",allData)
          var medicalQuestions= []
          allData.map((item,index)=>{
            if(item.question_type == 'medical_history'){
              console.log("all question by medical history",item)
              medicalQuestions.push(item)
            }
          })
          medicalQuestions.forEach((value,index)=>{
              console.log("loop question answer",value)
              if(value.answer_type == 'multiple'){
                  value.question_option.forEach((item,index)=>{
                    var answerIndex = value.user_answer.findIndex((ans,index) => ans.option_id == item.id)
                    if(answerIndex >= 0){
                      console.log("answer index",answerIndex)
                      Object.assign(item, { selected: true });
                    }else{
                      Object.assign(item, { selected: false });
                    }
                    Object.assign(item, { noneSelected: false });
                    
                  })
              }
            return value
          })
          console.log("medical questions",medicalQuestions);
          this.setState({loading:false})
          this.setState({ medicalQuestion: medicalQuestions,questionLength:medicalQuestions.length})
          console.log('Coming from medical state state: ',this.state.medicalQuestion)

          
        }
    })
  }

  selectDisease(props) {

    const data = [...this.state.diseases];
    const index = data.findIndex((value, index, arr) => index === props);

    //console.log('found index,: ', index)
    data[index] = { ...data[index], selected: !this.state.diseases[index].selected };
    this.setState({ diseases: data });

  }
  selectFamilyDisease(props) {

    const data = [...this.state.familyDiseases];
    const index = data.findIndex((value, index, arr) => index === props);

    //console.log('found index,: ', index)
    data[index] = { ...data[index], selected: !this.state.familyDiseases[index].selected };
    this.setState({ familyDiseases: data });

  }

  selectAllergies(props) {
    const data = [...this.state.allergies];
    const index = data.findIndex((value, index, arr) => index === props);
    data[index] = { ...data[index], selected: !this.state.allergies[index].selected };
    this.setState({ allergies: data });

  }

  addAnswerOption = (currentQuestions,currentQestionindex,selectedOptionIndex) => {
   

    let answer = {question_id:currentQuestions[currentQestionindex].id,options:`${currentQuestions[currentQestionindex].question_option[selectedOptionIndex].id}`}
    this.state.questionAnswer.push(answer)
  }

  addAnswer = (currentQestionindex,selectedOptionIndex) => {
    let currentQuestions = [...this.state.medicalQuestion]
    console.log("current question",currentQuestions[currentQestionindex])
    if(this.state.questionAnswer.length == 0){
      this.addAnswerOption(currentQuestions,currentQestionindex,selectedOptionIndex) 
    }else{
      let selectQuestion = currentQuestions[currentQestionindex].id
      let oldQuestion = this.state.questionAnswer.findIndex((value, index, arr) => value.question_id === selectQuestion)
      console.log("old question",oldQuestion)
      if(oldQuestion >=0 ){
        var questionAnswerArray = this.state.questionAnswer
        var oldQuestionIndex = questionAnswerArray.findIndex((value,index)=> value.question_id == currentQuestions[currentQestionindex].id)
        var oldOptionArray = questionAnswerArray[oldQuestionIndex].options.split(',')
        var optionFind = oldOptionArray.findIndex((value,index)=> value == currentQuestions[currentQestionindex].question_option[selectedOptionIndex].id)
        if(optionFind == -1){
          this.state.questionAnswer[oldQuestion].options +=`,${currentQuestions[currentQestionindex].question_option[selectedOptionIndex].id}`
        }

      }else{
        this.addAnswerOption(currentQuestions,currentQestionindex,selectedOptionIndex) 
      }
      console.log("all questions", this.state.questionAnswer)
    }
  }
  saveQuestionAnswerInDb() {
      this.setState({questionAnswer: this.state.inputAnswer},()=>{
        console.log("user question answer submit",this.state.questionAnswer)

        this.setState({loading:true})
        this.state.medicalQuestion.map((value,questionIndex) => {
          value.question_option.map((item,optionIndex) => {
            if(item.selected == true){
              console.log("true item", item)
              this.addAnswer(questionIndex,optionIndex)
            }
          })
           
        })
    
        if(this.state.fromProfile ==  false){
          if(this.state.questionLength == this.state.questionAnswer.length){
            this.sendtoApi()
          }else{
            this.setState({loading:false})
            alert(I18n.t('please_answer_all_questions'))
          }
        }else{
          this.sendtoApi()
        }
    })
    
    
  }
  sendtoApi(){
    let userAnswer = {'user_id': this.props.user.id,'questions':this.state.questionAnswer}
    console.log("anwers on submit",userAnswer)
    var _this = this

    route.updateData('user-answer',userAnswer,this.props.myToken).then(async response => {
      if (response.error) {
        console.log(response.error)
        this.props.stopLoading();
        alert("Error", JSON.stringify(response.error));
        this.setState({loading:false})
      } else {
       // console.log(response);
       this.setState({loading:false})
            route.checkTokenExpire(response)
        console.log("user answer here",userAnswer)
        //_this.props.setQuestionAnswer(userAnswer)
        // this.setState({ showConfirmation: true });
        if(this.state.fromProfile == false){
          this.setState({loading:false})
          this.props.navigation.navigate("lifestyleScreen");
          this.props.isQuestionAnswerSetup(true);
        }else{
          this.setState({loading:false})
          this.props.navigation.navigate("profile");
        }
      }
    });
  }
  checkItem(item){
    var currentQestions = [...this.state.medicalQuestion]
    
    if(item.value == 'None'){
      const noneOptionQuestion = currentQestions.findIndex((value, index, arr) => value.id === item.question_id);
      console.log("question found",currentQestions[noneOptionQuestion]);
      
      if(item.selected == false){
        currentQestions[noneOptionQuestion].question_option.map((option,index)=>{
          if(option.value == "None"){
            option.selected = true
            option.noneSelected = true
          }else{
            option.selected = false
            option.noneSelected = true
          }
        })
        this.setState({ medicalQuestion: currentQestions },()=>{
          console.log('selecte none question',this.state.medicalQuestion)
        });
      }else{
        currentQestions[noneOptionQuestion].question_option.map((option,index)=>{
          
            option.selected = false
            option.noneSelected = false
          
        
      })
      this.setState({ medicalQuestion: currentQestions },()=>{
        console.log('selecte none question',this.state.medicalQuestion)
      });
      }
      
    }else{
      const currentQestionindex = currentQestions.findIndex((value, index, arr) => value.id === item.question_id);
      console.log(currentQestionindex)
      
      var selectedOptionIndex = currentQestions[currentQestionindex].question_option.findIndex((value, index, arr) => value.id === item.id);
      var itemSelectedNew = this.state.medicalQuestion[currentQestionindex].question_option[selectedOptionIndex].selected
      currentQestions[currentQestionindex].question_option[selectedOptionIndex] = { ...currentQestions[currentQestionindex].question_option[selectedOptionIndex], selected: itemSelectedNew != true ? true : false };
      // this.addAnswer(currentQestionindex,selectedOptionIndex)

      this.setState({ medicalQuestion: currentQestions });
    }
    

  }


  addInputAnswer(id,answer){
    var questionId = id
    var answerText = answer
    var inputAnswer = this.state.inputAnswer
    var answerIndexFind = this.state.inputAnswer.findIndex((value, index) => value.question_id == questionId);
    console.log("question answer wali id find",answerIndexFind)
    if(answerIndexFind < 0){
      let answer = { question_id: id, answer: answerText };
      // inputAnswer.push(answer)
      this.state.inputAnswer.push(answer)
    }else{
      this.state.inputAnswer[answerIndexFind].answer = answerText
    }
    console.log("input answer")
    this.setState({inputAnswer:inputAnswer})
  }

  skipAll(){
    this.props.skipSurvyScreen();
    this.props.navigation.navigate("App");
  }

  replaceSpaceToUnderScore(string) {
        let lowerCaseString = string.toLowerCase();
        let underScoreString = lowerCaseString.split(' ').join('_');
        let brackitLess = underScoreString.split("(").join("");
        let brackitLess2 = brackitLess.split(")").join("");
        let removeQuestionMark = brackitLess2.split("?").join("");
        let removeQoma = removeQuestionMark.split(",").join("");
        let removeDot = removeQoma.split(".").join("");
        let removeColon = removeDot.split(":").join("");
        let removeSemiColon = removeColon.split(";").join("");
        
        let singleUnderScore = removeSemiColon.split("-").join("_");
        singleUnderScore = singleUnderScore.split("__").join("_");
        singleUnderScore = singleUnderScore.split("/").join("_");
        var newString = singleUnderScore
        // console.log("string length",newString.length)
        
        if(singleUnderScore.length > 57){
            let cutLength = 57 - singleUnderScore.length;
            newString = singleUnderScore.substring(0, 57)
            // console.log("new sting with cut",newString)
        }
        // console.log("under score string", newString);
        return newString;
    }

 
  render() {
    return (
      <View style={styles.container}>
      <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t('please_wait')}...`}
          textStyle={{color:color.primary}}
          cancelable={true}
          color="#F16638"
        />
        <Header
          label={I18n.t("medical_history")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <KeyboardAwareScrollView>
          <View
            style={{
              width: "90%",
              flexDirection: "row",
              justifyContent: "flex-end",
              top: "10%"
            }}
          >
           {this.state.fromProfile == false &&
                            <Text style={{ color: "#e5e5e5", fontSize: 17 }}>2 {I18n.t('of')} 5</Text>
                        }
          </View>

          <Text style={styles.title}>{I18n.t('medical_history')}</Text>

          <View style={{ width: "90%", paddingVertical: 14, flexWrap: "wrap" }}>
            <Text style={styles.title1}>
              {I18n.t('please_provide_as_much_information_as_possible_recommendataions')}
            </Text>
          </View>



        {this.state.medicalQuestion != []? 
        this.state.medicalQuestion.map((value,index)=>{
          return <View>
              <Text style={styles.title2}>
                {++index +
                  " " +
                  I18n.t(
                    this.replaceSpaceToUnderScore(value.question)
                  )}
              </Text>

              {value.answer_type == "multiple" && <View style={{ width: "90%", //height: "35%",
                    alignSelf: "center",
                    paddingVertical: 8, flexWrap: "wrap", flexDirection: "row" }}>
                  {value.question_option.map((item, index) => {
                    console.log("every item", item.selected);
                    return <TouchableOpacity onPress={() => {
                      if(item.noneSelected == false || item.value == "None"){
                        this.checkItem(item)
                      }
                      
                      
                      }} key={index} style={[styles.listItem, { backgroundColor: item.selected ? color.primary : "white" }]}>
                        <Text
                          style={{
                            color: item.selected ? "white" : "black"
                          }}
                        >
                        {I18n.t(this.replaceSpaceToUnderScore(item.value))}
                        </Text>
                      </TouchableOpacity>;
                  })}
                </View>}

            {value.answer_type == "input" && <View style={{
              width: "90%", //height: "35%",
              alignSelf: "center",
              paddingVertical: 8, flexWrap: "wrap", flexDirection: "row"
            }}>
              
                <TextInput
                  style={styles.input1}
                  selectionColor={"#dfe6e9"}
                  onChangeText={text => {
                    if (text != "") {
                      this.addInputAnswer(value.id, text);
                    }else{
                      if(value.user_answer.length > 0){
                        this.addInputAnswer(value.id, value.user_answer[0].answer)
                      }
                    }
                  }
                  }
                  placeholder={value.user_answer.length > 0 ? value.user_answer[0].answer : I18n.t(this.replaceSpaceToUnderScore(value.question))}
                  autoCapitalize="words"
                  autoCorrect={true}
                  underlineColorAndroid="transparent"
                //placeholderTextColor="#969696"
                />
            </View>}


            </View>;

        })
        
        :
        null
        
        }


          {/* <View style={styles.form}>
            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={text => console.log(text)}
              placeholder={I18n.t('other_optional')}
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
            //placeholderTextColor="#969696"
            />

          </View> */}
          {this.state.fromProfile == true &&
            <Button
            label={I18n.t('submit')}
            buttonStyle={[styles.button,{marginBottom:15}]}
            onPress={() => this.saveQuestionAnswerInDb()}
          />
          
          }
          {this.state.fromProfile == false &&
            <Button
            label={I18n.t('next')}
            buttonStyle={styles.button}
            onPress={() => this.saveQuestionAnswerInDb()}
          />
          
          }
          {/* {this.state.fromProfile == false &&
            <TouchableOpacity activeOpacity={0.9} onPress={() => this.skipAll()} style={styles.footer}>
              <Text>{I18n.t('skip_i_dont_know')}</Text>
            </TouchableOpacity>
          
          } */}
          

          
        </KeyboardAwareScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "15%",
    paddingLeft: 30
  },
  listItem: {
    paddingVertical: 10,
    paddingHorizontal: 18,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
    marginRight: 4,
    marginVertical: 5,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  iconView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingHorizontal: 15
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "left",
    paddingLeft: 30

    //paddingHorizontal: "20%"
  },
  list1: {
    width: "90%",
    //height: "35%",
    alignSelf: "center",

    paddingVertical: 8,
    flexWrap: "wrap",
    flexDirection: "row"
  },
  title2: {
    color: "#969696",
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "left",
    fontWeight: "700",
    paddingLeft: 30,
    paddingRight: 20
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {

    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input2: {
    textAlignVertical: 'top',
    marginVertical: 10,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 14,
    width: "90%",
    /* borderColor: "pink",
      borderWidth: 2, */
    alignSelf: "center"
  },

  form2: {
    paddingVertical: 3,
    width: "90%",
    /* borderColor: "pink",
      borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    marginBottom: 15,


    /* borderColor: "red",
      borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",

    paddingVertical: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    borderBottomColor: "#F4AE96",
    borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
console.log('====================================');
console.log(state.user);
console.log('====================================');
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MedicalHistory);
