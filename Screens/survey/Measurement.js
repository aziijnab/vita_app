import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform
} from "react-native";

import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Route from "../../network/route.js";
import Spinner from "react-native-loading-spinner-overlay";
import Toast, { DURATION } from "react-native-easy-toast";
import Header from "../../components/Header";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

class Measurement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weight: "",
      height: "",
      waistCircumference: "",
      bloodPressure: "",
      capillaryGlucose: "",
      measurementQuestions: [],
      userAnswers: [],
      fromProfile: false,
      loading: false
    };
  }
  componentDidMount() {
    const { params } = this.props.navigation.state;
    if (typeof params !== "undefined") {
      this.setState({ fromProfile: params.fromProfile });
    }
    this.setState({ loading: true });
    route
      .getAuthenticated("questions", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
          console.log(response);
          route.checkTokenExpire(response);
          var allData = response.allQuestions;
          var questions = [];
          console.log("all questions", allData);
          allData.map((item, index) => {
            if (item.question_type == "vitals") {
              questions.push(item);
            }
          });
        }
        this.setState({ measurementQuestions: questions });
        console.log("measurementQuestions", this.state.measurementQuestions);
      });
  }
  userAnswerValue(value, id) {
    console.log("user Answer", value);
    console.log("question_id", id);

    var userAnswer = this.state.userAnswers;
    var answerIndex = userAnswer.findIndex(
      (value, index) => value.question_id == id
    );
    if (answerIndex >= 0) {
      console.log((userAnswer[answerIndex].answer = value));
    } else {
      let answer = { question_id: id, answer: value };
      userAnswer.push(answer);
    }
    this.setState({ userAnswers: userAnswer });
  }
  vitaAnswerSubmit() {
    console.log("vita answer submit", this.state.userAnswers);
    let answer = { questions: this.state.userAnswers };
    this.setState({ loading: true });
    route
      .updateData("user-answer", answer, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          console.log("Symptoms screen response");
          console.log(response);
          this.setState({ loading: false });
          route.checkTokenExpire(response);
          if (response) {
            if (this.state.fromProfile == false) {
              this.setState({ loading: false });
              this.props.navigation.navigate("App");
            } else {
              this.setState({ loading: false });
              this.props.navigation.navigate("profile");
            }
          }
        }
      });
  }

  replaceSpaceToUnderScore(string) {
    let lowerCaseString = string.toLowerCase();
    let underScoreString = lowerCaseString.split(" ").join("_");
    let brackitLess = underScoreString.split("(").join("");
    let brackitLess2 = brackitLess.split(")").join("");
    let removeQuestionMark = brackitLess2.split("?").join("");
    let removeQoma = removeQuestionMark.split(",").join("");
    let removeDot = removeQoma.split(".").join("");
    let removeColon = removeDot.split(":").join("");
    let removeSemiColon = removeColon.split(";").join("");

    let singleUnderScore = removeSemiColon.split("-").join("_");
    singleUnderScore = singleUnderScore.split("__").join("_");
    singleUnderScore = singleUnderScore.split("/").join("_");
    var newString = singleUnderScore;
    console.log("string length", newString.length);

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length;
      newString = singleUnderScore.substring(0, 57);
      console.log("new sting with cut", newString);
    }
    console.log("under score string", newString);
    return newString;
  }
  render() {
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Header
          label={`${I18n.t("vitals")} & ${I18n.t("measurement")}`}
          onbackPress={() => this.props.navigation.pop()}
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: "red" }}
          position="top"
          positionValue={80}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{ color: "white" }}
        />
        <KeyboardAwareScrollView>
          <View
            style={{
              width: "90%",
              flexDirection: "row",
              justifyContent: "flex-end",
              top: "10%"
            }}
          >
            {this.state.fromProfile == false && (
              <Text style={{ color: "#e5e5e5", fontSize: 17 }}>
                5 {I18n.t("of")} 5
              </Text>
            )}
          </View>
          <Text style={styles.title}>{I18n.t("vitals")} &</Text>
          <Text style={styles.title2}>{I18n.t("measurement")}</Text>

          <View style={styles.form}>
            {this.state.measurementQuestions != []
              ? this.state.measurementQuestions.map((item, index) => {
                  {
                    var answerValue = "";
                    console.log("user answer", item.user_answer.length);
                    if (item.user_answer.length > 0) {
                      answerValue = item.user_answer[0].answer;
                    }
                  }
                  return (
                    <View>
                        <View>
                          <Text
                            style={{
                              color: "#969696",
                              fontSize: 15,
                              letterSpacing: 0.5,
                              textAlign: "left",
                              fontWeight: "700",
                              paddingLeft: 2,
                              paddingRight: 20
                            }}
                          >
                            {++index +
                              " " +
                              I18n.t(
                                this.replaceSpaceToUnderScore(item.question)
                              )}
                          </Text>
                          <TextInput
                            style={styles.input1}
                            selectionColor={"#dfe6e9"}
                            returnKeyType="next"
                            onChangeText={text => {
                              console.log("user text", text);
                              if (text == "") {
                                this.userAnswerValue(answerValue, item.id);
                              } else {
                                this.userAnswerValue(text, item.id);
                              }
                            }}
                            placeholder={
                              answerValue == "" ||
                              item.user_answer[0].answer == null
                                ? I18n.t(
                                    this.replaceSpaceToUnderScore(item.question)
                                  )
                                : answerValue
                            }
                            autoCorrect={true}
                            underlineColorAndroid="transparent"
                            placeholderTextColor="#969696"
                          />
                        </View>
                    </View>
                  );
                })
              : null}

            {/* <TextInput
              style={styles.input1}
              ref='SecondInput'
              selectionColor={"#dfe6e9"}
              returnKeyType="next"
              onSubmitEditing={(event) => {
                this.refs.ThirdInput.focus();
              }}
              onChangeText={height => this.setState({ height })}
              placeholder="Height (cm)"
              keyboardType='number-pad'
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />

            <TextInput
              style={styles.input1}
              ref='ThirdInput'
              selectionColor={"#dfe6e9"}
              returnKeyType="next"
              onSubmitEditing={(event) => {
                this.refs.FourthInput.focus();
              }}
              onChangeText={waistCircumference => this.setState({ waistCircumference })}
              placeholder="Waist Circumference (cm)"
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />

            <TextInput
              style={styles.input1}
              ref='FourthInput'
              selectionColor={"#dfe6e9"}
              returnKeyType="next"
              onSubmitEditing={(event) => {
                this.refs.FifthInput.focus();
              }}
              onChangeText={bloodPressure => this.setState({ bloodPressure })}
              placeholder="Blood Pressure (optional)"
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            />

            <TextInput
              style={styles.input1}
              ref='FifthInput'
              selectionColor={"#dfe6e9"}
              returnKeyType="done"
              onChangeText={capillaryGlucose => this.setState({ capillaryGlucose })}
              placeholder="Capillary Glucose (optional)"
              autoCapitalize="words"
              autoCorrect={true}
              underlineColorAndroid="transparent"
              placeholderTextColor="#969696"
            /> */}
          </View>

          <Button
            label="SUBMIT"
            buttonStyle={[styles.button, { marginBottom: 15 }]}
            onPress={() => this.vitaAnswerSubmit()}
          />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "15%",
    paddingLeft: 30
  },
  title2: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: 6,
    paddingLeft: 30
  },
  iconView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingHorizontal: 15
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center"
    //paddingHorizontal: "20%"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 10,
    width: "90%",
    /* borderColor: "pink",
      borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    marginBottom: 15

    /* borderColor: "red",
      borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    borderBottomColor: "#F4AE96",
    borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    questionAnswer: state.userQuestionAnswer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Measurement);
