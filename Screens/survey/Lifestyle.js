import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Picker
} from "react-native";
import lodash from "lodash";
import Header from "../../components/Header";
import { Dropdown } from "../../components/customLib/react-native-material-dropdown";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import lifeStyleData from "../../components/Dummydata/lifeStyleData.json";
import Route from "../../network/route.js";
import Spinner from "react-native-loading-spinner-overlay";
import { NavigationEvents } from "react-navigation";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");
class Lifestyle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userAnswers: [],
      regularSmoking: "",
      yearEnded: "",
      alcoholConsumption: "",
      drinkingRoutine: "",
      dailyAlcoholIntake: "",
      dietPortion: "",
      supplimentsIntake: "",
      sexualPartners: "",
      dialysis: "",
      survyQuestions: [],
	  tmpSurvyQuestions: [],
      fromProfile: false,
      loading: false
    };
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    if (typeof params !== "undefined") {
      this.setState({ fromProfile: params.fromProfile });
    }
    this.fetchQuestionFromApi()
    
  }

  fetchQuestionFromApi(){
    this.setState({ loading: true });
    route
      .getAuthenticated("questions", this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });
        } else {
          this.setState({ loading: false });
          console.log(response);
          route.checkTokenExpire(response);
          var allData = response.allQuestions;
          var questions = [];
		  var tmpQuestions = [];
		  
          allData.map((item, index) => {
            if (item.question_type == "life_style") {
				console.log('questions for lifestyle', item);
              questions.push(item);
			  tmpQuestions.push(JSON.parse(JSON.stringify(item)));
            }
          });
          this.setState({ survyQuestions: questions, tmpSurvyQuestions: tmpQuestions },()=>{
              console.log("life style surty question",this.state.survyQuestions)
          });
        }
      });
  }

  submitToApi(){
    let userAnswer = {
        user_id: this.props.user.id,
        questions: this.state.userAnswers
      };
    console.log("life sytle submit ", userAnswer);
    route
      .updateData("user-answer", userAnswer, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.props.stopLoading();
          this.setState({ loading: false });
          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          console.log("life style screen response");
          console.log(response);

          this.setState({ loading: false });
          route.checkTokenExpire(response);
          if (this.state.fromProfile == false) {
            this.setState({ loading: false });
            this.props.navigation.navigate("symptomScreen");
          } else {
            this.setState({ loading: false });
            this.props.navigation.navigate("profile");
          }
        }
      });
  }

  lifeStyleSubmit() {
    //this.props.navigation.navigate("symptomScreen")
    // console.log("logged user detail",this.props.user)
    this.setState({ loading: true });
    
    console.log("user answer life style submit",this.state.userAnswers)
    if(this.state.fromProfile == false){
        if(this.state.survyQuestions.length === this.state.userAnswers.length){
            console.log("yeah find")
            this.submitToApi()
        }else{
            this.setState({loading:false})
          alert(I18n.t('please_answer_all_questions'))
        }
    }else{
        this.submitToApi()
    }
    
    // this.props.navigation.navigate('lifestyleScreen')
  }
   findIndexWithAttr(array, index) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i].question_id=== index) {
            return i;
        }
    }
    return -1;
}
  addOptionsToAnswer(index, value, options) {
      console.log('question answers',value);
	  console.log('options for anwswers',options);
      var userAnswers = this.state.userAnswers
      if(this.state.userAnswers.length > 0){
        console.log("user answers",this.state.userAnswers, options[index].question_id);
        // var findAnswerIndex = lodash.findIndex(this.state.userAnswers,(questionAnswer)=>{questionAnswer.question_id == options[index].question_id})
        // var findAnswerIndex = userAnswers.findIndex((value,index)=> value.question_id == options[index].question_id)
        var findAnswerIndex = this.findIndexWithAttr(this.state.userAnswers, options[index].question_id)
        console.log("user answer question id",findAnswerIndex)
        if(findAnswerIndex == -1){
           userAnswers.push({
              question_id: options[index].question_id,
              options: `${options[index].id}`
            });
            this.setState({userAnswers:userAnswers},()=>{
                console.log("state set")
              })
        }else{
          userAnswers[findAnswerIndex].options = `${options[index].id}`
          
          this.setState({userAnswers:userAnswers},()=>{
            console.log("state set")
          })
        }
     
      }else{
        userAnswers.push({
            question_id: options[index].question_id,
            options: `${options[index].id}`
          });
          this.setState({userAnswers:userAnswers},()=>{
            console.log("state set")
          })
      }
  }

  replaceSpaceToUnderScore(string) {
    let lowerCaseString = string.toLowerCase();
    let underScoreString = lowerCaseString.split(" ").join("_");
    let brackitLess = underScoreString.split("(").join("");
    let brackitLess2 = brackitLess.split(")").join("");
    let removeQuestionMark = brackitLess2.split("?").join("");
    let removeQoma = removeQuestionMark.split(",").join("");
    let removeDot = removeQoma.split(".").join("");
    let removeColon = removeDot.split(":").join("");
    let removeSemiColon = removeColon.split(";").join("");

    let singleUnderScore = removeSemiColon.split("-").join("_");
    singleUnderScore = singleUnderScore.split("__").join("_");
    singleUnderScore = singleUnderScore.split("/").join("_");
    var newString = singleUnderScore;
    console.log("string length", newString.length);

    if (singleUnderScore.length > 57) {
      let cutLength = 57 - singleUnderScore.length;
      newString = singleUnderScore.substring(0, 57);
      console.log("new sting with cut", newString);
    }
    console.log("under score string", newString);
    return newString;
  }

  arrayToStringUnderScore(stringArray) {
	  console.log('array options for dropdown', stringArray);
    var underScoreArray = [];
    stringArray.map((string, index) => {
      let lowerCaseString = string.value.toLowerCase();
      let underScoreString = lowerCaseString.split(" ").join("_");
      let brackitLess = underScoreString.split("(").join("");
      let brackitLess2 = brackitLess.split(")").join("");
      let removeQuestionMark = brackitLess2.split("?").join("");
      let removeQoma = removeQuestionMark.split(",").join("");
      let removeDot = removeQoma.split(".").join("");
      let removeColon = removeDot.split(":").join("");
      let removeSemiColon = removeColon.split(";").join("");

      let singleUnderScore = removeSemiColon.split("-").join("_");
      singleUnderScore = singleUnderScore.split("__").join("_");
      singleUnderScore = singleUnderScore.split("/").join("_");
      singleUnderScore = singleUnderScore.split("+").join("_");
      var newString = singleUnderScore;
      console.log("string length", newString.length);

      if (singleUnderScore.length > 57) {
        let cutLength = 57 - singleUnderScore.length;
        newString = singleUnderScore.substring(0, 57);
        console.log("new sting with cut", newString);
      }
      console.log("array under score string", newString);
      string.value = I18n.t(newString);
      underScoreArray.push(string);
    });
	this.state.survyQuestions = JSON.parse(JSON.stringify(this.state.tmpSurvyQuestions));
    return underScoreArray;
  }
  render() {
    return (
      <View style={styles.container}>
        {/* <NavigationEvents onDidFocus={() => this.fetchQuestionFromApi()} /> */}

        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
         <Header
          label={I18n.t("lifestyle")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <ScrollView keyboardShouldPersistTaps={"always"}>
          <View
            style={{
              width: "90%",
              flexDirection: "row",
              justifyContent: "flex-end",
              top: "10%"
            }}
          >
            {this.state.fromProfile == false && (
              <Text style={{ color: "#e5e5e5", fontSize: 17 }}>
                3 {I18n.t("of")} 5
              </Text>
            )}
          </View>
          <Text style={styles.title}>{I18n.t("lifestyle")}</Text>

          <View style={styles.form}>
            {this.state.survyQuestions != []
              ? this.state.survyQuestions.map((item, index) => {
				  var tmpItem = JSON.parse(JSON.stringify(item));
                  var optionValue = "null";
                  console.log("user answer length", this.state.tmpSurvyQuestions);
                  if (item.user_answer.length > 0) {
                    var optionIndex = item.question_option.findIndex(
                      (value, index) =>
                        value.id == item.user_answer[0].option_id
                    );
                    optionValue = tmpItem.question_option[optionIndex].value;
					
					console.log("optionValue value", tmpItem.question_option);
                  }
                  return (
                    <View>
                      {item.answer_type == "single" && (
                        <View>
                          <Text style={styles.title2}>
                            {++index +
                              " " +
                              I18n.t(
                                this.replaceSpaceToUnderScore(item.question)
                              )}
                          </Text>
                          {/* {console.log("option string",item.question_option)} */}
                          <Dropdown
                            inputContainerStyle={{
                              borderBottomColor: "transparent"
                            }}
                            onChangeText={(itemValue, itemIndex, items) =>
                              this.addOptionsToAnswer(
                                itemIndex,
                                itemValue,
                                items
                              )
                            }
                            labelFontSize={15}
                            rippleOpacity={0.1}
                            dropdownMargins={{ min: 0, max: 0 }}
                            dropdownOffset={{ top: 4, left: 2 }}
                            containerStyle={styles.pickerSt}
                            pickerStyle={{ paddingHorizontal: 10 }}
                            textColor={"#969696"}
                            data={this.arrayToStringUnderScore(
                              item.question_option
                            )}
                            overlayStyle={{
                              borderColor: "yellow",
                              borderWidth: 1
                            }}
                            value={I18n.t(
                              this.replaceSpaceToUnderScore(optionValue +"")
                            )}
                          />
                        </View>
                      )}
                    </View>
                  );
                })
              : null}
          </View>
          {this.state.fromProfile == true && (
            <Button
              label={I18n.t("submit")}
              buttonStyle={[styles.button, { marginBottom: 15 }]}
              onPress={() => this.lifeStyleSubmit()}
            />
          )}
          {this.state.fromProfile == false && (
            <Button
              label={I18n.t("next")}
              buttonStyle={styles.button}
              onPress={() => this.lifeStyleSubmit()}
            />
          )}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "100%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "96%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "15%",
    paddingLeft: 30
  },
  iconView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingHorizontal: 15
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center"
    //paddingHorizontal: "20%"
  },
  title2: {
    color: "#969696",
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "left",
    fontWeight: "700",
    paddingLeft: 2,
    paddingRight: 20
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 5,
    width: "48%",
    flexDirection: "row",
    paddingVertical: 2,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  fieldView: {
    marginVertical: 20
  },

  textView: {
    marginVertical: 15
  },

  pickerview: {
    marginVertical: 7,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 2,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 20,
    width: "90%",
    /* borderColor: "pink",
          borderWidth: 2, */
    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    marginBottom: 15
    /* borderColor: "red",
          borderWidth: 2 */
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  pickerSt: {
    marginVertical: 17,
    width: "100%",
    alignSelf: "center",
    //flexDirection: "row",
    paddingTop: 6,
    paddingHorizontal: 15,
    justifyContent: "center",
    //alignItems: "center",
    //paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  inputPicker: {
    marginVertical: 3,
    width: "48%",
    //alignSelf: "center",
    //flexDirection: "row",
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: "center",
    //alignItems: "center",
    //paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  pickerSt1: {
    marginVertical: 3,
    width: "48%",
    alignSelf: "center",
    //flexDirection: "row",
    paddingTop: 6,
    paddingHorizontal: 15,
    justifyContent: "center",
    //alignItems: "center",
    //paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  pickerSt2: {
    marginVertical: 7,
    width: "100%",
    alignSelf: "center",
    //flexDirection: "row",
    paddingTop: 6,
    paddingHorizontal: 15,
    justifyContent: "center",
    //alignItems: "center",
    //paddingLeft: 19,
    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    borderBottomColor: "#F4AE96",
    borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  return {
    user: state.user.userInfo,
    myToken: state.user.userToken,
    questionAnswer: state.userQuestionAnswer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Lifestyle);
