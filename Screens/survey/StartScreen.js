import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import Route from "../../network/route.js";

// const route = new Route("https://vita-health.herokuapp.com/api/");
const route = new Route("http://192.168.100.18:8000/api/");

import { Dropdown } from "react-native-material-dropdown";
import { color, font, apiURLs } from "../../components/Constant";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions"; //Import your actions
import I18n from "../../i18n/i18n";
const HEIGHT = Dimensions.get("window").height;
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/Button";
import InputField from "../../components/InputField";
import personalData from "../../components/Dummydata/personalData.json";
import Spinner from "react-native-loading-spinner-overlay";
import Toast, { DURATION } from "react-native-easy-toast";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Header from "../../components/Header";

class StartScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      company: this.props.userInfo.user_profile.company_name || '',
      maritalStatus: this.props.userInfo.user_profile.marital_status || '',
      numOfChildrens: this.props.userInfo.user_profile.no_of_children || '',
      education: this.props.userInfo.user_profile.education || '',
      incomeRange: this.props.userInfo.user_profile.income || '',
      mystate: this.props.userInfo.user_profile.state || '',
      postcode: this.props.userInfo.user_profile.postcode || '',
      address:this.props.userInfo.user_profile.address || '',
      personalHistory: "",
      loading: false,
      fromProfile: false
    };
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    console.log("params.fromProfile", params.fromProfile);

    if (typeof params.fromProfile !== "undefined") {
      this.setState({ fromProfile: params.fromProfile });
    }

    console.log("user info", this.props.userInfo);
  }
  validateData() {
    var errorMessages = [];
    if (this.state.maritalStatus == "") {
      errorMessages.push(I18n.t("please_select_your_marriage_status"));
    }

    if (this.state.numOfChilds == "") {
      errorMessages.push(I18n.t("please_select_your_number_of_childs"));
    }

    if (this.state.education == "") {
      errorMessages.push(I18n.t("please_select_your_education"));
    }

    if (this.state.mystate == "") {
      errorMessages.push(I18n.t("please_select_your_state"));
    }

    if (this.state.postcode == "") {
      errorMessages.push(I18n.t("please_select_your_post_code"));
    }

    if (errorMessages.length == 0) {
      this.saveProfileWithData();
    } else {
      let errorArray = errorMessages.join("\n");
      this.refs.toast.show(errorArray, DURATION.SHORT);
    }
  }
  saveProfileWithData() {
    const { params } = this.props.navigation.state;

    console.log(params.basicData);
    var userData = params.basicData;
    console.log("user Param Data", userData);
    var profileData = null;

    if (this.state.fromProfile === true) {
      profileData = {
        company_name: this.state.company,
        marital_status: this.state.maritalStatus,
        no_of_children: this.state.numOfChilds,
        education: this.state.education,
        income: this.state.incomeRange,
        state: this.state.mystate,
        postcode: this.state.postcode,
        address:this.state.address
      };
    } else {
      profileData = {
        mobile: userData.contact,
        full_name: userData.name,
        date_of_birth: userData.dob,
        gender: userData.gender,
        age: userData.age,
        company_name: this.state.company,
        marital_status: this.state.maritalStatus,
        no_of_children: this.state.numOfChilds,
        education: this.state.education,
        income: this.state.incomeRange,
        state: this.state.mystate,
        postcode: this.state.postcode,
        
      };
    }
    console.log(profileData);
    //this.props.userLogin(credentials);
    this.setState({ loading: true });

    route
      .updateData("profile", profileData, this.props.myToken)
      .then(async response => {
        if (response.error) {
          console.log(response.error);
          this.setState({ loading: false });

          Alert.alert("Error", JSON.stringify(response.error));
        } else {
          this.setState({ loading: false });
          route.checkTokenExpire(response);
          console.log("user profile create", response);

          // this.props.setUser(response.token);
          response.token = this.props.myToken;
          this.props.setUser(response);
          this.props.isProfileSetup(true);
          this.setState({ loading: false });
          console.log("from profile", this.state.fromProfile);
          if (this.state.fromProfile == false) {
            this.props.navigation.navigate("medicalHistory");
          } else {
            this.props.navigation.navigate("profile");
          }

          // this.setState({ showConfirmation: true });

          //Alert.alert('Success', 'Successfuly registered.')

          //dispatch(NavigationActions.navigate({ routeName: 'App' }))
        }
      });
  }

  render() {
    console.log("Personal Data", personalData);
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.loading}
          textContent={`${I18n.t("please_wait")}...`}
          textStyle={{ color: color.primary }}
          cancelable={true}
          color="#F16638"
        />
        <Header
          label={I18n.t("personal_data")}
          onbackPress={() => this.props.navigation.pop()}
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: "red" }}
          position="top"
          positionValue={80}
          fadeInDuration={750}
          fadeOutDuration={1000}
          textStyle={{ color: "white" }}
        />

        <KeyboardAwareScrollView>
          {this.state.fromProfile == false && (
            <View
              style={{
                width: "90%",
                flexDirection: "row",
                justifyContent: "flex-end",
                top: "10%"
              }}
            >
              <Text style={{ color: "#e5e5e5", fontSize: 17 }}>
                1 {I18n.t("of")} 5
              </Text>
            </View>
          )}
          <Text style={styles.title}>{I18n.t("personal_data")}</Text>

          <View style={styles.form}>
            <Text
              style={{
                color: "#969696",
                fontSize: 15,
                letterSpacing: 0.5,
                textAlign: "left",
                fontWeight: "700",
                paddingLeft: 10
              }}
            >
              {I18n.t("company_coporate_only")}
            </Text>
            <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={company => this.setState({ company })}
              placeholder={I18n.t("company_coporate_only")}
              autoCapitalize="words"
              returnKeyType="done"
              autoCorrect={true}
              value={this.state.company}
              underlineColorAndroid="transparent"
            />
            {/* <TextInput
              style={styles.input1}
              selectionColor={"#dfe6e9"}
              onChangeText={personalHistory => this.setState({ personalHistory })}
              placeholder={I18n.t("personal_history")}
              autoCapitalize="words"
              returnKeyType="done"
              autoCorrect={true}
              underlineColorAndroid="transparent"
            /> */}
            <Text
              style={{
                color: "#969696",
                fontSize: 15,
                letterSpacing: 0.5,
                textAlign: "left",
                fontWeight: "700",
                paddingLeft: 10
              }}
            >
              {I18n.t("marriage_status")}
            </Text>
            <Dropdown
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              onChangeText={(itemValue, itemIndex) =>
                this.setState({ maritalStatus: itemValue })
              }
              labelFontSize={15}
              rippleOpacity={0.1}
              dropdownMargins={{ min: 0, max: 0 }}
              dropdownOffset={{ top: 4, left: 2 }}
              containerStyle={styles.pickerSt}
              pickerStyle={{ paddingHorizontal: 10 }}
              textColor={"#969696"}
              data={personalData[0].marriageStatus}
              overlayStyle={{ borderColor: "yellow", borderWidth: 1 }}
              value={this.state.maritalStatus}
              baseColor="#576574"
            />

            <Text
              style={{
                color: "#969696",
                fontSize: 15,
                letterSpacing: 0.5,
                textAlign: "left",
                fontWeight: "700",
                paddingLeft: 10
              }}
            >
              {I18n.t("no_of_child")}
            </Text>
            <Dropdown
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              onChangeText={(itemValue, itemIndex) =>
                this.setState({ numOfChilds: itemValue })
              }
              labelFontSize={15}
              rippleOpacity={0.1}
              dropdownMargins={{ min: 0, max: 0 }}
              dropdownOffset={{ top: 4, left: 2 }}
              containerStyle={styles.pickerSt}
              pickerStyle={{ paddingHorizontal: 10 }}
              textColor={"#969696"}
              data={personalData[0].numOfChildrens}
              overlayStyle={{ borderColor: "yellow", borderWidth: 1 }}
              value={this.state.numOfChildrens}
              baseColor="#576574"
            />
            <Text
              style={{
                color: "#969696",
                fontSize: 15,
                letterSpacing: 0.5,
                textAlign: "left",
                fontWeight: "700",
                paddingLeft: 10
              }}
            >
              {I18n.t("highest_education_attained")}
            </Text>
            <Dropdown
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              onChangeText={(itemValue, itemIndex) =>
                this.setState({ education: itemValue })
              }
              labelFontSize={15}
              rippleOpacity={0.1}
              dropdownMargins={{ min: 0, max: 0 }}
              dropdownOffset={{ top: 4, left: 2 }}
              containerStyle={styles.pickerSt}
              pickerStyle={{ paddingHorizontal: 10 }}
              textColor={"#969696"}
              data={personalData[0].education}
              overlayStyle={{ borderColor: "yellow", borderWidth: 1 }}
              value={this.state.education}
              baseColor="#576574"
            />
            
          </View>

          <View style={[styles.custom, { marginBottom: 10 }]}>
            <Text style={{ color: "#969696", fontWeight: "700" }}>
              {I18n.t("primary_address")}
            </Text>
          </View>

          <Text
            style={{
              color: "#969696",
              fontSize: 15,
              letterSpacing: 0.5,
              textAlign: "left",
              fontWeight: "700",
              paddingLeft: 25
            }}
          >
            {I18n.t("state")}
          </Text>
          <Dropdown
            inputContainerStyle={{
              borderBottomColor: "transparent",
              width: "100%"
            }}
            onChangeText={(itemValue, itemIndex) =>
              this.setState({ mystate: itemValue })
            }
            labelFontSize={15}
            rippleOpacity={0.1}
            dropdownMargins={{ min: 0, max: 0 }}
            dropdownOffset={{ top: 4, left: 2 }}
            containerStyle={styles.pickerSt1}
            pickerStyle={{ paddingHorizontal: 10, width: "100%" }}
            textColor={"#969696"}
            data={personalData[0].state}
            overlayStyle={{ borderColor: "yellow", borderWidth: 1 }}
            value={this.state.mystate}
            baseColor="#576574"
          />

          {/* <Dropdown
              inputContainerStyle={{ borderBottomColor: "transparent" }}
              onChangeText={(itemValue, itemIndex) =>
                this.setState({ location: itemValue })
              }
              labelFontSize={15}
              rippleOpacity={0.1}
              dropdownMargins={{ min: 0, max: 0 }}
              dropdownOffset={{ top: 4, left: 2 }}
              containerStyle={styles.pickerSt1}
              pickerStyle={{ paddingHorizontal: 10 }}
              textColor={"#969696"}
              data={personalData[0].location}
              overlayStyle={{ borderColor: "yellow", borderWidth: 1 }}
              value={I18n.t('location')}
              baseColor="#576574"
            /> */}

          <Text
            style={{
              color: "#969696",
              fontSize: 15,
              letterSpacing: 0.5,
              textAlign: "left",
              fontWeight: "700",
              paddingLeft: 25
            }}
          >
            {I18n.t("postcode")}
          </Text>
          <TextInput
            style={styles.postCode}
            selectionColor={"#dfe6e9"}
            onChangeText={text => this.setState({ postcode: text })}
            placeholder={I18n.t("postcode")}
            value={this.state.postcode}
            keyboardType={"phone-pad"}
            returnKeyType="done"
            underlineColorAndroid="transparent"
          />

          {this.state.fromProfile ? (
            <Button
              label={I18n.t("submit")}
              buttonStyle={[styles.button, { marginBottom: 15 }]}
              onPress={() => this.validateData()}
            />
          ) : (
            <Button
              label={I18n.t("next")}
              buttonStyle={[styles.button, { marginBottom: 15 }]}
              onPress={() => this.validateData()}
            />
          )}
        </KeyboardAwareScrollView>

        {/* onPress={() => this.props.navigation.navigate('medicalHistory')} */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  custom: {
    flexDirection: "row",
    width: "90%",
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "flex-start"
  },
  custom1: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  title: {
    fontSize: 25,
    fontWeight: "700",
    letterSpacing: 0.5,
    paddingTop: "15%",
    paddingLeft: 30
  },
  iconView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingHorizontal: 15
  },
  title1: {
    fontSize: 15,
    letterSpacing: 0.5,
    textAlign: "center"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 15,
    alignSelf: "center",
    width: "90%",
    marginVertical: 10,

    backgroundColor: color.primary
  },
  touchable: {
    marginVertical: 17,
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  touchable3: {
    marginVertical: 17,
    width: "40%",
    flexDirection: "row",
    paddingVertical: 10,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },
  input1: {
    marginVertical: 17,
    width: "100%",
    height: 50,
    flexDirection: "row",
    paddingVertical: 8,
    alignItems: "center",
    paddingLeft: 19,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  postCode: {
    marginVertical: 17,
    height: 50,
    width: "90%",
    paddingLeft: 20,
    flexDirection: "row",
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#dfe6e9",
    alignSelf: "center"
  },

  pickerSt: {
    marginVertical: 17,
    width: "100%",
    alignSelf: "center",

    paddingTop: 6,
    paddingHorizontal: 15,
    justifyContent: "center",

    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  pickerSt1: {
    marginVertical: 17,
    width: "90%",
    alignSelf: "center",

    paddingTop: 6,
    paddingHorizontal: 15,
    justifyContent: "center",

    borderRadius: 27,
    borderWidth: 1,
    borderColor: "#dfe6e9"
  },

  remember: {
    alignSelf: "center",
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 4
  },
  form: {
    paddingVertical: 10,
    width: "90%",

    alignSelf: "center"
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    marginBottom: 15
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
    alignSelf: "center"
  },
  header: {
    height: 50,
    width: "100%",
    justifyContent: "flex-start",
    flexDirection: "row",
    alignSelf: "center",
    borderBottomColor: "#F4AE96",
    borderBottomWidth: 1,
    marginTop: Platform.OS == "ios" ? 20 : 0,
    backgroundColor: "white"
  }
});

function mapStateToProps(state, props) {
  console.log("========Start screen =======");
  console.log(state.user);
  console.log("====================================");
  return {
    userInfo: state.user.userInfo,
    userState: state.user,
    myToken: state.user.userToken,
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StartScreen);
