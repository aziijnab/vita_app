import { SELECTED_USER_PACKAGES,QUESTION_ANSWER_SETUP,PROFILE_SETUP, SELECTED_LANGUAGE,IS_PIN_CODE_SET,CHOSE_AT_CLINIC,PIN_CODE,FIRST_TIME_DISCOUNT,APPOINTMENT_TOTAL_PRICE,SELECTED_USER_PACKAGES_DETAIL, INBOX_COUNT, NOTIFICATION_COUNT, SET_USER,BOARD_DONE,SKIP_SURVY, REMOVE_USER, SET_QUESTION_ANSWER, ALTER_USER, USER_LOGIN, USER_SIGNUP, REQUEST_DATA,START_LOADING,STOP_LOADING } from '../actions/types';

const initialState = {
  userDetail: "",
  userToken: "",
  user: null,
  userInfo:null,
  loading: false,
  testData:'Heloo from store',
  userQuestionAnswer:null,
  boardDone:false,
  skipSurvy:false,
  selectUserPackages:[],
  selectUserPackagesDetail:[],
  appointmentTotalPrice:null,
  selectedLanguage:'',
  inboxCount:0,
  notificationCount:0,
  pinCode:'',
  firstTimeDiscount: false,
  isPinCodeSet:false,
  isChoseAtClinicPayment:false,
  isQuestionAnswerSetUp:false,
  isProfileSetUp:false,
};

const userReducer = (state = initialState, action) => {
  //console.log('in user reducer');
  switch (action.type) {
    
    
    case START_LOADING:
      return { ...state, loading: true };
    
    case STOP_LOADING:
      return { ...state, loading: false };


    case REQUEST_DATA:
      return { ...state, loading: !state.loading };

    case BOARD_DONE:
    return { ...state, boardDone: true }

    case SET_USER:
      console.log("Set user action with Token :", action.payload.loggedUser);
      // state = Object.assign({}, state, {userToken: action.payload, loading: false});
      // return state;
      return { ...state, userToken: action.payload.token, user:action.payload.loggedUser,userInfo:action.payload.loggedUser,loading: false };

    // case REMOVE_USER:
    //   return { ...initialState };.log
    case ALTER_USER:
      console.log("Alter User details with:", action.payload);

      // console.log("updated Data : ", action.payload);
      // state = Object.assign({}, state, {
      //   userDetail: action.payload,
      //   loading: false
      // });
      return { ...state, user:action.payload,loading:false };
    
    case SKIP_SURVY:
      return { ...state,skipSurvy:true };
    
    case SET_QUESTION_ANSWER:
     return { ...state, userQuestionAnswer:action.payload };

     case INBOX_COUNT:
     return { ...state, inboxCount:action.payload };

     case NOTIFICATION_COUNT:
     return { ...state, notificationCount:action.payload };
    
     case SELECTED_LANGUAGE:
     return { ...state, selectedLanguage:action.payload };

     case SELECTED_USER_PACKAGES_DETAIL:
     return { ...state, selectUserPackagesDetail:action.payload }

     case APPOINTMENT_TOTAL_PRICE:
     return { ...state, appointmentTotalPrice:action.payload }

     case PIN_CODE:
     return { ...state, pinCode:action.payload }

     case FIRST_TIME_DISCOUNT:
     return { ...state,firstTimeDiscount:action.payload}

     case CHOSE_AT_CLINIC:
     return { ...state,isChoseAtClinicPayment:action.payload}

    case IS_PIN_CODE_SET:
      return { ...state, isPinCodeSet: action.payload }

    case QUESTION_ANSWER_SETUP:
    return { ...state, isQuestionAnswerSetUp: action.payload }
  
    case PROFILE_SETUP:
    return { ...state, isProfileSetUp: action.payload }

    case SELECTED_USER_PACKAGES:
    return { ...state, selectUserPackages:action.payload }
    default:
      return state;
  }
};

export default userReducer;
