import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity,
  ScrollView, } from 'react-native'

export default class DrawerContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { navigation } = this.props;
    
    return (
      
      <View style={{flex:1}}>

        
        <View style={{margin:30, alignItems:'flex-start'}}>
          <Text style={{fontSize:20,}}>Menu</Text>
        </View>
        <View  style={{flex:5}}>
            <ScrollView style={{flexDirection:'column', padding:20, paddingTop:0, }}>
              
              <TouchableOpacity style={{marginVertical:10}}  onPress={() => this.props.navigation.navigate('drawerStack')}>
                <View style={styles.rowContainer}>
                  
                  <Text style={styles.rowText}>HOME</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={{marginVertical:10}}  onPress={() => this.props.navigation.navigate('ViewProfile')}>
                <View style={styles.rowContainer}>
                  
                  <Text style={styles.rowText}>PROFILE</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={{marginVertical:10}}  onPress={() => this.props.navigation.navigate('EventScreen')}>
                <View style={styles.rowContainer}>
                  
                  <Text style={styles.rowText}>EVENTS</Text>
                </View>
              </TouchableOpacity>

          
            </ScrollView>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

rowContainer: {
  flexDirection:'row', justifyContent:'flex-start', height:40, alignItems:'center'
},
rowText : {
  alignItems:'center', marginHorizontal:10
},

});

