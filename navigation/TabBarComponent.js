import React from 'react'

import { StyleSheet, View, Image, TouchableOpacity, Text } from 'react-native';
import { color, font, appStrings } from '../components/Constant';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions'; //Import your actions
import Icon from 'react-native-vector-icons/Feather';
import { StackActions, NavigationActions } from 'react-navigation';

class TabBarComponent extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
        userFullName: '',
        userImageProfile: ''
    }
  }

  clearCounter(){
    this.props.clearNotification();
    this.props.navigation.navigate('Appointment');
  }

  render(){
    // const { userDetail, dispatch } = this.props
    // console.log('props: ', this.props.state);
    const { container, innerContainer, containerIconStyle, iconStyle } = styles;

    return (
      <View style={container}>
            <View style={innerContainer}>

              {this.props.navigation.state.index == 0 ?
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                   onPress={() => this.props.navigation.navigate('Appointment')}>
                  <Icon name="calendar" size={27} color={color.white} />
                </TouchableOpacity>
                :
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> this.props.navigation.navigate('Appointment') }>
                  <Icon name="calendar" size={27} color={'#2e3131'} />
                </TouchableOpacity>
              }

              {this.props.navigation.state.index == 1 ?
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> {
                    this.props.navigation.navigate('testScreen') 
                  }
                  }>
                  <Icon name="life-buoy" size={27} color={color.white} />
                </TouchableOpacity>
                :
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center', }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> {
                    this.props.navigation.navigate('testScreen') 
                    }
                   }>
                  <Icon name="life-buoy" size={27} color={'#2e3131'} />
                </TouchableOpacity>
              }

              {this.props.navigation.state.index == 2 ?
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center',marginTop:-40,backgroundColor:color.primary,height:50,borderTopEndRadius:40,borderTopLeftRadius:40 }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> {
                    this.props.navigation.navigate('userScreen') 
                    } }>
                  <Icon name="user" size={27} color={color.white} />
                </TouchableOpacity>
                :
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center',marginTop:-40,backgroundColor:color.primary,height:50,borderTopEndRadius:40,borderTopLeftRadius:40 }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> {
                    this.props.navigation.navigate('userScreen') 
                    } }>
                  <Icon name="user" size={27} color={'#2e3131'} />
                </TouchableOpacity>
              }

              {this.props.navigation.state.index == 3 ?
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> {
                    this.props.navigation.navigate('analysisScreen') 
                    } }>
                  <Icon name="bar-chart-2" size={27} color={color.white} />
                </TouchableOpacity>
                :
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> {
                    this.props.navigation.navigate('analysisScreen') 
                    } }>
                  <Icon name="bar-chart-2" size={27} color={'#2e3131'} />
                </TouchableOpacity>
              }

              {this.props.navigation.state.index == 4 ?
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> {
                    this.props.navigation.navigate('settingsScreen') 
                    }}>
                  <Icon name="settings" size={27} color={color.white} />
                </TouchableOpacity>
                :
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> {
                    this.props.navigation.navigate('settingsScreen') 
                    }}>
                  <Icon name="settings" size={27} color={'#2e3131'} />
                </TouchableOpacity>
              }


            </View>
		  </View>
    )
  }
}

function mapStateToProps(state, props) {
  //console.log('unreadCount: ',state.notify.unreadCount);
  return {
    userDetail: state.user.userDetail,
    //notificationCount: state.notify.unreadCount,
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(TabBarComponent);

const styles = StyleSheet.create({
    container: {
      height: 50,
      backgroundColor: color.primary,
      margin: 0,
      padding:0,
      // borderTopWidth: 0.5,
      // borderTopColor: color.silver
    },
    innerContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      margin: 0,
      padding:0,
      backgroundColor: color.primary,
    },
      containerIconStyle: {
      height: 60
    },
    iconStyle: {
      fontSize: 20,
      color: '#FFF'
    },
});
  