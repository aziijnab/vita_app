import React from 'react';
import { createBottomTabNavigator, createStackNavigator, createDrawerNavigator } from 'react-navigation';
import AppointmentScreen from '../Screens/appointment';
import EventScreen from '../Screens/event';
import EventDetails from '../Screens/event/event_details';
import TestsScreen from '../Screens/tests';
import UserScreen from '../Screens/user';
import AccountHistory from '../Screens/settings/AccountHistory';
import SettingsScreen from '../Screens/settings';
import AnalysisScreen from '../Screens/analysis';
import DrawerContainer from './DrawerContainer';
import TabBarComponent from './TabBarComponent';
import Clinincs from '../Screens/tests/Clinincs';
import Map from '../Screens/tests/Map';
import ClinicProfile from '../Screens/tests/ClinicProfile';
import Profile from '../Screens/settings/Profile';
import Myuploads from '../Screens/settings/Myuploads';
import SetPin from '../Screens/settings/SetPin';
import Auditlogs from '../Screens/settings/Auditlogs';
import LanguageScreen from "../Screens/login/LanguageScreen";
import Terms from '../Screens/settings/Terms';
import Privacy from '../Screens/settings/Privacy';
import Notifications from '../Screens/Notifications';
import BookAppointment from '../Screens/tests/BookAppointment';
import AppointmentDetails from '../Screens/tests/AppointmentDetails';
import Docotrs from '../Screens/tests/Doctors';
import Information from '../Screens/tests/Information';
import Location from '../Screens/tests/Location';
import Review from '../Screens/tests/Review';
import Call from '../Screens/tests/Call';
import Results from '../Screens/appointment/Results';
import Measurement from "../Screens/survey/Measurement";
import Symptoms from "../Screens/survey/Symptoms";
// import UpdatePassword from '../Screens/settings/updatePassword';
import StartScreen from "../Screens/survey/StartScreen";

import CameraView from '../Screens/settings/cameraView';

import Lifestyle from "../Screens/survey/Lifestyle";
import MedicalHistory from "../Screens/survey/MedicalHistory";

import TestResults from '../Screens/user/TestResults';
import History from '../Screens/user/History';


import PaymentMethod from '../Screens/tests/PaymentMethod';
import Inbox from '../Screens/Inbox';
import Message from '../Screens/Message';
import SelfMonitoring from '../Screens/user/SelfMonitoring';
import updatePassword from '../Screens/settings/updatePassword';
import UpdatePinCode from "../Screens/settings/updatePinCode";

const AppointmentStack = createStackNavigator({
  
  // appointmentScreen: { screen: AppointmentScreen },
  eventScreen: { screen: EventScreen },
  EventDetails: { screen: EventDetails },
  notifications: { screen: Notifications },
  results: { screen: Results },
  inbox: { screen: Inbox },
  //message: { screen: Message }

}, 
{
    headerMode: 'none',
    initialRouteName: 'eventScreen',
  }
  
  )
const EventStack = createStackNavigator({
  
  eventScreen: { screen: EventScreen },
  EventDetails: { screen: EventDetails },
  notifications: { screen: Notifications },
  results: { screen: Results },
  inbox: { screen: Inbox },
  //message: { screen: Message }

}, 
{
    headerMode: 'none',
    initialRouteName: 'eventScreen',
  }
  
  )

const TestStack = createStackNavigator({
  testScreen: { screen: TestsScreen },
  clinics: { screen: Clinincs },
  mapview: { screen: Map },
  clinicprofile: { screen: ClinicProfile },
  notifications: { screen: Notifications },
  bookAppointment: { screen: BookAppointment },
  appointmentDetails: { screen: AppointmentDetails },
  paymentMethod: { screen: PaymentMethod },
  inbox: { screen: Inbox },
  //message: { screen: Message },
  doctorScreen: { screen: Docotrs },
  information: { screen: Information },
  location: { screen: Location },
  review: { screen: Review },
  call: { screen: Call }

}, {
    headerMode: 'none',
    initialRouteName: 'testScreen',
  })

const UserStack = createStackNavigator({
  userScreen: { screen: UserScreen },
  testResults: { screen: TestResults },
  notifications: { screen: Notifications },
  history: { screen: History },
  inbox: { screen: Inbox },
  selfMonitoring: { screen: SelfMonitoring },
  
  //message: { screen: Message }



}, {
    headerMode: 'none',
    initialRouteName: 'userScreen',
  })


const AnalysisStack = createStackNavigator({
  analysisScreen: { screen: AnalysisScreen },
  notifications: { screen: Notifications },
  inbox: { screen: Inbox },
  //message: { screen: Message }



}, {
    headerMode: 'none',
    initialRouteName: 'analysisScreen',
  })

const SettingsStack = createStackNavigator({

  settingsScreen: { screen: SettingsScreen },
  profile: { screen: Profile },
  myuploads: { screen: Myuploads },
  accountHistory: { screen: AccountHistory },
  medicalHistory: { screen: MedicalHistory },
  startScreen: { screen: StartScreen },
  lifestyleHistory: { screen: Lifestyle },
  setpin: { screen: SetPin },
  auditLogs: { screen: Auditlogs },
  terms: { screen: Terms },
  privacy: { screen: Privacy },
  symptomScreen: { screen: Symptoms },
  measurementScreen: { screen: Measurement },
  notifications: { screen: Notifications },
  inbox: { screen: Inbox },
  cameraScreen:{screen:CameraView},
  updatePassword:{screen:updatePassword},
  updatePinCode: { screen: UpdatePinCode },
  languageScreen: { screen: LanguageScreen }
  //message: { screen: Message }

}, {
    headerMode: 'none',
    initialRouteName: 'settingsScreen',
  })



const bottomTabs = createBottomTabNavigator(
  {
    Appointment: { screen: AppointmentStack },
    Event: { screen: EventStack },
    Tests: { screen: TestStack },
    User: { screen: UserStack },
    Analysis: { screen: AnalysisStack },
    Settings: { screen: SettingsStack },
    //message: { screen: Message }


  },
  {

  //  navigationOptions:({navigation}) =>({
  //     tabBarOnPress:(scene,jumpToIndex)=>{
  //       console.log("tab press",scene.route)
  //     }
  //   }),
    navigationOptions: ({ navigation }) => (console.log("tab bar press 123"),{
      tabBarOnPress: ({previousScene, scene, jumpToIndex}) => {
        console.log("prev")
        // const { route, index, focused} = scene;
        //  console.log("tab press on press by screen
        // if(focused){
        //     navigation.state.params.scrollToscreen
        // }
        // jumpToIndex(0)
      },
      tabBarIcon: ({ focused, tintColor }) => {screen
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home`;
          return <Ionicons name={iconName} size={30} color={tintColor} />
        } else if (routeName === 'Notification') {
          iconName = `ios-notifications`;
          return <Ionicons name={iconName} size={30} color={tintColor} />
        } else if (routeName === 'Search') {
            iconName = `ios-search`;
            return <Ionicons name={iconName} size={30} color={tintColor} />
        } else if (routeName === 'Profile') {
            iconName = `user-circle`;
            return <FontAwesome name={iconName} size={30} color={tintColor} />
        } else if (routeName === 'Group') {
            iconName = `users`;
            return <FontAwesome name={iconName} size={25} color={tintColor} />
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        //return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    // tabBarComponent: TabBarBottom,
    // tabBarPosition: 'bottom',
    tabBarComponent: props => <TabBarComponent {...props} />,
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
      showLabel: false,
      activeTintColor: 'white',
      inactiveTintColor: 'grey',
      style: {
        backgroundColor: '#F9812A',
        borderTopWidth: 0,
      },
    },
    initialRouteName: 'User',
    backBehavior:'initialRoute'
  }
);

export default createStackNavigator({
  bottomTabs: { screen: bottomTabs },
}, {
    headerMode: 'none'
    // gesturesEnabled: false,
    // disableGestures:'disable',
    //contentComponent: DrawerContainer,
  });