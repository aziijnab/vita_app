import { Text, Animated, Easing } from "react-native";
import { createStackNavigator, createSwitchNavigator } from "react-navigation";
import LoginScreen from "../Screens/login/LoginScreen";
import SignupScreen from "../Screens/login/SignupScreen";
import ForgetPasswordScreen from "../Screens/login/ForgetPasswordScreen";
import UpdateForgetPassword from "../Screens/login/UpdateForgetPassword";
import PhoneConfirmation from "../Screens/login/PhoneConfirmation";
import MeasurementsScreen from "../Screens/login/MeasurementsScreen";
import WelcomeScreen from "../Screens/login/WelcomeScreen";
import Onboard from "../Screens/login/Onboard";
// import LifeStyle from "../Screens/login/Lifestyle";
import CurrentSymptoms from "../Screens/login/CurrentSymptoms";

import CreateProfileScreen from "../Screens/login/CreateProfileScreen";
import TabNavigator from "./BottomTabs";
import MainApp from "../Screens/MainApp";
import Getstarted from "../Screens/login/Getstarted";
import SignupScreen1 from "../Screens/login/SignupScreen1";
import SetPin from '../Screens/settings/SetPin';


import StartScreen from "../Screens/survey/StartScreen";
import MedicalHistory from "../Screens/survey/MedicalHistory";
import Lifestyle from "../Screens/survey/Lifestyle";
import Symptoms from "../Screens/survey/Symptoms";
import Measurement from "../Screens/survey/Measurement";
import TermsView from '../Screens/settings/Terms';
import Privacy from '../Screens/settings/Privacy';

import Message from '../Screens/Message';
// https://github.com/react-community/react-navigation/issues/1254
const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0
  }
});

const DashBoard = createStackNavigator(
  {
    tabNavigator: { screen: TabNavigator },
    message: { screen: Message }

  },
  {
    headerMode: "none",
    initialRouteName: "tabNavigator"
  }
);

// login stack
const LoginStack = createStackNavigator(
  {
    welcomeScreen: { screen: WelcomeScreen },
    onboardingScreen: { screen: Onboard },
    startScreen: { screen: StartScreen },
    medicalHistory: { screen: MedicalHistory },
    lifestyleScreen: { screen: Lifestyle },
    symptomScreen: { screen: Symptoms },
    measurementScreen: { screen: Measurement },
    termsView: { screen: TermsView },
    loginScreen: { screen: LoginScreen },
    signupScreen1: { screen: SignupScreen1 },
    signupScreen: { screen: SignupScreen },
    forgetPasswordScreen: { screen: ForgetPasswordScreen },
    updateForgetPassword: { screen: UpdateForgetPassword },
    createProfileScreen: { screen: CreateProfileScreen },
    getStarted: { screen: Getstarted },
    privacyPolicy: { screen: Privacy }

  },
  {
    headerMode: "none",
    initialRouteName: "welcomeScreen"
  }
);

export default (SwitchNavigator = createSwitchNavigator(
  {
    setpin: { screen: SetPin },
    AuthLoading: MainApp,
    App: DashBoard,
    Auth: LoginStack
  },
  {
    initialRouteName: "AuthLoading",
    transitionConfig: noTransitionConfig
  }
));
