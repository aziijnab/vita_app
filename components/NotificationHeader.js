import React , { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, Platform, View } from "react-native";
import PropTypes from "prop-types";
import { color, font, apiURLs } from "./Constant";
import Icon from 'react-native-vector-icons/Ionicons'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions"; //Import your actions;
//import liraries

// create a component
class NotificationHeader extends Component {

    constructor(props) {
        super(props);
        // this.state = {
        //     loading:false,
        //     modalVisible: false,
        //     inboxCount:null,
        //     dashBoardData:[],
        //     rightData:[],
        //     imageLink: require('../../asset/images/heartbeat.png')
        // };
    }

    render() {
        return (

            <View style={styles.header}>

            <View style={styles.headerInner}>

                {/* <View style={styles.icon}>

                    <Icon name="md-arrow-back" size={26} color={'transparent'} />
                </View> */}
            </View>


            <View style={styles.headerinner2}>
                <Text style={styles.headerText}>{this.props.label}</Text>

            </View>

            <View style={styles.headerInner}>

                <TouchableOpacity onPress={this.props.onMailPress} activeOpacity={0.9} style={styles.icon}>
                    
                    <Icon name={'ios-mail'} size={27} color={color.white} />


                    {/* {this.props.inboxCount > 0 ?
                        (<View style={styles.counter}>
                            <Text style={{ color: color.primary }}>{this.props.inboxCount}</Text>
                        </View>)
                        : null
                    } */}
                </TouchableOpacity>
            </View>

            <View style={styles.headerInner}>

                <TouchableOpacity onPress={this.props.onNotificationPress} activeOpacity={0.9} style={styles.icon}>

                    <Icon name={'ios-notifications'} size={27} color={color.white} />

                    {this.props.notificationCount > 0 ?
                        (<View style={styles.counter}>
                            <Text style={{ color: color.primary }}>{this.props.notificationCount}</Text>
                        </View>)
                        : null
                    }


                </TouchableOpacity>
            </View>



        </View>
        );
    }
}

// define your styles
// const styles = StyleSheet.create({
    
// });

//make this component available to the app


// export default function NotificationHeader({

//     label = "Add Title",
//     onNotificationPress,
//     onMailPress,
//     textStyle = styles.headerText,
//     notificationCount,
//     inboxCount,

// }) {
//     return (
        
//     );
// }

NotificationHeader.propTypes = {
    onMailPress: PropTypes.func.isRequired,
    onNotificationPress: PropTypes.func.isRequired,
    label: PropTypes.string,
    textStyle: PropTypes.any,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    header: {
        height: 52,

        alignItems: 'center',
        //marginTop: Platform.OS == "ios" ? 20 : 0,
        flexDirection: 'row',
        width: '100%',

        backgroundColor: color.primary
    },
    counter: {
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        position: 'absolute',
        top: 0,
        right: 0,
    },
    headerText: {
        fontSize: 22,
        color: 'white',
        fontWeight: '400'
    },
    headerInner: {

        backgroundColor: 'transparent',

        paddingHorizontal: 7,
        height: '100%',


        alignItems: 'center',
        justifyContent: 'center'
    },
    headerinner2: {
        flex: 1,
        marginLeft: 54,
        height: '100%',


        backgroundColor: 'transparent',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },

    icon: {
        padding: 4

    }
});

function mapStateToProps(state, props) {
    return {
      userDetail: state.user.userDetail,
      inboxCount: state.user.inboxCount,
      notificationCount: state.user.notificationCount,
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
  }

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(NotificationHeader);
