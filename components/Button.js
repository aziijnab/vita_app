import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { color, font, apiURLs } from "./Constant";

export default function Button({
  onPress,
  label = "Click me",
  buttonStyle = styles.button,
  textColor = styles.text
}) {
  return (
    <TouchableOpacity activeOpacity={0.9} style={buttonStyle} onPress={onPress}>
      <Text style={textColor}>{label.toUpperCase()}</Text>
    </TouchableOpacity>
  );
}

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  label: PropTypes.string,
  buttonStyle: PropTypes.any,
  textColor: PropTypes.any
};

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginBottom: 10,
    bottom: 0,
    position: "absolute",

    backgroundColor: color.primary
  },
  text: {
    color: "white",
    fontSize: 16,
    fontWeight: "400"
  }
});
