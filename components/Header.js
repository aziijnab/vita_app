import React from "react";
import { StyleSheet, Text, TouchableOpacity, Platform, View } from "react-native";
import PropTypes from "prop-types";
import { color, font, apiURLs } from "./Constant";
import Icon from 'react-native-vector-icons/Ionicons'
export default function Header({
    onbackPress,
    label = "Add Title",

}) {
    return (
        <View style={styles.header}>
            <View style={styles.headerInner}>

                <TouchableOpacity onPress={onbackPress} activeOpacity={0.9} style={styles.icon}>

                    <Icon name="md-arrow-back" size={26} color={color.white} />
                </TouchableOpacity>
            </View>
            <View style={styles.headerinner2}>
                <Text style={styles.headerText}>{label}</Text>

            </View>

            

        </View>
    );
}

Header.propTypes = {
    onbackPress: PropTypes.func.isRequired,
    label: PropTypes.string,

};

const styles = StyleSheet.create({
    header: {
        height: 52,

        alignItems: 'center',
        //marginTop: Platform.OS == "ios" ? 20 : 0,
        flexDirection: 'row',
        width: '100%',

        backgroundColor: color.primary
    },
    headerText: {
        fontSize: 22,
        color: 'white',
        fontWeight: '400'
    },
    headerInner: {
        backgroundColor: 'transparent',
        paddingHorizontal: 7,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerinner2: {
        flex: 1,
        height: '100%',
        backgroundColor: 'transparent',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 22,
    },

    icon: {
        padding: 4

    }
});
