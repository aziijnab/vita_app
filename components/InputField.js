import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput
} from "react-native";
import PropTypes from "prop-types";
import { color, font, apiURLs } from "./Constant";
import Icon from "react-native-vector-icons/Ionicons";

export default function InputField({
  onChangeText,
  icon = "ios-add-circle",
  autoCapitalize = "none",
  inputStyle = styles.input,
  placeholderTextColor = "#969696",
  iconSize = 27,
  keyboardType = "default",
  iconColor = "#969696",
  label = "Add label here",
  autoCorrect = false,
  returnKeyType = "next",
  secureTextEntry = false
}) {
  return (
    /* <TouchableOpacity activeOpacity={0.9} style={buttonStyle} onPress={onPress}>
      <Text style={textColor}>{label.toUpperCase()}</Text>
    </TouchableOpacity> */

    <View style={inputStyle}>
      <Icon
        name={icon}
        size={iconSize}
        color={iconColor}
        style={{ padding: 10 }}
      />
      <TextInput
        style={{
          /* paddingLeft: 8, fontSize: 17, width: "100%" */
          //width: "100%",
          flex: 1,
          marginRight: 10,
          paddingTop: 10,
          paddingRight: 10,
          paddingBottom: 10,
          paddingLeft: 0,
          backgroundColor: "#fff",

          color: "#424242"
        }}
        selectionColor={"#dfe6e9"}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        multiline={false}
        placeholder={label}
        autoCapitalize={autoCapitalize}
        autoCorrect={autoCorrect}
        returnKeyType={returnKeyType}
        keyboardType={keyboardType}
        underlineColorAndroid="transparent"
        placeholderTextColor={placeholderTextColor}
      />
    </View>
  );
}

InputField.propTypes = {
  inputStyle: PropTypes.any,
  label: PropTypes.string,
  onChangeText: PropTypes.func,
  icon: PropTypes.string,
  autoCapitalize: PropTypes.string,
  keyboardType: PropTypes.any,
  iconSize: PropTypes.number,
  iconColor: PropTypes.string,
  autoCorrect: PropTypes.bool,
  returnKeyType: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  placeholderTextColor: PropTypes.string
};

const styles = StyleSheet.create({
  input: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingLeft: 9,
    paddingRight: 12,
    borderWidth: 1,
    borderColor: "#dfe6e9",
    marginVertical: 13,
    borderRadius: 25
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    paddingVertical: 13,
    alignSelf: "center",
    width: "90%",
    marginBottom: 10,
    bottom: 0,
    position: "absolute",

    backgroundColor: color.primary
  },
  text: {
    color: "white",
    fontSize: 16
  }
});
