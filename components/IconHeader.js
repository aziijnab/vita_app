import React from "react";
import { StyleSheet, Text, TouchableOpacity, Platform, View } from "react-native";
import PropTypes from "prop-types";
import { color, font, apiURLs } from "./Constant";
import Icon from 'react-native-vector-icons/Ionicons'
export default function IconHeader({
    onbackPress,
    label = "Add Title",
    iconRight = 'md-add-circle',
    onIconPress,

}) {
    return (
        <View style={styles.header}>
            <View style={styles.headerInner}>

                <TouchableOpacity onPress={onbackPress} activeOpacity={0.9} style={styles.icon}>

                    <Icon name="md-arrow-back" size={26} color={color.white} />
                </TouchableOpacity>
            </View>
            <View style={styles.headerinner2}>
                <Text style={styles.headerText}>{label}</Text>

            </View>

            <View style={styles.headerInner}>

                <TouchableOpacity onPress={onIconPress} activeOpacity={0.9} style={styles.icon}>

                    <Icon name={iconRight} size={26} color={color.white} />
                </TouchableOpacity>
            </View>



        </View>
    );
}

IconHeader.propTypes = {
    onbackPress: PropTypes.func.isRequired,
    onIconPress: PropTypes.func.isRequired,
    iconRight: PropTypes.string,
    label: PropTypes.string,

};

const styles = StyleSheet.create({
    header: {
        height: 52,

        alignItems: 'center',
        //marginTop: Platform.OS == "ios" ? 20 : 0,
        flexDirection: 'row',
        width: '100%',

        backgroundColor: color.primary
    },
    headerText: {


        fontSize: 19,
        color: 'white',
        fontWeight: '400'
    },
    headerInner: {

        backgroundColor: 'transparent',

        paddingHorizontal: 7,
        height: '100%',


        alignItems: 'center',
        justifyContent: 'center'
    },
    headerinner2: {
        flex: 1,

        height: '100%',


        backgroundColor: 'transparent',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },

    icon: {
        padding: 4

    }
});
