import { Platform } from 'react-native';
const color = {
  primary: "#F16638",
  dark: "#373535",
  black: "#000000",
  white: "#FFF",
  lightGrey: "#D3D3D3",
  grey: "rgb(151,151,151)",
  darkgrey: "rgb(74,74,74)",
  red: "rgb(208,2,27)",
  transparent: "transparent"
};

const font = {
  sansLight: "SourceSansPro-Light",
  sansRegular: "SourceSansPro-Regular",
  sansSemibold: "SourceSansPro-Semibold"
};

const appStrings = {
  appName: "Defaullt"
};

const apiURLs = {
  baseURL: "https://warm-cliffs-67509.herokuapp.com"
  // baseURL: 'http://192.168.100.27:4040'
};



export const MIN_COMPOSER_HEIGHT = Platform.select({
  ios: 33,
  android: 41,
});
export const MAX_COMPOSER_HEIGHT = 200;
export const DEFAULT_PLACEHOLDER = 'Type a message...';
export const DATE_FORMAT = 'll';
export const TIME_FORMAT = 'LT';


export { color, font, appStrings, apiURLs };
