import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    marginBottom:10,
    flexWrap: 'wrap'
    
    // borderBottomWidth: 1,
    // borderBottomColor: 'grey',
  },
});
