const dashBoardImages = [
    {
        name: "heartbeat",
        image: require('../../asset/images/heartbeat.png'),
        image_green: require('../../asset/images/heartbeat_green.png'),
        image_yellow: require('../../asset/images/heartbeat_yellow.png'),
        image_red: require('../../asset/images/heartbeat_red.png'),
    },
    {
        name: "droplet",
        image: require('../../asset/images/droplet.png'),
        image_green: require('../../asset/images/droplet_green.png'),
        image_yellow: require('../../asset/images/droplet_yellow.png'),
        image_red: require('../../asset/images/droplet_red.png'),
    },
    {
        name: "ying",
        image: require('../../asset/images/ying.png'),
        image_green: require('../../asset/images/ying_green.png'),
        image_yellow: require('../../asset/images/ying_yellow.png'),
        image_red: require('../../asset/images/ying_red.png'),
    },
    {
        name: "shield",
        image: require('../../asset/images/shield.png'),
        image_green: require('../../asset/images/shield_green.png'),
        image_yellow: require('../../asset/images/shield_yellow.png'),
        image_red: require('../../asset/images/shield_red.png'),
    },
    {
        name: "vitamins",
        image: require('../../asset/images/vitamins.png'),
        image_green: require('../../asset/images/vitamins_green.png'),
        image_yellow: require('../../asset/images/vitamins_yellow.png'),
        image_red: require('../../asset/images/vitamins_red.png'),
    },
    {
        name: "plague",
        image: require('../../asset/images/plague.png'),
        image_green: require('../../asset/images/plague_green.png'),
        image_yellow: require('../../asset/images/plague_yellow.png'),
        image_red: require('../../asset/images/plague_red.png'),
    },
    {
        name: "dna",
        image: require('../../asset/images/dna.png'),
        image_green: require('../../asset/images/dna_green.png'),
        image_yellow: require('../../asset/images/dna_yellow.png'),
        image_red: require('../../asset/images/dna_red.png'),
    },
    {
        name: "cancer",
        image: require('../../asset/images/cancer.png'),
        image_green: require('../../asset/images/cancer_green.png'),
        image_yellow: require('../../asset/images/cancer_yellow.png'),
        image_red: require('../../asset/images/cancer_red.png'),
    }
];

export default dashBoardImages;