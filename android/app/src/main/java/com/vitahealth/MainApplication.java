package com.vitahealth;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.mapbox.rctmgl.RCTMGLPackage;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
import com.rnfs.RNFSPackage;
import com.ipay88.IPay88Package;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.idehub.GoogleAnalyticsBridge.GoogleAnalyticsBridgePackage;
import com.evollu.react.fcm.FIRMessagingPackage;
import org.reactnative.camera.RNCameraPackage;
import com.heanoria.library.reactnative.locationenabler.RNAndroidLocationEnablerPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.horcrux.svg.SvgPackage;


import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RCTMGLPackage(),
            new ReactNativeConfigPackage(),
            new ReanimatedPackage(),
            new SvgPackage(),
            new RNFSPackage(),
            new IPay88Package(),
            new RNFetchBlobPackage(),
            new RNGestureHandlerPackage(),
            new RNFileViewerPackage(),
            new VectorIconsPackage(),
            new SplashScreenReactPackage(),
            new MapsPackage(),
            new PickerPackage(),
            new RNI18nPackage(),
            new RNGoogleSigninPackage(),
            new GoogleAnalyticsBridgePackage(),
            new RNFirebasePackage(),
            new FIRMessagingPackage(),
            new RNCameraPackage(),
            new RNAndroidLocationEnablerPackage(),
            new RNFirebaseAnalyticsPackage(),
            new RNFusedLocationPackage());
            
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
