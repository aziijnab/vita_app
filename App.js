import React from "react";
import { StyleSheet, Platform, StatusBar,Text, View, ActivityIndicator } from "react-native";
import SwitchNavigator from "./navigation/AppNavigator";
import { color, font } from "./components/Constant";
import { Provider } from "react-redux";
import {logger} from "redux-logger";
import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import { createStackNavigator, createAppContainer ,createSwitchNavigator} from 'react-navigation';
import NavigationService from './navigation/NavigationService';
import screenTracking from './Screens/ScreenTracking'
import rootReducer from './reducers'; //Import the reducer
import { PersistGate } from "redux-persist/integration/react";
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';
import firebase from "react-native-firebase";
//const { store, persistor } = configureStore();
import { SafeAreaView } from "react-navigation";
import Color from "./components/Color";


const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);



const presistConfig = {
  key:"root",
  storage
}
// const tracker = new GoogleAnalyticsTracker("UA-131919399-1");
const googleAnalytics = firebase.analytics();
googleAnalytics.setAnalyticsCollectionEnabled(true); 
// gets the current screen from navigation state
function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
}



const persisttReducer = persistReducer(presistConfig,rootReducer);
const store = createStore(persisttReducer,applyMiddleware(thunk,logger));
let persistor = persistStore(store);
// const TopLevelNavigator = createSwitchNavigator({ SwitchNavigator})

const AppContainer = createAppContainer(SwitchNavigator);
export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: true
    };
  }

   componentWillMount() {
    this.setState({ isReady: true });
    // this.notificationListner();

  }
  componentDidUpdate(){
    NavigationService.navigate('notifications');
  }

 

  render() {
    return this.state.isReady ?
    <SafeAreaView style={{ flex: 1 , backgroundColor:color.primary}}>
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
        <MyStatusBar backgroundColor={color.primary} barStyle="light-content" />
         
            {/* <SwitchNavigator /> */}

            <AppContainer ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef);
              }} onNavigationStateChange={(prevState, currentState) => {
                const currentScreen = getActiveRouteName(currentState);
                const prevScreen = getActiveRouteName(prevState);

                if (prevScreen !== currentScreen) {
                  // the line below uses the Google Analytics tracker
                  // change the tracker here to use other Mobile analytics SDK.
                  
                  googleAnalytics.setCurrentScreen(currentScreen);
                  // tracker.trackScreenView(currentScreen);
                }
              }} />
          
        </PersistGate>
      </Provider>
      </SafeAreaView> : <View style={{ backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color={color.primary} />
      </View>;
  }
}

const STATUSBAR_HEIGHT = Platform.OS === "ios" ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === "ios" ? 44 : 56;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#79B45D',
    height: APPBAR_HEIGHT,
  },
   content: {
    flex: 1,
    backgroundColor: '#33373B',
  },
});
