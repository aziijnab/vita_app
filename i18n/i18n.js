import I18n from "react-native-i18n";
import en from "./locales/en";
import ms from "./locales/ms";
import bm from "./locales/bm";
import moment from "moment";
import 'moment/locale/en-gb'

I18n.fallbacks = true;

I18n.translations = {
  en,
  ms,
  bm
};
I18n.defaultLocale = "en";
moment.locale('en-gb');
export default I18n;
